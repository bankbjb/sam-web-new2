set standard_conforming_strings = on;

Insert into S_MENUS
   (ID, NAME, PATH, PARENT_ID, ORDINAL, IS_ACTION, ACCESS_LEVEL, ACTIONS)
 Values
   (10, 'Users & Roles', ' ', 0, 10, 'N', 99, null);
Insert into S_MENUS
   (ID, NAME, PATH, PARENT_ID, ORDINAL, IS_ACTION, ACCESS_LEVEL, ACTIONS)
 Values
   (11, 'User', 'adminUser', 10, 11, 'N', 99, null);
Insert into S_MENUS
   (ID, NAME, PATH, PARENT_ID, ORDINAL, IS_ACTION, ACCESS_LEVEL, ACTIONS)
 Values
   (12, 'Role', 'adminRole', 10, 12, 'N', 99, null);

Insert into S_MENUS
   (ID, NAME, PATH, PARENT_ID, ORDINAL, IS_ACTION, ACCESS_LEVEL, ACTIONS)
 Values
   (20, 'Settings', ' ', 0, 20, 'N', 49, null);
Insert into S_MENUS
   (ID, NAME, PATH, PARENT_ID, ORDINAL, IS_ACTION, ACCESS_LEVEL, ACTIONS)
 Values
   (21, 'Parameter', 'adminParameter', 20, 21, 'N', 99, null);
Insert into S_MENUS
   (ID, NAME, PATH, PARENT_ID, ORDINAL, IS_ACTION, ACCESS_LEVEL, ACTIONS)
 Values
   (22, 'Fitur', 'manageFeature', 20, 22, 'N', 49, null);
Insert into S_MENUS
   (ID, NAME, PATH, PARENT_ID, ORDINAL, IS_ACTION, ACCESS_LEVEL, ACTIONS)
 Values
   (23, 'Mapping Message', 'manageMessageMapping', 20, 23, 'N', 49, null);
Insert into S_MENUS
   (ID, NAME, PATH, PARENT_ID, ORDINAL, IS_ACTION, ACCESS_LEVEL, ACTIONS)
 Values
   (24, 'Scheduler', 'adminScheduler', 20, 24, 'N', 99, null);


Insert into S_MENUS
   (ID, NAME, PATH, PARENT_ID, ORDINAL, IS_ACTION, ACCESS_LEVEL, ACTIONS)
 Values
   (30, 'Monitoring', ' ', 0, 30, 'N', 1, null);
Insert into S_MENUS
   (ID, NAME, PATH, PARENT_ID, ORDINAL, IS_ACTION, ACCESS_LEVEL, ACTIONS)
 Values
   (31, 'Session', 'adminSession', 30, 31, 'N', 99, null);
Insert into S_MENUS
   (ID, NAME, PATH, PARENT_ID, ORDINAL, IS_ACTION, ACCESS_LEVEL, ACTIONS)
 Values
   (32, 'Log Message', 'viewLogMessage', 30, 32, 'N', 49, null);
Insert into S_MENUS
   (ID, NAME, PATH, PARENT_ID, ORDINAL, IS_ACTION, ACCESS_LEVEL, ACTIONS)
 Values
   (33, 'Log Activity', 'viewLogActivity', 30, 33, 'N', 49, null);
Insert into S_MENUS
   (ID, NAME, PATH, PARENT_ID, ORDINAL, IS_ACTION, ACCESS_LEVEL, ACTIONS)
 Values
   (34, 'Data Transaksi Per Agg', 'viewLogTransaction', 30, 34, 'N', 1, null);
 
 
Insert into S_MENUS
   (ID, NAME, PATH, PARENT_ID, ORDINAL, IS_ACTION, ACCESS_LEVEL, ACTIONS)
 Values
   (40, 'Supervisor', ' ', 0, 40, 'N', 49, null);
Insert into S_MENUS
   (ID, NAME, PATH, PARENT_ID, ORDINAL, IS_ACTION, ACCESS_LEVEL, ACTIONS)
 Values
   (41, 'Approval', 'processApproval', 40, 41, 'N', 49, 'Aggregator,Feature,Role,SystemParameter,UserAggregator');

Insert into S_MENUS
   (ID, NAME, PATH, PARENT_ID, ORDINAL, IS_ACTION, ACCESS_LEVEL, ACTIONS)
 Values
   (50, 'Reporting', ' ', 0, 50, 'N', 1, null);
Insert into S_MENUS
   (ID, NAME, PATH, PARENT_ID, ORDINAL, IS_ACTION, ACCESS_LEVEL, ACTIONS)
 Values
   (51, 'Report Transaction', 'viewLogTransaction', 50, 51, 'N', 49, null);
   
Insert into S_MENUS
   (ID, NAME, PATH, PARENT_ID, ORDINAL, IS_ACTION, ACCESS_LEVEL, ACTIONS)
 Values
   (60, 'Administration', ' ', 0, 60, 'N', 49, null);
Insert into S_MENUS
   (ID, NAME, PATH, PARENT_ID, ORDINAL, IS_ACTION, ACCESS_LEVEL, ACTIONS)
 Values
   (61, 'Aggregator', 'manageAggregator', 60, 61, 'N', 49, null);
Insert into S_MENUS
   (ID, NAME, PATH, PARENT_ID, ORDINAL, IS_ACTION, ACCESS_LEVEL, ACTIONS)
 Values
   (62, 'Collecting Agent', 'manageCA', 60, 62, 'N', 49, null);
Insert into S_MENUS
   (ID, NAME, PATH, PARENT_ID, ORDINAL, IS_ACTION, ACCESS_LEVEL, ACTIONS)
 Values
   (63, 'Mapping Agg-CA-Fitur', 'manageFeatureMapping', 60, 63, 'N', 49, null);
Insert into S_MENUS
   (ID, NAME, PATH, PARENT_ID, ORDINAL, IS_ACTION, ACCESS_LEVEL, ACTIONS)
 Values
   (64, 'Tiering Komisi', 'manageTiering', 60, 64, 'N', 49, null);

Insert into S_MENUS
   (ID, NAME, PATH, PARENT_ID, ORDINAL, IS_ACTION, ACCESS_LEVEL, ACTIONS)
 Values
   (70, 'Administration', ' ', 0, 70, 'N', 9, null);
Insert into S_MENUS
   (ID, NAME, PATH, PARENT_ID, ORDINAL, IS_ACTION, ACCESS_LEVEL, ACTIONS)
 Values
   (71, 'Administrasi Aggregator', 'manageAggregatorProfile', 70, 71, 'N', 9, null);

Insert into S_ROLES
   (ID, CODE, NAME, STATUS, CREATE_DATE, CREATE_WHO, CHANGE_DATE, CHANGE_WHO, APPROVAL_STATUS, APPROVAL_DATE, APPROVAL_WHO)
 Values
   (1, '0000001', 'HDC Maker', 1, TO_TIMESTAMP('10/08/2016 12:00:00 AM','fmMMfm/fmDDfm/YYYY fmHH12fm:MI:SS.FF AM'), 'hdc1', TO_TIMESTAMP('10/08/2016 12:00:00 AM','fmMMfm/fmDDfm/YYYY fmHH12fm:MI:SS.FF AM'), 'admin1', 'Approved - Edit', TO_TIMESTAMP('10/08/2016 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'admin2');
Insert into S_ROLES
   (ID, CODE, NAME, STATUS, CREATE_DATE, CREATE_WHO, CHANGE_DATE, CHANGE_WHO, APPROVAL_STATUS, APPROVAL_DATE, APPROVAL_WHO)
 Values
   (2, '0000002', 'HDC Approver', 1, TO_TIMESTAMP('10/08/2016 12:00:00 AM','fmMMfm/fmDDfm/YYYY fmHH12fm:MI:SS.FF AM'), 'hdc2', TO_TIMESTAMP('10/08/2016 12:00:00 AM','fmMMfm/fmDDfm/YYYY fmHH12fm:MI:SS.FF AM'), 'admin1', 'Approved - Edit', TO_TIMESTAMP('10/08/2016 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'admin2');
Insert into S_ROLES
   (ID, CODE, NAME, STATUS, CREATE_DATE, CREATE_WHO, CHANGE_DATE, CHANGE_WHO, APPROVAL_STATUS, APPROVAL_DATE, APPROVAL_WHO)
 Values
   (3, '0000003', 'UEB Maker', 1, TO_TIMESTAMP('10/08/2016 12:00:00 AM','fmMMfm/fmDDfm/YYYY fmHH12fm:MI:SS.FF AM'), 'ueb1', TO_TIMESTAMP('10/08/2016 12:00:00 AM','fmMMfm/fmDDfm/YYYY fmHH12fm:MI:SS.FF AM'), 'admin1', 'Approved - Edit', TO_TIMESTAMP('10/08/2016 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'admin2');
Insert into S_ROLES
   (ID, CODE, NAME, STATUS, CREATE_DATE, CREATE_WHO, CHANGE_DATE, CHANGE_WHO, APPROVAL_STATUS, APPROVAL_DATE, APPROVAL_WHO)
 Values
   (4, '0000004', 'UEB Approver', 1, TO_TIMESTAMP('10/08/2016 12:00:00 AM','fmMMfm/fmDDfm/YYYY fmHH12fm:MI:SS.FF AM'), 'ueb2', TO_TIMESTAMP('10/08/2016 12:00:00 AM','fmMMfm/fmDDfm/YYYY fmHH12fm:MI:SS.FF AM'), 'admin1', 'Approved - Edit', TO_TIMESTAMP('10/08/2016 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'admin2');
Insert into S_ROLES
   (ID, CODE, NAME, STATUS, CREATE_DATE, CREATE_WHO, CHANGE_DATE, CHANGE_WHO, APPROVAL_STATUS, APPROVAL_DATE, APPROVAL_WHO)
 Values
   (5, '0000005', 'Aggregator', 1, TO_TIMESTAMP('10/08/2016 12:00:00 AM','fmMMfm/fmDDfm/YYYY fmHH12fm:MI:SS.FF AM'), 'agg', TO_TIMESTAMP('10/08/2016 12:00:00 AM','fmMMfm/fmDDfm/YYYY fmHH12fm:MI:SS.FF AM'), 'admin1', 'Approved - Edit', TO_TIMESTAMP('10/08/2016 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'admin2');
   
Insert into S_ROLE_MENUS
   (ROLE_ID, MENU_ID)
 Values
   (1, 10);
Insert into S_ROLE_MENUS
   (ROLE_ID, MENU_ID)
 Values
   (1, 11);
Insert into S_ROLE_MENUS
   (ROLE_ID, MENU_ID)
 Values
   (1, 12);
Insert into S_ROLE_MENUS
   (ROLE_ID, MENU_ID)
 Values
   (1, 20);
Insert into S_ROLE_MENUS
   (ROLE_ID, MENU_ID)
 Values
   (1, 21);
Insert into S_ROLE_MENUS
   (ROLE_ID, MENU_ID)
 Values
   (1, 22);
Insert into S_ROLE_MENUS
   (ROLE_ID, MENU_ID)
 Values
   (1, 23);
Insert into S_ROLE_MENUS
   (ROLE_ID, MENU_ID)
 Values
   (1, 30);
Insert into S_ROLE_MENUS
   (ROLE_ID, MENU_ID)
 Values
   (1, 31);
Insert into S_ROLE_MENUS
   (ROLE_ID, MENU_ID)
 Values
   (1, 32);
Insert into S_ROLE_MENUS
   (ROLE_ID, MENU_ID)
 Values
   (5, 33);
Insert into S_ROLE_MENUS
   (ROLE_ID, MENU_ID)
 Values
   (5, 34);

Insert into S_ROLE_MENUS
   (ROLE_ID, MENU_ID)
 Values
   (2, 40);
Insert into S_ROLE_MENUS
   (ROLE_ID, MENU_ID, ACTIONS)
 Values
   (2, 41, 'UserAggregator,Role,SystemParameter,Feature');


Insert into S_ROLE_MENUS
   (ROLE_ID, MENU_ID)
 Values
   (3, 50);
Insert into S_ROLE_MENUS
   (ROLE_ID, MENU_ID)
 Values
   (3, 51);
Insert into S_ROLE_MENUS
   (ROLE_ID, MENU_ID)
 Values
   (3, 60);
Insert into S_ROLE_MENUS
   (ROLE_ID, MENU_ID)
 Values
   (3, 61);
Insert into S_ROLE_MENUS
   (ROLE_ID, MENU_ID)
 Values
   (3, 62);
Insert into S_ROLE_MENUS
   (ROLE_ID, MENU_ID)
 Values
   (3, 63);
Insert into S_ROLE_MENUS
   (ROLE_ID, MENU_ID)
 Values
   (3, 64);

Insert into S_ROLE_MENUS
   (ROLE_ID, MENU_ID)
 Values
   (4, 40);
Insert into S_ROLE_MENUS
   (ROLE_ID, MENU_ID, ACTIONS)
 Values
   (4, 41, 'Aggregator');

Insert into S_ROLE_MENUS
   (ROLE_ID, MENU_ID)
 Values
   (4, 50);
Insert into S_ROLE_MENUS
   (ROLE_ID, MENU_ID)
 Values
   (4, 51);
Insert into S_ROLE_MENUS
   (ROLE_ID, MENU_ID)
 Values
   (4, 70);
Insert into S_ROLE_MENUS
   (ROLE_ID, MENU_ID)
 Values
   (4, 71);

 Insert into S_ROLE_MENUS
   (ROLE_ID, MENU_ID)
 Values
   (5, 70);
Insert into S_ROLE_MENUS
   (ROLE_ID, MENU_ID)
 Values
   (5, 71);
   
Insert into M_BRANCHES
   (CODE, NAME, DESCRIPTION, PARENT_ID, CHANGE_DATE, CHANGE_WHO, APPROVAL_DATE, APPROVAL_WHO, APPROVAL_STATUS)
 Values
   ('0001', 'HQ', 'Head Quarter.', null, TO_TIMESTAMP('10/08/2016 12:00:00 AM','fmMMfm/fmDDfm/YYYY fmHH12fm:MI:SS.FF AM'), 'cms2', TO_TIMESTAMP('10/08/2016 12:00:00 AM','fmMMfm/fmDDfm/YYYY fmHH12fm:MI:SS.FF AM'), 'cms2', 'Approved - Edit');
Insert into M_BRANCHES
   (CODE, NAME, DESCRIPTION, PARENT_ID, CHANGE_DATE, CHANGE_WHO, APPROVAL_DATE, APPROVAL_WHO, APPROVAL_STATUS)
 Values
   ('9999', 'AGG', 'Aggregator Office', '0001', TO_TIMESTAMP('10/08/2016 12:00:00 AM','fmMMfm/fmDDfm/YYYY fmHH12fm:MI:SS.FF AM'), 'cms2', TO_TIMESTAMP('10/08/2016 12:00:00 AM','fmMMfm/fmDDfm/YYYY fmHH12fm:MI:SS.FF AM'), 'cms2', 'Approved - Create');


Insert into S_PARAMETERS
   (ID, NAME, VALUE, DESCRIPTION, CREATE_DATE, CHANGE_DATE, CHANGE_WHO, STATUS, APPROVAL_STATUS)
 Values
   ('20151219PARAM0001', 'MOTD', 
'Welcome in SAM BJB Support System. 
Please make sure you have approved all pending tasks!', 
'Message of Today', TO_DATE('12/17/2015 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), TO_DATE('12/30/2015 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'cms1', 1, 'Approved - Create');

Insert into S_PARAMETERS
   (ID, NAME, VALUE, DESCRIPTION, CREATE_DATE, CHANGE_DATE, CHANGE_WHO, STATUS, APPROVAL_STATUS)
 Values
   ('20151219PARAM0002', 'TOP_FEE_BASED_INCOME_ITEM', '5', 'Max # of Bar for Top Fee Based Income Chart', TO_DATE('12/17/2015 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), TO_DATE('12/30/2015 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'cms1', 1, 'Approved - Create');
Insert into S_PARAMETERS
   (ID, NAME, VALUE, DESCRIPTION, CREATE_DATE, CHANGE_DATE, CHANGE_WHO, STATUS, APPROVAL_STATUS)
 Values
   ('20151219PARAM0003', 'TOP_BEST_SELLER_ITEM', '5', 'Max # of Bar for Top Fee Based Income Chart', TO_DATE('12/17/2015 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), TO_DATE('12/30/2015 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'cms1', 1, 'Approved - Create');
Insert into S_PARAMETERS
   (ID, NAME, VALUE, DESCRIPTION, CREATE_DATE, CHANGE_DATE, CHANGE_WHO, STATUS, APPROVAL_STATUS)
 Values
   ('20151219PARAM0004', 'THIS_URL', 'https://www.javanerds.com/sam-web/index.jsp', 'URL for this application', TO_DATE('12/17/2015 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), TO_DATE('12/30/2015 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'cms1', 1, 'Approved - Create');
Insert into S_PARAMETERS
   (ID, NAME, VALUE, DESCRIPTION, CREATE_DATE, CHANGE_DATE, CHANGE_WHO, STATUS, APPROVAL_STATUS)
 Values
   ('20151219PARAM0005', 'WELCOME_SUBJECT', 'Welcome to SAM BJB!', 'Subject of Welcome Email', TO_DATE('12/17/2015 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), TO_DATE('12/30/2015 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'cms1', 1, 'Approved - Create');
Insert into S_PARAMETERS
   (ID, NAME, VALUE, DESCRIPTION, CREATE_DATE, CHANGE_DATE, CHANGE_WHO, STATUS, APPROVAL_STATUS)
 Values
   ('20151219PARAM0006', 'WELCOME_TEXT', 
'Dear #{#user.fullName},

Congratulation! You are now SAM BJB member.

Your User ID: #{#user.userName}
Your Password : #{#PLAIN_PASSWORD}

Please visit #{#WELCOME_URL} to login to our system. 
You will be asked to change your password on first login', 
'Content of Welcome Email', TO_DATE('12/17/2015 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), TO_DATE('12/30/2015 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'cms1', 1, 'Approved - Create');

Insert into S_PARAMETERS
   (ID, NAME, VALUE, DESCRIPTION, CREATE_DATE, CHANGE_DATE, CHANGE_WHO, STATUS, APPROVAL_STATUS)
 Values
   ('20151219PARAM0007', 'TRANSACTION_TIMEOUT_SAM_TO_SWITCHING', '60', 'Max time to wait response from BJB Switching', TO_DATE('12/17/2015 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), TO_DATE('12/30/2015 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'cms1', 1, 'Approved - Create');
Insert into S_PARAMETERS
   (ID, NAME, VALUE, DESCRIPTION, CREATE_DATE, CHANGE_DATE, CHANGE_WHO, STATUS, APPROVAL_STATUS)
 Values
   ('20151219PARAM0008', 'DISPLAY_ROWS_PER_PAGE', '20', 'Number of rows per page', TO_DATE('12/17/2015 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), TO_DATE('12/30/2015 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'cms1', 1, 'Approved - Create');
Insert into S_PARAMETERS
   (ID, NAME, VALUE, DESCRIPTION, CREATE_DATE, CHANGE_DATE, CHANGE_WHO, STATUS, APPROVAL_STATUS)
 Values
   ('20151219PARAM0009', 'LOG_MESSAGE_RETENTION', '30', 'Number of days before message log is archived', TO_DATE('12/17/2015 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), TO_DATE('12/30/2015 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'cms1', 1, 'Approved - Create');
Insert into S_PARAMETERS
   (ID, NAME, VALUE, DESCRIPTION, CREATE_DATE, CHANGE_DATE, CHANGE_WHO, STATUS, APPROVAL_STATUS)
 Values
   ('20151219PARAM0010', 'LOG_ACTIVITY_RETENTION', '30', 'Number of days before activity log is archived', TO_DATE('12/17/2015 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), TO_DATE('12/30/2015 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'cms1', 1, 'Approved - Create');
Insert into S_PARAMETERS
   (ID, NAME, VALUE, DESCRIPTION, CREATE_DATE, CHANGE_DATE, CHANGE_WHO, STATUS, APPROVAL_STATUS)
 Values
   ('20151219PARAM0011', 'LOG_TRANSACTION_RETENTION', '366', 'Number of days before transaction log is archived', TO_DATE('12/17/2015 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), TO_DATE('12/30/2015 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'cms1', 1, 'Approved - Create');   
Insert into S_PARAMETERS
   (ID, NAME, VALUE, DESCRIPTION, CREATE_DATE, CHANGE_DATE, CHANGE_WHO, STATUS, APPROVAL_STATUS)
 Values
   ('20151219PARAM0012', 'LOG_SYSTEM_RETENTION', '30', 'Number of days before system log is archived', TO_DATE('12/17/2015 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), TO_DATE('12/30/2015 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'cms1', 1, 'Approved - Create');   
Insert into S_PARAMETERS
   (ID, NAME, VALUE, DESCRIPTION, CREATE_DATE, CHANGE_DATE, CHANGE_WHO, STATUS, APPROVAL_STATUS)
 Values
   ('20151219PARAM0013', 'LOGIN_MAXIMUM_FAILED_ATTEMPT', '3', 'Maximum time of failed login before user is blocked', TO_DATE('12/17/2015 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), TO_DATE('12/30/2015 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'cms1', 1, 'Approved - Create');   
   
Insert into S_PARAMETERS
   (ID, NAME, VALUE, DESCRIPTION, CREATE_DATE, CHANGE_DATE, CHANGE_WHO, STATUS, APPROVAL_STATUS)
 Values
   ('20151219PARAM0014', 'BALANCE_NOTIF_SUBJECT', 'Balance below limit!', 'Subject of Balance Notification', TO_DATE('12/17/2015 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), TO_DATE('12/30/2015 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'cms1', 1, 'Approved - Create');
Insert into S_PARAMETERS
   (ID, NAME, VALUE, DESCRIPTION, CREATE_DATE, CHANGE_DATE, CHANGE_WHO, STATUS, APPROVAL_STATUS)
 Values
   ('20151219PARAM0015', 'BALANCE_NOTIF_TEXT', 
'Dear #{#user.fullName},

This is to notify that your balance is below limit.

Current balance: #{#CURRENT_BALANCE}
Minimum balance: #{#MINIMUM_BALANCE}

Please top up your balance to prevent transaction failure.

Thank you
SAM Support', 
'Content of Balance Notification Email', TO_DATE('12/17/2015 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), TO_DATE('12/30/2015 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'cms1', 1, 'Approved - Create');
   
Insert into S_PARAMETERS
   (ID, NAME, VALUE, DESCRIPTION, CREATE_DATE, CHANGE_DATE, CHANGE_WHO, STATUS, APPROVAL_STATUS)
 Values
   ('20151219PARAM0016', 'FORGET_PASSWORD_SUBJECT', 'Password Reset Request', 'Subject of Forget Password Notification', TO_DATE('12/17/2015 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), TO_DATE('12/30/2015 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'cms1', 1, 'Approved - Create');
Insert into S_PARAMETERS
   (ID, NAME, VALUE, DESCRIPTION, CREATE_DATE, CHANGE_DATE, CHANGE_WHO, STATUS, APPROVAL_STATUS)
 Values
   ('20151219PARAM0017', 'FORGET_PASSWORD_TEXT', 
'Dear #{#user.fullName},

We are notified that you asked for password reset. 

Please find below your new account information:
 
Your User ID: #{#user.userName}
Your Password : #{#PLAIN_PASSWORD}

Please visit #{#WELCOME_URL} to login to our system. 
You will be asked to change your password on first login. 

Thank you
SAM Support', 
'Content of Forget Password Notification', TO_DATE('12/17/2015 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), TO_DATE('12/30/2015 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'cms1', 1, 'Approved - Create');

Insert into S_PARAMETERS
   (ID, NAME, VALUE, DESCRIPTION, CREATE_DATE, CHANGE_DATE, CHANGE_WHO, STATUS, APPROVAL_STATUS)
 Values
   ('20151219PARAM0018', 'FORGET_USERNAME_SUBJECT', 'Forget Username', 'Subject of Forget User Id Notification', TO_DATE('12/17/2015 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), TO_DATE('12/30/2015 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'cms1', 1, 'Approved - Create');
Insert into S_PARAMETERS
   (ID, NAME, VALUE, DESCRIPTION, CREATE_DATE, CHANGE_DATE, CHANGE_WHO, STATUS, APPROVAL_STATUS)
 Values
   ('20151219PARAM0019', 'FORGET_USERNAME_TEXT', 
'Dear #{#user.fullName},

Please find below your User Id to login to SAM BJB:
 
Your User ID: #{#user.userName}
 
Thank you
SAM Support', 
'Content of Forget User Id Notification', TO_DATE('12/17/2015 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), TO_DATE('12/30/2015 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'cms1', 1, 'Approved - Create');

Insert into S_PARAMETERS
   (ID, NAME, VALUE, DESCRIPTION, CREATE_DATE, CHANGE_DATE, CHANGE_WHO, STATUS, APPROVAL_STATUS)
 Values
   ('20151219PARAM0020', 'MIN_PASSWORD_LENGTH', '6', 'Minimum Length of Password', TO_DATE('12/17/2015 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), TO_DATE('12/30/2015 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'cms1', 1, 'Approved - Create');

Insert into S_PARAMETERS
   (ID, NAME, VALUE, DESCRIPTION, CREATE_DATE, CHANGE_DATE, CHANGE_WHO, STATUS, APPROVAL_STATUS)
 Values
   ('20151219PARAM0021', 'SMTP_HOST', 'smtp.gmail.com', 'SMTP Address for Emailing Support', TO_DATE('12/17/2015 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), TO_DATE('12/30/2015 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'cms1', 1, 'Approved - Create');

Insert into S_PARAMETERS
   (ID, NAME, VALUE, DESCRIPTION, CREATE_DATE, CHANGE_DATE, CHANGE_WHO, STATUS, APPROVAL_STATUS)
 Values
   ('20151219PARAM0022', 'SMTP_PORT', '587', 'SMTP Port for Emailing Support', TO_DATE('12/17/2015 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), TO_DATE('12/30/2015 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'cms1', 1, 'Approved - Create');
Insert into S_PARAMETERS
   (ID, NAME, VALUE, DESCRIPTION, CREATE_DATE, CHANGE_DATE, CHANGE_WHO, STATUS, APPROVAL_STATUS)
 Values
   ('20151219PARAM0023', 'MAILER_ACCOUNT', 'bjbsam.mailer@gmail.com', 'Mailer Account Name', TO_DATE('12/17/2015 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), TO_DATE('12/30/2015 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'cms1', 1, 'Approved - Create');
Insert into S_PARAMETERS
   (ID, NAME, VALUE, DESCRIPTION, CREATE_DATE, CHANGE_DATE, CHANGE_WHO, STATUS, APPROVAL_STATUS)
 Values
   ('20151219PARAM0024', 'MAILER_PASSWORD', 'bjbP@ssw0rd', 'Mailer Account Password', TO_DATE('12/17/2015 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), TO_DATE('12/30/2015 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'cms1', 1, 'Approved - Create');
   
Insert into S_PARAMETERS
   (ID, NAME, VALUE, DESCRIPTION, CREATE_DATE, CHANGE_DATE, CHANGE_WHO, STATUS, APPROVAL_STATUS)
 Values
   ('20151219PARAM0031', 'WS_MIDDLEWARE_ENDPOINT_ADDRESS', 'http://AGUSTA:8088/mockSaldoWebServiceSoapBinding', 'Middleware Web Service Endpoint Address', TO_DATE('12/17/2015 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), TO_DATE('12/30/2015 00:00:00', 'MM/DD/YYYY HH24:MI:SS'), 'cms1', 1, 'Approved - Create');

COMMIT;
