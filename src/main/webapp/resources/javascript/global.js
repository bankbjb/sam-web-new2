var originalPrimeFacesAjaxResponseFunction = PrimeFaces.ajax.AjaxResponse;
PrimeFaces.ajax.AjaxResponse = function(responseXML) {
  var newView = $(responseXML.documentElement).find("update[id='javax.faces.ViewRoot']").text();

  if (newView) {
    $('head').html(newView.substring(newView.indexOf("<head>") + 6, newView.indexOf("</head>")));
    $('body').html(newView.substring(newView.indexOf("<body>") + 6, newView.indexOf("</body>")));
  }
  else {
    originalPrimeFacesAjaxResponseFunction.apply(this, arguments);
  }
};


/**
 * Global function on document ready
 */
$(document).ready(function() {
	
	/**
	 * this is for equalIgnoreCase function
	 */
	$.expr[":"].contains = $.expr
			.createPseudo(function(arg) {
				return function(elem) {
					return $(elem).text().toUpperCase().indexOf(
							arg.toUpperCase()) >= 0;
				};
			});
	
	
	/**
	 * When Search textbox is typed
	 */
	$(".search-menu").keyup(function() {
		var search = $(this).val();
		$(".ui-menu-list").show();
		if (search) {
			$("li.ui-menuitem").not(":contains(" + search + ")").hide();
			$("li.ui-widget-header").hide();
		} else {
			$("li.ui-menuitem").not(":contains(" + search + ")").show();
			$("li.ui-widget-header").show();
		}
		$("li.ui-menuitem:contains(" + search + ")").show();
	});
	
});

