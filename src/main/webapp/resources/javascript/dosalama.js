function setSudah(){
	document.getElementById('form2:sudah').value = "yes";
}
function setSudahNo(){
	document.getElementById('form2:sudah').value = "no";
}
function hapus(){
	document.getElementById('form:inquir').click();
}
function hapus2(){
	document.getElementById('form:inquir2').click();
}
function savedAccNo(){
	document.getElementById('form:reloadProvider').click();
}
function billerSelectEdit(){
	document.getElementById('form2:billEdit').click();
}
function addDebitAcc(){
	document.getElementById('form:addDebitAcc2').click();
}
function onChangeBankCode(bankCode){
	var bankCodeVal = bankCode.value;
	var bankCodeID = bankCode.id;
	var splitedID = bankCodeID.split(':');
	var indexBankCodeID = splitedID[2];
	var isDisabled = false;
	var bankCodeClass = bankCode.className;
	var buttonClass = "ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only";
	var cssClass = bankCodeClass.replace(" ui-state-focus"," ");
	
	if(bankCodeVal != '008'){
		isDisabled = false;
		buttonClass = buttonClass+"  ui-state-disabled";
		
		document.getElementById("form2:inputs:"+indexBankCodeID+":acct_status").value= "";
		
	}else{
		isDisabled = true;

		document.getElementById("form2:inputs:"+indexBankCodeID+":acct_name").value= "";
		document.getElementById("form2:inputs:"+indexBankCodeID+":acct_type").value= "";
		document.getElementById("form2:inputs:"+indexBankCodeID+":acct_cur").value= "";
		document.getElementById("form2:inputs:"+indexBankCodeID+":acct_cif_no").value= "";

		cssClass = cssClass+'ui-state-disabled';
		
	}
	
	document.getElementById("form2:inputs:"+indexBankCodeID+":acct_name").required=!isDisabled;
	document.getElementById("form2:inputs:"+indexBankCodeID+":acct_type").required=!isDisabled;
	document.getElementById("form2:inputs:"+indexBankCodeID+":acct_cur").required=!isDisabled;
	document.getElementById("form2:inputs:"+indexBankCodeID+":acct_cif_no").required=!isDisabled;
	
	document.getElementById("form2:inputs:"+indexBankCodeID+":acct_name").className = cssClass;
	document.getElementById("form2:inputs:"+indexBankCodeID+":acct_type").className=cssClass;
	document.getElementById("form2:inputs:"+indexBankCodeID+":acct_cur").className=cssClass;
	document.getElementById("form2:inputs:"+indexBankCodeID+":acct_cif_no").className=cssClass;
	document.getElementById("form2:inputs:"+indexBankCodeID+":btnInq").className = buttonClass;
	
	document.getElementById("form2:inputs:"+indexBankCodeID+":btnInq").disabled=!isDisabled;
	document.getElementById("form2:inputs:"+indexBankCodeID+":acct_name").readonly=isDisabled;
	document.getElementById("form2:inputs:"+indexBankCodeID+":acct_type").disabled=isDisabled;
	document.getElementById("form2:inputs:"+indexBankCodeID+":acct_cur").disabled=isDisabled;
	document.getElementById("form2:inputs:"+indexBankCodeID+":acct_cif_no").disabled=isDisabled;
}


function onChangeBankCodeEdit(bankCode){
	
	var isDisabled = true;
	var buttonClass = "ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only";
	var bankCodeClass = bankCode.className;
	var cssClass = bankCodeClass.replace(" ui-state-focus"," ");
	
	if(bankCode.value != '008'){
		isDisabled = false;
		buttonClass = buttonClass+"  ui-state-disabled";
	}else{
		isDisabled = true;

		document.getElementById("formEdit:acct_name").value= "";
		document.getElementById("formEdit:acct_type").value= "";
		document.getElementById("formEdit:acct_cur").value= "";
		document.getElementById("formEdit:acct_cif_no").value= "";
		
		cssClass = cssClass+'ui-state-disabled';
		document.getElementById('form:emptyAccInqEdit').click();
	}
	
	document.getElementById("formEdit:acct_name").className = cssClass;
	document.getElementById("formEdit:acct_type").className=cssClass;
	document.getElementById("formEdit:acct_cur").className=cssClass;
	document.getElementById("formEdit:acct_cif_no").className=cssClass;
	document.getElementById("formEdit:btnInq").className = buttonClass;
	
	document.getElementById("formEdit:btnInq").disabled=!isDisabled;
	document.getElementById("formEdit:acct_name").disabled=isDisabled;
	document.getElementById("formEdit:acct_type").disabled=isDisabled;
	document.getElementById("formEdit:acct_cur").disabled=isDisabled;
	document.getElementById("formEdit:acct_cif_no").disabled=isDisabled;
}

function changeAccNoEdit(accNo){
	var accNoID = accNo.id;
	var isDisabled = false;
	var bankCodeBal = document.getElementById("formEdit:bankCode").value;
	var bankCodeClass = accNo.className;
	var buttonClass = "ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only";
	var cssClass = bankCodeClass.replace(" ui-state-focus"," ");
	
	if(bankCodeBal == '008'){
		document.getElementById("formEdit:acct_name").value= "";
		document.getElementById("formEdit:acct_type").value= "";
		document.getElementById("formEdit:acct_cur").value= "";
		document.getElementById("formEdit:acct_cif_no").value= "";
		
		document.getElementById('form:emptyAccInqEdit').click();
		isDisabled = true;
		cssClass = cssClass+'ui-state-disabled';
	}else{
		isDisabled = false;
		buttonClass = buttonClass+"  ui-state-disabled";
	}

	document.getElementById("formEdit:acct_name").className = cssClass;
	document.getElementById("formEdit:acct_type").className=cssClass;
	document.getElementById("formEdit:acct_cur").className=cssClass;
	document.getElementById("formEdit:acct_cif_no").className=cssClass;
	document.getElementById("formEdit:btnInq").className = buttonClass;
	
	document.getElementById("formEdit:btnInq").disabled=!isDisabled;
	document.getElementById("formEdit:acct_name").disabled=isDisabled;
	document.getElementById("formEdit:acct_type").disabled=isDisabled;
	document.getElementById("formEdit:acct_cur").disabled=isDisabled;
	document.getElementById("formEdit:acct_cif_no").disabled=isDisabled;
}

function tesInqLanjut(accNo){
	var accNoID = accNo.id;
	var splitedID = accNoID.split(':');
	var indexaccNoID = splitedID[2];
	var isDisabled = false;
	var bankCodeBal = document.getElementById("form2:inputs:"+indexaccNoID+":bankCode").value;
	var bankCodeClass = accNo.className;
	var buttonClass = "ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only";
	var cssClass = bankCodeClass.replace(" ui-state-focus"," ");
	
	if(bankCodeBal == '008'){
		document.getElementById("form2:inputs:"+indexaccNoID+":acct_name").value= "";
		document.getElementById("form2:inputs:"+indexaccNoID+":acct_type").value= "";
		document.getElementById("form2:inputs:"+indexaccNoID+":acct_cur").value= "";
		document.getElementById("form2:inputs:"+indexaccNoID+":acct_cif_no").value= "";
		
		document.getElementById('form:emptyAccInq').click();
		isDisabled = true;
		cssClass = cssClass+'ui-state-disabled';
	}else{
		isDisabled = false;
		buttonClass = buttonClass+"  ui-state-disabled";
	}

	document.getElementById("form2:inputs:"+indexaccNoID+":acct_name").className = cssClass;
	document.getElementById("form2:inputs:"+indexaccNoID+":acct_type").className=cssClass;
	document.getElementById("form2:inputs:"+indexaccNoID+":acct_cur").className=cssClass;
	document.getElementById("form2:inputs:"+indexaccNoID+":acct_cif_no").className=cssClass;
	document.getElementById("form2:inputs:"+indexaccNoID+":btnInq").className = buttonClass;
	
	document.getElementById("form2:inputs:"+indexaccNoID+":btnInq").disabled=!isDisabled;
	document.getElementById("form2:inputs:"+indexaccNoID+":acct_name").readonly=isDisabled;
	document.getElementById("form2:inputs:"+indexaccNoID+":acct_type").disabled=isDisabled;
	document.getElementById("form2:inputs:"+indexaccNoID+":acct_cur").disabled=isDisabled;
	document.getElementById("form2:inputs:"+indexaccNoID+":acct_cif_no").disabled=isDisabled;

}


function changeAccNoEditPartnerAcc(accNo){
	document.getElementById("formEdit:acct_name").value= "";
	document.getElementById("formEdit:acct_type").value= "";
	document.getElementById("formEdit:acct_cur").value= "";
	document.getElementById("formEdit:acct_cif_no").value= "";
	
	document.getElementById('form:emptyAccInqEdit').click();
	
}

function tesInqLanjutPartner(accNo){
	var accNoID = accNo.id;
	var splitedID = accNoID.split(':');
	var indexaccNoID = splitedID[2];
	var buttonClass = "ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only";
	alert(indexaccNoID);
	
	document.getElementById("form2:inputs:"+indexaccNoID+":acct_name").value= "";
	document.getElementById("form2:inputs:"+indexaccNoID+":acct_type").value= "";
	document.getElementById("form2:inputs:"+indexaccNoID+":acct_cur").value= "";
	document.getElementById("form2:inputs:"+indexaccNoID+":acct_cif_no").value= "";
		
	document.getElementById('form:emptyAccInq').click();
	
}