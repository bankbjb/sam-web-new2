package com.bjb.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

public class User extends Auditable<BigDecimal> {
	private static final long serialVersionUID = 1L;
	
	private BigDecimal id;
	private String code;
	private String userName;
	private String fullName;
	private Date lastLogin;
	private Branch branch;
	private Integer failedLogin;
	private Date failedLoginDate;	
	private String roleNameTemp;
	private Set<Role> roles;
	private Integer status;
	private Integer	lockedStatus;

	private String email;
	
	public User() {}
	
	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public BigDecimal getId() {
		return id;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	public String getCode() {
		return code;
	}

	public String getName() {
		return fullName;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}
	
	public String getRoleNameTemp() {
		return roleNameTemp;
	}

	public void setRoleNameTemp(String roleNameTemp) {
		this.roleNameTemp = roleNameTemp;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public Date getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}

	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}
	
	public Integer getFailedLogin() {
		return failedLogin;
	}

	public void setFailedLogin(Integer failedLogin) {
		this.failedLogin = failedLogin;
	}

	public Date getFailedLoginDate() {
		return failedLoginDate;
	}

	public void setFailedLoginDate(Date failedLoginDate) {
		this.failedLoginDate = failedLoginDate;
	}
	

	@Override
    public int hashCode() {
        return (id != null) ? (this.getClass().hashCode() + id.hashCode()) : super.hashCode();
    }

	@Override
    public boolean equals(Object other) {
        return (other instanceof User) && (id != null) ? id.equals(((User) other).id) : (other == this);
    }

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getLockedStatus() {
		return lockedStatus;
	}

	public void setLockedStatus(Integer lockedStatus) {
		this.lockedStatus = lockedStatus;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}	
	
}
