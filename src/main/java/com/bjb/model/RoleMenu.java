package com.bjb.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RoleMenu implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Integer roleId;
	private Integer menuId;
	private String actions;
	private Menu menu;
	
	private List<Action> actionList = new ArrayList<Action>();
	
	public RoleMenu() {
		
	}
	
	public RoleMenu(Integer roleId, Menu menu) {
		super();
		this.roleId = roleId;
		this.menu = menu;
		this.menuId = (menu==null)?null:menu.getId();
		
		if (menu!=null && menu.getActionList()!=null) {
			for (Action action:menu.getActionList()) {
				if (action.isSelected()) {
					this.actionList.add(new Action(true, action.getName()));
				}
			}
		}

	}

	public Integer getRoleId() {
		return roleId;
	}
	
	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}
	
	public Integer getMenuId() {
		return menuId;
	}
	
	public void setMenuId(Integer menuId) {
		this.menuId = menuId;
	}
	
	public String getActions() {
		actions = null;
		for (Action action:this.actionList) {
			if (action.isSelected()) {
				if (actions==null) {
					actions = action.getName();
				} else {
					actions = actions + "," + action.getName();
				}
			}
		}
		return actions;
	}
	
	public void setActions(String actions) {
		this.actions = actions;
		if (actions != null && !actions.trim().equals("")) {
			String[] actionArray = actions.split("\\,");
			for (String action:actionArray) {
				this.actionList.add(new Action(true, action));
			}
		}
	}
	public List<Action> getActionList() {
		return actionList;
	}
	public void setActionList(List<Action> actionList) {
		this.actionList = actionList;
	}
	public Menu getMenu() {
		return menu;
	}
	public void setMenu(Menu menu) {
		this.menu = menu;
	}

}
