package com.bjb.model;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.bjb.constant.Constants;

public class Aggregator extends Auditable<BigDecimal> {

	private static final long serialVersionUID = 1L;

	private BigDecimal id;
	private String code;
	private String name;
	private String address;
	private String postalCode;
	private String city;
	private String province;
	private String phone;
	private String fax;
	private String pic;
	private String emailPic;

	private String pksNo;
	private Date pksDate;
	private Date pksExpiredDate;
	private String accountNo;
	private String accountGlNo;
	private String minimumBalance;
	private Integer statusNotification;
	private String messageType;
	private Integer statusCA;
	private String ipAddress;
	private String port;
	UserAggregator user;

	private Integer status;

	public Aggregator() {

	}

	public BigDecimal getId() {
		return id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public String getEmailPic() {
		return emailPic;
	}

	public void setEmailPic(String emailPic) {
		this.emailPic = emailPic;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public Date getRegistrationDate() {
		return getCreateDate();
	}

	public void setRegistrationDate(Date registrationDate) {
		setCreateDate(registrationDate);
	}

	public String getRegistrationTime() {
		SimpleDateFormat dt = new SimpleDateFormat(Constants.FORMAT_TIME);
		return (getCreateDate() != null) ? dt.format(getCreateDate()) : null;
	}

	public String getPksNo() {
		return pksNo;
	}

	public void setPksNo(String pksNo) {
		this.pksNo = pksNo;
	}

	public Date getPksDate() {
		return pksDate;
	}

	public void setPksDate(Date pksDate) {
		this.pksDate = pksDate;
	}

	public Date getPksExpiredDate() {
		return pksExpiredDate;
	}

	public void setPksExpiredDate(Date pksExpiredDate) {
		this.pksExpiredDate = pksExpiredDate;
	}

	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public String getMinimumBalance() {
		return minimumBalance;
	}

	public void setMinimumBalance(String minimumBalance) {
		this.minimumBalance = minimumBalance;
	}

	public Integer getStatusNotification() {
		return statusNotification;
	}

	public void setStatusNotification(Integer statusNotification) {
		this.statusNotification = statusNotification;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public void setStatusCA(Integer statusCA) {
		this.statusCA = statusCA;
	}

	public Integer getStatusCA() {
		return statusCA;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public UserAggregator getUser() {
		return user;
	}

	public void setUser(UserAggregator user) {
		this.user = user;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getAccountGlNo() {
		return accountGlNo;
	}

	public void setAccountGlNo(String accountGlNo) {
		this.accountGlNo = accountGlNo;
	}

}