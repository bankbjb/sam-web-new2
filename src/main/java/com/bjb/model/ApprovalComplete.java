package com.bjb.model;

import java.util.Date;

public class ApprovalComplete extends Approval {
	
	private static final long serialVersionUID = 1L;
	private Date   approveDate;
	private String statusCode;
	private String statusMessage;
	
	public ApprovalComplete() {
		// TODO Auto-generated constructor stub
	}

	public Date getApproveDate() {
		return approveDate;
	}

	public void setApproveDate(Date approveDate) {
		this.approveDate = approveDate;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}
		
}