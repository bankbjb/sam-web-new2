package com.bjb.model;

import java.math.BigDecimal;

public class FeatureMapping extends Auditable<BigDecimal> {
	private static final long serialVersionUID = 1L;
	
	public BigDecimal id;
	public String code;
	public Feature feature;
	public Aggregator aggregator;
	public CollectingAgent collectingAgent;
	public BigDecimal commisionAmount;
	public Integer status;
	
	public BigDecimal getId() {
		return id;
	}
	public void setId(BigDecimal id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return code;
	}
	public Feature getFeature() {
		return feature;
	}
	public void setFeature(Feature feature) {
		this.feature = feature;
	}
	public Aggregator getAggregator() {
		return aggregator;
	}
	public void setAggregator(Aggregator aggregator) {
		this.aggregator = aggregator;
	}
	public CollectingAgent getCollectingAgent() {
		return collectingAgent;
	}
	public void setCollectingAgent(CollectingAgent collectingAgent) {
		this.collectingAgent = collectingAgent;
	}
	public BigDecimal getCommisionAmount() {
		return commisionAmount;
	}
	public void setCommisionAmount(BigDecimal commisionAmount) {
		this.commisionAmount = commisionAmount;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}

}
