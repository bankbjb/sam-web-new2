package com.bjb.model;

import java.math.BigDecimal;

public class CommisionTiering extends Auditable<BigDecimal> {

	private static final long serialVersionUID = 1L;
	
	public BigDecimal id;
	public String code;
	public FeatureMapping featureMapping;
	public BigDecimal lowerLimit;
	public BigDecimal upperLimit;
	public BigDecimal commisionAggregator;
	public BigDecimal commissionBjb;
	public Integer status;

	public BigDecimal getId() {
		return id;
	}
	public void setId(BigDecimal id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return code;
	}
	public FeatureMapping getFeatureMapping() {
		return featureMapping;
	}
	public void setFeatureMapping(FeatureMapping featureMapping) {
		this.featureMapping = featureMapping;
	}
	public BigDecimal getLowerLimit() {
		return lowerLimit;
	}
	public void setLowerLimit(BigDecimal lowerLimit) {
		this.lowerLimit = lowerLimit;
	}
	public BigDecimal getUpperLimit() {
		return upperLimit;
	}
	public void setUpperLimit(BigDecimal upperLimit) {
		this.upperLimit = upperLimit;
	}
	public BigDecimal getCommisionAggregator() {
		return commisionAggregator;
	}
	public void setCommisionAggregator(BigDecimal commisionAggregator) {
		this.commisionAggregator = commisionAggregator;
	}
	public BigDecimal getCommissionBjb() {
		return commissionBjb;
	}
	public void setCommissionBjb(BigDecimal commissionBjb) {
		this.commissionBjb = commissionBjb;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}

}
