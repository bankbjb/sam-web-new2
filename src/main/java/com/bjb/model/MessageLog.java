package com.bjb.model;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.bjb.constant.Constants;

public class MessageLog implements DomainObject {

	private static final long serialVersionUID = 1L;
	
	public BigDecimal id;
	public String aggregatorCode;
	public String caCode;
	public String featureCode;
	public String responseCode;
	public String mti;
	public String billingId;
	public String stan;
	public String rawRequest;
	public String rawResponse;
	public Date createDate;
	public BigDecimal getId() {
		return id;
	}
	public void setId(BigDecimal id) {
		this.id = id;
	}
	public String getAggregatorCode() {
		return aggregatorCode;
	}
	public void setAggregatorCode(String aggregatorCode) {
		this.aggregatorCode = aggregatorCode;
	}
	public String getCaCode() {
		return caCode;
	}
	public void setCaCode(String caCode) {
		this.caCode = caCode;
	}
	public String getFeatureCode() {
		return featureCode;
	}
	public void setFeatureCode(String featureCode) {
		this.featureCode = featureCode;
	}
	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public String getMti() {
		return mti;
	}
	public void setMti(String mti) {
		this.mti = mti;
	}
	public String getBillingId() {
		return billingId;
	}
	public void setBillingId(String billingId) {
		this.billingId = billingId;
	}
	public String getStan() {
		return stan;
	}
	public void setStan(String stan) {
		this.stan = stan;
	}
	public String getRawRequest() {
		return rawRequest;
	}
	public void setRawRequest(String rawRequest) {
		this.rawRequest = rawRequest;
	}
	public String getRawResponse() {
		return rawResponse;
	}
	public void setRawResponse(String rawResponse) {
		this.rawResponse = rawResponse;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}	
	public String getCreateTime() {
		SimpleDateFormat dt = new SimpleDateFormat(Constants.FORMAT_TIME);
		return (getCreateDate() != null)?dt.format(getCreateDate()):null;
	}
	
}
