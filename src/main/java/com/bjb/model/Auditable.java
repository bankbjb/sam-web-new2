package com.bjb.model;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.bjb.constant.Constants;

public abstract class Auditable<K> implements AuditableObject<K> {
	private static final long serialVersionUID = 1L;
	
	private Date createDate;
	private BigDecimal createBy;
	private String createWho;
	private Date changeDate;
	private BigDecimal changeBy;
	private String changeWho;
	private Date approvalDate;
	private BigDecimal approvalBy;
	private String approvalWho;
	private String approvalReason;
	private String approvalStatus;
	private Boolean disableDelete;
	private Boolean disableEdit;
	
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public BigDecimal getCreateBy() {
		return createBy;
	}
	public void setCreateBy(BigDecimal createBy) {
		this.createBy = createBy;
	}
	public String getCreateWho() {
		return createWho;
	}
	public void setCreateWho(String createWho) {
		this.createWho = createWho;
	}
	public Date getChangeDate() {
		return changeDate;
	}
	public void setChangeDate(Date changeDate) {
		this.changeDate = changeDate;
	}
	public BigDecimal getChangeBy() {
		return changeBy;
	}
	public void setChangeBy(BigDecimal changeBy) {
		this.changeBy = changeBy;
	}
	public String getChangeWho() {
		return changeWho;
	}
	public void setChangeWho(String changeWho) {
		this.changeWho = changeWho;
	}
	
	public String getApprovalTime() {
		SimpleDateFormat dt = new SimpleDateFormat(Constants.FORMAT_TIME);
		return (getApprovalDate() != null)?dt.format(getApprovalDate()):null;
	}
	
	public String getCreateTime() {
		SimpleDateFormat dt = new SimpleDateFormat(Constants.FORMAT_TIME);
		return (getCreateDate() != null)?dt.format(getCreateDate()):null;
	}

	public Date getApprovalDate() {
		return approvalDate;
	}

	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}

	public BigDecimal getApprovalBy() {
		return approvalBy;
	}

	public void setApprovalBy(BigDecimal approvalBy) {
		this.approvalBy = approvalBy;
	}

	public String getApprovalWho() {
		return approvalWho;
	}
	public void setApprovalWho(String approvalWho) {
		this.approvalWho = approvalWho;
	}
	public String getApprovalStatus() {
		return approvalStatus;
	}

	public void setApprovalStatus(String status) {
		this.approvalStatus = status;
		this.disableDelete = (status==null || this.approvalStatus.equals(Constants.APPROVAL_STATUS_PENDING_CREATE) || (this.approvalStatus.equals(Constants.APPROVAL_STATUS_PENDING_EDIT))) || this.approvalStatus.equals(Constants.APPROVAL_STATUS_PENDING_DELETE);
		this.disableEdit = (status==null || this.approvalStatus.equals(Constants.APPROVAL_STATUS_PENDING_DELETE));
	}
	
	public String getApprovalReason() {
		return approvalReason;
	}
	
	public void setApprovalReason(String approvalReason) {
		this.approvalReason = approvalReason;
	}
	
	public Boolean getDisableEdit() {
		return disableEdit;
	}

	public Boolean getDisableDelete() {
		return disableDelete;
	}
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Auditable other = (Auditable) obj;
		if (getId() == null) {
			if (other.getId() != null)
				return false;
		} else if (!getId().equals(other.getId()))
			return false;
		return true;
	}

}
