package com.bjb.model;

public class Branch extends Auditable<String> {
	
	private static final long serialVersionUID = 1L;
	
	private String	parentId;
	private String 	code;
	private String 	name;
	private String 	description;
	private Integer status;
	
	public Branch(){
		
	}

	public String getId() {
		return code;
	}

	public void setId(String id) {
		this.code = id;
	}
	
	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
	
}
