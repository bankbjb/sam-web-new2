package com.bjb.model;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.bjb.constant.Constants;

public class TransactionLog implements DomainObject {

	private static final long serialVersionUID = 1L;
	
	public BigDecimal id;
	public Date createDate;
	public String terminalId;
	public String name;
	public FeatureMapping featureMapping;
	public String featureCode;
	public String caCode;
	public String aggregatorCode;
	public BigDecimal commisionAgent;
	public BigDecimal commisionBjb;
	public BigDecimal amount;
	public String billingId;
	public String responseCode;
	
	public BigDecimal getId() {
		return id;
	}
	public void setId(BigDecimal id) {
		this.id = id;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public String getTerminalId() {
		return terminalId;
	}
	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public FeatureMapping getFeatureMapping() {
		return featureMapping;
	}
	public void setFeatureMapping(FeatureMapping featureMapping) {
		this.featureMapping = featureMapping;
	}
	public String getFeatureCode() {
		return featureCode;
	}
	public void setFeatureCode(String featureCode) {
		this.featureCode = featureCode;
	}
	public String getCaCode() {
		return caCode;
	}
	public void setCaCode(String caCode) {
		this.caCode = caCode;
	}
	public String getAggregatorCode() {
		return aggregatorCode;
	}
	public void setAggregatorCode(String aggregatorCode) {
		this.aggregatorCode = aggregatorCode;
	}
	public BigDecimal getCommisionAgent() {
		return commisionAgent;
	}
	public void setCommisionAgent(BigDecimal commisionAgent) {
		this.commisionAgent = commisionAgent;
	}
	public BigDecimal getCommisionBjb() {
		return commisionBjb;
	}
	public void setCommisionBjb(BigDecimal commisionBjb) {
		this.commisionBjb = commisionBjb;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public String getBillingId() {
		return billingId;
	}
	public void setBillingId(String billingId) {
		this.billingId = billingId;
	}
	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public String getCreateTime() {
		SimpleDateFormat dt = new SimpleDateFormat(Constants.FORMAT_TIME);
		return (getCreateDate() != null)?dt.format(getCreateDate()):null;
	}
	public String createDate(String format) {
		SimpleDateFormat dt = new SimpleDateFormat(format);
		return (getCreateDate() != null)?dt.format(getCreateDate()):null;
	}

	
}
