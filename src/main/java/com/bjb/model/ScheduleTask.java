package com.bjb.model;

import java.math.BigDecimal;

import com.bjb.constant.Constants;

public class ScheduleTask extends Auditable<BigDecimal> {

	private static final long serialVersionUID = 1L;

	private BigDecimal id;
	private String code;
	private String taskId;;
	private String description;
	private String jobName; 
	private String executionService; 
	private String schedulerType; 
	private String date; 
	private String time; 
	private String interval; 
	private String startDate; 
	private String endDate; 
	private String startTime; 
	private String endTime; 
	private String maskYear; 
	private String maskMonth; 
	private String maskDay; 
	private String maskWeeklyDay; 
	private String maskHour; 
	private String maskHourEnd; 
	private String maskMinute; 
	private String schedulerStatus;
	private String repeating;
	private Integer status;
	
	public ScheduleTask() {
		// TODO Auto-generated constructor stub
	}
	
	public BigDecimal getId() {
		return id;
	}
	public void setId(BigDecimal id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return jobName;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public String getExecutionService() {
		return executionService;
	}
	public void setExecutionService(String executionService) {
		this.executionService = executionService;
	}
	public String getSchedulerType() {
		return schedulerType;
	}
	public void setSchedulerType(String schedulerType) {
		this.schedulerType = schedulerType;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getInterval() {
		return interval;
	}
	public void setInterval(String interval) {
		this.interval = interval;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getMaskYear() {
		return maskYear;
	}
	public void setMaskYear(String maskYear) {
		this.maskYear = maskYear;
	}
	public String getMaskMonth() {
		return maskMonth;
	}
	public void setMaskMonth(String maskMonth) {
		this.maskMonth = maskMonth;
	}
	public String getMaskDay() {
		return maskDay;
	}
	public void setMaskDay(String maskDay) {
		this.maskDay = maskDay;
	}
	public String getMaskWeeklyDay() {
		return maskWeeklyDay;
	}
	public void setMaskWeeklyDay(String maskWeeklyDay) {
		this.maskWeeklyDay = maskWeeklyDay;
	}
	public String getMaskHour() {
		return maskHour;
	}
	public void setMaskHour(String maskHour) {
		this.maskHour = maskHour;
	}
	
	public String getMaskHourEnd() {
		return maskHourEnd;
	}

	public void setMaskHourEnd(String maskHourEnd) {
		this.maskHourEnd = maskHourEnd;
	}

	public String getMaskMinute() {
		return maskMinute;
	}
	public void setMaskMinute(String maskMinute) {
		this.maskMinute = maskMinute;
	}
	public String getSchedulerStatus() {
		return schedulerStatus;
	}
	public void setSchedulerStatus(String schedulerStatus) {
		this.schedulerStatus = schedulerStatus;
	}
	public String getRepeating() {
		return repeating;
	}
	public void setRepeating(String repeating) {
		this.repeating = repeating;
	}

	public Boolean getDisableStopStart() {
		if (getApprovalStatus().equals(Constants.APPROVAL_STATUS_PENDING_CREATE))
			return true;
		else{
			return false;
		}
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}	
	
}
