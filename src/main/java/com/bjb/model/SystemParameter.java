package com.bjb.model;

public class SystemParameter extends Auditable<String> {
    private static final long serialVersionUID = 1L;
    private String code;
    private String name;
    private String description;
    private String value;
    private Integer status;
    private Integer secured;
    
    public SystemParameter() {}
	
	public String getId() {
		return code;
	}

	public void setId(String code) {
		this.code = code;
	}
	
	public void setCode(String name) {
		this.name = name;
	}
	
	public String getCode() {
		return name;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getSecured() {
		return secured;
	}

	public void setSecured(Integer secured) {
		this.secured = secured;
	}
  
 }