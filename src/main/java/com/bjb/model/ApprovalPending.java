package com.bjb.model;

import java.sql.Blob;

public class ApprovalPending extends Approval {
	
	private static final long serialVersionUID = 1L;
	private byte[] oldByteObject;
	private byte[] newByteObject;
	private Blob   oldBlobObject;
	private Blob   newBlobObject;
	private boolean selected;

	public ApprovalPending() {
		// TODO Auto-generated constructor stub
	}
	
	public byte[] getOldByteObject() {
		return oldByteObject;
	}

	public void setOldByteObject(byte[] oldByteObject) {
		this.oldByteObject = oldByteObject;
	}

	public byte[] getNewByteObject() {
		return newByteObject;
	}

	public void setNewByteObject(byte[] newByteObject) {
		this.newByteObject = newByteObject;
	}

	public Blob getOldBlobObject() {
		return oldBlobObject;
	}

	public void setOldBlobObject(Blob oldBlobObject) {
		this.oldBlobObject = oldBlobObject;
	}

	public Blob getNewBlobObject() {
		return newBlobObject;
	}

	public void setNewBlobObject(Blob newBlobObject) {
		this.newBlobObject = newBlobObject;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	
}