package com.bjb.model;

public class UserBank extends User {

	private static final long serialVersionUID = 1L;

	private String nip;
	private String branchCode;
	private String parentBranchCode;
	private String branchName;
	private String parentBranchName;
	private String regionalOfficeCode;
	private String regionalOfficeName;
	private String jobTitle;
	private String jobPosition;
	private String password;
	private String mobileNo;
	private String gradeCode;
	private String gradeName;
	private String hrPosition;
	private String isActive;
	private String isExpire;
	private String isLocked;
	private String functionCode;
	private String addOnFunctionCode;
	private String debitLimitTransaction;
	private String creditLimitTransaction;
	private String isSupervisor;
	private String limitScope;
	private String socialId;
	
	public String getNip() {
		return nip;
	}
	public void setNip(String nip) {
		this.nip = nip;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getParentBranchCode() {
		return parentBranchCode;
	}
	public void setParentBranchCode(String parentBranchCode) {
		this.parentBranchCode = parentBranchCode;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getParentBranchName() {
		return parentBranchName;
	}
	public void setParentBranchName(String parentBranchName) {
		this.parentBranchName = parentBranchName;
	}
	public String getRegionalOfficeCode() {
		return regionalOfficeCode;
	}
	public void setRegionalOfficeCode(String regionalOfficeCode) {
		this.regionalOfficeCode = regionalOfficeCode;
	}
	public String getRegionalOfficeName() {
		return regionalOfficeName;
	}
	public void setRegionalOfficeName(String regionalOfficeName) {
		this.regionalOfficeName = regionalOfficeName;
	}
	public String getJobTitle() {
		return jobTitle;
	}
	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}
	public String getJobPosition() {
		return jobPosition;
	}
	public void setJobPosition(String jobPosition) {
		this.jobPosition = jobPosition;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getGradeCode() {
		return gradeCode;
	}
	public void setGradeCode(String gradeCode) {
		this.gradeCode = gradeCode;
	}
	public String getGradeName() {
		return gradeName;
	}
	public void setGradeName(String gradeName) {
		this.gradeName = gradeName;
	}
	public String getHrPosition() {
		return hrPosition;
	}
	public void setHrPosition(String hrPosition) {
		this.hrPosition = hrPosition;
	}
	public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	public String getIsExpire() {
		return isExpire;
	}
	public void setIsExpire(String isExpire) {
		this.isExpire = isExpire;
	}
	public String getIsLocked() {
		return isLocked;
	}
	public void setIsLocked(String isLocked) {
		this.isLocked = isLocked;
	}
	public String getFunctionCode() {
		return functionCode;
	}
	public void setFunctionCode(String functionCode) {
		this.functionCode = functionCode;
	}
	public String getAddOnFunctionCode() {
		return addOnFunctionCode;
	}
	public void setAddOnFunctionCode(String addOnFunctionCode) {
		this.addOnFunctionCode = addOnFunctionCode;
	}
	public String getDebitLimitTransaction() {
		return debitLimitTransaction;
	}
	public void setDebitLimitTransaction(String debitLimitTransaction) {
		this.debitLimitTransaction = debitLimitTransaction;
	}
	public String getCreditLimitTransaction() {
		return creditLimitTransaction;
	}
	public void setCreditLimitTransaction(String creditLimitTransaction) {
		this.creditLimitTransaction = creditLimitTransaction;
	}
	public String getIsSupervisor() {
		return isSupervisor;
	}
	public void setIsSupervisor(String isSupervisor) {
		this.isSupervisor = isSupervisor;
	}
	public String getLimitScope() {
		return limitScope;
	}
	public void setLimitScope(String limitScope) {
		this.limitScope = limitScope;
	}
	public String getSocialId() {
		return socialId;
	}
	public void setSocialId(String socialId) {
		this.socialId = socialId;
	}

}
