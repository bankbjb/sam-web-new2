package com.bjb.model;

import java.math.BigDecimal;
import java.util.Date;

public class ActivityLog implements DomainObject {

	private static final long serialVersionUID = 1L;

	private BigDecimal id;
	private Activity activity;
	
	private Date createDate;
	private BigDecimal createBy;
	private String createWho;
	
	private String objectType;
	private byte[]  dataBefore;
	private byte[]  dataAfter;
	
	private Integer status;
	
	public BigDecimal getId() {
		return id;
	}
	public void setId(BigDecimal id) {
		this.id = id;
	}
	public Activity getActivity() {
		return activity;
	}
	public void setActivity(Activity activity) {
		this.activity = activity;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public BigDecimal getCreateBy() {
		return createBy;
	}
	public void setCreateBy(BigDecimal createBy) {
		this.createBy = createBy;
	}
	public String getCreateWho() {
		return createWho;
	}
	public void setCreateWho(String createWho) {
		this.createWho = createWho;
	}
	public String getObjectType() {
		return objectType;
	}
	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}
	public  byte[]  getDataBefore() {
		return dataBefore;
	}
	public void setDataBefore(byte[]  dataBefore) {
		this.dataBefore = dataBefore;
	}
	public  byte[]  getDataAfter() {
		return dataAfter;
	}
	public void setDataAfter(byte[]  dataAfter) {
		this.dataAfter = dataAfter;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	
}
