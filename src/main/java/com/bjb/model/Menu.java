package com.bjb.model;

import java.util.ArrayList;
import java.util.List;

public class Menu implements DomainObject, Comparable<Menu>{
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String name;
	private String path;
	private String actions;
	private List<Action> actionList = new ArrayList<Action>();
	
	/**
	 * Note: <br/>
	 * must filled. <br/>
	 * if it's root(have no parent) parentId = 0
	 */
	private Integer parentId;
	private Integer ordinal;
	private Integer accessLevel;
	private boolean isAction;
	
	/**
	 * not database field
	 */
	private boolean selected;
	
	public Menu() {
	}
	public Menu(Menu menu){
		this.id = menu.getId();
		this.name = menu.getName();
		this.isAction = menu.isAction();
		this.parentId = menu.getParentId();
		this.path = menu.getPath();
		this.ordinal = menu.getOrdinal();
		this.actions = menu.getActions();
		this.actionList.addAll( menu.getActionList() );
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}	
	public Integer getParentId() {
		return parentId;
	}
	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}
	public Integer getOrdinal() {
		return ordinal;
	}
	public void setOrdinal(Integer ordinal) {
		this.ordinal = ordinal;
	}	

	public Integer getAccessLevel() {
		return accessLevel;
	}
	public void setAccessLevel(Integer accessLevel) {
		this.accessLevel = accessLevel;
	}
	public String getPath() {
		return path;
	}
	public void setMenuPath(String path) {
		this.path = path;
	}
	public boolean isAction() {
		return isAction;
	}
	public void setAction(boolean action) {
		this.isAction = action;
	}

	public String getActions() {
		actions = null;
		for (Action action:this.actionList) {
			if (action.isSelected()) {
				if (actions==null) {
					actions = action.getName();
				} else {
					actions = actions + "," + action.getName();
				}
			}
		}
		return actions;
	}
	
	public void setActions(String actions) {
		this.actions = actions;
		if (actions != null && !actions.trim().equals("")) {
			String[] actionArray = actions.split("\\,");
			for (String action:actionArray) {
				this.actionList.add(new Action(true, action));
			}
		}
	}	
	
	public List<Action> getActionList() {
		return actionList;
	}
	public void setActionList(List<Action> actionList) {
		this.actionList = actionList;
	}

	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	
	@Override
	public int compareTo(Menu otherMenu) {
		int compareOrdinal = otherMenu.getOrdinal();
		//ascending order
		return this.ordinal - compareOrdinal;
		 
		//descending order
		//return compareQuantity - this.quantity;
	}
	
}
