package com.bjb.model;

import java.math.BigDecimal;
import java.util.Date;

public class Holiday extends Auditable<BigDecimal>  {
	
	private static final long serialVersionUID = 1L;
	
	private BigDecimal id;

	private String code;
	
	private String name;
	
	private String description;
	
	private String sDate;
	
	private Date date;
	
	private Integer status;
	
	public Holiday() {
		// TODO Auto-generated constructor stub
	}

	public BigDecimal getId() {
		return id;
	}

	public void setId(BigDecimal holidayId) {
		this.id = holidayId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDateString() {
		return sDate;
	}

	public void setDateString(String sDate) {
		this.sDate = sDate;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}
