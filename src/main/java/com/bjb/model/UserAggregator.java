package com.bjb.model;

import java.math.BigDecimal;

public class UserAggregator extends User {

	private static final long serialVersionUID = 1L;
	
	private String contactNo;
	private BigDecimal debitLimitTransaction;
	private String department;
	private String division;
	private String mobileNo;
	private String password;
	private String plainPassword;
	private String phone;
	private BigDecimal aggregatorId;

	public BigDecimal getAggregatorId() {
		return aggregatorId;
	}

	public void setAggregatorId(BigDecimal aggregatorId) {
		this.aggregatorId = aggregatorId;
	}

	public BigDecimal getDebitLimitTransaction() {
		return debitLimitTransaction;
	}

	public String getDepartment() {
		return department;
	}

	public String getDivision() {
		return division;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public String getPassword() {
		return password;
	}

	public String getPlainPassword() {
		return plainPassword;
	}

	public void setPlainPassword(String plainPassword) {
		this.plainPassword = plainPassword;
	}

	public String getPhone() {
		return phone;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public String getContactNo() {
		return contactNo;
	}
	
	public void setDebitLimitTransaction(BigDecimal debitLimitTransaction) {
		this.debitLimitTransaction = debitLimitTransaction;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

}
