package com.bjb.model;

public interface AuditableObject<K> extends DomainObject {
	public K getId();
	public String getCode();
	public String getName();
}
