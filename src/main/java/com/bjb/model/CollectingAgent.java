package com.bjb.model;

import java.math.BigDecimal;

public class CollectingAgent extends Auditable<BigDecimal> {

	private static final long serialVersionUID = 1L;

	public BigDecimal id;
	public String code;
	public String name;
	public Integer maxAgent;
	public Integer maxAmount;
	
	
	public Integer status;


	public BigDecimal getId() {
		return id;
	}


	public void setId(BigDecimal id) {
		this.id = id;
	}


	public String getCode() {
		return code;
	}


	public void setCode(String code) {
		this.code = code;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Integer getMaxAgent() {
		return maxAgent;
	}


	public void setMaxAgent(Integer maxAgent) {
		this.maxAgent = maxAgent;
	}


	public Integer getMaxAmount() {
		return maxAmount;
	}


	public void setMaxAmount(Integer maxAmount) {
		this.maxAmount = maxAmount;
	}


	public Integer getStatus() {
		return status;
	}


	public void setStatus(Integer status) {
		this.status = status;
	}
}
