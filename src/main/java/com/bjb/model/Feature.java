package com.bjb.model;

import java.math.BigDecimal;

public class Feature extends Auditable<BigDecimal> {

	private static final long serialVersionUID = 1L;
	
	private BigDecimal id;
	private String code;
	private String isoCode;
	private String commisionAccountNo;
	private String name;
	private String description;
	private Integer status;	
	
	public BigDecimal getId() {
		return id;
	}
	public void setId(BigDecimal id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getIsoCode() {
		return isoCode;
	}
	public void setIsoCode(String isoCode) {
		this.isoCode = isoCode;
	}
	public String getCommisionAccountNo() {
		return commisionAccountNo;
	}
	public void setCommisionAccountNo(String commisionAccountNo) {
		this.commisionAccountNo = commisionAccountNo;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
}
