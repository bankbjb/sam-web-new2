package com.bjb.model;

public class MessageField extends Auditable<Integer> {

	private static final long serialVersionUID = 1L;

	private Integer id;
	private String messageType;
	private String name;
	private String description;
	private Integer length;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getLength() {
		return length;
	}

	public void setLength(Integer length) {
		this.length = length;
	}

	@Override
	public String getCode() {
		// TODO Auto-generated method stub
		return null;
	}

}
