package com.bjb.model;

import java.math.BigDecimal;
import java.util.List;

public class MessageMapping implements DomainObject {

	private static final long serialVersionUID = 1L;

	public BigDecimal id;
	public String code;
	public String messageType;
	public Feature feature;
	public List<FieldMapping> details;
	public Integer status;
	
	public BigDecimal getId() {
		return id;
	}
	public void setId(BigDecimal id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessageType() {
		return messageType;
	}
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	public Feature getFeature() {
		return feature;
	}
	public void setFeature(Feature feature) {
		this.feature = feature;
	}
	public List<FieldMapping> getDetails() {
		return details;
	}
	public void setDetails(List<FieldMapping> details) {
		this.details = details;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
}
