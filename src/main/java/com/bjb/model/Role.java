package com.bjb.model;

import java.util.Collection;
import java.util.HashSet;

public class Role extends Auditable<Integer> {
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String code;
	private String name;
	private Collection<RoleMenu> roleMenus = new HashSet<RoleMenu>();
	private Integer status;
	
	public Role() {}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public Collection<RoleMenu> getRoleMenus() {
		return roleMenus;
	}
	public void setRoleMenus(Collection<RoleMenu> roleMenus) {
		this.roleMenus = roleMenus;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}
