package com.bjb.model;

import java.math.BigDecimal;

public class FieldMapping implements DomainObject {

	private static final long serialVersionUID = 1L;

	public BigDecimal messageMappingId;
	public Integer index;
	public String name;
	public Integer startAt;
	public Integer endAt;
	public Integer fieldPosition;
	public Integer request;
	public Integer response;
	public String variable;
	public String padding;
	public String trxType;
	
	public String getVariable() {
		return variable;
	}

	public void setVariable(String variable) {
		this.variable = variable;
	}

	public BigDecimal getId(){
		return getMessageMappingId();
	}
	
	public BigDecimal getMessageMappingId() {
		return messageMappingId;
	}
	public void setMessageMappingId(BigDecimal messageMappingId) {
		this.messageMappingId = messageMappingId;
	}
	public Integer getIndex() {
		return index;
	}
	public void setIndex(Integer index) {
		this.index = index;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getStartAt() {
		return startAt;
	}
	public void setStartAt(Integer startAt) {
		this.startAt = startAt;
	}
	public Integer getEndAt() {
		return endAt;
	}
	public void setEndAt(Integer endAt) {
		this.endAt = endAt;
	}
	public Integer getFieldPosition() {
		return fieldPosition;
	}
	public void setFieldPosition(Integer fieldPosition) {
		this.fieldPosition = fieldPosition;
	}

	public Integer getRequest() {
		return request;
	}

	public void setRequest(Integer request) {
		this.request = request;
	}

	public Integer getResponse() {
		return response;
	}

	public void setResponse(Integer response) {
		this.response = response;
	}

	public String getPadding() {
		return padding;
	}

	public void setPadding(String padding) {
		this.padding = padding;
	}

	public String getTrxType() {
		return trxType;
	}

	public void setTrxType(String trxType) {
		this.trxType = trxType;
	}
	
	
}
