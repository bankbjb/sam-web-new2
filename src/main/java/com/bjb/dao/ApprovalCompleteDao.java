package com.bjb.dao;

import java.math.BigDecimal;

import com.bjb.model.ApprovalComplete;
import com.bjb.model.ApprovalPending;

public interface ApprovalCompleteDao extends GenericDao<ApprovalComplete, BigDecimal> {
	
	public void insert(ApprovalPending approvalPending, String statusCode, String statusMessage);
	
}
