package com.bjb.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.bjb.model.MessageLog;

public interface MessageLogDao extends GenericDao<MessageLog, BigDecimal> {
	public List<MessageLog> selectByCriteria(int startingAt, int maxResult, Date startDate, Date endDate, MessageLog messageLog);
	public MessageLog findByName(String name);
	public int countByCriteria(Date startDate, Date endDate, MessageLog messageLog);
	public List<MessageLog> selectValidMessageLog();

}
