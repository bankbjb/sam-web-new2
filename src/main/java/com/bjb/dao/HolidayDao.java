package com.bjb.dao;

import java.math.BigDecimal;
import java.util.List;

import com.bjb.model.Holiday;

public interface HolidayDao extends GenericDao<Holiday, BigDecimal> {
	public List<Holiday> getHolidayByCategory(int maxPerPage, int startingAt, String code, String name, String sDate, List<String> status);
	public Integer countHolidayByCategory(String code, String name, String sDate, List<String> status);
	public Holiday getDetailedHolidayByName(String name);
}
