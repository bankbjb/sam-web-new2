package com.bjb.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.bjb.constant.Constants;
import com.bjb.model.User;

@Repository
public class UserDaoImpl extends GenericDaoImpl<User, BigDecimal> implements UserDao{
	
	@Override
	public User findByUserName(String userName) {
		return (User) getCurrentSession().createCriteria(User.class).
				add(Restrictions.eq("userName", userName)).
				uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> selectByCriteria(int startingAt, int maxResult,
			String userName, String fullName, String branchId, List<Integer> status, List<String> approvalStatus) {
		Criteria criteria = getCurrentSession().createCriteria(User.class).
			add(Restrictions.in("status", status)).
			add(Restrictions.in("approvalStatus", approvalStatus)).
			setFirstResult(startingAt).
			setMaxResults(maxResult);
		if (userName != null && !"".equals(userName.trim())) {
			criteria.add(Restrictions.ilike("userName", "%" + userName + "%"));
		}
		if (fullName != null && !"".equals(fullName.trim())) {
			criteria.add(Restrictions.ilike("fullName", "%" + fullName + "%"));
		}
		if (branchId != null && !"".equals(branchId.trim())) {
			criteria
				.createAlias("branch", "branch")
				.add(Restrictions.eq("branch.id", branchId));
		}
		return criteria.list();
	}

	@Override
	public int countByCriteria(String userName, String fullName, String branchId, 
			List<Integer> status, List<String> approvalStatus) {
		Criteria criteria = getCurrentSession().createCriteria(User.class).
			add(Restrictions.in("status", status)).
			add(Restrictions.in("approvalStatus", approvalStatus)).
			setProjection(Projections.rowCount());
		if (userName != null && !"".equals(userName.trim())) {
			criteria.add(Restrictions.ilike("userName", "%" + userName + "%"));
		}
		if (fullName != null && !"".equals(fullName.trim())) {
			criteria.add(Restrictions.ilike("fullName", "%" + fullName + "%"));
		}
		if (branchId != null && !"".equals(branchId.trim())) {
			criteria
			.createAlias("branch", "branch")
			.add(Restrictions.eq("branch.id", branchId));
		}
		return ((Long)criteria.uniqueResult()).intValue();
	}
	
	@Override
	public User findValidUserByUsername(String userName) {
		List<String> listStatus = new ArrayList<String>();
						
		listStatus.add(Constants.APPROVAL_STATUS_PENDING_CREATE);
		listStatus.add(Constants.APPROVAL_STATUS_REJECTED_CREATE);
		
		return (User) getCurrentSession().createCriteria(User.class).
				add(Restrictions.ilike("userName", userName)).
				add(Restrictions.not(Restrictions.in("approvalStatus", listStatus))).
				uniqueResult();
	}


	public int countUserWithRole(Integer roleId) {
		return ((Long)getCurrentSession().createCriteria(User.class).
				createAlias("roles", "role").
				add(Restrictions.eq("role.id", roleId)).
				setProjection(Projections.rowCount()).
				uniqueResult()).intValue();
	}

}
