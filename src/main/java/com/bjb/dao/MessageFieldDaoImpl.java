package com.bjb.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.bjb.constant.Constants;
import com.bjb.model.MessageField;

@Repository
public class MessageFieldDaoImpl extends GenericDaoImpl<MessageField, BigDecimal>
		implements MessageFieldDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<MessageField> selectByCriteria(int startingAt, int maxResult, String code, String name,
			String description, List<Integer> status, List<String> approvalStatus) {
		return getCurrentSession().createCriteria(MessageField.class)
				.add(Restrictions.like("code", '%' + code + '%'))
				.add(Restrictions.like("name", '%' + name + '%'))
				.add(Restrictions.like("description", '%' + description + '%'))
				.add(Restrictions.in("status", status))
				.add(Restrictions.in("approvalStatus", approvalStatus))
				.setFirstResult(startingAt)
				.setMaxResults(maxResult)
				.list();
	}

	@Override
	public MessageField findByName(String name) {
		List<String> listStatus = new ArrayList<String>();
		listStatus.add(Constants.APPROVAL_STATUS_PENDING_CREATE);
		listStatus.add(Constants.APPROVAL_STATUS_REJECTED_CREATE);
		return (MessageField) getCurrentSession()
				.createCriteria(MessageField.class)
				.add(Restrictions.eq("name", name))
				.add(Restrictions.not(Restrictions.in("approvalStatus", listStatus)))
				.uniqueResult();
	}
	
	@Override
	public MessageField findByIdAndMessageType(Integer id, String messageType) {
		return (MessageField) getCurrentSession()
				.createCriteria(MessageField.class)
				.add(Restrictions.eq("id", id))
				.add(Restrictions.eq("messageType", messageType))
				.uniqueResult();
	}

	@Override
	public int countByCriteria(String code, String name, String description,
			List<Integer> status, List<String> approvalStatus) {
		return ((Long) getCurrentSession().createCriteria(MessageField.class)
				.add(Restrictions.like("code", '%' + code + '%'))
				.add(Restrictions.like("name", '%' + name + '%'))
				.add(Restrictions.like("description", '%' + description + '%'))
				.add(Restrictions.in("status", status))
				.add(Restrictions.in("approvalStatus", approvalStatus))
				.setProjection(Projections.rowCount())
				.uniqueResult())
				.intValue();
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<MessageField> selectValidMessageField() {

		List<String> listStatus = new ArrayList<String>();
						
		listStatus.add(Constants.APPROVAL_STATUS_PENDING_CREATE);
		listStatus.add(Constants.APPROVAL_STATUS_REJECTED_CREATE);
		
		return getCurrentSession().createCriteria(MessageField.class).
				add(Restrictions.not(Restrictions.in("approvalStatus", listStatus))).
				list();
	}
	
	@SuppressWarnings("unchecked")
	public List<MessageField> selectByMessageType(String messageType){
		return getCurrentSession().createCriteria(MessageField.class).
				add(Restrictions.eq("messageType", messageType)).
				list();
	}

	@SuppressWarnings("unchecked")
	public List<MessageField> selectByMessageTypeOrderByIdAsc(String messageType){
		return getCurrentSession().createCriteria(MessageField.class).
				add(Restrictions.eq("messageType", messageType)).
				addOrder(Order.asc("id")).
				list();
	}
}
