package com.bjb.dao;

import java.math.BigDecimal;

import com.bjb.model.Menu;

public interface MenuDao extends GenericDao<Menu, Integer> {
	public int getMenuAuthority(BigDecimal userId, String menuPath);
}
