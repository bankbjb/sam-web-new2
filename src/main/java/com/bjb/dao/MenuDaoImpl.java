package com.bjb.dao;

import java.math.BigDecimal;
import java.util.List;

import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.bjb.model.Menu;
import com.bjb.model.Role;
import com.bjb.model.User;

@Repository
public class MenuDaoImpl extends GenericDaoImpl<Menu, Integer>  implements MenuDao {
	@SuppressWarnings("unchecked")
	@Override
	public int getMenuAuthority(BigDecimal userId, String menuPath) {
		// retrieve all possible role who has menu with given menuPath
		List <Integer> roleIds = getCurrentSession().createCriteria(Role.class).
			createAlias("roleMenus", "roleMenu").
			createAlias("roleMenu.menu", "menu").
			add(Restrictions.eq("menu.path", menuPath)).
			setProjection(Projections.id()).
			list();
		return ((Long) getCurrentSession().createCriteria(User.class).
			createAlias("roles", "role").
			add(Restrictions.eq("id", userId)).
			add(Restrictions.in("role.id", roleIds)).
			setProjection(Projections.rowCount()).
			uniqueResult()).intValue();
	}
}
