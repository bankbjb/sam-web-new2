package com.bjb.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.bjb.constant.Constants;
import com.bjb.model.MessageMapping;

@Repository
public class MessageMappingDaoImpl extends GenericDaoImpl<MessageMapping, BigDecimal>
		implements MessageMappingDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<MessageMapping> selectByCriteria(int startingAt, int maxResult, String code, String feature, String messageType,List<Integer> status) {
		return getCurrentSession().createCriteria(MessageMapping.class)
				.createAlias("feature", "feature")
				.add(Restrictions.ilike("code", "%"+code+"%",MatchMode.ANYWHERE))
				.add(Restrictions.or(Restrictions.ilike("feature.code", "%"+feature+"%", MatchMode.ANYWHERE),Restrictions.ilike("feature.name", "%"+feature+"%", MatchMode.ANYWHERE)))
				.add(Restrictions.ilike("messageType", "%"+messageType+"%",MatchMode.ANYWHERE))
				.add(Restrictions.in("status", status))
				.addOrder(Order.asc("code"))
				.setFirstResult(startingAt)
				.setMaxResults(maxResult).list();
	}

	@Override
	public MessageMapping findByName(String name) {
		List<String> listStatus = new ArrayList<String>();
		listStatus.add(Constants.APPROVAL_STATUS_PENDING_CREATE);
		listStatus.add(Constants.APPROVAL_STATUS_REJECTED_CREATE);
		return (MessageMapping) getCurrentSession()
				.createCriteria(MessageMapping.class)
				.add(Restrictions.eq("name", name))
				.add(Restrictions.not(Restrictions.in("approvalStatus", listStatus)))
				.uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<MessageMapping> selectValidMessageMapping() {

		List<Integer> listStatus = new ArrayList<Integer>();
						
		listStatus.add(Constants.STATUS_ACTIVE);
		listStatus.add(Constants.STATUS_INACTIVE);
		
		return getCurrentSession().createCriteria(MessageMapping.class).
				add(Restrictions.in("status", listStatus)).
				list();
	}

	@Override
	public int countByCriteria(String code, String feature, String messageType, List<Integer> status) {
		return ((Long) getCurrentSession().createCriteria(MessageMapping.class)
				.createAlias("feature", "feature")
				.add(Restrictions.ilike("code", "%"+code+"%",MatchMode.ANYWHERE))
				.add(Restrictions.or(Restrictions.ilike("feature.code", "%"+feature+"%", MatchMode.ANYWHERE),Restrictions.ilike("feature.name", "%"+feature+"%", MatchMode.ANYWHERE)))
				.add(Restrictions.ilike("messageType", "%"+messageType+"%",MatchMode.ANYWHERE))
				.add(Restrictions.in("status", status))
				.setProjection(Projections.rowCount())
				.uniqueResult())
				.intValue();
	}

}
