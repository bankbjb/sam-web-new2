package com.bjb.dao;

import java.math.BigDecimal;
import java.util.List;

import com.bjb.model.FeatureMapping;

public interface FeatureMappingDao extends GenericDao<FeatureMapping, BigDecimal> {
	public List<FeatureMapping> selectByCriteria(int startingAt, int maxResult, String code, String feature, String aggregator, String ca, List<Integer> status, List<String> approvalStatus); 
	public FeatureMapping findByName(String name);
	public List<FeatureMapping> selectValidFeatureMapping();
	public int countByCriteria(String code, String feature, String aggregator, String ca, List<Integer> status, List<String> approvalStatus) ;
	public List<FeatureMapping> findByFeatureAggregatorCA(String feature, String aggregator, String ca);
}
