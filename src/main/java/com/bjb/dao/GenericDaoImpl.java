package com.bjb.dao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.formula.functions.T;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.dialect.Dialect;
import org.hibernate.dialect.resolver.DialectResolver;
import org.hibernate.dialect.resolver.StandardDialectResolver;
import org.hibernate.jdbc.Work;
import org.springframework.beans.factory.annotation.Autowired;

import com.bjb.model.DomainObject;

public abstract class GenericDaoImpl<E extends DomainObject, K extends Serializable> implements GenericDao<E, K> {

	private Class<T> entityClass;
	private Class<T> keyClass;
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	public GenericDaoImpl() {
		this.entityClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass())
				.getActualTypeArguments()[0];
		this.keyClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass())
				.getActualTypeArguments()[1];
	}

    public Class<T> getEntityClass() {  
        return entityClass;  
    }  
  
    public Class<T> getKeyClass() {  
        return keyClass;  
    }  

    @Autowired
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	protected Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}
	
	   // For Hibernate 3
    @SuppressWarnings("unchecked")
	public K generateId() {
    	final String sequenceName = "seq_" + getEntityClass().getSimpleName().toLowerCase();
        final List<Long> ids = new ArrayList<Long>(1);

        sessionFactory.getCurrentSession().doWork(new Work() {
            public void execute(Connection connection) throws SQLException {
                DialectResolver dialectResolver = new StandardDialectResolver();
                Dialect dialect =  dialectResolver.resolveDialect(connection.getMetaData());
                PreparedStatement preparedStatement = null;
                ResultSet resultSet = null;
                try {
                    preparedStatement = connection.prepareStatement( dialect.getSequenceNextValString(sequenceName));
                    resultSet = preparedStatement.executeQuery();
                    resultSet.next();
                    ids.add(resultSet.getLong(1));
                }catch (SQLException e) {
                    throw e;
                } finally {
                    if(preparedStatement != null) {
                        preparedStatement.close();
                    }
                    if(resultSet != null) {
                        resultSet.close();
                    }
                }
            }
        });
        
        if (getKeyClass().equals(BigDecimal.class)) {
        	return (K) new BigDecimal(ids.get(0));
        } else if (getKeyClass().equals(Integer.class)) {
        	return (K) (new Integer(ids.get(0).intValue()));
        } else if (getKeyClass().equals(Long.class)) {
        	return (K) (new Long(ids.get(0)));
        } else if (getKeyClass().equals(String.class)) {
        	return (K) (ids.get(0).toString());
        } else {
        	return null;
        }

    }


	@Override
	public void insert(E entity) {
		getCurrentSession().save(entity);
	}

	@Override
	public void insertOrUpdate(E entity) {
		getCurrentSession().saveOrUpdate(entity);
	}

	@Override
	public void update(E entity) {
		getCurrentSession().saveOrUpdate(entity);
	}

	@Override
	public void delete(E entity) {
		getCurrentSession().delete(entity);
	}

	@Override
	public E deleteByKey(K key) {
		E entity = find(key);
		if (entity != null) {
			delete(entity);
			return entity;
		} else {
			return null;
		}
	}
	
	@Override
	public E deleteByCode(String code) {
		E entity = findByCode(code);
		if (entity != null) {
			delete(entity);
			return entity;
		} else {
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public E find(K key) {
		return (E) getCurrentSession().get(entityClass, key);
	}
	
	@SuppressWarnings("unchecked")
	@Override
    public E findByCode(String code) {
		return (E) getCurrentSession().createCriteria(entityClass)
			.add(Restrictions.eq("code",  code))
			.uniqueResult();	
	}

	@Override
	public List<E> select() {
		return selectByCriteria(new Criterion[0]);
	}

    /** 
     * Use this inside subclasses as a convenience method. 
     */  
    @SuppressWarnings("unchecked")  
    public List<E> selectByCriteria(Criterion[] criterion) {  
        Criteria crit = getCurrentSession().createCriteria(getEntityClass());  
        for (Criterion c : criterion) {  
            crit.add(c);  
        }  
        return crit.list();  
    }  
    
	@Override
	public List<E> select(int startingAt, int maxResults) {
		return selectByCriteria(startingAt, maxResults, new Criterion[0]);
	}

	/** 
     * Use this inside subclasses as a convenience method. 
     */  
    @SuppressWarnings("unchecked")  
    public List<E> selectByCriteria(int startingAt, int maxResults, Criterion[] criterion) {  
        Criteria crit = getCurrentSession().createCriteria(getEntityClass());  
        for (Criterion c : criterion) {  
            crit.add(c);  
        }  
        crit.setFirstResult(startingAt);
        crit.setMaxResults(maxResults);
        return crit.list();  
    }  
    
	@SuppressWarnings("unchecked")
	@Override
	public List<E> selectByExample(Example example) {
        return getCurrentSession().createCriteria(getEntityClass())
        		.add(example)
        		.list();  
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<E> selectByExample(int startingAt, int maxResults, Example example) {
        return getCurrentSession().createCriteria(getEntityClass())
        		.setFirstResult(startingAt)
        		.setMaxResults(maxResults)
        		.add(example)
        		.list();  
	}

	@Override
	public int count() {
		return ((Long) getCurrentSession().createCriteria(getEntityClass())
			.setProjection(Projections.rowCount())
			.uniqueResult()).intValue();
	}

	@Override
	public int countByCriteria(Criterion[] criterion) {
        Criteria crit = getCurrentSession().createCriteria(getEntityClass());  
        for (Criterion c : criterion) {  
            crit.add(c);  
        }  
		return ((Long) crit
			.setProjection(Projections.rowCount())
			.uniqueResult()).intValue();
	}

	@Override
	public int countByExample(Example example) {
		return ((Long) getCurrentSession().createCriteria(getEntityClass())
			.add(example)
			.setProjection(Projections.rowCount())
			.uniqueResult()).intValue();
	}
	
}
