package com.bjb.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.bjb.constant.Constants;
import com.bjb.model.Aggregator;

@Repository
public class AggregatorDaoImpl extends GenericDaoImpl<Aggregator, BigDecimal>
		implements AggregatorDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<Aggregator> selectByCriteria(int startingAt, int maxResult, String code, String name,
			String address, String phone, String pic, String emailPic, List<Integer> status, List<String> approvalStatus) {
		Criteria criteria = getCurrentSession().createCriteria(Aggregator.class)
				.add(Restrictions.in("status",status))
				.add(Restrictions.in("approvalStatus",approvalStatus))
				.addOrder(Order.asc("code"))
				.addOrder(Order.asc("name"))
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
				.setFirstResult(startingAt)
				.setMaxResults(maxResult);
		
		if ((code != null) && (!code.equals("")))
			criteria.add(Restrictions.ilike("code", code, MatchMode.ANYWHERE));

		if ((name != null) && (!name.equals("")))
			criteria.add(Restrictions.ilike("name", name, MatchMode.ANYWHERE));

		if ((address != null) && (!address.equals("")))
			criteria.add(Restrictions.ilike("address", code, MatchMode.ANYWHERE));
		
		if ((phone != null) && (!phone.equals("")))
			criteria.add(Restrictions.ilike("phone", phone, MatchMode.ANYWHERE));

		if ((pic != null) && (!pic.equals("")))
			criteria.add(Restrictions.ilike("pic", pic, MatchMode.ANYWHERE));

		if ((emailPic != null) && (!emailPic.equals("")))
			criteria.add(Restrictions.ilike("emailPic", emailPic, MatchMode.ANYWHERE));
		
		return criteria.list();
	}

	@Override
	public Aggregator findByName(String name) {
		List<String> listStatus = new ArrayList<String>();
		listStatus.add(Constants.APPROVAL_STATUS_PENDING_CREATE);
		listStatus.add(Constants.APPROVAL_STATUS_REJECTED_CREATE);
		return (Aggregator) getCurrentSession()
				.createCriteria(Aggregator.class)
				.add(Restrictions.eq("name", name))
				.add(Restrictions.not(Restrictions.in("approvalStatus", listStatus)))
				.uniqueResult();
	}

	@Override
	public int countByCriteria(String code, String name, String address, String phone, String pic, String emailPic,
			List<Integer> status, List<String> approvalStatus) {
		Criteria criteria = getCurrentSession().createCriteria(Aggregator.class)
				.add(Restrictions.in("status", status))
				.add(Restrictions.in("approvalStatus", approvalStatus))
				.setProjection(Projections.rowCount());

		if ((code != null) && (!code.equals("")))
			criteria.add(Restrictions.ilike("code", code, MatchMode.ANYWHERE));

		if ((name != null) && (!name.equals("")))
			criteria.add(Restrictions.ilike("name", name, MatchMode.ANYWHERE));

		if ((address != null) && (!address.equals("")))
			criteria.add(Restrictions.ilike("address", code, MatchMode.ANYWHERE));
		
		if ((phone != null) && (!phone.equals("")))
			criteria.add(Restrictions.ilike("phone", phone, MatchMode.ANYWHERE));

		if ((pic != null) && (!pic.equals("")))
			criteria.add(Restrictions.ilike("pic", pic, MatchMode.ANYWHERE));

		if ((emailPic != null) && (!emailPic.equals("")))
			criteria.add(Restrictions.ilike("emailPic", emailPic, MatchMode.ANYWHERE));
		
		return ((Long) criteria.uniqueResult()).intValue(); 
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public List<Aggregator> selectValidAggregator() {

		List<String> listStatus = new ArrayList<String>();
						
		listStatus.add(Constants.APPROVAL_STATUS_PENDING_CREATE);
		listStatus.add(Constants.APPROVAL_STATUS_REJECTED_CREATE);
		
		return getCurrentSession().createCriteria(Aggregator.class).
				add(Restrictions.not(Restrictions.in("approvalStatus", listStatus))).
				setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).
				list();
	}

}
