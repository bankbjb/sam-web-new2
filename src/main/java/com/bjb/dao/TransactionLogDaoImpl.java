package com.bjb.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.time.DateUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.bjb.constant.Constants;
import com.bjb.model.TransactionLog;
import com.bjb.web.transformer.ChartDataTransformer;

@Repository
public class TransactionLogDaoImpl extends GenericDaoImpl<TransactionLog, BigDecimal> implements TransactionLogDao {


	@SuppressWarnings("unchecked")
	@Override
	public List<TransactionLog> selectByCriteria(int startingAt, int maxResult, String aggregatorCode, Date startDate, Date endDate, TransactionLog transactionLog) {
		Criteria criteria = getCurrentSession().createCriteria(TransactionLog.class)
				.add(Restrictions.between("createDate"	, DateUtils.truncate(startDate, Calendar.DATE), DateUtils.truncate(endDate, Calendar.DATE)))
				.setFirstResult(startingAt)
				.setMaxResults(maxResult);

		if ((transactionLog.getAggregatorCode() != null) && (!transactionLog.getAggregatorCode().equals("")))
			criteria.add(Restrictions.ilike("aggregatorCode", transactionLog.getAggregatorCode(), MatchMode.ANYWHERE));

		if ((transactionLog.getTerminalId() != null) && (!transactionLog.getTerminalId().equals("")))
			criteria.add(Restrictions.ilike("terminalId", transactionLog.getTerminalId(), MatchMode.ANYWHERE));

		if ((transactionLog.getName() != null) && (!transactionLog.getName().equals("")))
			criteria.add(Restrictions.ilike("name", transactionLog.getName(), MatchMode.ANYWHERE));

		if ((transactionLog.getFeatureCode() != null) && (!transactionLog.getFeatureCode().equals("")))
			criteria.add(Restrictions.ilike("featureCode", transactionLog.getFeatureCode(), MatchMode.ANYWHERE));

		if ((transactionLog.getCaCode() != null) && (!transactionLog.getCaCode().equals("")))
			criteria.add(Restrictions.ilike("caCode", transactionLog.getCaCode(), MatchMode.ANYWHERE));

		if ((transactionLog.getBillingId() != null) && (!transactionLog.getBillingId().equals("")))
			criteria.add(Restrictions.ilike("billingId", transactionLog.getBillingId(), MatchMode.ANYWHERE));

		if ((transactionLog.getCommisionAgent() != null)
				&& (!transactionLog.getCommisionAgent().equals(BigDecimal.valueOf(0))))
			criteria.add(Restrictions.eq("commisionAgent", transactionLog.getCommisionAgent()));

		if ((transactionLog.getCommisionBjb() != null)
				&& (!transactionLog.getCommisionBjb().equals(BigDecimal.valueOf(0))))
			criteria.add(Restrictions.eq("commisionBjb", transactionLog.getCommisionBjb()));

		if ((transactionLog.getAmount() != null) && (!transactionLog.getAmount().equals(BigDecimal.valueOf(0))))
			criteria.add(Restrictions.eq("amount", transactionLog.getAmount()));

		return criteria.list();
	}
	

	@Override
	public TransactionLog findByName(String name) {
		List<String> listStatus = new ArrayList<String>();
		listStatus.add(Constants.APPROVAL_STATUS_PENDING_CREATE);
		listStatus.add(Constants.APPROVAL_STATUS_REJECTED_CREATE);
		return (TransactionLog) getCurrentSession()
				.createCriteria(TransactionLog.class)
				.add(Restrictions.eq("name", name))
				.add(Restrictions.not(Restrictions.in("approvalStatus", listStatus)))
				.uniqueResult();
	}

	@Override
	public int countByCriteria(String aggregatorCode, Date startDate, Date endDate, TransactionLog transactionLog) {
		Criteria criteria = getCurrentSession().createCriteria(TransactionLog.class)
				.add(Restrictions.between("createDate"	, DateUtils.truncate(startDate, Calendar.DATE), DateUtils.addDays(DateUtils.truncate(endDate, Calendar.DATE), 1)))
				.setProjection(Projections.rowCount());
		
		if ((transactionLog.getAggregatorCode() != null) && (!transactionLog.getAggregatorCode().equals("")))
			criteria.add(Restrictions.ilike("aggregatorCode", transactionLog.getAggregatorCode(), MatchMode.ANYWHERE));

		if ((transactionLog.getTerminalId() != null) && (!transactionLog.getTerminalId().equals("")))
			criteria.add(Restrictions.ilike("terminalId", transactionLog.getTerminalId(), MatchMode.ANYWHERE));

		if ((transactionLog.getName() != null) && (!transactionLog.getName().equals("")))
			criteria.add(Restrictions.ilike("name", transactionLog.getName(), MatchMode.ANYWHERE));

		if ((transactionLog.getFeatureCode() != null) && (!transactionLog.getFeatureCode().equals("")))
			criteria.add(Restrictions.ilike("featureCode", transactionLog.getFeatureCode(), MatchMode.ANYWHERE));

		if ((transactionLog.getCaCode() != null) && (!transactionLog.getCaCode().equals("")))
			criteria.add(Restrictions.ilike("caCode", transactionLog.getCaCode(), MatchMode.ANYWHERE));

		if ((transactionLog.getBillingId() != null) && (!transactionLog.getBillingId().equals("")))
			criteria.add(Restrictions.ilike("billingId", transactionLog.getBillingId(), MatchMode.ANYWHERE));

		if ((transactionLog.getCommisionAgent() != null)
				&& (!transactionLog.getCommisionAgent().equals(BigDecimal.valueOf(0))))
			criteria.add(Restrictions.eq("commisionAgent", transactionLog.getCommisionAgent()));

		if ((transactionLog.getCommisionBjb() != null)
				&& (!transactionLog.getCommisionBjb().equals(BigDecimal.valueOf(0))))
			criteria.add(Restrictions.eq("commisionBjb", transactionLog.getCommisionBjb()));

		if ((transactionLog.getAmount() != null) && (!transactionLog.getAmount().equals(BigDecimal.valueOf(0))))
			criteria.add(Restrictions.eq("amount", transactionLog.getAmount()));
		
		return ((Long)criteria.uniqueResult()).intValue();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<TransactionLog> selectValidTransactionLog() {

		List<String> listStatus = new ArrayList<String>();
						
		listStatus.add(Constants.APPROVAL_STATUS_PENDING_CREATE);
		listStatus.add(Constants.APPROVAL_STATUS_REJECTED_CREATE);
		
		return getCurrentSession().createCriteria(TransactionLog.class).
				add(Restrictions.not(Restrictions.in("approvalStatus", listStatus))).
				list();
	}

	
	@SuppressWarnings("unchecked")
	public List<Map<Object, Number>> selectTopFeeBasedIncome(Integer maxResults) {
		Calendar calendar = GregorianCalendar.getInstance();
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		Date fromDate = calendar.getTime();
		calendar.add(Calendar.MONTH, 1);
		Date toDate = calendar.getTime();
		return (List<Map<Object, Number>>) getCurrentSession().createCriteria(getEntityClass()).
				setCacheable(true).
				createAlias("featureMapping","featureMapping").
				createAlias("featureMapping.aggregator","aggregator").
				setProjection(Projections.projectionList().
						add(Projections.groupProperty("aggregator.name").as("aggregator")).
						add(Projections.sum("commisionBjb").as("commision"))).
				add(Restrictions.ge("createDate", fromDate)).
				add(Restrictions.lt("createDate", toDate)).
				addOrder( Order.asc("commision") ).
				setResultTransformer(new ChartDataTransformer()).
				list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<Object, Number>> selectTopBestSeller(Integer maxResults) {
		Calendar calendar = GregorianCalendar.getInstance();
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		Date fromDate = calendar.getTime();
		calendar.add(Calendar.MONTH, 1);
		Date toDate = calendar.getTime();
		return (List<Map<Object, Number>>) getCurrentSession().createCriteria(getEntityClass()).
				setCacheable(true).
				createAlias("featureMapping","featureMapping").
				createAlias("featureMapping.aggregator","aggregator").
				setProjection(Projections.projectionList().
						add(Projections.groupProperty("aggregator.name").as("aggregator")).
						add(Projections.count("id").as("times"))).
				add(Restrictions.ge("createDate", fromDate)).
				add(Restrictions.lt("createDate", toDate)).
				addOrder( Order.asc("times") ).
				setResultTransformer(new ChartDataTransformer()).
				list();
	}

}
