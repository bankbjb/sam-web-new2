package com.bjb.dao;

import java.util.List;

import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.bjb.model.SystemParameter;

@Repository
public class SystemParameterDaoImpl extends GenericDaoImpl<SystemParameter, String>  implements SystemParameterDao {
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SystemParameter> selectByCriteria(int startingAt, int maxResult, String name, String value, List<Integer> status, List<String> approvalStatus) {
		return getCurrentSession().createCriteria(SystemParameter.class).
			add(Restrictions.ilike("code",'%'+name+'%', MatchMode.ANYWHERE)).
			add(Restrictions.ilike("value",'%'+value+'%', MatchMode.ANYWHERE)).
			add(Restrictions.in("status",status)).
			add(Restrictions.in("approvalStatus",approvalStatus)).
			addOrder(Order.asc("code")).
			setFirstResult(startingAt).
			setMaxResults(maxResult).
			list();
	}
	
	@Override
	public int countByCriteria(String name, String value, List<Integer> status, List<String> approvalStatus) {
		return ((Long) getCurrentSession().createCriteria(SystemParameter.class).
				add(Restrictions.ilike("code",'%'+name+'%', MatchMode.ANYWHERE)).
				add(Restrictions.ilike("value",'%'+value+'%', MatchMode.ANYWHERE)).
				add(Restrictions.in("status",status)).
				add(Restrictions.in("approvalStatus",approvalStatus)).
				setProjection(Projections.rowCount()).
				uniqueResult()).intValue();
	}

	@Override
	public SystemParameter getDetailedParameterByParamName(String paramName) {
		SystemParameter systemParameter = new SystemParameter();
		try{
			//List<String> listStatus = new ArrayList<String>();
			//TODO add more validation ?
//			listStatus.add(Constants.APPROVAL_STATUS_PENDING_CREATE);
//			listStatus.add(Constants.APPROVAL_STATUS_REJECTED_CREATE);
			systemParameter = (SystemParameter) getCurrentSession().createCriteria(SystemParameter.class).
					 add(Restrictions.like("code", "%"+paramName+"%"))
					.uniqueResult();
		}catch(Exception e){
			e.printStackTrace();
		}
		return systemParameter;
	}

}
