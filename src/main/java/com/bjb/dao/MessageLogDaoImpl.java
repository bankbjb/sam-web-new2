package com.bjb.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.bjb.constant.Constants;
import com.bjb.model.MessageLog;

@Repository
public class MessageLogDaoImpl extends GenericDaoImpl<MessageLog, BigDecimal> implements MessageLogDao {


	@SuppressWarnings("unchecked")
	@Override
	public List<MessageLog> selectByCriteria(int startingAt, int maxResult, Date startDate, Date endDate, MessageLog messageLog) {
		Criteria criteria = getCurrentSession().createCriteria(MessageLog.class)
				.add(Restrictions.between("createDate"	, DateUtils.truncate(startDate, Calendar.DATE), DateUtils.truncate(endDate, Calendar.DATE)))
				.setFirstResult(startingAt)
				.setMaxResults(maxResult);
		
		if ((messageLog.getAggregatorCode() != null) && (!messageLog.getAggregatorCode().equals("")))
			criteria.add(Restrictions.ilike("aggregatorCode", messageLog.getAggregatorCode(), MatchMode.ANYWHERE));
		
		if ((messageLog.getCaCode() != null)  && (!messageLog.getCaCode().equals("")))
			criteria.add(Restrictions.ilike("caCode", messageLog.getCaCode(), MatchMode.ANYWHERE));
		
		if ((messageLog.getFeatureCode() != null) && (!messageLog.getFeatureCode().equals("")))
			criteria.add(Restrictions.ilike("featureCode", messageLog.getFeatureCode(), MatchMode.ANYWHERE));
		
		if ((messageLog.getResponseCode() != null) && (!messageLog.getResponseCode().equals("")))
			criteria.add(Restrictions.ilike("responseCode", messageLog.getResponseCode(), MatchMode.ANYWHERE));
		
		if ((messageLog.getMti() != null) && (!messageLog.getMti().equals("")))
			criteria.add(Restrictions.ilike("mti", messageLog.getMti(), MatchMode.ANYWHERE));
		
		if ((messageLog.getBillingId() != null) && (!messageLog.getBillingId().equals("")))
			criteria.add(Restrictions.ilike("billingId", messageLog.getBillingId(), MatchMode.ANYWHERE));
		
		if ((messageLog.getStan() != null) && (!messageLog.getStan().equals("")))
			criteria.add(Restrictions.ilike("stan", messageLog.getStan(), MatchMode.ANYWHERE));
		
		if ((messageLog.getRawRequest() != null) && (!messageLog.getRawRequest().equals("")))
			criteria.add(Restrictions.ilike("rawRequest", messageLog.getRawRequest(), MatchMode.ANYWHERE));
		
		if ((messageLog.getRawResponse() != null) && (!messageLog.getRawResponse().equals("")))
			criteria.add(Restrictions.ilike("rawResponse", messageLog.getRawResponse(), MatchMode.ANYWHERE));
		
		return criteria.list();
	}
	

	@Override
	public MessageLog findByName(String name) {
		List<String> listStatus = new ArrayList<String>();
		listStatus.add(Constants.APPROVAL_STATUS_PENDING_CREATE);
		listStatus.add(Constants.APPROVAL_STATUS_REJECTED_CREATE);
		return (MessageLog) getCurrentSession()
				.createCriteria(MessageLog.class)
				.add(Restrictions.eq("name", name))
				.add(Restrictions.not(Restrictions.in("approvalStatus", listStatus)))
				.uniqueResult();
	}

	@Override
	public int countByCriteria(Date startDate, Date endDate, MessageLog messageLog) {
		Criteria criteria = getCurrentSession().createCriteria(MessageLog.class)
				.add(Restrictions.between("createDate"	, DateUtils.truncate(startDate, Calendar.DATE), DateUtils.truncate(endDate, Calendar.DATE) ))
				.setProjection(Projections.rowCount());
		
		if ((messageLog.getAggregatorCode() != null) && (!messageLog.getAggregatorCode().equals("")))
			criteria.add(Restrictions.ilike("aggregatorCode", messageLog.getAggregatorCode(), MatchMode.ANYWHERE));
		
		if ((messageLog.getCaCode() != null)  && (!messageLog.getCaCode().equals("")))
			criteria.add(Restrictions.ilike("caCode", messageLog.getCaCode(), MatchMode.ANYWHERE));
		
		if ((messageLog.getFeatureCode() != null) && (!messageLog.getFeatureCode().equals("")))
			criteria.add(Restrictions.ilike("featureCode", messageLog.getFeatureCode(), MatchMode.ANYWHERE));
		
		if ((messageLog.getResponseCode() != null) && (!messageLog.getResponseCode().equals("")))
			criteria.add(Restrictions.ilike("responseCode", messageLog.getResponseCode(), MatchMode.ANYWHERE));
		
		if ((messageLog.getMti() != null) && (!messageLog.getMti().equals("")))
			criteria.add(Restrictions.ilike("mti", messageLog.getMti(), MatchMode.ANYWHERE));
		
		if ((messageLog.getBillingId() != null) && (!messageLog.getBillingId().equals("")))
			criteria.add(Restrictions.ilike("billingId", messageLog.getBillingId(), MatchMode.ANYWHERE));
		
		if ((messageLog.getStan() != null) && (!messageLog.getStan().equals("")))
			criteria.add(Restrictions.ilike("stan", messageLog.getStan(), MatchMode.ANYWHERE));
		
		if ((messageLog.getRawRequest() != null) && (!messageLog.getRawRequest().equals("")))
			criteria.add(Restrictions.ilike("rawRequest", messageLog.getRawRequest(), MatchMode.ANYWHERE));
		
		if ((messageLog.getRawResponse() != null) && (!messageLog.getRawResponse().equals("")))
			criteria.add(Restrictions.ilike("rawResponse", messageLog.getRawResponse(), MatchMode.ANYWHERE));
		
		return ((Long) criteria.uniqueResult()).intValue();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<MessageLog> selectValidMessageLog() {

		List<String> listStatus = new ArrayList<String>();
						
		listStatus.add(Constants.APPROVAL_STATUS_PENDING_CREATE);
		listStatus.add(Constants.APPROVAL_STATUS_REJECTED_CREATE);
		
		return getCurrentSession().createCriteria(MessageLog.class).
				add(Restrictions.not(Restrictions.in("approvalStatus", listStatus))).
				list();
	}

}