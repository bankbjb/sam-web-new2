package com.bjb.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.bjb.constant.Constants;
import com.bjb.model.CommisionTiering;

@Repository
public class CommisionTieringDaoImpl extends GenericDaoImpl<CommisionTiering, BigDecimal>
		implements CommisionTieringDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<CommisionTiering> selectByCriteria(int startingAt, int maxResult, String code, String featureMapping, List<Integer> status, List<String> approvalStatus) {
		Criteria criteria =  getCurrentSession().createCriteria(CommisionTiering.class)
				.createAlias("featureMapping", "featureMapping")
				.add(Restrictions.in("status",status))
				.add(Restrictions.in("approvalStatus",approvalStatus))
				.addOrder(Order.asc("code"))
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
				.setFirstResult(startingAt)
				.setMaxResults(maxResult);
		
		if ((code != null) && (!code.equals("")))
			criteria.add(Restrictions.ilike("code", code, MatchMode.ANYWHERE));
		
		if ((featureMapping != null) && (!featureMapping.equals("")))
			criteria.add(Restrictions.ilike("featureMapping.code", featureMapping, MatchMode.ANYWHERE));

		return criteria.list();
	}

	@Override
	public CommisionTiering findByName(String name) {
		List<String> listStatus = new ArrayList<String>();
		listStatus.add(Constants.APPROVAL_STATUS_PENDING_CREATE);
		listStatus.add(Constants.APPROVAL_STATUS_REJECTED_CREATE);
		return (CommisionTiering) getCurrentSession()
				.createCriteria(CommisionTiering.class)
				.add(Restrictions.eq("name", name))
				.add(Restrictions.not(Restrictions.in("approvalStatus", listStatus)))
				.uniqueResult();
	}

	@Override
	public int countByCriteria(String code, String featureMapping, List<Integer> status, List<String> approvalStatus) {
		Criteria criteria = getCurrentSession().createCriteria(CommisionTiering.class)
				.createAlias("featureMapping", "featureMapping")
				.add(Restrictions.in("status",status))
				.add(Restrictions.in("approvalStatus",approvalStatus))
				.setProjection(Projections.rowCount());
		

		if ((code != null) && (!code.equals("")))
			criteria.add(Restrictions.ilike("code", code, MatchMode.ANYWHERE));
		
		if ((featureMapping != null) && (!featureMapping.equals("")))
			criteria.add(Restrictions.ilike("featureMapping.code", featureMapping, MatchMode.ANYWHERE));
		
		return ((Long)criteria.uniqueResult()).intValue();
	}

}
