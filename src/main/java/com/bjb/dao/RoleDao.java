package com.bjb.dao;

import java.util.List;

import com.bjb.model.Role;

public interface RoleDao extends GenericDao<Role, Integer> {
	public List<Role> selectValidRole();
	public List<Role> selectValidRoleByRoleName(String roleName);
	public List<Role> getAllValidRoleByAccessLevel(Integer accessLevel);
	public List<Role> getLazyRoleByCategory(int startingAt, int maxResult, String roleCode, String roleName, List<Integer> status, List<String> approvalStatus);
	public int countAllRoleByCategory(String roleCode, String roleName, List<Integer> status, List<String> approvalStatus);
	public int countFilteredRole(String roleName);
	public Role findByRoleName(String roleName) ;
	public void deleteRoleMenu(Role role);
}
