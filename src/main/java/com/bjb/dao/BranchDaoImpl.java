package com.bjb.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.bjb.constant.Constants;
import com.bjb.model.Branch;

@Repository
public class BranchDaoImpl extends GenericDaoImpl<Branch, String> implements BranchDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<Branch> selectByCriteria(int maxPerPage,
			int startingAt, String code,
			String name, List<Integer> status,List<String> approvalStatus) {
		
		return getCurrentSession().createCriteria(Branch.class)
				.add(Restrictions.ilike("id",'%'+code+'%'))
				.add(Restrictions.ilike("name",'%'+name+'%'))
				.add(Restrictions.in("status",status))
				.add(Restrictions.in("approvalStatus",approvalStatus))
				.setFirstResult(startingAt)
				.setMaxResults(maxPerPage)
				.list();
		
	}

	@Override
	public Integer countByCriteria(String code,
			String name, List<Integer> status, List<String> approvalStatus) {

		return ((Long)getCurrentSession().createCriteria(Branch.class)
				.add(Restrictions.ilike("id",'%'+code+'%'))
				.add(Restrictions.ilike("name",'%'+name+'%'))
				.add(Restrictions.in("status",status))
				.add(Restrictions.in("approvalStatus",approvalStatus))
				.setProjection(Projections.rowCount())
				.uniqueResult())
				.intValue();

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Branch> selectValidBranch() {

		List<String> listStatus = new ArrayList<String>();
						
		listStatus.add(Constants.APPROVAL_STATUS_PENDING_CREATE);
		listStatus.add(Constants.APPROVAL_STATUS_REJECTED_CREATE);
		
		return getCurrentSession().createCriteria(Branch.class).
				add(Restrictions.not(Restrictions.in("approvalStatus", listStatus))).
				list();
	}

}


