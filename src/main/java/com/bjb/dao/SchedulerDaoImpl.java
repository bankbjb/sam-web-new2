package com.bjb.dao;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.bjb.constant.Constants;
import com.bjb.model.ScheduleTask;

@Repository
public class SchedulerDaoImpl extends GenericDaoImpl<ScheduleTask, BigDecimal> implements SchedulerDao{

	@SuppressWarnings("unchecked")
	@Override
	public List<ScheduleTask> getAllValidTask() {
		
		List<String> listStatus = new ArrayList<String>();
						
		listStatus.add(Constants.APPROVAL_STATUS_PENDING_CREATE);
		listStatus.add(Constants.APPROVAL_STATUS_REJECTED_CREATE);

		return getCurrentSession().createCriteria(ScheduleTask.class).
				add(Restrictions.not(Restrictions.in("approvalStatus", listStatus))).list();

	}

	@Override
	public ScheduleTask getDetailedTaskByName(String name) {
		return (ScheduleTask) getCurrentSession().createCriteria(ScheduleTask.class).
				add(Restrictions.eq("jobName", name)).
				uniqueResult();		
	}

	@Override
	public void updateDateTime(String task, String newDate, String newTime){
		getCurrentSession().createQuery("update SchedulerTask set startDate = :newDate, startTime= :newTime where jobName= :jobName").
			setString("newDate", newDate).
			setString("newTime", newTime).
			executeUpdate();
	}
}
