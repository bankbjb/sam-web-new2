package com.bjb.dao;

import java.util.List;

import com.bjb.model.SystemParameter;

public interface SystemParameterDao extends GenericDao<SystemParameter, String> {
	public List<SystemParameter> selectByCriteria(int startingAt, int maxResult, String name, String value, List<Integer> status, List<String> approvalStatus);
	public int countByCriteria(String name, String value, List<Integer> status, List<String> approvalStatus);
	public SystemParameter getDetailedParameterByParamName(String name);
}
