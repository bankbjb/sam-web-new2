package com.bjb.dao;

import java.math.BigDecimal;
import java.util.List;

import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.bjb.model.Holiday;

@Repository
public class HolidayDaoImpl extends GenericDaoImpl<Holiday, BigDecimal>  implements HolidayDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<Holiday> getHolidayByCategory(int maxPerPage,
			int startingAt, String code, String name,
			String  sDate, List<String> status) {

		return getCurrentSession().createCriteria(Holiday.class).
				add(Restrictions.ilike("code",'%'+code+'%')).
				add(Restrictions.ilike("name",'%'+name+'%')).
				add(Restrictions.ilike("date",'%'+sDate+'%')).
				add(Restrictions.in("approvalStatus",status)).
				setFirstResult(startingAt).
				setMaxResults(maxPerPage).
				list();
	}

	@Override
	public Integer countHolidayByCategory(String code, String name,
			String sDate, List<String> status) {
		return ((Long)getCurrentSession().createCriteria(Holiday.class).
				add(Restrictions.ilike("code",'%'+code+'%')).
				add(Restrictions.ilike("name",'%'+name+'%')).
				add(Restrictions.ilike("dateString",'%'+sDate+'%')).
				add(Restrictions.in("approvalStatus",status)).
				setProjection(Projections.rowCount()).
				uniqueResult()).intValue();
	}

	@Override
	public Holiday getDetailedHolidayByName(String holidayName) {
		return (Holiday) getCurrentSession().createCriteria(Holiday.class).
				add(Restrictions.eq("name", holidayName)).
				uniqueResult();
	}

}
