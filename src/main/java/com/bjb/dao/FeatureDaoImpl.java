package com.bjb.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.bjb.constant.Constants;
import com.bjb.model.Feature;

@Repository
public class FeatureDaoImpl extends GenericDaoImpl<Feature, BigDecimal>
		implements FeatureDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<Feature> selectByCriteria(int startingAt, int maxResult, String code, String name,
			String description, String commisionAccountNo, List<Integer> status, List<String> approvalStatus) {
		Criteria criteria = getCurrentSession().createCriteria(Feature.class)
				.add(Restrictions.in("status", status))
				.add(Restrictions.in("approvalStatus", approvalStatus))
				.addOrder(Order.asc("code"))
				.addOrder(Order.asc("name"))
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
				.setFirstResult(startingAt)
				.setMaxResults(maxResult);
		
		if ((code != null) && (!code.equals("")))
			criteria.add(Restrictions.ilike("code", code, MatchMode.ANYWHERE));

		if ((name != null) && (!name.equals("")))
			criteria.add(Restrictions.ilike("name", name, MatchMode.ANYWHERE));

		if ((description != null) && (!description.equals("")))
			criteria.add(Restrictions.ilike("description", description, MatchMode.ANYWHERE));

		if ((commisionAccountNo != null) && (!commisionAccountNo.equals("")))
			criteria.add(Restrictions.ilike("commisionAccountNo", commisionAccountNo, MatchMode.ANYWHERE));
		
		return criteria.list();
	}

	@Override
	public Feature findByName(String name) {
		List<String> listStatus = new ArrayList<String>();
		listStatus.add(Constants.APPROVAL_STATUS_PENDING_CREATE);
		listStatus.add(Constants.APPROVAL_STATUS_REJECTED_CREATE);
		return (Feature) getCurrentSession()
				.createCriteria(Feature.class)
				.add(Restrictions.eq("name", name))
				.add(Restrictions.not(Restrictions.in("approvalStatus", listStatus)))
				.uniqueResult();
	}

	@Override
	public int countByCriteria(String code, String name, String description, String commisionAccountNo, 
			List<Integer> status, List<String> approvalStatus) {
		
		Criteria criteria = getCurrentSession().createCriteria(Feature.class)
				.add(Restrictions.in("status", status))
				.add(Restrictions.in("approvalStatus", approvalStatus))
				.setProjection(Projections.rowCount());

		if ((code != null) && (!code.equals("")))
			criteria.add(Restrictions.ilike("code", code, MatchMode.ANYWHERE));

		if ((name != null) && (!name.equals("")))
			criteria.add(Restrictions.ilike("name", name, MatchMode.ANYWHERE));

		if ((description != null) && (!description.equals("")))
			criteria.add(Restrictions.ilike("description", description, MatchMode.ANYWHERE));

		if ((commisionAccountNo != null) && (!commisionAccountNo.equals("")))
			criteria.add(Restrictions.ilike("commisionAccountNo", commisionAccountNo, MatchMode.ANYWHERE));
		
		return ((Long)criteria.uniqueResult()).intValue();
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<Feature> selectValidFeature() {

		List<String> listStatus = new ArrayList<String>();
						
		listStatus.add(Constants.APPROVAL_STATUS_PENDING_CREATE);
		listStatus.add(Constants.APPROVAL_STATUS_REJECTED_CREATE);
		
		return getCurrentSession().createCriteria(Feature.class).
				add(Restrictions.not(Restrictions.in("approvalStatus", listStatus))).
				setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).
				list();
	}

}
