package com.bjb.dao;

import java.math.BigDecimal;
import java.util.List;

import com.bjb.model.ApprovalPending;

public interface ApprovalPendingDao extends GenericDao<ApprovalPending, BigDecimal> {

	public List<ApprovalPending> selectByCriteria(String paramater, BigDecimal approverId);
	public List<ApprovalPending> selectByCriteria(BigDecimal approverId, String parameterCode,String parameterName,List<String> searchParameter,List<String> searchAction, String branchId);
	public void updateApprovalByParameterCode(String parameterCode, byte[] newByteObject, String parameterName);
	public void bulkDelete(List<ApprovalPending> approvalPendings);
	public List<ApprovalPending> selectByCriteria(int startingAt, int maxResult, BigDecimal approverId, String parameterCode,String parameterName,List<String> searchParameter,List<String> searchAction, String branchId);
	public List<ApprovalPending> selectByParameterCode(String parameterCode);
	public int countByCriteria(BigDecimal approverId, String parameterCode, String parameterName, List<String> searchParameter,List<String> searchAction, String branchId);
	public void assignApprover(BigDecimal approverId);
	public void removeApprover(List<BigDecimal> listApproverId);
	public void removeAllApprover();
	
}
