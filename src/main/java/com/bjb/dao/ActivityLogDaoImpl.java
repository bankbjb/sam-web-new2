package com.bjb.dao;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.bjb.model.ActivityLog;

@Repository
public class ActivityLogDaoImpl extends GenericDaoImpl<ActivityLog, BigDecimal> implements ActivityLogDao {
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ActivityLog> selectByCriteria(int startingAt, int maxResult, BigDecimal userId, BigDecimal activityId, Date startDate, Date endDate, ActivityLog activityLog, List<Integer> status) {
		Criteria criteria = getCurrentSession().createCriteria(ActivityLog.class)
				.createAlias("activity", "activity")
				.add(Restrictions.between("createDate", DateUtils.truncate(startDate, Calendar.DATE), DateUtils.addDays(DateUtils.truncate(endDate, Calendar.DATE), 1)))
				.add(Restrictions.ilike("activity.description", "%" + (activityLog.getActivity().getDescription() != null ? activityLog.getActivity().getDescription() : "")  + "%"))
				.add(Restrictions.in("status", status))
				.setFirstResult(startingAt)
				.setMaxResults(maxResult);
		if (userId != null) {
			criteria.add(Restrictions.eq("createBy", userId));
		}
		if (activityId != null) {
			criteria.createAlias("activity", "activity")
				.add(Restrictions.eq("activity.id", activityId));
		}
		
		return criteria.list();
	}
	
	@Override
	public int countByCriteria(BigDecimal userId, BigDecimal activityId, Date startDate, Date endDate, ActivityLog activityLog, List<Integer> status) {
		Criteria criteria = getCurrentSession().createCriteria(ActivityLog.class)
				.createAlias("activity", "activity")
				.add(Restrictions.between("createDate", DateUtils.truncate(startDate, Calendar.DATE), DateUtils.addDays(DateUtils.truncate(endDate, Calendar.DATE), 1)))
				.add(Restrictions.ilike("activity.description", "%" + (activityLog.getActivity().getDescription() != null ? activityLog.getActivity().getDescription() : "")  + "%"))
				.add(Restrictions.in("status", status))
				.setProjection(Projections.rowCount());
		if (userId != null) {
			criteria.add(Restrictions.eq("createBy", userId));
		}
		if (activityId != null) {
			criteria.createAlias("activity", "activity")
			.add(Restrictions.eq("activity.id", activityId));
		}
		return ((Long) criteria.uniqueResult()).intValue();
	}
	
}
