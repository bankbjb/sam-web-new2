package com.bjb.dao;

import java.math.BigDecimal;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.bjb.model.FieldMapping;

@Repository
public class FieldMappingDaoImpl extends
		GenericDaoImpl<FieldMapping, BigDecimal> implements FieldMappingDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<FieldMapping> findByMessageMappingId(BigDecimal messageMappingId) {
		return getCurrentSession().createCriteria(FieldMapping.class)
				.add(Restrictions.eq("messageMappingId", messageMappingId))
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
	}

}
