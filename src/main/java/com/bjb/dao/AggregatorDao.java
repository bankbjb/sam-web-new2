package com.bjb.dao;

import java.math.BigDecimal;
import java.util.List;

import com.bjb.model.Aggregator;

public interface AggregatorDao extends GenericDao<Aggregator, BigDecimal> {
	public List<Aggregator> selectByCriteria(int startingAt, int maxResult, String code, String name, String address, String phone, String pic, String emailPic, List<Integer> status, List<String> approvalStatus);
	public Aggregator findByName(String name);
	public int countByCriteria(String code, String name, String address, String phone, String pic, String emailPic, List<Integer> status, List<String> approvalStatus);
	public List<Aggregator> selectValidAggregator();
}
