package com.bjb.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.bjb.model.ActivityLog;

public interface ActivityLogDao extends GenericDao<ActivityLog, BigDecimal> {

	public List<ActivityLog> selectByCriteria(int startingAt, int maxResult, BigDecimal userId, BigDecimal activityId, Date startDate, Date endDate, ActivityLog activityLog, List<Integer> status);
	public int countByCriteria(BigDecimal userId, BigDecimal activityId, Date startDate, Date endDate, ActivityLog activityLog, List<Integer> status);

}
