package com.bjb.dao;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.stereotype.Repository;

import com.bjb.model.ApprovalComplete;
import com.bjb.model.ApprovalPending;

@Repository
public class ApprovalCompleteDaoImpl extends GenericDaoImpl<ApprovalComplete, BigDecimal> implements ApprovalCompleteDao {

	@Override
	public void insert(ApprovalPending approvalPending, String statusCode, String statusMessage) {
		// TODO Auto-generated method stub
		ApprovalComplete approvalComplete = new ApprovalComplete();
		approvalComplete.setParameter(approvalPending.getParameter());
		approvalComplete.setParameterCode(approvalPending.getParameterCode());
		approvalComplete.setAction(approvalPending.getAction());
		approvalComplete.setApproverId(approvalPending.getApproverId());
		approvalComplete.setBranchId(approvalPending.getBranchId());
		approvalComplete.setApproveDate(new Date());
		approvalComplete.setStatusCode(statusCode);
		approvalComplete.setStatusMessage(statusMessage);
		insert(approvalComplete);
	}

}
