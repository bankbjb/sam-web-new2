package com.bjb.dao;

import org.springframework.stereotype.Repository;

import com.bjb.model.Activity;

@Repository
public class ActivityDaoImpl extends GenericDaoImpl<Activity, Integer> implements ActivityDao {
	
}
