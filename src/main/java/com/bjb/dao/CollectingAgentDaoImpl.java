package com.bjb.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.bjb.constant.Constants;
import com.bjb.model.CollectingAgent;

@Repository
public class CollectingAgentDaoImpl extends GenericDaoImpl<CollectingAgent, BigDecimal>
		implements CollectingAgentDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<CollectingAgent> selectByCriteria(int startingAt, int maxResult, String code, String name, Date createDate, Integer maxAgent, Integer maxAmount, List<Integer> status) {
		Criteria criteria = getCurrentSession().createCriteria(CollectingAgent.class)
				.add(Restrictions.in("status", status))
				.setFirstResult(startingAt).setMaxResults(maxResult);

		if ((code != null) && (!code.equals("")))
			criteria.add(Restrictions.ilike("code", code, MatchMode.ANYWHERE));
		if ((name != null) && (!name.equals("")))
			criteria.add(Restrictions.ilike("name", name, MatchMode.ANYWHERE));
		if (createDate != null)
			criteria.add(Restrictions.between("createDate", DateUtils.truncate(createDate, Calendar.DATE),
					DateUtils.addDays(DateUtils.truncate(createDate, Calendar.DATE), 1)));
		if ((maxAgent != null) && (!maxAgent.equals(Integer.valueOf(0))))
			criteria.add(Restrictions.eq("maxAgent", maxAgent));
		if ((maxAmount != null) && (!maxAmount.equals(Integer.valueOf(0))))
			criteria.add(Restrictions.eq("maxAmount", maxAmount));

		return criteria.list();
	}

	@Override
	public CollectingAgent findByName(String name) {
		List<String> listStatus = new ArrayList<String>();
		listStatus.add(Constants.APPROVAL_STATUS_PENDING_CREATE);
		listStatus.add(Constants.APPROVAL_STATUS_REJECTED_CREATE);
		return (CollectingAgent) getCurrentSession()
				.createCriteria(CollectingAgent.class)
				.add(Restrictions.eq("name", name))
				.add(Restrictions.not(Restrictions.in("approvalStatus", listStatus)))
				.uniqueResult();
	}

	@Override
	public int countByCriteria(String code, String name, Date createDate, Integer maxAgent, Integer maxAmount, List<Integer> status) {
		Criteria criteria = getCurrentSession().createCriteria(CollectingAgent.class)
				.add(Restrictions.in("status",status))
				.setProjection(Projections.rowCount());

		if ((code != null) && (!code.equals("")))
			criteria.add(Restrictions.ilike("code", code, MatchMode.ANYWHERE));
		if ((name != null) && (!name.equals("")))
			criteria.add(Restrictions.ilike("name", name, MatchMode.ANYWHERE));
		if (createDate != null)
			criteria.add(Restrictions.between("createDate", DateUtils.truncate(createDate, Calendar.DATE),
					DateUtils.addDays(DateUtils.truncate(createDate, Calendar.DATE), 1)));
		if ((maxAgent != null) && (!maxAgent.equals(Integer.valueOf(0))))
			criteria.add(Restrictions.eq("maxAgent", maxAgent));
		if ((maxAmount != null) && (!maxAmount.equals(Integer.valueOf(0))))
			criteria.add(Restrictions.eq("maxAmount", maxAmount));

		return  ((Long)criteria.uniqueResult()).intValue();
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public List<CollectingAgent> selectValidCollectingAgent() {

		List<String> listStatus = new ArrayList<String>();
						
		listStatus.add(Constants.APPROVAL_STATUS_PENDING_CREATE);
		listStatus.add(Constants.APPROVAL_STATUS_REJECTED_CREATE);
		
		return getCurrentSession().createCriteria(CollectingAgent.class).
				add(Restrictions.not(Restrictions.in("approvalStatus", listStatus))).
				setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).
				list();
	}

}
