package com.bjb.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.bjb.constant.Constants;
import com.bjb.model.FeatureMapping;

@Repository
public class FeatureMappingDaoImpl extends GenericDaoImpl<FeatureMapping, BigDecimal>
		implements FeatureMappingDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<FeatureMapping> selectByCriteria(int startingAt, int maxResult, String code, String feature, String aggregator, String ca, List<Integer> status, List<String> approvalStatus) {
		Criteria criteria = getCurrentSession().createCriteria(FeatureMapping.class)
				.createAlias("feature", "feature")
				.createAlias("aggregator", "aggregator")
				.createAlias("collectingAgent", "collectingAgent")
				.add(Restrictions.in("status", status))
				.add(Restrictions.in("approvalStatus", approvalStatus))
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
				.setFirstResult(startingAt)
				.setMaxResults(maxResult);
	
		if ((code != null) && (!code.equals("")))
			criteria.add(Restrictions.ilike("code", code, MatchMode.ANYWHERE));

		if ((feature != null) && (!feature.equals("")))
			criteria.add(Restrictions.or(Restrictions.ilike("feature.code", feature, MatchMode.ANYWHERE),Restrictions.ilike("feature.name", feature, MatchMode.ANYWHERE)));
		
		if ((aggregator != null) && (!aggregator.equals("")))
			criteria.add(Restrictions.or(Restrictions.ilike("aggregator.code", aggregator, MatchMode.ANYWHERE),Restrictions.ilike("aggregator.name", aggregator, MatchMode.ANYWHERE)));

		if ((ca != null) && (!ca.equals("")))
			criteria.add(Restrictions.or(Restrictions.ilike("collectingAgent.code", ca, MatchMode.ANYWHERE),Restrictions.ilike("collectingAgent.name", ca, MatchMode.ANYWHERE)));
		
		return criteria.list();
	}

	@Override
	public FeatureMapping findByName(String name) {
		List<String> listStatus = new ArrayList<String>();
		listStatus.add(Constants.APPROVAL_STATUS_PENDING_CREATE);
		listStatus.add(Constants.APPROVAL_STATUS_REJECTED_CREATE);
		return (FeatureMapping) getCurrentSession()
				.createCriteria(FeatureMapping.class)
				.add(Restrictions.eq("name", name))
				.add(Restrictions.not(Restrictions.in("approvalStatus", listStatus)))
				.uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<FeatureMapping> selectValidFeatureMapping() {

		List<Integer> listStatus = new ArrayList<Integer>();
						
		listStatus.add(Constants.STATUS_ACTIVE);
		listStatus.add(Constants.STATUS_INACTIVE);
		
		return getCurrentSession().createCriteria(FeatureMapping.class).
				add(Restrictions.in("status", listStatus)).
				setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).
				list();
	}

	@Override
	public int countByCriteria(String code, String feature, String aggregator, String ca, List<Integer> status, List<String> approvalStatus) {
		Criteria criteria = getCurrentSession().createCriteria(FeatureMapping.class)
				.createAlias("feature", "feature")
				.createAlias("aggregator", "aggregator")
				.createAlias("collectingAgent", "collectingAgent")
				.add(Restrictions.in("status", status))
				.add(Restrictions.in("approvalStatus", approvalStatus))
				.setProjection(Projections.rowCount());
		
		if ((code != null) && (!code.equals("")))
			criteria.add(Restrictions.ilike("code", code, MatchMode.ANYWHERE));

		if ((feature != null) && (!feature.equals("")))
			criteria.add(Restrictions.or(Restrictions.ilike("feature.code", feature, MatchMode.ANYWHERE),Restrictions.ilike("feature.name", feature, MatchMode.ANYWHERE)));
		
		if ((aggregator != null) && (!aggregator.equals("")))
			criteria.add(Restrictions.or(Restrictions.ilike("aggregator.code", aggregator, MatchMode.ANYWHERE),Restrictions.ilike("aggregator.name", aggregator, MatchMode.ANYWHERE)));

		if ((ca != null) && (!ca.equals("")))
			criteria.add(Restrictions.or(Restrictions.ilike("collectingAgent.code", ca, MatchMode.ANYWHERE),Restrictions.ilike("collectingAgent.name", ca, MatchMode.ANYWHERE)));
	
		return ((Long) criteria.uniqueResult()).intValue();
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<FeatureMapping> findByFeatureAggregatorCA(String feature, String aggregator, String ca) {
		return getCurrentSession().createCriteria(FeatureMapping.class)
				.createAlias("feature", "feature")
				.createAlias("aggregator", "aggregator")
				.createAlias("collectingAgent", "collectingAgent")
				.add(Restrictions.like("feature.code", feature))
				.add(Restrictions.like("aggregator.code", aggregator))
				.add(Restrictions.like("collectingAgent.code", ca))
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
				.list();
	}

}
