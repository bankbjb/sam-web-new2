package com.bjb.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.bjb.constant.Constants;
import com.bjb.model.Role;

@Repository
public class RoleDaoImpl extends GenericDaoImpl<Role, Integer> implements RoleDao{

	@SuppressWarnings("unchecked")
	@Override
	public List<Role> selectValidRole() {
		List<String> listStatus = new ArrayList<String>();
						
		listStatus.add(Constants.APPROVAL_STATUS_PENDING_CREATE);
		listStatus.add(Constants.APPROVAL_STATUS_REJECTED_CREATE);
		
		return getCurrentSession().createCriteria(Role.class).
				add(Restrictions.not(Restrictions.in("approvalStatus", listStatus))).list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Role> selectValidRoleByRoleName(String roleName) {
		List<String> listStatus = new ArrayList<String>();
						
		listStatus.add(Constants.APPROVAL_STATUS_PENDING_CREATE);
		listStatus.add(Constants.APPROVAL_STATUS_REJECTED_CREATE);
		
		return getCurrentSession().createCriteria(Role.class).
				add(Restrictions.not(Restrictions.in("approvalStatus", listStatus))).
				add(Restrictions.eq("name", roleName)).list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Role> getAllValidRoleByAccessLevel(Integer accessLevel) {
		List<String> listStatus = new ArrayList<String>();
						
		listStatus.add(Constants.APPROVAL_STATUS_PENDING_CREATE);
		listStatus.add(Constants.APPROVAL_STATUS_REJECTED_CREATE);
		
		List<Integer> roleIds = getCurrentSession().getNamedQuery("Role#ValidRoleByAccessLevel").
				setParameterList("statuses", listStatus).
				setParameter("accessLevel", accessLevel).
				list();
		
		return getCurrentSession().createCriteria(Role.class).
				add(Restrictions.in("id", roleIds)).
				list();				
	}
	
	@Override
	public int countFilteredRole(String code) {
		return ((Long)getCurrentSession().createCriteria(Role.class).
				add(Restrictions.like("code", '%' + code + '%')).
				setProjection(Projections.rowCount()).
				uniqueResult()).intValue();
	}

	@Override
	public Role findByRoleName(String roleName) {
		return (Role) getCurrentSession().createCriteria(Role.class).
				add(Restrictions.eq("name", roleName)).uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Role> getLazyRoleByCategory(int startingAt, int maxResult,
			String roleCode, String roleName, List<Integer> status, List<String> approvalStatus) {
		return getCurrentSession().createCriteria(Role.class).
				add(Restrictions.ilike("code", "%" + roleCode + "%", MatchMode.ANYWHERE)).
				add(Restrictions.ilike("name", "%" + roleName + "%", MatchMode.ANYWHERE)).
				add(Restrictions.in("status", status)).
				add(Restrictions.in("approvalStatus", approvalStatus)).
				setFirstResult(startingAt).
				setMaxResults(maxResult).
				addOrder(Order.asc("code")).
				list();
	}

	@Override
	public int countAllRoleByCategory(String roleCode, String roleName, List<Integer> status, List<String> approvalStatus) {
		return ((Long)getCurrentSession().createCriteria(Role.class).
				add(Restrictions.ilike("code", "%" + roleCode + "%", MatchMode.ANYWHERE)).
				add(Restrictions.ilike("name", "%" + roleName + "%", MatchMode.ANYWHERE)).
				add(Restrictions.in("status", status)).
				add(Restrictions.in("approvalStatus", approvalStatus)).
				setProjection(Projections.rowCount()).
				uniqueResult()).intValue();
	}
	
	public void deleteRoleMenu(Role role){
		getCurrentSession().getNamedQuery("RoleMenu#DeleteRoleMenus").
				setParameter("id", role.getId()).executeUpdate();
	}


}
