package com.bjb.dao;

import java.math.BigDecimal;
import java.util.List;

import com.bjb.model.CommisionTiering;

public interface CommisionTieringDao extends GenericDao<CommisionTiering, BigDecimal> {
	public List<CommisionTiering> selectByCriteria(int startingAt, int maxResult, String code, String featureMapping, List<Integer> status, List<String> approvalStatus);
	public CommisionTiering findByName(String name);
	public int countByCriteria(String code, String featureMapping, List<Integer> status, List<String> approvalStatus);
}
