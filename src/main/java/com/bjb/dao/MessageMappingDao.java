package com.bjb.dao;

import java.math.BigDecimal;
import java.util.List;

import com.bjb.model.MessageMapping;

public interface MessageMappingDao extends GenericDao<MessageMapping, BigDecimal> {
	public List<MessageMapping> selectByCriteria(int startingAt, int maxResult, String code,String feature, String messageType, List<Integer> status); 
	public MessageMapping findByName(String name);
	public List<MessageMapping> selectValidMessageMapping();
	public int countByCriteria(String code,String feature, String messageType,List<Integer> status) ;
}
