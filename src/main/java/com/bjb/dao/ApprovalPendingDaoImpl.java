package com.bjb.dao;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.List;

import javax.sql.rowset.serial.SerialBlob;
import javax.sql.rowset.serial.SerialException;

import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.bjb.model.ApprovalPending;

@Repository
public class ApprovalPendingDaoImpl extends GenericDaoImpl<ApprovalPending, BigDecimal> implements ApprovalPendingDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<ApprovalPending> selectByCriteria(int startingAt,
			int maxResult, BigDecimal approverId, String parameterCode, String parameterName, List<String> searchParameter, List<String> searchAction, String branchId) {

		return getCurrentSession().createCriteria(ApprovalPending.class)
				.add(Restrictions.eq("approverId", approverId))
				.add(Restrictions.eq("branchId", branchId))
				.add(Restrictions.ilike("parameterCode", '%' + parameterCode + '%'))
				.add(Restrictions.ilike("parameterName", '%' + parameterName + '%'))
				.add(Restrictions.in("parameter", searchParameter))
				.add(Restrictions.in("action", searchAction))
				.addOrder(Order.asc("approverId"))
				.setFirstResult(startingAt)
				.setMaxResults(maxResult)
				.list();
	}

	@Override
	public int countByCriteria(BigDecimal approverId, String parameterCode, String parameterName, List<String> searchParameter,List<String> searchAction, String branchId) {

		return  ((Long) getCurrentSession().createCriteria(ApprovalPending.class)
				.add(Restrictions.eq("approverId", approverId))
				.add(Restrictions.eq("branchId", branchId))
				.add(Restrictions.ilike("parameterCode", '%' + parameterCode + '%'))
				.add(Restrictions.ilike("parameterName", '%' + parameterName + '%'))
				.add(Restrictions.in("parameter", searchParameter))
				.add(Restrictions.in("action", searchAction))
				.setProjection(Projections.rowCount())
				.uniqueResult()).intValue();
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ApprovalPending> selectByCriteria(String parameter, BigDecimal approverId) {
		List<ApprovalPending> listApproval = getCurrentSession().createCriteria(ApprovalPending.class)
			.add(Restrictions.eq("approverId", approverId))
			.add(Restrictions.eq("parameter", parameter))
			.list();
		
		try{
			for (ApprovalPending a : listApproval){
				if (a.getNewByteObject() != null){
					a.setNewBlobObject(new SerialBlob(a.getNewByteObject()));
				}
				if (a.getOldByteObject() != null){
					a.setOldBlobObject(new SerialBlob(a.getOldByteObject()));
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	
		return listApproval;
	}

	@Override
	public void bulkDelete(List<ApprovalPending> approvalPendings) {
		for (ApprovalPending approvalPending : approvalPendings){
			delete(approvalPending);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ApprovalPending> selectByParameterCode(String parameterCode) {
		List<ApprovalPending> listApproval = getCurrentSession().createCriteria(ApprovalPending.class)
					.add(Restrictions.eq("parameterCode", parameterCode))
					.list();
		try {
			for (ApprovalPending a : listApproval) {
				if (a.getNewByteObject() != null){
						a.setNewBlobObject(new SerialBlob(a.getNewByteObject()));
				}
				if (a.getOldByteObject() != null){
					a.setOldBlobObject(new SerialBlob(a.getOldByteObject()));
				}			
			}
		} catch (SerialException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return listApproval;
	}

	@Override
	public void updateApprovalByParameterCode(String parameterCode, byte[] newByteObject, String parameterName) {

		@SuppressWarnings("unchecked")
		List <ApprovalPending> listApproval = getCurrentSession().createCriteria(ApprovalPending.class)
				.add(Restrictions.eq("parameterCode", parameterCode))
				.list();
		
		for (ApprovalPending approvalPending:listApproval) {
			approvalPending.setNewByteObject(newByteObject);
			approvalPending.setParameterName(parameterName);
			insertOrUpdate(approvalPending);
		}
		
	}

	@Override
	public void assignApprover(BigDecimal approverId){
		
		@SuppressWarnings("unchecked")
		List <ApprovalPending> listApproval = getCurrentSession().createCriteria(ApprovalPending.class)
				.add(Restrictions.isNull("approverId"))
				.setMaxResults(30)
				.list();
		
		for (ApprovalPending approvalPending:listApproval) {
			approvalPending.setApproverId(approverId);
			insertOrUpdate(approvalPending);
		}		
	}

	@Override
	@SuppressWarnings("unchecked")
	public void removeApprover(List<BigDecimal> listApproverId) {
		List <ApprovalPending> listApproval = getCurrentSession().createCriteria(ApprovalPending.class)
				.add(Restrictions.not(Restrictions.in("approverId", listApproverId)))
				.list();
		
		for (ApprovalPending approvalPending:listApproval) {
			approvalPending.setApproverId(null);
			insertOrUpdate(approvalPending);
		}	
		
	}
	
	@Override
	public void removeAllApprover() {
		getCurrentSession().createQuery("update com.bjb.model.Approval set approverId = null")
			.executeUpdate();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ApprovalPending> selectByCriteria(BigDecimal approverId,
			String parameterCode, String parameterName,
			List<String> searchParameter, List<String> searchAction,
			String branchId) {
		
		List <ApprovalPending> listApproval = getCurrentSession().createCriteria(ApprovalPending.class)
				.add(Restrictions.eq("approverId", approverId))
				.add(Restrictions.eq("branchId", branchId))
				.add(Restrictions.ilike("parameterCode", '%' + parameterCode + '%'))
				.add(Restrictions.ilike("parameterName", '%' + parameterName + '%'))
				.add(Restrictions.in("parameter", searchParameter))
				.add(Restrictions.in("action", searchAction))
				.list();
		
		try {
			for (ApprovalPending a : listApproval){
				if (a.getNewByteObject() != null){
						a.setNewBlobObject(new SerialBlob(a.getNewByteObject()));
				}
				if (a.getOldByteObject() != null){
					a.setOldBlobObject(new SerialBlob(a.getOldByteObject()));
				}
			}
		} catch (SerialException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return listApproval;
	}

}
