package com.bjb.dao;

import java.math.BigDecimal;
import java.util.List;

import com.bjb.model.FieldMapping;

public interface FieldMappingDao extends GenericDao<FieldMapping, BigDecimal> {
	
	public List<FieldMapping> findByMessageMappingId(BigDecimal messageMappingId);
	
}
