package com.bjb.dao;

import java.math.BigDecimal;
import java.util.List;

import com.bjb.model.ScheduleTask;

public interface SchedulerDao extends GenericDao<ScheduleTask, BigDecimal> {
	public ScheduleTask getDetailedTaskByName(String name);
	public List<ScheduleTask> getAllValidTask();
	public void updateDateTime(String task, String newDate, String newTime);
}
