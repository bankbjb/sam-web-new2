package com.bjb.dao;

import java.math.BigDecimal;
import java.util.List;

import com.bjb.model.MessageField;

public interface MessageFieldDao extends GenericDao<MessageField, BigDecimal> {
	public List<MessageField> selectByCriteria(int startingAt, int maxResult, String code, String name, String description, List<Integer> status, List<String> approvalStatus);
	public List<MessageField> selectByMessageType(String messageType);
	public List<MessageField> selectByMessageTypeOrderByIdAsc(String messageType);
	public MessageField findByName(String name);
	public MessageField findByIdAndMessageType(Integer id, String messageType);
	public int countByCriteria(String code, String name, String description, List<Integer> status, List<String> approvalStatus);
	public List<MessageField> selectValidMessageField();
}
