package com.bjb.dao;

import java.math.BigDecimal;
import java.util.List;

import com.bjb.model.User;

public interface UserDao extends GenericDao<User, BigDecimal> {
	public List<User> selectByCriteria(int startingAt, int maxResult, String userName, String fullName, String branchId, List<Integer> status, List<String> approvalStatus);	
	public int countByCriteria(String userName, String fullName, String branchId, List<Integer> status, List<String> approvalStatus);
	public User findByUserName(String userName);
	public User
	findValidUserByUsername(String userName);
	public int countUserWithRole(Integer roleId);
}
