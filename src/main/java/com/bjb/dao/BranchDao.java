package com.bjb.dao;

import java.util.List;

import com.bjb.model.Branch;

public interface BranchDao extends GenericDao<Branch, String> {
	public List<Branch> selectByCriteria(int maxPerPage, int startingAt, String code, String name, List<Integer> status, List<String> approvalStatus);
	public Integer countByCriteria(String code, String name, List<Integer> status, List<String> approvalStatus);
	public List<Branch> selectValidBranch();
}
