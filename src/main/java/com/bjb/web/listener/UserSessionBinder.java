package com.bjb.web.listener;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;

import com.bjb.model.User;
import com.bjb.web.helper.FacesUtils;
import com.bjb.web.model.UserSession;

@WebListener
public class UserSessionBinder implements HttpSessionAttributeListener  {

	private static Map<User, UserSession> logins = new HashMap<User, UserSession>();

	public static Map<User, UserSession> getLogins() {
		return logins;
	}

    @Override
    public void attributeAdded(HttpSessionBindingEvent event) {
        if (event.getValue() instanceof User) {
        	User user = (User) event.getValue();
        	// remove old session that probably bind to this user
        	UserSession userSession = logins.remove(user);
    	    if (userSession != null) {
    	        userSession.getSession().invalidate();
    	    }
    	    
    	    userSession = new UserSession();
    	    userSession.setUser(user);
    	    userSession.setSession(event.getSession());
    	    userSession.setCurrentTerminal (FacesUtils.getClientIpAddress());
    	    userSession.setUserAgent (FacesUtils.getClientUserAgent());
    	    logins.put(user, userSession);
        }
    }

    @Override
    public void attributeRemoved(HttpSessionBindingEvent event) {
        if (event.getValue() instanceof User) {
    		logins.remove(event.getValue());
        }
    }

    @Override
    public void attributeReplaced(HttpSessionBindingEvent event) {
        if (event.getValue() instanceof User) {
            // An instance of YourObject has been replaced in the session.
        }
    }
    
}
