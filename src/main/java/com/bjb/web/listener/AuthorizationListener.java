package com.bjb.web.listener;

import javax.faces.application.NavigationHandler;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.servlet.http.HttpSession;

public class AuthorizationListener implements PhaseListener {
	private static final long serialVersionUID = 1L;

	@Override
	public void afterPhase(PhaseEvent event) {
//		System.out.println(">>> START AFTER PHASE AuthorizationListener");
		FacesContext facesContext = event.getFacesContext();
		String currentPage = facesContext.getViewRoot().getViewId();
		 
		boolean isUnsecuredPage = (currentPage.lastIndexOf("login.xhtml") > -1)
				|| (currentPage.lastIndexOf("pleaseChangePassword.xhtml") > -1)
		|| (currentPage.lastIndexOf("forgetPassword.xhtml") > -1);
		
		HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
		if(session == null){
			NavigationHandler nh = facesContext.getApplication().getNavigationHandler();
			nh.handleNavigation(facesContext, null, "login");
		}else{
			Object currentUser = session.getAttribute("user");
			if (!UserSessionBinder.getLogins().containsKey(currentUser)) {
				currentUser = null;
				session.invalidate();
			}
			NavigationHandler nh = facesContext.getApplication().getNavigationHandler();
			if (!isUnsecuredPage && (currentUser == null)) {
				nh.handleNavigation(facesContext, null, "login");
				
			}else if(isUnsecuredPage && currentUser != null){
				nh.handleNavigation(facesContext, null, "welcome");
			}
		}
//		System.out.println(">>> END AFTER PHASE AuthorizationListener");
	}

	@Override
	public void beforePhase(PhaseEvent event) {
//		System.out.println(">>> BEFORE PHASE AuthorizationListener");
	}

	@Override
	public PhaseId getPhaseId() {
		return PhaseId.RESTORE_VIEW;
	}

}
