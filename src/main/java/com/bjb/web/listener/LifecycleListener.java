package com.bjb.web.listener;

import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;

public class LifecycleListener implements PhaseListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2882291582332750539L;

	@Override
	public void afterPhase(PhaseEvent e) {
//		System.out.println(">>> END PHASE "+getPhaseName());
	}

	@Override
	public void beforePhase(PhaseEvent e) {
//		System.out.println(">>> START PHASE "+getPhaseName());
	}	

	private String getPhaseName() {
		
		if(getPhaseId().equals(PhaseId.RESTORE_VIEW))return "RESTORE_VIEW";
		else if(getPhaseId().equals(PhaseId.APPLY_REQUEST_VALUES))return "APPLY_REQUEST_VALUES";
		else if(getPhaseId().equals(PhaseId.PROCESS_VALIDATIONS))return "PROCESS_VALIDATIONS";
		else if(getPhaseId().equals(PhaseId.UPDATE_MODEL_VALUES))return "UPDATE_MODEL_VALUES";
		else if(getPhaseId().equals(PhaseId.INVOKE_APPLICATION))return "INVOKE_APPLICATION";
		else if(getPhaseId().equals(PhaseId.RENDER_RESPONSE))return "RENDER_RESPONSE";
		return "...";
	}

	@Override
	public PhaseId getPhaseId() {
		return PhaseId.ANY_PHASE;
	}
	
}
