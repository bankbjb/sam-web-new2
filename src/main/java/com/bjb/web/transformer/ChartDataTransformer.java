package com.bjb.web.transformer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.transform.ResultTransformer;

public class ChartDataTransformer implements ResultTransformer {

	private static final long serialVersionUID = 1L;
	
	public static final String HIGHEST_VALUE = "Highest Value";
	public static final String LOWEST_VALUE = "Lowest Value";
	
	@Override
	public Object transformTuple(Object[] row, String[] arg1) {
		return row;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List transformList(List rows) {
		List result = new ArrayList();
		Number highestValue = 0;
		Number lowestValue = 0;
		Map<Object, Number> mapStats = new HashMap<Object, Number>();
		
		Map<Object, Number> map = new HashMap<Object, Number>();
		for (Object row:rows) {
			Object[] tuple = (Object[]) row;
			map.put(tuple[0], (Number) tuple[1]);
			highestValue = Math.max(highestValue.doubleValue(), ((Number) tuple[1]).doubleValue());
			lowestValue = Math.max(lowestValue.doubleValue(), ((Number) tuple[1]).doubleValue());
		}
		mapStats.put(HIGHEST_VALUE, highestValue) ;
		mapStats.put(LOWEST_VALUE, lowestValue);

		result.add(map);
		result.add(mapStats);
		
		return result;
	}

}
