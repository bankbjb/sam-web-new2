package com.bjb.web.bean;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.TreeNode;

import com.bjb.constant.Constants;
import com.bjb.model.Activity;
import com.bjb.model.ActivityLog;
import com.bjb.service.ActivityLogService;
import com.bjb.service.SessionService;
import com.bjb.util.HelperUtils;
import com.bjb.web.bean.support.DiffService;
import com.bjb.web.helper.FacesUtils;

@ManagedBean
@ViewScoped
public class ActivityLogHandler implements Serializable {
	private static final long serialVersionUID = 1L;

	private Object data;
	private Object dataTemp;
	private TreeNode diffTreeTable;

	private String action;
	
	private boolean failed;
	private boolean success;
	private List<Integer> status;

	private Date startDate = new Date();
	private Date endDate = new Date();
	private BigDecimal activityId; 
	
	private boolean saveActivityLog;

	private ActivityLog activityLogSearch;
	
	private LazyDataModel<ActivityLog> activityLogList;

	private boolean doShowSearch = false;

	private boolean doShowHistory = false;

	@ManagedProperty(value="#{sessionService}")
	private SessionService sessionService;
	
	@ManagedProperty(value="#{activityLogService}")
	private ActivityLogService activityLogService;

	@ManagedProperty(value="#{diffService}")
	private DiffService diffService;
	
	public void setDiffService(DiffService diffService) {
		this.diffService = diffService;
	}

	public TreeNode getDiffTreeTable() {
		return diffTreeTable==null?new DefaultTreeNode():diffTreeTable; 
	}

	public void setDiffTreeTable(TreeNode diffTreeTable) {
		this.diffTreeTable = diffTreeTable; 
	}
	
	public void setActivityLogService(ActivityLogService activityLogService) {
		this.activityLogService = activityLogService;
	}

	@PostConstruct
	public void init() {
		sessionService.authMenu(sessionService.getUser().getId(), "viewLogActivity");

		activityLogList = new ActivityLogDataModel();
		activityLogSearch = new ActivityLog();
		activityLogSearch.setActivity(new Activity());
		
		failed = false;
		success = false;

		doSearch();
	}

	public void viewData(ActivityLog activityLog) {
		try {
			dataTemp = HelperUtils.deserialize(new ByteArrayInputStream(activityLog.getDataBefore()));
			data = HelperUtils.deserialize(new ByteArrayInputStream(activityLog.getDataAfter()));
			setDiffTreeTable(diffService.diff(dataTemp, data));
		} catch (ClassNotFoundException | IOException e) {
			e.printStackTrace();
		}
		RequestContext.getCurrentInstance().execute("PF('viewData').show()");
	}
	
	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}
	
	public List<Integer> getStatus() {
		return status;
	}

	public void setStatus(List<Integer> status) {
		this.status = status;
	}

	public void showHistory() {
		doShowHistory = true;
	}

	public void hideHistory() {
		doShowHistory = false;
	}
	
	public boolean isFailed() {
		return failed;
	}

	public void setFailed(boolean failed) {
		this.failed = failed;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}
	
	public ActivityLog getActivityLogSearch() {
		return activityLogSearch;
	}

	public void setActivityLogSearch(ActivityLog activityLogSearch) {
		this.activityLogSearch = activityLogSearch;
	}

	public boolean isDoShowHistory() {
		return doShowHistory;
	}

	public void setDoShowHistory(boolean doShowHistory) {
		this.doShowHistory = doShowHistory;
	}

	public SessionService getSessionService() {
		return sessionService;
	}

	public void setSessionService(SessionService sessionService) {
		this.sessionService = sessionService;
	}

	public boolean isSaveActivityLog() {
		return saveActivityLog;
	}

	public void setSaveActivityLog(boolean saveActivityLog) {
		this.saveActivityLog = saveActivityLog;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public BigDecimal getActivityId() {
		return activityId;
	}

	public void setActivityId(BigDecimal activityId) {
		this.activityId = activityId;
	}

	public LazyDataModel<ActivityLog> getActivityLogList() {
		return activityLogList;
	}

	public void setActivityLogList(LazyDataModel<ActivityLog> activityLogList) {
		this.activityLogList = activityLogList;
	}

	public boolean isDoShowSearch() {
		return doShowSearch;
	}

	public void setDoShowSearch(boolean doShowSearch) {
		this.doShowSearch = doShowSearch;
	}

	public void showSearch() {
		doShowSearch = true;
	}

	public void hideSearch() {
		doShowSearch = false;
	}

	public void doSearch() {

		status = new ArrayList<Integer>();
		if (!success && !failed) {
			status.add(Constants.STATUS_ACTIVE);
			status.add(Constants.STATUS_INACTIVE);
		} else {
			if (success) {
				status.add(Constants.STATUS_ACTIVE);
			}
			if (failed) {
				status.add(Constants.STATUS_INACTIVE);
			}
		}
		
		FacesUtils.resetPage("activityLogForm:activityLogTable");
		activityLogList = new ActivityLogDataModel();

	}

	private class ActivityLogDataModel extends LazyDataModel<ActivityLog> {
		private static final long serialVersionUID = 1L;

		public ActivityLogDataModel() {

		}

		@Override
	    public List<ActivityLog> load(int startingAt, int maxResult, String sortField, SortOrder sortOrder, Map<String, Object> filters) {

			List<ActivityLog> activityLog = null;

			activityLog = activityLogService.selectByCriteria(startingAt, maxResult, sessionService.getUser().getId(), activityId, startDate, endDate, activityLogSearch, status);
			Integer jmlAll = activityLogService.countByCriteria(sessionService.getUser().getId(), activityId, startDate, endDate, activityLogSearch, status);
			setRowCount(jmlAll);
			setPageSize(maxResult);

			return activityLog;
		}

	}

}
