package com.bjb.web.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.TreeNode;

import com.bjb.constant.Constants;
import com.bjb.model.Branch;
import com.bjb.model.Role;
import com.bjb.model.User;
import com.bjb.model.UserAggregator;
import com.bjb.service.BranchService;
import com.bjb.service.HolidayService;
import com.bjb.service.RoleService;
import com.bjb.service.SchedulerService;
import com.bjb.service.SessionService;
import com.bjb.service.SystemParameterService;
import com.bjb.service.UserService;
import com.bjb.util.HelperUtils;
import com.bjb.web.bean.support.DiffService;
import com.bjb.web.helper.FacesUtils;

@ManagedBean
@ViewScoped
public class UserWebHandler implements Serializable {
	private static final long serialVersionUID = 1L;
	private User user;
	private User userTemp;
	private LazyDataModel <User> userList;
	private List <SelectItem> roleSelectItem = new ArrayList <SelectItem> ();
	private List<SelectItem> branchSelectItem = new ArrayList<SelectItem>();
	private String action;
	private boolean saveUser;
	private boolean addRole;

	private boolean saveUserPending;
	private boolean addRolePending;

	private String userName = "";
	private String fullName = "";
	private String roleName = "";
	private String branchId;
	private List<String> rolesName;
	private List<String> rolesNamePending;
	private List<Integer> status;;
	private List<String> approvalStatus;

	private boolean active;
	private boolean inactive;

	private boolean pending;
	private boolean approved;
	private boolean rejected;
	
	private String currentUsername;
	
	private String duplicateRole;

	private boolean doShowSearch = false;
	
	private boolean doShowHistory = false;
	
	@ManagedProperty(value="#{sessionService}")
	private SessionService sessionService;
	
	@ManagedProperty(value="#{userService}")
	private UserService userService;

	@ManagedProperty(value="#{roleService}")
	private RoleService roleService;

	@ManagedProperty(value="#{systemParameterService}")
	private SystemParameterService systemParameterService;

	@ManagedProperty(value="#{branchService}")
	private BranchService branchService;

	@ManagedProperty(value="#{schedulerService}")
	private SchedulerService schedulerService;

	@ManagedProperty(value="#{holidayService}")
	private HolidayService holidayService;

	@ManagedProperty(value="#{diffService}")
	private DiffService diffService;
	
	public void setDiffService(DiffService diffService) {
		this.diffService = diffService;
	}

	public TreeNode getDiffTreeTable() {
		return diffService.diff(userTemp, user);
	}
	
	public void setSessionService(SessionService sessionService) {
		this.sessionService = sessionService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public void setRoleService(RoleService roleService){
		this.roleService = roleService;
	}

	public void setSystemParameterService(SystemParameterService systemParameterService){
		this.systemParameterService = systemParameterService;
	}

	public void setBranchService(BranchService branchService) {
		this.branchService = branchService;
	}

	public void setSchedulerService(SchedulerService schedulerService) {
		this.schedulerService = schedulerService;
	}

	public void setHolidayService(HolidayService holidayService) {
		this.holidayService = holidayService;
	}

	@PostConstruct
	public void init() {

		sessionService.authMenu(sessionService.getUser().getId(), "adminUser");
		
		userList = new UserDataModel();
		saveUser = true;
		this.addRole = true;
		this.roleSelectItem = new ArrayList < SelectItem > ();
		//List < Role > allRoles = roleService.getAllValidRoleByAccessLevel(Constants.ACCESS_LEVEL_AGGREGATOR);
		List < Role > allRoles = roleService.selectValidRole();
		for (Role role: allRoles) {
			SelectItem selectItem = new SelectItem();
			selectItem.setValue(role);
			selectItem.setLabel(role.getName());
			this.roleSelectItem.add(selectItem);
		}
		loadBranchList();

		doSearch();
	}
	
	private void loadBranchList(){
		this.branchSelectItem = new ArrayList<SelectItem>();
		List<Branch> allBranch = branchService.getAllValidBranch();
		for(Branch branch : allBranch){
			SelectItem selectItem = new SelectItem();
			selectItem.setValue(branch);
			selectItem.setLabel(branch.getName()+" - "+branch.getCode());
			this.branchSelectItem.add(selectItem);
		}
	}

	public boolean isDoShowSearch() {
		return doShowSearch;
	}

	public void setDoShowSearch(boolean doShowSearch) {
		this.doShowSearch = doShowSearch;
	}

	public void showSearch() {
		doShowSearch = true;
	}

	public void hideSearch() {
		doShowSearch = false;
	}
	
	
	public void showHistory(){
		doShowHistory = true;
	}
	
	public void hideHistory(){
		doShowHistory = false;
	}
	
	public boolean isDoShowHistory() {
		return doShowHistory;
	}

	public void setDoShowHistory(boolean doShowHistory) {
		this.doShowHistory = doShowHistory;
	}

	public List<SelectItem> getBranchSelectItem() {
		return branchSelectItem;
	}

	public void setBranchSelectItem(List<SelectItem> branchSelectItem) {
		this.branchSelectItem = branchSelectItem;
	}
	
	public List<String> getRolesName() {
		return rolesName;
	}

	public void setRolesName(List<String> rolesName) {
		this.rolesName = rolesName;
	}

	public List<String> getRolesNamePending() {
		return rolesNamePending;
	}

	public void setRolesNamePending(List<String> rolesNamePending) {
		this.rolesNamePending = rolesNamePending;
	}
	
	public boolean isSaveUserPending() {
		return saveUserPending;
	}

	public void setSaveUserPending(boolean saveUserPending) {
		this.saveUserPending = saveUserPending;
	}

	public boolean isAddRolePending() {
		return addRolePending;
	}

	public void setAddRolePending(boolean addRolePending) {
		this.addRolePending = addRolePending;
	}
	
	public void viewDetailedUser(User user) {
		this.user = user;

		if (this.user.getApprovalStatus().equals(
				Constants.APPROVAL_STATUS_PENDING_EDIT)) {
			
			this.userTemp = userService.findPendingApprovalByCode(user.getCode());
					
			if (userTemp!=null) {
				RequestContext.getCurrentInstance().execute(
							"PF('viewUserEdit').show()");
				return;
			}
						
		} 
		RequestContext.getCurrentInstance().execute("PF('viewUser').show()");

	}

	public void viewDetailedUserEdit(User user) {

		this.user = user;

		if (this.user.getDisableEdit() == true) {
			FacesUtils.setApprovalWarning("parameter",
					this.user.getName(),
					this.user.getApprovalStatus());
		} else {
			this.action = "edit";
			if (this.user.getApprovalStatus().equals(
					Constants.APPROVAL_STATUS_PENDING_EDIT)) {

				this.userTemp = userService.findPendingApprovalByCode(user.getCode());
				if (userTemp != null) {
					currentUsername = this.userTemp.getName();
					RequestContext.getCurrentInstance().execute(
							"PF('insertOrEditUserPending').show()");
					return;
				} 
			}
			this.userTemp = HelperUtils.clone(user);
			RequestContext.getCurrentInstance().execute(
						"PF('insertOrEditUser').show()");
		}
	}

	public void viewDetailedDelete(User user) {

		this.user = user;

		if (this.user.getDisableDelete() == true) {
			FacesUtils.setApprovalWarning("user", this.user.getUserName(), this.user.getApprovalStatus());
		} else {
			RequestContext.getCurrentInstance().execute("PF('dlgConfirm').show()");
		}
	}

	public void checkDuplicateRole(Role role) {
		int duplicateRole = 0;
		for (Role role2: this.getUser().getRoles()) {
			if (role2.getId() != null && role.getId().equals(role2.getId()) ) {
				duplicateRole++;
			}
		}
		if (duplicateRole == 0) {
			this.saveUser = true;
			this.addRole = true;
		} else {
			this.saveUser = false;
			this.addRole = false;
			FacesUtils.setErrorMessageDialog("Error",
				"Duplicate Role " + role.getName());
		}
	}
	
	public boolean checkDuplicateRole(User user){
		
		if (user.getRoles() == null) return true;
		
		for (Role role1 : user.getRoles()){
			int count = 0;
			for (Role role2 : user.getRoles()){
				if (role1.getId().equals(role2.getId())){
					count ++;
				}
			}
			if (count > 1){
				Role role = roleService.find(role1.getId());
				this.duplicateRole = role.getName();
				return true;
			}
		}
		return false;
	}
	

	public void newUser() {
		this.user = new UserAggregator();
		BigDecimal seq = userService.generateId();
		this.user.setId(seq);
		this.user.setCode("W"+String.format("%04d", seq.intValue())+"USER");
		this.action = "";
	}

	public void addRoleForm() {
		if (user!=null && user.getRoles() != null) {
			Role role = new Role();
			user.getRoles().add(role);
		}
	}
	
	public void addRoleFormTemp() {
		if (userTemp!=null && userTemp.getRoles() != null) {
			Set<Role> roles = userTemp.getRoles();
			Role role = new Role();
			userTemp.getRoles().add(role);
		}
	}

	public void removeRoleForm(Role role) {
		if (user!=null && user.getRoles() != null) {
			user.getRoles().remove(role);
			this.saveUser = true;
			this.addRole = true;
		}
	}
	
	public void removeRoleFormTemp(Role role) {
		if (userTemp!=null && userTemp.getRoles() != null) {
			userTemp.getRoles().remove(role);
			this.saveUserPending = true;
			this.addRolePending = true;
		}
	}

	public void checkRolePending() {
		Set<Role> roles = userTemp.getRoles();
	}
	
	public void doSave() {
		
		if (this.action != null && this.action.equals("edit")) {
			editUser(this.user);
		} else {
			try {

				User userExist = userService
						.findByCode(
								this.user.getName());

				if (userExist == null
						|| (userExist.getName()
								.equals(currentUsername))) {
					createUser(this.user);
				} else {
					String headerMessage = "Attention";
					String bodyMessage = "User "
							+ this.user.getName()
							+ " already exist in the application.";

					FacesUtils.setWarningMessageDialog(headerMessage,
							bodyMessage);
				}

			} catch (Exception e) {
				String headerMessage = "Error";
				String bodyMessage = "System error";

				FacesUtils.setErrorMessageDialog(headerMessage, bodyMessage);
				e.printStackTrace();
			}
		}

	}

	public void editUser(User user) {
		checkRolePending();
		user = userService.requestToModify(userTemp, user);
		
		RequestContext.getCurrentInstance().execute(
				"PF('insertOrEditUser').hide()");
		RequestContext.getCurrentInstance().execute(
				"PF('insertOrEditUserPending').hide()");

		String headerMessage = "Edit User Successfully";
		String bodyMessage = "Supervisor must approve the editing of the <br/>user &apos;"
				+ user.getName()
				+ "&apos; to completely the process.";

		FacesUtils.setInfoMessageDialog(headerMessage, bodyMessage);
	}

	public void createUser(User user) throws Exception {

		userService.requestToCreate(user);
		
		String headerMessage = "Create User Successfully";
		String bodyMessage = "Supervisor must approve the creating of the <br/>user &apos;" + this.user.getUserName() + "&apos; to completely the process.";

		FacesUtils.setInfoMessageDialog(headerMessage, bodyMessage);
	}

	public void doSearch() {

		status = new ArrayList<Integer>();
		if (!active && !inactive) {
			status.add(Constants.STATUS_ACTIVE);
			status.add(Constants.STATUS_INACTIVE);
		} else {
			if (active) {
				status.add(Constants.STATUS_ACTIVE);
			}
			if (inactive) {
				status.add(Constants.STATUS_INACTIVE);
			}
		}
		approvalStatus = new ArrayList<String>();

		if (!pending && !approved && !rejected) {
			approvalStatus.add(Constants.APPROVAL_STATUS_PENDING_CREATE);
			approvalStatus.add(Constants.APPROVAL_STATUS_PENDING_EDIT);
			approvalStatus.add(Constants.APPROVAL_STATUS_PENDING_DELETE);
			approvalStatus.add(Constants.APPROVAL_STATUS_APPROVED_CREATE);
			approvalStatus.add(Constants.APPROVAL_STATUS_APPROVED_EDIT);
			approvalStatus.add(Constants.APPROVAL_STATUS_REJECTED_CREATE);
			approvalStatus.add(Constants.APPROVAL_STATUS_REJECTED_EDIT);
			approvalStatus.add(Constants.APPROVAL_STATUS_REJECTED_DELETE);
		} else {
			if (pending == true) {
				approvalStatus.add(Constants.APPROVAL_STATUS_PENDING_CREATE);
				approvalStatus.add(Constants.APPROVAL_STATUS_PENDING_EDIT);
				approvalStatus.add(Constants.APPROVAL_STATUS_PENDING_DELETE);
			}
			if (approved == true) {
				approvalStatus.add(Constants.APPROVAL_STATUS_APPROVED_CREATE);
				approvalStatus.add(Constants.APPROVAL_STATUS_APPROVED_EDIT);
			}
			if (rejected == true) {
				approvalStatus.add(Constants.APPROVAL_STATUS_REJECTED_CREATE);
				approvalStatus.add(Constants.APPROVAL_STATUS_REJECTED_EDIT);
				approvalStatus.add(Constants.APPROVAL_STATUS_REJECTED_DELETE);
			}
		}

		FacesUtils.resetPage("userForm:userTable");
		userList = new UserDataModel();
	}

	public void deleteUser() {
		
		userService.requestToRemove(user);

		if (user.getApprovalStatus().equals(Constants.APPROVAL_STATUS_REJECTED_CREATE)){
			
			userService.deleteByKey(user.getId());
			
			String headerMessage = "Success";
			String bodyMessage = "Delete User " + this.user.getUserName() + " Successfully";
	
			FacesUtils.setInfoMessageDialog(headerMessage, bodyMessage);
			
		}else{
			
			String headerMessage = "Delete User Successfully";
			String bodyMessage = "Supervisor must approve the deleting of the <br/>user &apos;" + this.user.getUserName() + "&apos; to completely the process.";
	
			FacesUtils.setInfoMessageDialog(headerMessage, bodyMessage);
		}

	}

	public void viewDetailedChangeStatus(User user) {

		this.user = user;

		if (this.user.getDisableDelete() == true) {
			FacesUtils.setApprovalWarning("parameter",
					this.user.getFullName(),
					this.user.getApprovalStatus());
		} else {
			RequestContext.getCurrentInstance().execute("PF('dlgConfirmChangeStatus').show()");
		}

	}

	public void changeStatusUser() {
		
		this.userTemp = (User) HelperUtils.clone(user);
		
		user.setStatus(user.getStatus()==Constants.STATUS_ACTIVE?Constants.STATUS_INACTIVE:Constants.STATUS_ACTIVE);
		user = userService.requestToModify(userTemp, user);

		String headerMessage = "Change Status User Successfully";
		String bodyMessage = "Supervisor must approve the change status of the <br/>user &apos;"
				+ user.getFullName()
				+ "&apos; to completely the process.";

		FacesUtils.setInfoMessageDialog(headerMessage, bodyMessage);
	}


	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public User getTempUser() {
		return userTemp;
	}

	public void setTempUser(User tempUser) {
		this.userTemp = tempUser;
	}

	public String getBranchId() {
		return branchId;
	}

	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}

	public List < String > getStatus() {
		return approvalStatus;
	}

	public void setStatus(List < String > status) {
		this.approvalStatus = status;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public boolean isInactive() {
		return inactive;
	}

	public void setInactive(boolean inactive) {
		this.inactive = inactive;
	}

	public boolean isPending() {
		return pending;
	}

	public void setPending(boolean pending) {
		this.pending = pending;
	}

	public boolean isApproved() {
		return approved;
	}

	public void setApproved(boolean approved) {
		this.approved = approved;
	}

	public boolean isRejected() {
		return rejected;
	}

	public void setRejected(boolean rejected) {
		this.rejected = rejected;
	}

	private class UserDataModel extends LazyDataModel <User> {
		private static final long serialVersionUID = 1L;

		public UserDataModel() {

		}

		@Override
	    public List<User> load(int startingAt, int maxResult, String sortField, SortOrder sortOrder, Map<String, Object> filters) {

			Branch branch = sessionService.getUser().getBranch();
			
			// if the user is from root branch (identified by parent branch id is null), then user can see user from all other branches 
			if (branch != null) {
				branchId = (branch.getParentId() == null) ? null:branch.getId();
			} else {
				// user is not allowed to see any data
				branchId = "";
			};
			
			List <User> users = userService.selectByCriteria(startingAt, maxResult, userName, fullName, branchId, status, approvalStatus);
			Integer jmlAll = userService.countByCriteria(userName, fullName, branchId, status, approvalStatus);
			setRowCount(jmlAll);
			setPageSize(maxResult);

			return users;
		}
	}

	public LazyDataModel <User> getUserList() {
		return userList;
	}

	public void setUserList(LazyDataModel <User> userList) {
		this.userList = userList;
	}

	public List < SelectItem > getRoleSelectItem() {
		return roleSelectItem;
	}

	public void setRoleSelectItem(List < SelectItem > roleSelectItem) {
		this.roleSelectItem = roleSelectItem;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public boolean isSaveUser() {
		return saveUser;
	}

	public void setSaveUser(boolean saveUser) {
		this.saveUser = saveUser;
	}

	public boolean isAddRole() {
		return addRole;
	}

	public void setAddRole(boolean addRole) {
		this.addRole = addRole;
	}

}