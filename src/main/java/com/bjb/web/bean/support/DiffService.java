package com.bjb.web.bean.support;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import com.bjb.web.model.DiffItem;

import de.danielbechler.diff.ObjectDifferBuilder;
import de.danielbechler.diff.node.DiffNode;
import de.danielbechler.diff.node.Visit;

@ManagedBean(name = "diffService")
@ApplicationScoped
public class DiffService implements Serializable {

	private static final long serialVersionUID = 1L;

	private String getHumanize(String propertyName) {
	    if (propertyName==null) return null;
		String[] names=propertyName.split("\\/");
		if (names.length==0) return "";
		String name = 
				names[names.length-1].replaceAll("([A-Z][a-z]+)", " $1") // Words beginning with UC
			             .replaceAll("([A-Z][A-Z]+)", " $1") // "Words" of only UC
			             .replaceAll("([^A-Za-z ]+)", " $1") // "Words" of non-letters			             
			             .trim();
		char first = Character.toUpperCase(name.charAt(0));
		name = first + name.substring(1);	
		return name;
	}
	
	public TreeNode diff(final Object objectA, final Object objectB) {
	   
	    if (objectA == null && objectB == null) return new DefaultTreeNode();
	    final Map<DiffNode, TreeNode> parentMap = new HashMap<DiffNode, TreeNode>();
	    final DiffNode root = ObjectDifferBuilder.buildDefault().compare(objectA, objectB);
	    final TreeNode diffRoot;
	   
        Object baseValue = root.canonicalGet(objectA);
        Object workingValue = root.canonicalGet(objectB);
        String propertyName = root.getPath().toString();
        diffRoot = new DefaultTreeNode(new DiffItem(getHumanize(propertyName), baseValue, workingValue), null);
 	    parentMap.put(root, diffRoot);

		root.visit(new DiffNode.Visitor()
		{
		    public void node(DiffNode node, Visit visit)
		    {
		    	if (node != root) {
			        final Object baseValue = node.canonicalGet(objectA);
			        final Object workingValue = node.canonicalGet(objectB);
			        final String propertyName = node.getPath().toString();
			        final TreeNode diff = new DefaultTreeNode(new DiffItem(getHumanize(propertyName), baseValue, workingValue), parentMap.get(node.getParentNode()));
			        parentMap.put(node, diff);
		    	}
		    }
		});
 
	    return diffRoot;
	}

}
