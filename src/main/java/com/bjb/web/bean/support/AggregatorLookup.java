package com.bjb.web.bean.support;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import com.bjb.model.Aggregator;
import com.bjb.service.AggregatorService;

@ManagedBean
@ViewScoped
public class AggregatorLookup implements Serializable {
	private static final long serialVersionUID = 1L;

	private List<SelectItem> aggregatorList = new ArrayList<SelectItem>();

	@ManagedProperty(value="#{aggregatorService}")
	private AggregatorService aggregatorService;
	
	@PostConstruct
	public void init() {
		
		SelectItem selectItem;
		this.aggregatorList = new ArrayList<SelectItem>();
		List<Aggregator> allAggregator = aggregatorService.selectValidAggregator();
		for(Aggregator aggregator : allAggregator){
			selectItem = new SelectItem();
			selectItem.setValue(aggregator);
			selectItem.setLabel(aggregator.getCode()+" - "+aggregator.getName());
			this.aggregatorList.add(selectItem);
		}
		
	}
	
	public void setAggregatorService(AggregatorService aggregatorService) {
		this.aggregatorService = aggregatorService;
	}

	public List<SelectItem> getAggregatorList() {
		return aggregatorList;
	}

}
