package com.bjb.web.bean.support;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import com.bjb.service.SessionService;

@ManagedBean
@ViewScoped
public class ConnectedUserWeb implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@ManagedProperty(value="#{sessionService}")
	private SessionService sessionService;
	
	private boolean doShowSearch = false;
	
	public void setSessionService(SessionService sessionService) {
		this.sessionService = sessionService;
	}

	@PostConstruct
	public void init(){
		sessionService.authMenu(sessionService.getUser().getId(), "adminSession");
	}

	public boolean isDoShowSearch() {
		return doShowSearch;
	}

	public void setDoShowSearch(boolean doShowSearch) {
		this.doShowSearch = doShowSearch;
	}

}