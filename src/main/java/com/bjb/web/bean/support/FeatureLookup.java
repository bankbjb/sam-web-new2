package com.bjb.web.bean.support;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import com.bjb.model.Feature;
import com.bjb.service.FeatureService;

@ManagedBean
@ViewScoped
public class FeatureLookup implements Serializable {
	private static final long serialVersionUID = 1L;

	private List<SelectItem> featureList = new ArrayList<SelectItem>();

	@ManagedProperty(value="#{featureService}")
	private FeatureService featureService;
	@PostConstruct
	public void init() {
		SelectItem selectItem;
		this.featureList = new ArrayList<SelectItem>();
		List<Feature> allFeature = featureService.selectValidFeature();
		for(Feature feature : allFeature){
			selectItem = new SelectItem();
			selectItem.setValue(feature);
			selectItem.setLabel(feature.getCode()+" - "+feature.getName());
			this.featureList.add(selectItem);
		}
	}
		
	public void setFeatureService(FeatureService featureService) {
		this.featureService = featureService;
	}
	
	
	public List<SelectItem> getFeatureList() {
		return featureList;
	}


}
