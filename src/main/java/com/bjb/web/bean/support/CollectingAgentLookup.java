package com.bjb.web.bean.support;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import com.bjb.model.CollectingAgent;
import com.bjb.service.CollectingAgentService;

@ManagedBean
@ViewScoped
public class CollectingAgentLookup implements Serializable {
	private static final long serialVersionUID = 1L;

	private List<SelectItem> collectingAgentList = new ArrayList<SelectItem>();

	@ManagedProperty(value="#{collectingAgentService}")
	private CollectingAgentService collectingAgentService;

	@PostConstruct
	public void init() {
		
		SelectItem selectItem;
		this.collectingAgentList = new ArrayList<SelectItem>();
		List<CollectingAgent> allCollectingAgent = collectingAgentService.selectValidCollectingAgent();
		for(CollectingAgent collectingAgent : allCollectingAgent){
			selectItem = new SelectItem();
			selectItem.setValue(collectingAgent);
			selectItem.setLabel(collectingAgent.getCode()+" - "+collectingAgent.getName());
			this.collectingAgentList.add(selectItem);
		}

	}
	
	public void setCollectingAgentService(
			CollectingAgentService collectingAgentService) {
		this.collectingAgentService = collectingAgentService;
	}

	public List<SelectItem> getCollectingAgentList() {
		return collectingAgentList;
	}

}
