package com.bjb.web.bean.support;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.faces.bean.ManagedProperty;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import com.bjb.util.ReportConfigUtils;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.j2ee.servlets.BaseHttpServlet;
 
public abstract class AbstractReport {
 
    public enum ExportOption {
        PDF, HTML, EXCEL, RTF
    }
    private ExportOption exportOption;
    private String message;
 
	@ManagedProperty(value="#{dataSource}")
	DataSource dataSource;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

    public AbstractReport() {
        super();
        setExportOption(ExportOption.PDF);
    }
 
    protected void prepareReport() throws JRException, IOException {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
 
        ServletContext context = (ServletContext) externalContext.getContext();
        HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();
        HttpServletResponse response = (HttpServletResponse) externalContext.getResponse();
 
        ReportConfigUtils.compileReport(context, getCompileDir(), getCompileFileName());
 
        File reportFile = new File(ReportConfigUtils.getJasperFilePath(context, getCompileDir(), getCompileFileName() + ".jasper"));
 
        Connection conn = null;
        try {
            conn = getConnection();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        
        JasperPrint jasperPrint = ReportConfigUtils.fillReport(reportFile, getReportParameters(), conn);
 
        if (getExportOption().equals(ExportOption.HTML)) {
            ReportConfigUtils.exportReportAsHtml(jasperPrint, response.getWriter());
        } else if (getExportOption().equals(ExportOption.EXCEL)) {
            ReportConfigUtils.exportReportAsExcel(jasperPrint, response.getWriter());
        } else {
            request.getSession().setAttribute(BaseHttpServlet.DEFAULT_JASPER_PRINT_SESSION_ATTRIBUTE, jasperPrint);
            response.sendRedirect(request.getContextPath() + "/resources/components/" + getExportOption());
        }
        
        FacesContext.getCurrentInstance().responseComplete();
    }
 
    public ExportOption getExportOption() {
        return exportOption;
    }
 
    public void setExportOption(ExportOption exportOption) {
        this.exportOption = exportOption;
    }
 
    protected Map<String, Object> getReportParameters() {
        return new HashMap<String, Object>();
    }
 
    protected String getCompileDir() {
        return new String();
    }
 
    protected abstract String getCompileFileName();
 
    public String getMessage() {
        return message;
    }
 
    public void setMessage(String message) {
        this.message = message;
    }
    
    public Connection getConnection() throws SQLException {

        Connection conn = null;
        Properties connectionProps = new Properties();
        connectionProps.put("user", "postgres");
        connectionProps.put("password", "admin");
        conn = dataSource.getConnection();
        //conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/asabri-database/", connectionProps);
    		
        System.out.println("Connected to database");
        return conn;
    }
    
    
}