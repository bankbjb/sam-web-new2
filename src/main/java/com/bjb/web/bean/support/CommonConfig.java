package com.bjb.web.bean.support;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

import com.bjb.service.SystemParameterService;

@ManagedBean
@ApplicationScoped
public class CommonConfig implements Serializable {
	private static final long serialVersionUID = 1L;

	private String rowsPerPageTemplate = "5, 10, 15";
	
	@ManagedProperty(value="#{systemParameterService}")
	private SystemParameterService systemParameterService;

	public void setSystemParameterService(SystemParameterService systemParameterService) {
		this.systemParameterService = systemParameterService;
	}
	
	@PostConstruct
	public void init() {
		Integer rowsPerPage = Integer.parseInt(systemParameterService.getSystemParameter("DISPLAY_ROWS_PER_PAGE", "10"));
		rowsPerPageTemplate = rowsPerPage + ", " + rowsPerPage*2 + ", " + rowsPerPage*3;
	}

	
	public String getRowsPerPageTemplate() {
		return rowsPerPageTemplate;
	}


}
