package com.bjb.web.bean.support;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.model.SelectItem;

import com.bjb.web.converter.StatusSelectItemsConverter;

@ManagedBean
@ApplicationScoped
public class CommonLookup implements Serializable {
	private static final long serialVersionUID = 1L;

	private List<SelectItem> statusOptions = StatusSelectItemsConverter.getOptions();

	private List<SelectItem> messageTypeOptions = new ArrayList<SelectItem>();
	
	private List<SelectItem> trxOptions = new ArrayList<SelectItem>();

	@PostConstruct
	public void init() {
		
		SelectItem selectItem = new SelectItem();
		selectItem.setValue("ISO");
		selectItem.setLabel("ISO");
		this.messageTypeOptions.add(selectItem);
		selectItem = new SelectItem();
		selectItem.setValue("XML");
		selectItem.setLabel("XML");
		this.messageTypeOptions.add(selectItem);
		selectItem = new SelectItem();
		selectItem.setValue("JSON");
		selectItem.setLabel("JSON");
		this.messageTypeOptions.add(selectItem);

		selectItem = new SelectItem();
		selectItem.setValue("INQUIRY_PAYMENT");
		selectItem.setLabel("INQUIRY_PAYMENT");
		this.trxOptions.add(selectItem);
		selectItem = new SelectItem();
		selectItem.setValue("PAYMENT");
		selectItem.setLabel("PAYMENT");
		this.trxOptions.add(selectItem);
		selectItem = new SelectItem();
		selectItem.setValue("PURCHASE");
		selectItem.setLabel("PURCHASE");
		this.trxOptions.add(selectItem);

	}
	
	public List<SelectItem> getStatusOptions() {
		return statusOptions;
	}

	public void setStatusOptions(List<SelectItem> statusOptions) {
		this.statusOptions = statusOptions;
	}

	public List<SelectItem> getMessageTypeOptions() {
		return messageTypeOptions;
	}

	public void setMessageTypeOptions(List<SelectItem> messageTypeList) {
		this.messageTypeOptions = messageTypeList;
	}

	public List<SelectItem> getTrxOptions() {
		return trxOptions;
	}

	public void setTrxOptions(List<SelectItem> trxOptions) {
		this.trxOptions = trxOptions;
	}

}
