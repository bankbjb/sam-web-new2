package com.bjb.web.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.TreeNode;

import com.bjb.constant.Constants;
import com.bjb.model.CommisionTiering;
import com.bjb.model.FeatureMapping;
import com.bjb.service.AggregatorService;
import com.bjb.service.CollectingAgentService;
import com.bjb.service.CommisionTieringService;
import com.bjb.service.FeatureMappingService;
import com.bjb.service.SessionService;
import com.bjb.util.HelperUtils;
import com.bjb.web.bean.support.DiffService;
import com.bjb.web.helper.FacesUtils;

@ManagedBean
@ViewScoped
public class CommisionTieringHandler implements Serializable {
	private static final long serialVersionUID = 1L;

	private CommisionTiering commisionTiering;
	private CommisionTiering commisionTieringTemp;

	private String action;

	private String code;
	private String featureMapping;
	private List<Integer> status;
	private List<String> approvalStatus;

	private boolean active;
	private boolean inactive;

	private boolean maxUpperLimit;

	private boolean pending;
	private boolean approved;
	private boolean rejected;

	private boolean saveCommisionTiering;

	private LazyDataModel<CommisionTiering> commisionTieringList;

	private boolean doShowSearch = false;

	private boolean doShowHistory = false;

	private List<SelectItem> featureMappingLookup = new ArrayList<SelectItem>();

	@ManagedProperty(value="#{sessionService}")
	private SessionService sessionService;

	@ManagedProperty(value="#{commisionTieringService}")
	private CommisionTieringService commisionTieringService;

	@ManagedProperty(value="#{featureMappingService}")
	private FeatureMappingService featureMappingService;

	@ManagedProperty(value="#{aggregatorService}")
	private AggregatorService aggregatorService;

	@ManagedProperty(value="#{collectingAgentService}")
	private CollectingAgentService collectingAgentService;

	@ManagedProperty(value="#{diffService}")
	private DiffService diffService;

	public void setDiffService(DiffService diffService) {
		this.diffService = diffService;
	}

	public TreeNode getDiffTreeTable() {
		return diffService.diff(commisionTieringTemp, commisionTiering);
	}

	@PostConstruct
	public void init() {
		sessionService.authMenu(sessionService.getUser().getId(), "manageTiering");

		commisionTieringList = new CommisionTieringDataModel();

		doSearch();

		this.featureMappingLookup = new ArrayList<SelectItem>();
		List<FeatureMapping> allFeatureMapping = featureMappingService.selectValidFeatureMapping();
		SelectItem selectItem = new SelectItem();
		for(FeatureMapping featureMapping : allFeatureMapping){
			selectItem = new SelectItem();
			selectItem.setValue(featureMapping);
			selectItem.setLabel(featureMapping.getCode());
			selectItem.setDescription("["+featureMapping.getFeature().getCode()+"] "+featureMapping.getFeature().getName()+" ["+featureMapping.getAggregator().getCode()+"] "+featureMapping.getAggregator().getName()+" ["+featureMapping.getCollectingAgent().getCode()+"] "+featureMapping.getCollectingAgent().getName());
			this.featureMappingLookup.add(selectItem);
		}

	}

	public void setCommisionTieringService(CommisionTieringService commisionTieringService) {
		this.commisionTieringService = commisionTieringService;
	}

	public void setFeatureMappingService(FeatureMappingService featureMappingService) {
		this.featureMappingService = featureMappingService;
	}

	public void setAggregatorService(AggregatorService aggregatorService) {
		this.aggregatorService = aggregatorService;
	}

	public void setCollectingAgentService(
			CollectingAgentService collectingAgentService) {
		this.collectingAgentService = collectingAgentService;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public boolean isPending() {
		return pending;
	}

	public boolean isActive() {
		return active;
	}

	public String getFeatureMapping() {
		return featureMapping;
	}

	public void setFeatureMapping(String featureMapping) {
		this.featureMapping = featureMapping;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public boolean isInactive() {
		return inactive;
	}

	public void setInactive(boolean inactive) {
		this.inactive = inactive;
	}

	public boolean isMaxUpperLimit() {
		return maxUpperLimit;
	}

	public void setMaxUpperLimit(boolean maxUpperLimit) {
		this.maxUpperLimit = maxUpperLimit;
	}

	public void setPending(boolean pending) {
		this.pending = pending;
	}

	public boolean isApproved() {
		return approved;
	}

	public void setApproved(boolean approved) {
		this.approved = approved;
	}

	public boolean isRejected() {
		return rejected;
	}

	public void setRejected(boolean rejected) {
		this.rejected = rejected;
	}

	public void showHistory() {
		doShowHistory = true;
	}

	public void hideHistory() {
		doShowHistory = false;
	}

	public boolean isDoShowHistory() {
		return doShowHistory;
	}

	public void setDoShowHistory(boolean doShowHistory) {
		this.doShowHistory = doShowHistory;
	}

	public SessionService getSessionService() {
		return sessionService;
	}

	public void setSessionService(SessionService sessionService) {
		this.sessionService = sessionService;
	}

	public boolean isSaveCommisionTiering() {
		return saveCommisionTiering;
	}

	public void setSaveCommisionTiering(boolean saveCommisionTiering) {
		this.saveCommisionTiering = saveCommisionTiering;
	}

	public CommisionTiering getCommisionTiering() {
		return commisionTiering;
	}

	public CommisionTiering getCommisionTieringTemp() {
		return commisionTieringTemp;
	}

	public void setCommisionTieringTemp(CommisionTiering commisionTieringTemp) {
		this.commisionTieringTemp = commisionTieringTemp;
	}

	public void setCommisionTiering(CommisionTiering commisionTiering) {
		this.commisionTiering = commisionTiering;
	}

	public LazyDataModel<CommisionTiering> getCommisionTieringList() {
		return commisionTieringList;
	}

	public void setCommisionTieringList(LazyDataModel<CommisionTiering> commisionTieringList) {
		this.commisionTieringList = commisionTieringList;
	}

	public boolean isDoShowSearch() {
		return doShowSearch;
	}

	public void setDoShowSearch(boolean doShowSearch) {
		this.doShowSearch = doShowSearch;
	}

	public void showSearch() {
		doShowSearch = true;
	}

	public void hideSearch() {
		doShowSearch = false;
	}

	public List<SelectItem> getFeatureMappingLookup() {
		return featureMappingLookup;
	}

	public void viewDetailedCommisionTiering(CommisionTiering commisionTiering) {
		this.commisionTiering = commisionTiering;

		RequestContext.getCurrentInstance().execute("PF('viewCommisionTiering').show()");
	}

	public void viewDetailedCommisionTieringEdit(CommisionTiering commisionTiering) {

		this.commisionTieringTemp = HelperUtils.clone(commisionTiering);
		this.commisionTiering = commisionTiering;
		this.action = "edit";
		this.maxUpperLimit = commisionTiering.upperLimit.compareTo(new BigDecimal(-1)) == 0;

		RequestContext.getCurrentInstance().execute("PF('insertOrEditCommisionTiering').show()");
	}

	public void newCommisionTiering() {
		this.commisionTiering = new CommisionTiering();
		SimpleDateFormat dateFormat = new SimpleDateFormat(Constants.FORMAT_DATE_SHORT);
		BigDecimal seq = commisionTieringService.generateId();
		this.commisionTiering.setCode(dateFormat.format(new Date()) + "Tier" + String.format("%04d", seq.intValue()));
		this.commisionTiering.setStatus(Constants.STATUS_INACTIVE);;
		this.action = "";
		this.maxUpperLimit=false;
	}

	public void createCommisionTiering(CommisionTiering commisionTiering) {

		try{
			commisionTieringService.create(commisionTiering);
			String headerMessage = "Success";
			String bodyMessage = "Insert Commision Tiering Successfully";

			FacesUtils.setInfoMessageDialog(headerMessage, bodyMessage);
		}catch(Exception e){
			String headerMessage = "Error";
			String bodyMessage = "Insert Commision Tiering Failed";

			FacesUtils.setErrorMessageDialog(headerMessage, bodyMessage);
		}

	}

	public void editCommisionTiering(CommisionTiering commisionTiering) {
		try{
			commisionTieringService.update(commisionTiering);
			String headerMessage = "Success";
			String bodyMessage = "Edit Commision Tiering Successfully";

			FacesUtils.setInfoMessageDialog(headerMessage, bodyMessage);
		}catch(Exception e){
			String headerMessage = "Error";
			String bodyMessage = "Edit Commision Tiering Failed";

			FacesUtils.setErrorMessageDialog(headerMessage, bodyMessage);
		}
	}

	public void doSave() {
		if (maxUpperLimit) commisionTiering.setUpperLimit(new BigDecimal(-1));
		if (this.action != null && this.action.equals("edit")) {
			editCommisionTiering(commisionTiering);
		}else{
			createCommisionTiering(commisionTiering);
		}
	}

	public void doSearch() {

		status = new ArrayList<Integer>();
		if (!active && !inactive) {
			status.add(Constants.STATUS_ACTIVE);
			status.add(Constants.STATUS_INACTIVE);
		} else {
			if (active) {
				status.add(Constants.STATUS_ACTIVE);
			}
			if (inactive) {
				status.add(Constants.STATUS_INACTIVE);
			}
		}

		approvalStatus = new ArrayList<String>();
		approvalStatus.add(Constants.NO_APPROVAL_STATUS);
		FacesUtils.resetPage("commisionTieringForm:commisionTieringTable");
		commisionTieringList = new CommisionTieringDataModel();

	}


	public void viewDetailedDelete(CommisionTiering commisionTiering) {

		this.commisionTiering = commisionTiering;

		RequestContext.getCurrentInstance().execute("PF('dlgConfirm').show()");

	}

	public void deleteCommisionTiering() {

		try{
			commisionTieringService.remove(commisionTiering);
			String headerMessage = "Success";
			String bodyMessage = "Delete Commision Tiering Successfully";

			FacesUtils.setInfoMessageDialog(headerMessage, bodyMessage);
		}catch(Exception e){
			String headerMessage = "Error";
			String bodyMessage = "Delete Commision Tiering Failed";

			FacesUtils.setErrorMessageDialog(headerMessage, bodyMessage);
		}

	}

	public void changeStatusFeatureMapping() {

		try{
			commisionTiering.setChangeDate(HelperUtils.getCurrentDateTime());
			commisionTiering.setChangeWho(sessionService.getUser().getUserName());
			commisionTiering.setStatus(commisionTiering.getStatus()==Constants.STATUS_ACTIVE?Constants.STATUS_INACTIVE:Constants.STATUS_ACTIVE);
			commisionTieringService.update(commisionTiering);
			String headerMessage = "Success";
			String bodyMessage = "Change Status Feature Mapping Successfully";

			FacesUtils.setInfoMessageDialog(headerMessage, bodyMessage);
		}catch(Exception e){
			String headerMessage = "Error";
			String bodyMessage = "Change Status Feature Mapping Failed";

			FacesUtils.setErrorMessageDialog(headerMessage, bodyMessage);
		}

	}	

	public void viewDetailedChangeStatus(CommisionTiering commisionTiering) {

		this.commisionTiering = commisionTiering;
		RequestContext.getCurrentInstance().execute("PF('dlgConfirmChangeStatus').show()");

	}

	public boolean isOverLapping(){
		Map<Object, Object> condition = new HashMap<>();
		condition.put("featureMapping", commisionTiering.getFeatureMapping());
		condition.put("status", 1);
		Criterion cr = Restrictions.allEq(condition);
		List<CommisionTiering> list = commisionTieringService.selectByCriteria(cr);
		BigDecimal max = BigDecimal.valueOf(-1);
		double start1 = commisionTiering.getLowerLimit().doubleValue();
		double end1 = commisionTiering.getUpperLimit().doubleValue();
		if(commisionTiering.getUpperLimit().compareTo(max) == 0){
			end1 = Double.POSITIVE_INFINITY;
		}
		for (Iterator iterator = list.iterator(); iterator.hasNext();) {
			CommisionTiering com = (CommisionTiering) iterator.next();
			if(com.status.equals(Integer.valueOf(1))){
				double start2 = com.getLowerLimit().doubleValue();
				double end2 = com.getUpperLimit().doubleValue();
				if(com.getUpperLimit().compareTo(max) == 0){
					end2 = Double.POSITIVE_INFINITY;
				}
				System.out.println("comtier1 : "+start1+"-"+end1);
				System.out.println("comtier2 : "+start2+"-"+end2);
				if(((end1 > start2)) && ((end2 > start1))){
					return true;
				}
			}
		}
		return false;
	}

	public void changeStatusCommisionTiering() {

		try{
			if(commisionTiering.getStatus()==Constants.STATUS_INACTIVE){
				if(isOverLapping()){
					throw new Exception("There is overlapping values");
				}
			}
			commisionTiering.setChangeDate(HelperUtils.getCurrentDateTime());
			commisionTiering.setChangeWho(sessionService.getUser().getUserName());
			commisionTiering.setStatus(commisionTiering.getStatus()==Constants.STATUS_ACTIVE?Constants.STATUS_INACTIVE:Constants.STATUS_ACTIVE);
			commisionTieringService.update(commisionTiering);
			String headerMessage = "Success";
			String bodyMessage = "Change Status Commision Tiering Successfully";

			FacesUtils.setInfoMessageDialog(headerMessage, bodyMessage);

		}catch(Exception e){
			String headerMessage = "Error";
			String bodyMessage = "Change Status Commision Tiering Failed";
			if(e.getMessage().equalsIgnoreCase("There is overlapping values")){
				bodyMessage = "Change Status Commision Tiering Failed. There is overlapping values";
			}

			FacesUtils.setErrorMessageDialog(headerMessage, bodyMessage);
		}

	}	

	private class CommisionTieringDataModel extends LazyDataModel<CommisionTiering> {
		private static final long serialVersionUID = 1L;

		public CommisionTieringDataModel() {

		}

		@Override
		public List<CommisionTiering> load(int startingAt, int maxResult, String sortField, SortOrder sortOrder, Map<String, Object> filters) {

			List<CommisionTiering> commisionTiering = null;

			commisionTiering = commisionTieringService.selectByCriteria(startingAt, maxResult, code, featureMapping, status, approvalStatus);
			Integer jmlAll = commisionTieringService.countByCriteria(code, featureMapping, status, approvalStatus);
			setRowCount(jmlAll);
			setPageSize(maxResult);

			return commisionTiering;
		}

	}
	
	public String getIdentification(FeatureMapping fm){
		String fitur = fm.getFeature().getCode()+" "+fm.getFeature().getName();
		String agg = fm.getAggregator().getCode()+" "+fm.getAggregator().getName();
		String ca = fm.getCollectingAgent().getCode()+" "+fm.getCollectingAgent().getName();
		return fitur+"\n"+agg+"\n"+ca;
	}


}
