package com.bjb.web.bean;

import java.io.Serializable;
import java.io.StringReader;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.xml.parsers.DocumentBuilderFactory;

import org.primefaces.json.JSONObject;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.w3c.dom.Node;
import org.w3c.dom.bootstrap.DOMImplementationRegistry;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSSerializer;
import org.xml.sax.InputSource;

import com.bjb.model.MessageLog;
import com.bjb.service.MessageLogService;
import com.bjb.service.SessionService;
import com.bjb.web.helper.FacesUtils;

@ManagedBean
@ViewScoped
public class MessageLogHandler implements Serializable {
	private static final long serialVersionUID = 1L;

	private MessageLog messageLog;
	private MessageLog messageLogTemp;
	private MessageLog messageLogSearch;

	private String action;

	private Date startDate = new Date();
	private Date endDate = new Date();

	private boolean saveMessageLog;

	private LazyDataModel<MessageLog> messageLogList;

	private boolean doShowSearch = false;

	private boolean doShowHistory = false;

	@ManagedProperty(value = "#{sessionService}")
	private SessionService sessionService;

	@ManagedProperty(value = "#{messageLogService}")
	private MessageLogService messageLogService;

	public void setMessageLogService(MessageLogService messageLogService) {
		this.messageLogService = messageLogService;
	}

	@PostConstruct
	public void init() {
		sessionService.authMenu(sessionService.getUser().getId(), "viewLogMessage");

		messageLogList = new MessageLogDataModel();

		messageLogSearch = new MessageLog();

		doSearch();

	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public void showHistory() {
		doShowHistory = true;
	}

	public void hideHistory() {
		doShowHistory = false;
	}

	public boolean isDoShowHistory() {
		return doShowHistory;
	}

	public void setDoShowHistory(boolean doShowHistory) {
		this.doShowHistory = doShowHistory;
	}

	public SessionService getSessionBean() {
		return sessionService;
	}

	public void setSessionService(SessionService sessionService) {
		this.sessionService = sessionService;
	}

	public boolean isSaveMessageLog() {
		return saveMessageLog;
	}

	public void setSaveMessageLog(boolean saveMessageLog) {
		this.saveMessageLog = saveMessageLog;
	}

	public MessageLog getMessageLog() {
		return messageLog;
	}

	public MessageLog getMessageLogTemp() {
		return messageLogTemp;
	}

	public void setMessageLogTemp(MessageLog messageLogTemp) {
		this.messageLogTemp = messageLogTemp;
	}

	public void setMessageLog(MessageLog messageLog) {
		this.messageLog = messageLog;
	}

	public MessageLog getMessageLogSearch() {
		return messageLogSearch;
	}

	public void setMessageLogSearch(MessageLog messageLogSearch) {
		this.messageLogSearch = messageLogSearch;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public LazyDataModel<MessageLog> getMessageLogList() {
		return messageLogList;
	}

	public void setMessageLogList(LazyDataModel<MessageLog> messageLogList) {
		this.messageLogList = messageLogList;
	}

	public boolean isDoShowSearch() {
		return doShowSearch;
	}

	public void setDoShowSearch(boolean doShowSearch) {
		this.doShowSearch = doShowSearch;
	}

	public void showSearch() {
		doShowSearch = true;
	}

	public void hideSearch() {
		doShowSearch = false;
	}

	public void doSearch() {

		FacesUtils.resetPage("messageLogForm:messageLogTable");
		messageLogList = new MessageLogDataModel();

	}

	private class MessageLogDataModel extends LazyDataModel<MessageLog> {
		private static final long serialVersionUID = 1L;

		public MessageLogDataModel() {

		}

		@Override
		public List<MessageLog> load(int startingAt, int maxResult, String sortField, SortOrder sortOrder,
				Map<String, Object> filters) {

			List<MessageLog> messageLog = null;
			messageLog = messageLogService.selectByCriteria(startingAt, maxResult, startDate, endDate,
					messageLogSearch);
			Integer jmlAll = messageLogService.countByCriteria(startDate, endDate, messageLogSearch);
			setRowCount(jmlAll);
			setPageSize(maxResult);

			return messageLog;
		}

	}

	public String pretify(String a) {
		if (a.charAt(0) == '{') {
			JSONObject json = new JSONObject(a);
			return json.toString(2);
		} else if (a.charAt(0) == '<') {
			try {
				InputSource src = new InputSource(new StringReader(a));
				Node document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(src)
						.getDocumentElement();
				Boolean keepDeclaration = Boolean.valueOf(a.startsWith("<?xml"));

				// May need this:
				// System.setProperty(DOMImplementationRegistry.PROPERTY,"com.sun.org.apache.xerces.internal.dom.DOMImplementationSourceImpl");
				DOMImplementationRegistry registry = DOMImplementationRegistry.newInstance();
				DOMImplementationLS impl = (DOMImplementationLS) registry.getDOMImplementation("LS");
				LSSerializer writer = impl.createLSSerializer();

				writer.getDomConfig().setParameter("format-pretty-print", Boolean.TRUE); // Set
																							// this
																							// to
																							// true
																							// if
																							// the
																							// output
																							// needs
																							// to
																							// be
																							// beautified.
				writer.getDomConfig().setParameter("xml-declaration", false); // Set
																						// this
																						// to
																						// true
																						// if
																						// the
																						// declaration
																						// is
																						// needed
																						// to
																						// be
																						// outputted.

				return writer.writeToString(document);
			} catch (Exception e) {
				return "error";
			}
		} else
			return a;
	}

}
