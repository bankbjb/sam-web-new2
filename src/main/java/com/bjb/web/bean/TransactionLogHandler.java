package com.bjb.web.bean;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.bjb.model.Aggregator;
import com.bjb.model.TransactionLog;
import com.bjb.model.UserAggregator;
import com.bjb.service.AggregatorService;
import com.bjb.service.SessionService;
import com.bjb.service.TransactionLogService;
import com.bjb.support.TemplateService;
import com.bjb.web.helper.FacesUtils;

@ManagedBean
@ViewScoped
public class TransactionLogHandler implements Serializable {
	private static final long serialVersionUID = 1L;

	private TransactionLog transactionLog;
	private TransactionLog transactionLogTemp;
	private TransactionLog transactionLogSearch;

	private String action;

	private Date startDate = new Date();
	private Date endDate = new Date();
	
	private boolean saveTransactionLog;

	private LazyDataModel<TransactionLog> transactionLogList;

	private boolean doShowSearch = false;

	private boolean doShowHistory = false;
	
	private String aggregatorCode = null;

	@ManagedProperty(value="#{sessionService}")
	private SessionService sessionService;
	
	@ManagedProperty(value="#{aggregatorService}")
	private AggregatorService aggregatorService;

	@ManagedProperty(value="#{transactionLogService}")
	private TransactionLogService transactionLogService;

	@ManagedProperty(value="#{templateService}")
	private TemplateService templateService;

	public void setAggregatorService(AggregatorService aggregatorService) {
		this.aggregatorService = aggregatorService;
	}
	
	public void setTransactionLogService(TransactionLogService transactionLogService) {
		this.transactionLogService = transactionLogService;
	}

	public void setTemplateService(TemplateService templateService) {
		this.templateService = templateService;
	}

	@PostConstruct
	public void init() {
		
		sessionService.authMenu(sessionService.getUser().getId(), "viewLogTransaction");
		
		if (sessionService.getUser() instanceof UserAggregator) {
			BigDecimal aggregatorId = ((UserAggregator)sessionService.getUser()).getAggregatorId();
			Aggregator aggregator = aggregatorService.find(aggregatorId);
			if (aggregator!=null) {
				aggregatorCode = aggregator.getCode();
			}
		} 
		doSearch();
		transactionLogSearch = new TransactionLog();
	}

	public boolean isUserAggregator() {
		return aggregatorCode!=null;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public void showHistory() {
		doShowHistory = true;
	}

	public void hideHistory() {
		doShowHistory = false;
	}

	public boolean isDoShowHistory() {
		return doShowHistory;
	}

	public void setDoShowHistory(boolean doShowHistory) {
		this.doShowHistory = doShowHistory;
	}

	public SessionService getSessionBean() {
		return sessionService;
	}

	public void setSessionService(SessionService sessionService) {
		this.sessionService = sessionService;
	}

	public boolean isSaveTransactionLog() {
		return saveTransactionLog;
	}

	public void setSaveTransactionLog(boolean saveTransactionLog) {
		this.saveTransactionLog = saveTransactionLog;
	}

	public TransactionLog getTransactionLog() {
		return transactionLog;
	}

	public TransactionLog getTransactionLogTemp() {
		return transactionLogTemp;
	}

	public void setTransactionLogTemp(TransactionLog transactionLogTemp) {
		this.transactionLogTemp = transactionLogTemp;
	}

	public TransactionLog getTransactionLogSearch() {
		return transactionLogSearch;
	}

	public void setTransactionLogSearch(TransactionLog transactionLogSearch) {
		this.transactionLogSearch = transactionLogSearch;
	}

	public void setTransactionLog(TransactionLog transactionLog) {
		this.transactionLog = transactionLog;
	}
	
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public LazyDataModel<TransactionLog> getTransactionLogList() {
		return transactionLogList;
	}

	public void setTransactionLogList(LazyDataModel<TransactionLog> transactionLogList) {
		this.transactionLogList = transactionLogList;
	}

	public boolean isDoShowSearch() {
		return doShowSearch;
	}

	public void setDoShowSearch(boolean doShowSearch) {
		this.doShowSearch = doShowSearch;
	}

	public void showSearch() {
		doShowSearch = true;
	}

	public void hideSearch() {
		doShowSearch = false;
	}

	public void doSearch() {

		FacesUtils.resetPage("transactionLogForm:transactionLogTable");
		transactionLogList = new TransactionLogDataModel();

	}

	public void exportToTxt() throws IOException {

	    FacesContext fc = FacesContext.getCurrentInstance();
	    ExternalContext ec = fc.getExternalContext();

	    ec.setResponseContentType("text/plain");
	    ec.setResponseCharacterEncoding("UTF-8");
	    ec.setResponseHeader("Content-Disposition", "attachment; filename=\"" + "transaction.txt" + "\""); 

	    InputStream is = this.getClass().getClassLoader().getResourceAsStream("/template/viewLogTransaction.txt");
	    java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
	    String template = s.hasNext() ? s.next() : "";
		is.close();

		List<TransactionLog> transactionLogs = transactionLogService.selectByCriteria(0, Integer.MAX_VALUE, aggregatorCode, startDate, endDate, transactionLogSearch);
		for (TransactionLog item:transactionLogs) {
		    String line = templateService.evaluate(template, item) + "\r\n";
		    ec.getResponseOutputWriter().write(line);
		}
	    fc.responseComplete(); 
	}

	private class TransactionLogDataModel extends LazyDataModel<TransactionLog> {
		private static final long serialVersionUID = 1L;

		public TransactionLogDataModel() {

		}

		@Override
	    public List<TransactionLog> load(int startingAt, int maxResult, String sortField, SortOrder sortOrder, Map<String, Object> filters) {

			List<TransactionLog> transactionLog = transactionLogService.selectByCriteria(startingAt, maxResult, aggregatorCode, startDate, endDate, transactionLogSearch);
			Integer jmlAll = transactionLogService.countByCriteria(aggregatorCode, startDate, endDate, transactionLogSearch);
			setRowCount(jmlAll);
			setPageSize(maxResult);

			return transactionLog;
		}

	}

}
