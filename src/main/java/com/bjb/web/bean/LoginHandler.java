package com.bjb.web.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.servlet.http.HttpServletRequest;

import com.bjb.constant.LoginConstants;
import com.bjb.model.User;
import com.bjb.model.UserBank;
import com.bjb.service.SessionService;
import com.bjb.service.SystemParameterService;
import com.bjb.service.UserService;
import com.bjb.util.ChaptaKeyResolver;
import com.bjb.web.helper.FacesUtils;

@ManagedBean
@RequestScoped
@FacesValidator(value = "captchaValidator")
public class LoginHandler implements Serializable, Validator {
	private static final long serialVersionUID = 1L;
	
	private static Map<Integer, String> errorMap = new HashMap<Integer, String>();
	
	private User user = new User();
	private String chImage;
	
	private ExternalContext externalContext;
	private HttpServletRequest httpServletRequest;
	private String sessionId;
	
//	@ManagedProperty(value = "#{id}")
	private String userName;

	private String plainPassword;
	private String plainNewPassword;
	private String confirmPassword;

	private String password;
	private String newPassword;
	
	private boolean isSuccess;
	
	@ManagedProperty(value="#{sessionService}")
	private SessionService sessionService;
	
	@ManagedProperty(value="#{userService}")
	private UserService userService;

	@ManagedProperty(value="#{systemParameterService}")
	private SystemParameterService systemParameterService;

	static {
		errorMap.put(LoginConstants.ERROR_CONNECTION, "Connection to UIM error. Please contact administrator.");
		errorMap.put(LoginConstants.ERROR_USER_NOT_ACTIVE, "Username is not active.");
		errorMap.put(LoginConstants.ERROR_USER_LOCKED, "User is locked. Please contact administrator.");
		errorMap.put(LoginConstants.ERROR_USER_HAS_BEEN_DELETED, "Username no longer exists.");				
		errorMap.put(LoginConstants.ERROR_USER_NOT_FOUND, "Username / Password is invalid");
		errorMap.put(LoginConstants.ERROR_PASSWORD_WRONG, "Username / Password is invalid");
		errorMap.put(LoginConstants.ERROR_ACTION_NOT_APPLICABLE, "You are not allowed the perform this action. Please contact administrator.");
		errorMap.put(LoginConstants.ERROR_APPLICATION_UNREGISTERED, "System error. Please contact administrator.");
		errorMap.put(LoginConstants.ERROR_PASSWORD_HAS_BEEN_USED, "New password has been used. Choose other password.");				
		errorMap.put(LoginConstants.ERROR_USER_NOT_FOUND, "User is not found. Please contact administrator");				
	}
	
	public void setSessionService(SessionService sessionService) {
		this.sessionService = sessionService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}
	
	public void setSystemParameterService(SystemParameterService systemParameterService){
		this.systemParameterService = systemParameterService;
	}

	@PostConstruct
	public void init(){
	}
	
	public LoginHandler() {
		externalContext = FacesContext.getCurrentInstance().getExternalContext();

		httpServletRequest = (HttpServletRequest) externalContext.getRequest();
		
		sessionId = httpServletRequest.getSession().getId();
	}
	
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPlainPassword() {
		return plainPassword;
	}

	public String getPlainNewPassword() {
		return plainNewPassword;
	}

	public void setPlainNewPassword(String plainNewPassword) {
		this.plainNewPassword = plainNewPassword;
	}

	public void setPlainPassword(String plainPassword) {
		this.plainPassword = plainPassword;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	String validate;

	public String getChImage() {
		return chImage;
	}

	public void setChImage(String chImage) {
		this.chImage = chImage;
	}

	public String getValidate() {
		return validate;
	}

	public void setValidate(String validate) {
		this.validate = validate;
	}

	public void keepUserSessionAlive() {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		request.getSession();
	}


	public String doLogin() {
		Object[] result = userService.loginUserBank(getUserName(), password);
		Integer status = (Integer) result[0];
		User user = (User) result[1];
		if (status == LoginConstants.SUCCESS ) {
				
			// put in session and send welcome
			FacesUtils.getExternalContext().getSessionMap().put("user", user);
			sessionService.setUser(user);
			sessionService.buildMenu(user);
			return "welcome?faces-redirect=true";

		} else if (status == LoginConstants.ERROR_USER_NOT_FOUND) {
			
			// user is not registered in uim, try our database
			result = userService.loginUserAggregator(getUserName(), password);
			
			status = (Integer) result[0];
			user = (User) result[1];
			
			if (status==LoginConstants.SUCCESS) {
				// put in session and send welcome
				FacesUtils.getExternalContext().getSessionMap().put("user", user);
				sessionService.setUser(user);
				sessionService.buildMenu(user);
				return "welcome?faces-redirect=true";
			} else if (status == LoginConstants.ERROR_PASSWORD_EXPIRED) {
				return "pleaseChangePassword?faces-redirect=true&id="+userName;
			} else {
				FacesUtils.setErrorMessage("msg", errorMap.get(status), null);				
			}
		} else if (status == LoginConstants.ERROR_PASSWORD_EXPIRED) {
			return "pleaseChangePassword?faces-redirect=true&id="+userName;
		} else {
			FacesUtils.setErrorMessage("msg", errorMap.get(status), null);				
		}
		return null;
	}

	public String changePassword() {
		password = "";
		newPassword = "";
		confirmPassword = "";
		return "changePassword?faces-redirect=true&id="+this.user.getUserName();
	}

	public String doChangePassword() {
		if ( !confirmPassword.equals(plainNewPassword) ) {
			FacesUtils.setErrorMessage("msg", "Confirm password must same with new password.", null);				
			return null;
		};
		User user = sessionService.getUser();
		if ( user==null ) {
			// this case came from force change password page, try to change password in UIM 
			Integer status = userService.changePasswordUserBank(userName, password, newPassword, true);
			if (status == LoginConstants.SUCCESS) {
				password = newPassword;
				return doLogin();
			} else if (status == LoginConstants.ERROR_USER_NOT_FOUND) {
				status = userService.changePasswordUserAggregator(userName, password, newPassword, true);
				if (status == LoginConstants.SUCCESS) {
					password = newPassword;
					return doLogin();
				} else {
					FacesUtils.setErrorMessage("msg", errorMap.get(status), null);				
				}
			}			
		} else if (user instanceof UserBank) {
			Integer status = userService.changePasswordUserBank(user.getUserName(), password, newPassword, false);
			if (status == LoginConstants.SUCCESS) {
				// do nothing, just redirect to success page
				return "welcome?faces-redirect=true";
			} else {
				FacesUtils.setErrorMessage("msg", errorMap.get(status), null);				
			}
		} else {
			Integer status = userService.changePasswordUserAggregator(user.getUserName(), password, newPassword, false);
			if (status == LoginConstants.SUCCESS) {
				// do nothing, just redirect to success page
				return "welcome?faces-redirect=true";
			} else {
				FacesUtils.setErrorMessage("msg", errorMap.get(status), null);				
			}
		}
		return null;
	}
	
	public String forgetPassword() {
		userName = "";
		return "forgetPassword?faces-redirect=true";
	}
	
	public String doForgetPassword() {
		User user = sessionService.getUser();
		if ( user==null ) {
			Integer status = userService.forgetPasswordUserAggregator(userName);
			if (status == LoginConstants.SUCCESS) {
				isSuccess = true;
			} else {
				FacesUtils.setErrorMessage("msg", errorMap.get(status), null);				
			}			
		} else {
			FacesUtils.setErrorMessage("msg", errorMap.get(LoginConstants.ERROR_ACTION_NOT_APPLICABLE), null);				
		}
		return null;
	}

	public String doLogout() {
		FacesUtils.getExternalContext().getSessionMap().remove("user");
		return "login?faces-redirect=true";
	}
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public void validate(FacesContext context, UIComponent component,
	Object value) throws ValidatorException {
		String userInput = (String) value;
		String captcha = ChaptaKeyResolver.getInstance().chaptchaTemp.get(sessionId);

		if (userInput == null || userInput == "" || captcha == null || !userInput.equals(captcha)) {
			System.out.println("capcha is active");
			throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Wrong Captcha", 
                    null));
                   
		}
//        System.out.println("capcha is not active and not required!");
       
		ChaptaKeyResolver.getInstance().chaptchaTemp.remove(sessionId);

	}

	public String getPasswordRegex() {
		Integer minLen = Integer.parseInt(systemParameterService.getSystemParameter("MIN_PASSWORD_LENGTH", "6"));
		Integer maxLen = Integer.parseInt(systemParameterService.getSystemParameter("MAX_PASSWORD_LENGTH", "30"));
		return "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{"+minLen+","+maxLen+"})";
	}

	public String getPasswordValidationMessage() {
		Integer minLen = Integer.parseInt(systemParameterService.getSystemParameter("MIN_PASSWORD_LENGTH", "6"));
		return "Password harus mengandung huruf besar, huruf kecil, angka dan minimal "+minLen+" karakter";
	}

	public boolean isSuccess() {
		return isSuccess;
	}

}