package com.bjb.web.bean;

import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.NumberFormat;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.bjb.constant.Constants;
import com.bjb.integration.middleware.MiddlewareService;
import com.bjb.model.Aggregator;
import com.bjb.model.UserAggregator;
import com.bjb.service.AggregatorService;
import com.bjb.service.SessionService;

@ManagedBean
@ViewScoped
public class AggregatorProfileHandler implements Serializable {
	private static final long serialVersionUID = 1L;

	private Aggregator aggregator;
	
	@ManagedProperty(value="#{sessionService}")
	private SessionService sessionService;
	
	@ManagedProperty(value="#{aggregatorService}")
	private AggregatorService aggregatorService;
	
	@ManagedProperty(value="#{middlewareService}")
	private MiddlewareService middlewareService;
	
	public void setMiddlewareService(MiddlewareService middlewareService) {
		this.middlewareService = middlewareService;
	}

	public void setAggregatorService(AggregatorService aggregatorService) {
		this.aggregatorService = aggregatorService;
	}

	@PostConstruct
	public void init() {
		sessionService.authMenu(sessionService.getUser().getId(), "manageAggregatorProfile");
		if (sessionService.getUser() instanceof UserAggregator){
			UserAggregator userAggregator = (UserAggregator) sessionService.getUser();
			setAggregator(aggregatorService.find(userAggregator.getAggregatorId()));
		} else {
			try {
				FacesContext.getCurrentInstance().getExternalContext().redirect("welcome"+".jsf");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void setSessionService(SessionService sessionService) {
		this.sessionService = sessionService;
	}

	public Aggregator getAggregator() {
		return aggregator;
	}

	public void setAggregator(Aggregator aggregator) {
		this.aggregator = aggregator;
	}

	public String getBalance() {
		BigDecimal balance = middlewareService.getBalance(aggregator.getCode());
		if (balance == null) {
			return "N/A";
		}
		String nf = NumberFormat.getInstance().format(balance);
		return nf;
	}
	
	public boolean isNotificationStatus() {
		return (aggregator.getStatusNotification() == Constants.STATUS_ACTIVE);
	}

	public void setNotificationStatus(boolean notificationStatus) {
		aggregator.setStatusNotification(notificationStatus?Constants.STATUS_ACTIVE:Constants.STATUS_INACTIVE) ;
	}

	public void updateAggregator() {
		aggregatorService.update(aggregator);
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Notification status changed."));
	}
	

}
