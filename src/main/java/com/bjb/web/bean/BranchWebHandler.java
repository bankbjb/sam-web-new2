package com.bjb.web.bean;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.TreeNode;

import com.bjb.constant.Constants;
import com.bjb.model.Branch;
import com.bjb.service.BranchService;
import com.bjb.service.SessionService;
import com.bjb.util.HelperUtils;
import com.bjb.web.bean.support.DiffService;
import com.bjb.web.helper.FacesUtils;

@ManagedBean
@ViewScoped
public class BranchWebHandler implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private Branch branch;
	
	private Branch branchTemp;

	private LazyDataModel<Branch> branchList;
	
	private String action;
	
	private String branchId = "";
	private String branchCode = "";
	private String branchName = "";

	private List<Integer> status;;
	private List<String> approvalStatus;

	private boolean active;
	private boolean inactive;

	private boolean pending;
	private boolean approved;
	private boolean rejected;
	
	private boolean doShowSearch = false;
	
	private boolean doShowHistory = false;

	private String currentBranchCode;
	
	@ManagedProperty(value="#{sessionService}")
	private SessionService sessionService;
	
	@ManagedProperty(value="#{branchService}")
	private BranchService branchService;

	@ManagedProperty(value="#{diffService}")
	private DiffService diffService;
	
	public void setDiffService(DiffService diffService) {
		this.diffService = diffService;
	}

	public TreeNode getDiffTreeTable() {
		return diffService.diff(branchTemp, branch);
	}
	
	public void setSessionService(SessionService sessionService) {
		this.sessionService = sessionService;
	}

	public void setBranchService(BranchService branchService) {
		this.branchService = branchService;
	}

	@PostConstruct
	public void init(){
		sessionService.authMenu(sessionService.getUser().getId(), "branchWebAdministration");

		doSearch();
	}
	
	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}
	
	public Branch getBranchTemp() {
		return branchTemp;
	}

	public void setBranchTemp(Branch branchTemp) {
		this.branchTemp = branchTemp;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public LazyDataModel<Branch> getBranchList() {
		return branchList;
	}

	public void setBranchList(LazyDataModel<Branch> branchList) {
		this.branchList = branchList;
	}
	
	public String getBranchId() {
		return branchId;
	}

	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	
	public List<String> getStatus() {
		return approvalStatus;
	}

	public void setStatus(List<String> status) {
		this.approvalStatus = status;
	}

	public boolean isPending() {
		return pending;
	}

	public void setPending(boolean pending) {
		this.pending = pending;
	}

	public boolean isApproved() {
		return approved;
	}

	public void setApproved(boolean approved) {
		this.approved = approved;
	}

	public boolean isRejected() {
		return rejected;
	}

	public void setRejected(boolean rejected) {
		this.rejected = rejected;
	}

	public boolean isDoShowSearch() {
		return doShowSearch;
	}

	public void setDoShowSearch(boolean doShowSearch) {
		this.doShowSearch = doShowSearch;
	}

	public void showSearch(){
		doShowSearch = true;
	}
	
	public void hideSearch(){
		doShowSearch = false;
	}
	
	
	public void showHistory(){
		doShowHistory = true;
	}
	
	public void hideHistory(){
		doShowHistory = false;
	}
	
	public boolean isDoShowHistory() {
		return doShowHistory;
	}

	public void setDoShowHistory(boolean doShowHistory) {
		this.doShowHistory = doShowHistory;
	}
	
	public void doSearch(){
		status = new ArrayList<Integer>();
		if (!active && !inactive) {
			status.add(Constants.STATUS_ACTIVE);
			status.add(Constants.STATUS_INACTIVE);
		} else {
			if (active) {
				status.add(Constants.STATUS_ACTIVE);
			}
			if (inactive) {
				status.add(Constants.STATUS_INACTIVE);
			}
		}
		approvalStatus = new ArrayList<String>();

		if (!pending && !approved && !rejected) {
			approvalStatus.add(Constants.APPROVAL_STATUS_PENDING_CREATE);
			approvalStatus.add(Constants.APPROVAL_STATUS_PENDING_EDIT);
			approvalStatus.add(Constants.APPROVAL_STATUS_PENDING_DELETE);
			approvalStatus.add(Constants.APPROVAL_STATUS_APPROVED_CREATE);
			approvalStatus.add(Constants.APPROVAL_STATUS_APPROVED_EDIT);
			approvalStatus.add(Constants.APPROVAL_STATUS_REJECTED_CREATE);
			approvalStatus.add(Constants.APPROVAL_STATUS_REJECTED_EDIT);
			approvalStatus.add(Constants.APPROVAL_STATUS_REJECTED_DELETE);
		}else{
			if (pending == true){
				approvalStatus.add(Constants.APPROVAL_STATUS_PENDING_CREATE);
				approvalStatus.add(Constants.APPROVAL_STATUS_PENDING_EDIT);
				approvalStatus.add(Constants.APPROVAL_STATUS_PENDING_DELETE);
			}
			if (approved == true){
				approvalStatus.add(Constants.APPROVAL_STATUS_APPROVED_CREATE);
				approvalStatus.add(Constants.APPROVAL_STATUS_APPROVED_EDIT);
			}
			if (rejected == true){
				approvalStatus.add(Constants.APPROVAL_STATUS_REJECTED_CREATE);
				approvalStatus.add(Constants.APPROVAL_STATUS_REJECTED_EDIT);
				approvalStatus.add(Constants.APPROVAL_STATUS_REJECTED_DELETE);
			}
		}

		FacesUtils.resetPage("branchForm:branchTable");
		branchList = new BranchDataModel();
		
	}

	public void newBranch(){
		this.branch = new Branch();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd-hhmm-sss");
		Date date = new Date();
		String id = sdf.format(date);
		String seq = branchService.generateId();
		this.branch.setCode("BRH-"+id+String.format("%04d", seq));
		this.action = "";
	}
	
	public void doSave(){
		if (this.action!=null && this.action.equals("edit")) {
			editBranch(this.branch);
		} else {
			try {

				Branch branchExist = branchService
						.findByCode(
								this.branch.getName());

				if (branchExist == null
						|| (branchExist.getName()
								.equals(currentBranchCode))) {
					createBranch(this.branch);
				} else {
					String headerMessage = "Attention";
					String bodyMessage = "Branch "
							+ this.branch.getName()
							+ " already exist in the application.";

					FacesUtils.setWarningMessageDialog(headerMessage,
							bodyMessage);
				}

			} catch (Exception e) {
				String headerMessage = "Error";
				String bodyMessage = "System error";

				FacesUtils.setErrorMessageDialog(headerMessage, bodyMessage);
				e.printStackTrace();
			}
		}
			
	}

	public void editBranch(Branch branch){

		branch = branchService.requestToModify(branchTemp, branch);

		RequestContext.getCurrentInstance().execute("PF('insertOrEditBranch').hide()");
		RequestContext.getCurrentInstance().execute("PF('insertOrEditBranchPending').hide()");
		
		
		String headerMessage = "Edit Branch Successfully";
		String bodyMessage = "Supervisor must approve the editing of the <br/>branch &apos;"
				+ branch.getName() + "&apos; to completely the process.";

		FacesUtils.setInfoMessageDialog(headerMessage, bodyMessage);
	}
	
	public void createBranch(Branch branch){		
		branchService.requestToCreate(branch);

		RequestContext.getCurrentInstance().execute("PF('insertOrEditBranch').hide()");
		RequestContext.getCurrentInstance().execute("PF('insertOrEditBranchPending').hide()");
		
		String headerMessage = "Create Branch Successfully";
		String bodyMessage = "Supervisor must approve the creating of the <br/>branch &apos;"
				+ branch.getName() + "&apos; to completely the process.";

		FacesUtils.setInfoMessageDialog(headerMessage, bodyMessage);
	}
	
	public void viewDetailedBranch(Branch branch){
		this.branch = branch;

		if (this.branch.getApprovalStatus().equals(
				Constants.APPROVAL_STATUS_PENDING_EDIT)) {
			
			this.branchTemp = branchService.findPendingApprovalByCode(branch.getCode());
					
			if (branchTemp!=null) {
				RequestContext.getCurrentInstance().execute(
							"PF('viewBranchEdit').show()");
				return;
			}
						
		} 
		RequestContext.getCurrentInstance().execute("PF('viewBranch').show()");

	}
	
	public void viewDetailedBranchEdit(Branch branch){
		
		this.branch = branch;

		if (this.branch.getDisableEdit() == true) {
			FacesUtils.setApprovalWarning("parameter",
					this.branch.getName(),
					this.branch.getApprovalStatus());
		} else {
			this.action = "edit";
			if (this.branch.getApprovalStatus().equals(
					Constants.APPROVAL_STATUS_PENDING_EDIT)) {

				this.branchTemp = branchService.findPendingApprovalByCode(branch.getCode());

				if (branchTemp != null) {
					currentBranchCode = this.branchTemp.getCode();
					RequestContext.getCurrentInstance().execute(
							"PF('insertOrEditBranchPending').show()");
					return;
				} 
			}
			this.branchTemp = HelperUtils.clone(branch);
			RequestContext.getCurrentInstance().execute(
						"PF('insertOrEditBranch').show()");
		}
	}
	
	public void viewDetailedDelete(Branch branch){

		this.branch = branch;
		
		if (this.branch.getDisableDelete() == true){
			FacesUtils.setApprovalWarning("branch", this.branch.getName(), this.branch.getApprovalStatus());
		}else{
			RequestContext.getCurrentInstance().execute(
					"PF('dlgConfirm').show()");
		}
		
	}
	
	public void deleteBranch(){
		
		branchService.requestToRemove(branch);

		if (branch.getApprovalStatus().equals(Constants.APPROVAL_STATUS_REJECTED_CREATE)){

			String headerMessage = "Success";
			String bodyMessage = "Delete Branch " + this.branch.getName() + " Successfully";
	
			FacesUtils.setInfoMessageDialog(headerMessage, bodyMessage);
			
		}else{
		
			String headerMessage = "Delete Branch Successfully";
			String bodyMessage = "Supervisor must approve the deleting of the <br/>branch &apos;"
					+ branch.getName()+ "&apos; to completely the process.";
	
			FacesUtils.setInfoMessageDialog(headerMessage, bodyMessage);
		
		}
		
	}
	
	public void viewDetailedChangeStatus(Branch branch) {

		this.branch = branch;

		if (this.branch.getDisableDelete() == true) {
			FacesUtils.setApprovalWarning("parameter",
					this.branch.getName(),
					this.branch.getApprovalStatus());
		} else {
			RequestContext.getCurrentInstance().execute("PF('dlgConfirmChangeStatus').show()");
		}

	}

	public void changeStatusBranch() {
		
		this.branchTemp = (Branch) HelperUtils.clone(branch);
		
		branch.setStatus(branch.getStatus()==Constants.STATUS_ACTIVE?Constants.STATUS_INACTIVE:Constants.STATUS_ACTIVE);

		branch = branchService.requestToModify(branchTemp, branch);

		String headerMessage = "Change Status rranch Successfully";
		String bodyMessage = "Supervisor must approve the change status of the <br/>branch &apos;"
				+ branch.getName()
				+ "&apos; to completely the process.";

		FacesUtils.setInfoMessageDialog(headerMessage, bodyMessage);

	}

	private class BranchDataModel extends LazyDataModel<Branch>{
		private static final long serialVersionUID = 1L;
		public BranchDataModel() {
			
		}
		@Override
	    public List<Branch> load(int startingAt, int maxResult, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
			
			List<Branch> branchs;
			
			branchs = branchService.getBranchByCategory(startingAt, maxResult, branchCode, branchName, status, approvalStatus);
			Integer jmlAll = branchService.countBranchByCategory(branchCode, branchName, status, approvalStatus);
			setRowCount(jmlAll);
			setPageSize(maxResult);

			return branchs;
		}
	}
	
}