package com.bjb.web.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.TreeNode;

import com.bjb.constant.Constants;
import com.bjb.model.CollectingAgent;
import com.bjb.service.CollectingAgentService;
import com.bjb.service.SessionService;
import com.bjb.util.HelperUtils;
import com.bjb.web.bean.support.DiffService;
import com.bjb.web.helper.FacesUtils;

@ManagedBean
@ViewScoped
public class CollectingAgentHandler implements Serializable {
	private static final long serialVersionUID = 1L;

	private CollectingAgent collectingAgent;
	private CollectingAgent collectingAgentTemp;

	private String action;

	private String code;
	private String name;
	private Date createDate;
	private Integer maxAgent;
	private Integer maxAmount;
	
	private List<Integer> status;

	private boolean active;
	private boolean inactive;

	private boolean pending;
	private boolean approved;
	private boolean rejected;

	private boolean saveCollectingAgent;

	private LazyDataModel<CollectingAgent> collectingAgentList;

	private boolean doShowSearch = false;

	private boolean doShowHistory = false;

	private String currentName;
	
	private List<SelectItem> messageTypeLookup = new ArrayList<SelectItem>();
	
	@ManagedProperty(value="#{sessionService}")
	private SessionService sessionService;
	
	@ManagedProperty(value="#{collectingAgentService}")
	private CollectingAgentService collectingAgentService;

	@ManagedProperty(value="#{diffService}")
	private DiffService diffService;
	
	public void setDiffService(DiffService diffService) {
		this.diffService = diffService;
	}

	public TreeNode getDiffTreeTable() {
		return diffService.diff(collectingAgentTemp, collectingAgent);
	}
	
	public void setCollectingAgentService(CollectingAgentService collectingAgentService) {
		this.collectingAgentService = collectingAgentService;
	}

	@PostConstruct
	public void init() {
		sessionService.authMenu(sessionService.getUser().getId(), "manageCA");

		collectingAgentList = new CollectingAgentDataModel();

		pending = false;
		approved = false;
		rejected = false;
		

		doSearch();
		
		SelectItem selectItem = new SelectItem();
		selectItem.setValue("ISO");
		selectItem.setLabel("ISO");
		this.messageTypeLookup.add(selectItem);
		selectItem = new SelectItem();
		selectItem.setValue("XML");
		selectItem.setLabel("XML");
		this.messageTypeLookup.add(selectItem);
		selectItem = new SelectItem();
		selectItem.setValue("JSON");
		selectItem.setLabel("JSON");
		this.messageTypeLookup.add(selectItem);

	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public boolean isPending() {
		return pending;
	}

	public void setPending(boolean pending) {
		this.pending = pending;
	}

	public boolean isApproved() {
		return approved;
	}

	public void setApproved(boolean approved) {
		this.approved = approved;
	}

	public boolean isRejected() {
		return rejected;
	}

	public void setRejected(boolean rejected) {
		this.rejected = rejected;
	}

	public void showHistory() {
		doShowHistory = true;
	}

	public void hideHistory() {
		doShowHistory = false;
	}

	public boolean isDoShowHistory() {
		return doShowHistory;
	}

	public void setDoShowHistory(boolean doShowHistory) {
		this.doShowHistory = doShowHistory;
	}

	public SessionService getSessionBean() {
		return sessionService;
	}

	public void setSessionService(SessionService sessionService) {
		this.sessionService = sessionService;
	}

	public boolean isSaveCollectingAgent() {
		return saveCollectingAgent;
	}

	public void setSaveCollectingAgent(boolean saveCollectingAgent) {
		this.saveCollectingAgent = saveCollectingAgent;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public CollectingAgent getCollectingAgent() {
		return collectingAgent;
	}

	public CollectingAgent getCollectingAgentTemp() {
		return collectingAgentTemp;
	}

	public void setCollectingAgentTemp(CollectingAgent collectingAgentTemp) {
		this.collectingAgentTemp = collectingAgentTemp;
	}

	public void setCollectingAgent(CollectingAgent collectingAgent) {
		this.collectingAgent = collectingAgent;
	}

	public LazyDataModel<CollectingAgent> getCollectingAgentList() {
		return collectingAgentList;
	}

	public void setCollectingAgentList(LazyDataModel<CollectingAgent> collectingAgentList) {
		this.collectingAgentList = collectingAgentList;
	}

	public boolean isDoShowSearch() {
		return doShowSearch;
	}

	public void setDoShowSearch(boolean doShowSearch) {
		this.doShowSearch = doShowSearch;
	}

	public void showSearch() {
		doShowSearch = true;
	}

	public void hideSearch() {
		doShowSearch = false;
	}
	
	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Integer getMaxAgent() {
		return maxAgent;
	}

	public void setMaxAgent(Integer maxAgent) {
		this.maxAgent = maxAgent;
	}

	public Integer getMaxAmount() {
		return maxAmount;
	}

	public void setMaxAmount(Integer maxAmount) {
		this.maxAmount = maxAmount;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public boolean isInactive() {
		return inactive;
	}

	public void setInactive(boolean inactive) {
		this.inactive = inactive;
	}

	public List<SelectItem> getMessageTypeLookup() {
		return messageTypeLookup;
	}

	public void setMessageTypeLookup(List<SelectItem> messageTypeLookup) {
		this.messageTypeLookup = messageTypeLookup;
	}
	
	public void viewDetailedCollectingAgent(CollectingAgent collectingAgent) {
		this.collectingAgent = collectingAgent;

		RequestContext.getCurrentInstance().execute("PF('viewCollectingAgent').show()");
	}
	
	public void viewDetailedCollectingAgentEdit(CollectingAgent collectingAgent) {

		this.collectingAgentTemp = HelperUtils.clone(collectingAgent);
		this.collectingAgent = collectingAgent;
		this.action = "edit";

		RequestContext.getCurrentInstance().execute("PF('insertOrEditCollectingAgent').show()");
	}

	public void newCollectingAgent() {
		this.collectingAgent = new CollectingAgent();
		BigDecimal seq = collectingAgentService.generateId();
		this.collectingAgent.setId(seq);
		this.collectingAgent.setCode("CA" + String.format("%04d", seq.intValue()));
		this.collectingAgent.setStatus(Constants.STATUS_ACTIVE);
		this.collectingAgent.setCreateDate(new Date());
		this.action = "";
	}

	public void createCollectingAgent(CollectingAgent collectingAgent) {

		try{
			collectingAgentService.create(collectingAgent);
			
			RequestContext.getCurrentInstance().execute(
					"PF('insertOrEditCollectingAgent').hide()");
			
			String headerMessage = "Success";
			String bodyMessage = "Insert Collecting Agent Successfully";

			FacesUtils.setInfoMessageDialog(headerMessage, bodyMessage);
		}catch(Exception e){
			String headerMessage = "Error";
			String bodyMessage = "Insert Feature Mapping Failed";

			FacesUtils.setErrorMessageDialog(headerMessage, bodyMessage);
		}
		
	}

	public void editCollectingAgent(CollectingAgent collectingAgent) {
		
		try{
			collectingAgentService.modify(collectingAgentTemp, collectingAgent);
			
			RequestContext.getCurrentInstance().execute(
					"PF('insertOrEditCollectingAgent').hide()");
			
			String headerMessage = "Success";
			String bodyMessage = "Edit Collecting Agent Successfully";

			FacesUtils.setInfoMessageDialog(headerMessage, bodyMessage);
		}catch(Exception e){
			String headerMessage = "Error";
			String bodyMessage = "Edit Collecting Agent Failed";

			FacesUtils.setErrorMessageDialog(headerMessage, bodyMessage);
		}
	}

	public void doSave() {
		if (this.action != null && this.action.equals("edit")) {
			editCollectingAgent(collectingAgent);
		}else{
			createCollectingAgent(collectingAgent);
		}
	}

	public void doSearch() {

		status = new ArrayList<Integer>();
		if (!active && !inactive) {
			status.add(Constants.STATUS_ACTIVE);
			status.add(Constants.STATUS_INACTIVE);
		} else {
			if (active) {
				status.add(Constants.STATUS_ACTIVE);
			}
			if (inactive) {
				status.add(Constants.STATUS_INACTIVE);
			}
		}
		FacesUtils.resetPage("collectingAgentForm:collectingAgentTable");
		collectingAgentList = new CollectingAgentDataModel();

	}

	public void viewDetailedDelete(CollectingAgent collectingAgent) {

		this.collectingAgent = collectingAgent;

		if (this.collectingAgent.getDisableDelete() == true) {
			FacesUtils.setApprovalWarning("collectingAgent",
					this.collectingAgent.getName(),
					this.collectingAgent.getApprovalStatus());
		} else {
			RequestContext.getCurrentInstance().execute("PF('dlgConfirm').show()");
		}

	}

	public void deleteCollectingAgent() {
		
		collectingAgentService.requestToRemove(collectingAgent);

		if (collectingAgent.getApprovalStatus().equals(Constants.APPROVAL_STATUS_REJECTED_CREATE)){

			String headerMessage = "Success";
			String bodyMessage = "Delete System CollectingAgent " + this.collectingAgent.getName() + " Successfully";
	
			FacesUtils.setInfoMessageDialog(headerMessage, bodyMessage);
			
		}else{
		
			String headerMessage = "Delete CollectingAgent Successfully";
			String bodyMessage = "Supervisor must approve the deleting of the <br/>collectingAgent &apos;"
					+ collectingAgent.getName()
					+ "&apos; to completely the process.";
	
			FacesUtils.setInfoMessageDialog(headerMessage, bodyMessage);
		
		}

	}
	
	public void viewDetailedChangeStatus(CollectingAgent collectingAgent) {

		this.collectingAgent = collectingAgent;

		if (this.collectingAgent.getDisableDelete() == true) {
			FacesUtils.setApprovalWarning("parameter",
					this.collectingAgent.getName(),
					this.collectingAgent.getApprovalStatus());
		} else {
			RequestContext.getCurrentInstance().execute("PF('dlgConfirmChangeStatus').show()");
		}

	}

	public void changeStatusCollectingAgent() {
		
		try{
		
			this.collectingAgentTemp = (CollectingAgent) HelperUtils.clone(collectingAgent);
			
			collectingAgent.setStatus(collectingAgent.getStatus()==Constants.STATUS_ACTIVE?Constants.STATUS_INACTIVE:Constants.STATUS_ACTIVE);
	
			collectingAgentService.modify(collectingAgentTemp, collectingAgent);
	
			String headerMessage = "Success";
			String bodyMessage = "Change Status Collecting Agent Successfully";
	
			FacesUtils.setInfoMessageDialog(headerMessage, bodyMessage);
		
		}catch(Exception e){
			String headerMessage = "Error";
			String bodyMessage = "Change Status Collecting Agent Failed";

			FacesUtils.setErrorMessageDialog(headerMessage, bodyMessage);
		}
	}
	
	public boolean isRejectReason(){
		boolean isRejectReason = false;
		if (this.collectingAgent != null){
			if (this.collectingAgent.getApprovalStatus().equals(Constants.APPROVAL_STATUS_REJECTED_CREATE) || 
					this.collectingAgent.getApprovalStatus().equals(Constants.APPROVAL_STATUS_REJECTED_EDIT) ||
						this.collectingAgent.getApprovalStatus().equals(Constants.APPROVAL_STATUS_REJECTED_DELETE)){
				isRejectReason = true;
			}
		}
		return isRejectReason;
	}


	private class CollectingAgentDataModel extends LazyDataModel<CollectingAgent> {
		private static final long serialVersionUID = 1L;

		public CollectingAgentDataModel() {

		}

		@Override
	    public List<CollectingAgent> load(int startingAt, int maxResult, String sortField, SortOrder sortOrder, Map<String, Object> filters) {

			List<CollectingAgent> collectingAgent = null;
			
			collectingAgent = collectingAgentService.selectByCriteria(
					startingAt, maxResult, code, name, createDate, maxAgent, maxAmount, status);
			Integer jmlAll = collectingAgentService.countByCriteria(code,
					name, createDate, maxAgent, maxAmount, status);
			setRowCount(jmlAll);
			setPageSize(maxResult);

			return collectingAgent;
		}

	}

}
