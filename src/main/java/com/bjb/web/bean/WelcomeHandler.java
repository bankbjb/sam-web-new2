package com.bjb.web.bean;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.ChartSeries;

import com.bjb.service.SystemParameterService;
import com.bjb.service.TransactionLogService;
import com.bjb.web.transformer.ChartDataTransformer;

@ManagedBean
public class WelcomeHandler implements Serializable {
	 
	private static final long serialVersionUID = 1L;

	private BarChartModel topFeeBasedIncome;
	private BarChartModel topBestSeller;
	private Integer topFeeBasedIncomeItem = 5;
	private Integer topBestSellerItem = 5;

 
	@ManagedProperty(value="#{systemParameterService}")
	private SystemParameterService systemParameterService;
	 
	@ManagedProperty(value="#{transactionLogService}")
	private TransactionLogService transactionLogService;
		
	public void setSystemParameterService(SystemParameterService systemParameterService) {
		this.systemParameterService = systemParameterService;
	}
	
	public void setTransactionLogService(TransactionLogService transactionLogService) {
		this.transactionLogService = transactionLogService;
	}

	@PostConstruct
    public void init() {
		topFeeBasedIncomeItem = Integer.parseInt(systemParameterService.getSystemParameter("TOP_FEE_BASED_INCOME_ITEM", "5"));
		topBestSellerItem = Integer.parseInt(systemParameterService.getSystemParameter("TOP_BEST_SELLER_ITEM", "5"));
        createTopFeeBasedIncome();
        createTopBestSeller();
    }
 
    public String getMessageOfToday() {
        return systemParameterService.getSystemParameter("MOTD", "Welcome in BJB SAM Support System!");
    }
    
    public BarChartModel getTopFeeBasedIncome() {
        return topFeeBasedIncome;
    }
     
    public BarChartModel getTopBestSeller() {
        return topBestSeller;
    }

    private BarChartModel initTopFeeBasedIncome() {
        BarChartModel model = new BarChartModel();
 
        ChartSeries commisionBased = new ChartSeries();
        List<Map<Object, Number>> result = transactionLogService.selectTopFeeBasedIncome(topFeeBasedIncomeItem);
        
        commisionBased.setData( result.get(0));
        model.addSeries(commisionBased);
         
        Map<Object, Number> mapStats = result.get(1);

        Axis yAxis = model.getAxis(AxisType.Y);
        yAxis.setMin(0);
        yAxis.setMax((mapStats.get(ChartDataTransformer.HIGHEST_VALUE)).doubleValue()*1.2);
 
        return model;
    }
    
    private BarChartModel initTopBestSeller() {
        BarChartModel model = new BarChartModel();
 
        ChartSeries transactionBased = new ChartSeries();
        List<Map<Object, Number>> result = transactionLogService.selectTopBestSeller(topBestSellerItem);
        
        transactionBased.setData( result.get(0) );
        model.addSeries(transactionBased);
        
        Map<Object, Number> mapStats = result.get(1);
        
        Axis yAxis = model.getAxis(AxisType.Y);
        yAxis.setMin(0);
        yAxis.setMax((mapStats.get(ChartDataTransformer.HIGHEST_VALUE)).doubleValue()*1.2);
        
        return model;
    }
     
    private void createTopFeeBasedIncome() {
        topFeeBasedIncome = initTopFeeBasedIncome();
         
        topFeeBasedIncome.setTitle("Top "+topFeeBasedIncomeItem+" Fee Based Income");
         
        Axis xAxis = topFeeBasedIncome.getAxis(AxisType.X);
        xAxis.setLabel("Aggregator");
        xAxis.setTickAngle(-30);
        
        Axis yAxis = topFeeBasedIncome.getAxis(AxisType.Y);
        yAxis.setLabel("Commision");

    }
     
    private void createTopBestSeller() {
        topBestSeller = initTopBestSeller();
         
        topBestSeller.setTitle("Top " + topBestSellerItem + " Best Seller");
         
        Axis xAxis = topBestSeller.getAxis(AxisType.X);
        xAxis.setLabel("Aggregator");
        xAxis.setTickAngle(-30);
         
        Axis yAxis = topBestSeller.getAxis(AxisType.Y);
        yAxis.setLabel("Number of Transaction");

    }
  
	public String doHome() {
		return "welcome?faces-redirect=true";
	}

}
