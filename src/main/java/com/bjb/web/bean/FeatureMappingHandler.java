package com.bjb.web.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.TreeNode;

import com.bjb.constant.Constants;
import com.bjb.model.FeatureMapping;
import com.bjb.service.FeatureMappingService;
import com.bjb.service.SessionService;
import com.bjb.util.HelperUtils;
import com.bjb.web.bean.support.DiffService;
import com.bjb.web.helper.FacesUtils;

@ManagedBean
@ViewScoped
public class FeatureMappingHandler implements Serializable {
	private static final long serialVersionUID = 1L;

	private FeatureMapping featureMapping;
	private FeatureMapping featureMappingTemp;

	private String action;

	private String code;
	private String feature;
	private String aggregator;
	private String ca;
	private List<Integer> status;;
	private List<String> approvalStatus;

	private boolean active;
	private boolean inactive;

	private boolean pending;
	private boolean approved;
	private boolean rejected;

	private boolean saveFeatureMapping;

	private LazyDataModel<FeatureMapping> featureMappingList;

	private boolean doShowSearch = false;

	private boolean doShowHistory = false;

	
	@ManagedProperty(value="#{sessionService}")
	private SessionService sessionService;
	
	@ManagedProperty(value="#{featureMappingService}")
	private FeatureMappingService featureMappingService;
	
	@ManagedProperty(value="#{diffService}")
	private DiffService diffService;
	
	public void setDiffService(DiffService diffService) {
		this.diffService = diffService;
	}

	public TreeNode getDiffTreeTable() {
		return diffService.diff(featureMappingTemp, featureMapping);
	}
	
	@PostConstruct
	public void init() {
		sessionService.authMenu(sessionService.getUser().getId(), "manageFeatureMapping");

		featureMappingList = new FeatureMappingDataModel();

		code = "";
		feature = "";
		aggregator = "";
		ca = "";

		doSearch();
		
	}
	
	public void setFeatureMappingService(FeatureMappingService featureMappingService) {
		this.featureMappingService = featureMappingService;
	}
	
	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	

	public String getFeature() {
		return feature;
	}

	public void setFeature(String feature) {
		this.feature = feature;
	}

	public String getAggregator() {
		return aggregator;
	}

	public void setAggregator(String aggregator) {
		this.aggregator = aggregator;
	}

	public String getCa() {
		return ca;
	}

	public void setCa(String ca) {
		this.ca = ca;
	}

	public List<String> getStatus() {
		return approvalStatus;
	}

	public void setStatus(List<String> status) {
		this.approvalStatus = status;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public boolean isInactive() {
		return inactive;
	}

	public void setInactive(boolean inactive) {
		this.inactive = inactive;
	}

	public boolean isPending() {
		return pending;
	}

	public void setPending(boolean pending) {
		this.pending = pending;
	}

	public boolean isApproved() {
		return approved;
	}

	public void setApproved(boolean approved) {
		this.approved = approved;
	}

	public boolean isRejected() {
		return rejected;
	}

	public void setRejected(boolean rejected) {
		this.rejected = rejected;
	}

	public void showHistory() {
		doShowHistory = true;
	}

	public void hideHistory() {
		doShowHistory = false;
	}

	public boolean isDoShowHistory() {
		return doShowHistory;
	}

	public void setDoShowHistory(boolean doShowHistory) {
		this.doShowHistory = doShowHistory;
	}

	public SessionService getSessionService() {
		return sessionService;
	}

	public void setSessionService(SessionService sessionService) {
		this.sessionService = sessionService;
	}

	public boolean isSaveFeatureMapping() {
		return saveFeatureMapping;
	}

	public void setSaveFeatureMapping(boolean saveFeatureMapping) {
		this.saveFeatureMapping = saveFeatureMapping;
	}

	public FeatureMapping getFeatureMapping() {
		return featureMapping;
	}

	public FeatureMapping getFeatureMappingTemp() {
		return featureMappingTemp;
	}

	public void setFeatureMappingTemp(FeatureMapping featureMappingTemp) {
		this.featureMappingTemp = featureMappingTemp;
	}

	public void setFeatureMapping(FeatureMapping featureMapping) {
		this.featureMapping = featureMapping;
	}

	public LazyDataModel<FeatureMapping> getFeatureMappingList() {
		return featureMappingList;
	}

	public void setFeatureMappingList(LazyDataModel<FeatureMapping> featureMappingList) {
		this.featureMappingList = featureMappingList;
	}

	public boolean isDoShowSearch() {
		return doShowSearch;
	}

	public void setDoShowSearch(boolean doShowSearch) {
		this.doShowSearch = doShowSearch;
	}

	public void showSearch() {
		doShowSearch = true;
	}

	public void hideSearch() {
		doShowSearch = false;
	}

	public void viewDetailedFeatureMapping(FeatureMapping featureMapping) {
		this.featureMapping = featureMapping;

		RequestContext.getCurrentInstance().execute("PF('viewFeatureMapping').show()");
	}

	public void viewDetailedFeatureMappingEdit(FeatureMapping featureMapping) {

		this.featureMappingTemp = HelperUtils.clone(featureMapping);
		this.featureMapping = featureMapping;
		this.action = "edit";

		RequestContext.getCurrentInstance().execute("PF('insertOrEditFeatureMapping').show()");
	}

	public void newFeatureMapping() {
		this.featureMapping = new FeatureMapping();
		SimpleDateFormat dateFormat = new SimpleDateFormat(Constants.FORMAT_DATE_SHORT);
		BigDecimal seq = featureMappingService.generateId();
		this.featureMapping.setCode(dateFormat.format(new Date()) + "MAP" + String.format("%04d", seq.intValue()));
		this.featureMapping.setStatus(Constants.STATUS_ACTIVE);
		this.action = "";
	}

	public void createFeatureMapping(FeatureMapping featureMapping) {

		try{
			featureMappingService.create(featureMapping);

			String headerMessage = "Success";
			String bodyMessage = "Insert Feature Mapping Successfully";

			FacesUtils.setInfoMessageDialog(headerMessage, bodyMessage);
		}catch(Exception e){
			String headerMessage = "Error";
			String bodyMessage = "Insert Feature Mapping Failed";

			FacesUtils.setErrorMessageDialog(headerMessage, bodyMessage);
		}
		
		
	}
	
	public void editFeatureMapping(FeatureMapping featureMapping) {
		try{
			featureMappingService.modify(featureMappingTemp, featureMapping);
			String headerMessage = "Success";
			String bodyMessage = "Edit Feature Mapping Successfully";

			FacesUtils.setInfoMessageDialog(headerMessage, bodyMessage);
		}catch(Exception e){
			String headerMessage = "Error";
			String bodyMessage = "Edit Feature Mapping Failed";

			FacesUtils.setErrorMessageDialog(headerMessage, bodyMessage);
		}
	}

	public void doSave() {
		
		List<FeatureMapping> listFeatureMapping = featureMappingService.findByFeatureAggregatorCA(featureMapping.getFeature().getCode(), featureMapping.getAggregator().getCode(), featureMapping.getCollectingAgent().getCode());
		
		if (listFeatureMapping.size() == 0) {

			if (this.action != null && this.action.equals("")) {
				createFeatureMapping(featureMapping);
			}

			RequestContext.getCurrentInstance().execute("PF('insertOrEditFeatureMapping').hide()");

		} else if (listFeatureMapping.size() == 1) {
			if (this.action != null && this.action.equals("edit")) {
				editFeatureMapping(featureMapping);
			}
			
			RequestContext.getCurrentInstance().execute("PF('insertOrEditFeatureMapping').hide()");
		
		} else {
			String headerMessage = "Warning";
			String bodyMessage = "Feature " + featureMapping.getFeature().getCode() + ", aggregator "
					+ featureMapping.getAggregator().getCode() + ", and collecting agent "
					+ featureMapping.getCollectingAgent().getCode() + " already exist.";

			FacesUtils.setWarningMessageDialog(headerMessage, bodyMessage);
		}

	}

	public void doSearch() {

		status = new ArrayList<Integer>();
		if (!active && !inactive) {
			status.add(Constants.STATUS_ACTIVE);
			status.add(Constants.STATUS_INACTIVE);
		} else {
			if (active) {
				status.add(Constants.STATUS_ACTIVE);
			}
			if (inactive) {
				status.add(Constants.STATUS_INACTIVE);
			}
		}
		approvalStatus = new ArrayList<String>();
		approvalStatus.add(Constants.NO_APPROVAL_STATUS);
		FacesUtils.resetPage("featureMappingForm:featureMappingTable");
		featureMappingList = new FeatureMappingDataModel();

	}

	public void viewDetailedDelete(FeatureMapping featureMapping) {

		this.featureMapping = featureMapping;
		RequestContext.getCurrentInstance().execute("PF('dlgConfirm').show()");

	}
	
	public void deleteFeatureMapping() {

		try{
			featureMappingService.remove(featureMapping);
			String headerMessage = "Success";
			String bodyMessage = "Delete Feature Mapping Successfully";

			FacesUtils.setInfoMessageDialog(headerMessage, bodyMessage);
		}catch(Exception e){
			String headerMessage = "Error";
			String bodyMessage = "Delete Feature Mapping Failed";

			FacesUtils.setErrorMessageDialog(headerMessage, bodyMessage);
		}

	}
	
	public void viewDetailedChangeStatus(FeatureMapping featureMapping) {

		this.featureMapping = featureMapping;
		RequestContext.getCurrentInstance().execute("PF('dlgConfirmChangeStatus').show()");

	}

	public void changeStatusFeatureMapping() {
		
		try{
			featureMapping.setChangeDate(HelperUtils.getCurrentDateTime());
			featureMapping.setChangeWho(sessionService.getUser().getUserName());
			featureMapping.setStatus(featureMapping.getStatus()==Constants.STATUS_ACTIVE?Constants.STATUS_INACTIVE:Constants.STATUS_ACTIVE);
			featureMappingService.update(featureMapping);
			String headerMessage = "Success";
			String bodyMessage = "Change Status Feature Mapping Successfully";

			FacesUtils.setInfoMessageDialog(headerMessage, bodyMessage);
		}catch(Exception e){
			String headerMessage = "Error";
			String bodyMessage = "Change Status Feature Mapping Failed";

			FacesUtils.setErrorMessageDialog(headerMessage, bodyMessage);
		}

	}	

	private class FeatureMappingDataModel extends LazyDataModel<FeatureMapping> {
		private static final long serialVersionUID = 1L;

		public FeatureMappingDataModel() {

		}

		@Override
		public List<FeatureMapping> load(int startingAt, int maxResult, String sortField, SortOrder sortOrder, Map<String, Object> filters) {

			List<FeatureMapping> featureMapping = null;

			featureMapping = featureMappingService.selectByCriteria(startingAt, maxResult, code, feature, aggregator, ca, status, approvalStatus);
			Integer jmlAll = featureMappingService.countByCriteria(code,  feature, aggregator, ca, status, approvalStatus);
			setRowCount(jmlAll);
			setPageSize(maxResult);

			return featureMapping;
		}

	}

}
