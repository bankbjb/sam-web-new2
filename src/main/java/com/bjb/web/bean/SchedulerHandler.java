package com.bjb.web.bean;

import java.io.Serializable;
import java.math.BigDecimal;
//import java.rmi.RemoteException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
//import javax.xml.rpc.ServiceException;

import org.apache.commons.lang.time.DateUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.TreeNode;

import com.bjb.constant.Constants;
import com.bjb.integration.scheduler.SchedulerExecutor;
import com.bjb.model.ScheduleTask;
import com.bjb.service.SchedulerService;
import com.bjb.service.SessionService;
import com.bjb.service.SystemParameterService;
import com.bjb.util.HelperUtils;
import com.bjb.web.bean.support.DiffService;
import com.bjb.web.helper.FacesUtils;

@ManagedBean
@ViewScoped
public class SchedulerHandler implements Serializable {
	private static final long serialVersionUID = 1L;

	private ScheduleTask task;
	private ScheduleTask taskTemp;
	private String action;

	private LazyDataModel<ScheduleTask> taskList;
	
	private String checkScheduleType;
	
	private String checkScheduleTypePending;

	private List<Integer> hours;
	private List<Integer> minutes;

	private List<String> scheduleTypes;

	private boolean runOnce = false;
	private boolean repeating = false;
	private boolean complexRepeating = false;
	private boolean repeatingOrComplexRepeating = false;

	private boolean runOncePending = false;
	private boolean repeatingPending = false;
	private boolean complexRepeatingPending = false;
	private boolean repeatingOrComplexRepeatingPending = false;
	
	private boolean doShowHistory = false;
	
	private String currentJobName;
	
	@ManagedProperty(value="#{sessionService}")
	private SessionService sessionService;
	
	@ManagedProperty(value="#{systemParameterService}")
	private SystemParameterService systemParameterService;

	@ManagedProperty(value="#{schedulerService}")
	private SchedulerService schedulerService;

	@ManagedProperty(value="#{diffService}")
	private DiffService diffService;
	
	public void setDiffService(DiffService diffService) {
		this.diffService = diffService;
	}

	public TreeNode getDiffTreeTable() {
		return diffService.diff(taskTemp, task);
	}
	
	public void setSessionService(SessionService sessionService) {
		this.sessionService = sessionService;
	}

	public void setSystemParameterService(SystemParameterService systemParameterService){
		this.systemParameterService = systemParameterService;
	}

	public void setSchedulerService(SchedulerService schedulerService) {
		this.schedulerService = schedulerService;
	}

	@PostConstruct
	public void init() {
		sessionService.authMenu(sessionService.getUser().getId(), "adminScheduler");

		taskList = new TaskDataModel();
		initSelectItemData();
		
	}
	
	public String getCheckScheduleType() {
		return checkScheduleType;
	}

	public void setCheckScheduleType(String checkScheduleType) {
		this.checkScheduleType = checkScheduleType;
	}
	
	

	public String getCheckScheduleTypePending() {
		return checkScheduleTypePending;
	}

	public void setCheckScheduleTypePending(String checkScheduleTypePending) {
		this.checkScheduleTypePending = checkScheduleTypePending;
	}

	public ScheduleTask getTask() {
		return task;
	}

	public void setTask(ScheduleTask task) {
		this.task = task;
	}

	public ScheduleTask getTempTask() {
		return taskTemp;
	}

	public void setTempTask(ScheduleTask tempTask) {
		this.taskTemp = tempTask;
	}
	
	

	public boolean isRunOncePending() {
		return runOncePending;
	}

	public void setRunOncePending(boolean runOncePending) {
		this.runOncePending = runOncePending;
	}

	public boolean isRepeatingPending() {
		return repeatingPending;
	}

	public void setRepeatingPending(boolean repeatingPending) {
		this.repeatingPending = repeatingPending;
	}

	public boolean isComplexRepeatingPending() {
		return complexRepeatingPending;
	}

	public void setComplexRepeatingPending(boolean complexRepeatingPending) {
		this.complexRepeatingPending = complexRepeatingPending;
	}

	public boolean isRepeatingOrComplexRepeatingPending() {
		return repeatingOrComplexRepeatingPending;
	}

	public void setRepeatingOrComplexRepeatingPending(
			boolean repeatingOrComplexRepeatingPending) {
		this.repeatingOrComplexRepeatingPending = repeatingOrComplexRepeatingPending;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public List<Integer> getHours() {
		return hours;
	}

	public void setHours(List<Integer> hours) {
		this.hours = hours;
	}

	public List<Integer> getMinutes() {
		return minutes;
	}

	public void setMinutes(List<Integer> minutes) {
		this.minutes = minutes;
	}

	public List<String> getScheduleTypes() {
		return scheduleTypes;
	}

	public void setScheduleTypes(List<String> scheduleTypes) {
		this.scheduleTypes = scheduleTypes;
	}

	public boolean isRunOnce() {
		return runOnce;
	}

	public void setRunOnce(boolean runOnce) {
		this.runOnce = runOnce;
	}

	public boolean isRepeating() {
		return repeating;
	}

	public void setRepeating(boolean repeating) {
		this.repeating = repeating;
	}

	public boolean isComplexRepeating() {
		return complexRepeating;
	}

	public void setComplexRepeating(boolean complexRepeating) {
		this.complexRepeating = complexRepeating;
	}

	public boolean isRepeatingOrComplexRepeating() {
		return repeatingOrComplexRepeating;
	}

	public void setRepeatingOrComplexRepeating(
			boolean repeatingOrComplexRepeating) {
		this.repeatingOrComplexRepeating = repeatingOrComplexRepeating;
	}

	public LazyDataModel<ScheduleTask> getTaskList() {
		return taskList;
	}

	public void setTaskList(LazyDataModel<ScheduleTask> taskList) {
		this.taskList = taskList;
	}
	
	
	public void showHistory(){
		doShowHistory = true;
	}
	
	public void hideHistory(){
		doShowHistory = false;
	}
	
	public boolean isDoShowHistory() {
		return doShowHistory;
	}

	public void setDoShowHistory(boolean doShowHistory) {
		this.doShowHistory = doShowHistory;
	}

	public void initSelectItemData() {

		scheduleTypes = new ArrayList<String>();
		scheduleTypes.add(Constants.SCHEDULER_RUN_ONCE);
		scheduleTypes.add(Constants.SCHEDULER_REPEATING);
		scheduleTypes.add(Constants.SCHEDULER_COMPLEX_REPEATING);

		hours = new ArrayList<Integer>();
		for (int i = 0; i < 25; i++) {
			hours.add(i);
		}

		minutes = new ArrayList<Integer>();
		for (int i = 0; i < 61; i++) {
			minutes.add(i);
		}
	}

	public void newTask() {
		this.task = new ScheduleTask();
		task.setSchedulerType(Constants.SCHEDULER_RUN_ONCE);
		SimpleDateFormat sdf = new SimpleDateFormat(Constants.FORMAT_DATE_SHORT);
		String id = sdf.format(new Date());
		BigDecimal seq = schedulerService.generateId();
		this.task.setId(seq);
		this.task.setCode(id+"SCH"+String.format("%04d", seq.intValue()));
		this.task.setSchedulerStatus(Constants.SCHEDULER_STATUS_SHUTDOWN);
		this.action = "";
		checkScheduleType();
	}

	public void checkScheduleType() {
		if (task.getSchedulerType().equals(Constants.SCHEDULER_RUN_ONCE)) {
			runOnce = true;
			repeating = false;
			complexRepeating = false;
			repeatingOrComplexRepeating = false;
		} else if (task.getSchedulerType()
				.equals(Constants.SCHEDULER_REPEATING)) {
			runOnce = false;
			repeating = true;
			complexRepeating = false;
			repeatingOrComplexRepeating = true;
		} else if (task.getSchedulerType().equals(
				Constants.SCHEDULER_COMPLEX_REPEATING)) {
			runOnce = false;
			repeating = false;
			complexRepeating = true;
			repeatingOrComplexRepeating = true;
		} else {
			runOnce = false;
			repeating = false;
			complexRepeating = false;
			repeatingOrComplexRepeating = false;
		}
	}
	
	public void checkScheduleTypePending() {
		if (taskTemp.getSchedulerType().equals(Constants.SCHEDULER_RUN_ONCE)) {
			runOncePending = true;
			repeatingPending = false;
			complexRepeatingPending = false;
			repeatingOrComplexRepeatingPending = false;
		} else if (taskTemp.getSchedulerType()
				.equals(Constants.SCHEDULER_REPEATING)) {
			runOncePending = false;
			repeatingPending = true;
			complexRepeatingPending = false;
			repeatingOrComplexRepeatingPending = true;
		} else if (taskTemp.getSchedulerType().equals(
				Constants.SCHEDULER_COMPLEX_REPEATING)) {
			runOncePending = false;
			repeatingPending = false;
			complexRepeatingPending = true;
			repeatingOrComplexRepeatingPending = true;
		} else {
			runOncePending = false;
			repeatingPending = false;
			complexRepeatingPending = false;
			repeatingOrComplexRepeatingPending = false;
		}
	}

	public void viewDetailedTask(ScheduleTask task) {
		this.task = task;

		if (this.task.getApprovalStatus().equals(
				Constants.APPROVAL_STATUS_PENDING_EDIT)) {
			this.taskTemp = schedulerService.findPendingApprovalByCode(task.getCode());
			if (this.taskTemp != null) {
				checkScheduleType();
				checkScheduleTypePending();
				RequestContext.getCurrentInstance().execute(
						"PF('viewSchedulerEdit').show()");
				return;
			}
		} 
		checkScheduleType();
		RequestContext.getCurrentInstance().execute("PF('viewScheduler').show()");
	}

	public void viewDetailedTaskEdit(ScheduleTask task) {

		this.task = task;

		if (this.task.getDisableEdit() == true) {
			FacesUtils.setApprovalWarning("parameter",
					this.task.getName(),
					this.task.getApprovalStatus());
		} else {
			this.action = "edit";
			if (this.task.getApprovalStatus().equals(
					Constants.APPROVAL_STATUS_PENDING_EDIT)) {

				this.taskTemp = schedulerService.findPendingApprovalByCode(task.getCode());
				if (taskTemp != null) {
					currentJobName = this.taskTemp.getName();
					RequestContext.getCurrentInstance().execute(
							"PF('insertOrEditTaskPending').show()");
					return;
				} 
			}
			this.taskTemp = HelperUtils.clone(task);
			RequestContext.getCurrentInstance().execute(
						"PF('insertOrEditTask').show()");
		}
	}

	public void viewDetailedDelete(ScheduleTask task) {

		this.task = task;

		if (this.task.getDisableDelete() == true) {
			FacesUtils.setApprovalWarning("task", this.task.getJobName(),
					this.task.getApprovalStatus());
		} else {
			RequestContext.getCurrentInstance().execute("PF('dlgConfirm').show()");
		}
	}

	public void viewDetailedStop(ScheduleTask task) {

		this.task = task;

		if (this.task.getDisableStopStart() == true) {
			FacesUtils.setApprovalWarning("task", this.task.getJobName(),
					this.task.getApprovalStatus());
		} else {
			RequestContext.getCurrentInstance()
					.execute("PF('dlgConfirmStop').show()");
		}
	}

	public void viewDetailedStart(ScheduleTask task) {

		this.task = task;

		if (this.task.getDisableStopStart() == true) {
			FacesUtils.setApprovalWarning("task", this.task.getJobName(),
					this.task.getApprovalStatus());
		} else {

			RequestContext.getCurrentInstance().execute(
					"PF('dlgConfirmStart').show()");
		}

	}
	
	public void viewDetailedPause(ScheduleTask task) {

		this.task = task;

		if (this.task.getDisableStopStart() == true) {
			FacesUtils.setApprovalWarning("task", this.task.getJobName(),
					this.task.getApprovalStatus());
		} else {

			RequestContext.getCurrentInstance()
					.execute("PF('dlgConfirmPause').show()");
		}
	}

	public void viewDetailedResume(ScheduleTask task) {

		this.task = task;
		
		if (this.task.getDisableStopStart() == true) {
			FacesUtils.setApprovalWarning("task", this.task.getJobName(),
					this.task.getApprovalStatus());
		} else {

			RequestContext.getCurrentInstance().execute(
					"PF('dlgConfirmResume').show()");
		}

	}


	public void startTask() {
		try {
			if (!SchedulerExecutor.statusScheduler(task.getTaskId()).equals(
					Constants.SCHEDULER_STATUS_STARTED) && !this.task.getSchedulerStatus().equals(
							Constants.SCHEDULER_STATUS_STANDBY)) {
				SchedulerExecutor.startScheduler(this.task
						.getTaskId());

				task.setSchedulerStatus(SchedulerExecutor.statusScheduler(task.getTaskId()));	
				schedulerService.insert(task);
				FacesUtils.setInfoMessageDialog("Success",
						"Start scheduler "+task.getJobName()+" successfuly");
			} else {
				FacesUtils.setWarningMessageDialog("Warning",
						""+task.getJobName()+" scheduler already running / stand by");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			FacesUtils.setErrorMessageDialog("Error", "Start scheduler failed");
		}
	}
	
	public void pauseTask() {
		try {
			if (!SchedulerExecutor.statusScheduler(task.getTaskId()).equals(
					Constants.SCHEDULER_STATUS_SHUTDOWN)) {
				if (!SchedulerExecutor.statusScheduler(task.getTaskId()).equals(
						Constants.SCHEDULER_STATUS_STANDBY)) {
							SchedulerExecutor.pauseScheduler(this.task.getTaskId());

				task.setSchedulerStatus(SchedulerExecutor.statusScheduler(task.getTaskId()));	
				schedulerService.insert(task);
				FacesUtils.setInfoMessageDialog("Success",
						"Pause scheduler "+task.getJobName()+" successfuly.");
				} else {
					FacesUtils.setWarningMessageDialog("Warning",
							""+task.getJobName()+" scheduler already paused.");
				}
			} else {
				FacesUtils.setWarningMessageDialog("Warning",
						""+task.getJobName()+" scheduler has been stopped.");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			FacesUtils.setErrorMessageDialog("Error", "Stop scheduler failed.");
		}
	}

	public void resumeTask() {
		try {
			if (!SchedulerExecutor.statusScheduler(task.getTaskId()).equals(
					Constants.SCHEDULER_STATUS_STARTED)) {
				if (!SchedulerExecutor.statusScheduler(task.getTaskId()).equals(
						Constants.SCHEDULER_STATUS_SHUTDOWN)) {
							SchedulerExecutor.resumeScheduler(this.task.getTaskId());

				task.setSchedulerStatus(SchedulerExecutor.statusScheduler(task.getTaskId()));	
				schedulerService.insert(task);
				FacesUtils.setInfoMessageDialog("Success",
						"Start scheduler "+task.getJobName()+" success");
				} else {
					FacesUtils.setWarningMessageDialog("Warning",
							""+task.getJobName()+" scheduler has been stopped.");
				}
			} else {
				FacesUtils.setWarningMessageDialog("Warning",
						""+task.getJobName()+" scheduler already running.");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			FacesUtils.setErrorMessageDialog("Error", "Start scheduler failed");
		}
	}
	
	public void refresh(){
		System.out.println("Masuk Scheduler Refresh..");
		try {
			//update start date and start time
			for(ScheduleTask sch : schedulerService.getAllValidTask()){
				//for start date
				String startDate_ = "";
					DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
					Date date = new Date();
					startDate_ = dateFormat.format(date);
					schedulerService.getAllValidTask().remove(sch.getStartDate());
					sch.setStartDate(startDate_);
				
				//for start time	
				String nextTime = "";
					DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
					Date timeNow = new Date();
					Date time = DateUtils.addMinutes(timeNow, Constants.ADD_MINUTES_SCHEDULER);
					nextTime = timeFormat.format(time);
					schedulerService.getAllValidTask().remove(sch.getStartTime());
					sch.setStartTime(nextTime);
					
				//update to DB
				schedulerService.updateDateTime(sch.getJobName(), startDate_,nextTime);
			}
			
			for(ScheduleTask sch : schedulerService.getAllValidTask()){
				String taskId;

				try {
					//System.out.println("[Delete Scheduller]Task ID = "+sch.getTaskId());
					SchedulerExecutor.deleteScheduler(sch.getTaskId());
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				taskId = SchedulerExecutor.createScheduler(sch);
				
				schedulerService.getAllValidTask().remove(sch.getTaskId());
				//System.out.println("New Task ID = "+taskId);
				sch.setTaskId(taskId);
				//System.out.println("getDisableStopStart value = "+sch.getDisableStopStart());
				
				if (!sch.getDisableStopStart()){
					if (sch.getSchedulerStatus().equals(Constants.SCHEDULER_STATUS_STARTED)){
						//System.out.println("status is started with id "+sch.getTaskId());
						SchedulerExecutor.startScheduler(sch.getTaskId());
					}else if (sch.getSchedulerStatus().equals(Constants.SCHEDULER_STATUS_STANDBY)){
						//System.out.println("status is standby with id "+sch.getTaskId());
						SchedulerExecutor.startScheduler(sch.getTaskId());
						SchedulerExecutor.pauseScheduler(sch.getTaskId());
					}else{
						//System.out.println("unknown status with id "+sch.getTaskId());
						SchedulerExecutor.startScheduler(sch.getTaskId());
					}
				}
				
				System.out.println("[Refresh Service] status scheduler "+sch.getTaskId()+" is "+SchedulerExecutor.statusScheduler(sch.getTaskId()));
				sch.setSchedulerStatus(SchedulerExecutor.statusScheduler(sch.getTaskId()));
				
				schedulerService.insert(sch);
			}

			FacesUtils.setInfoMessageDialog("Success","Refresh all task successfully");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			FacesUtils.setErrorMessageDialog("Failed",
					"Refresh all task failed");
		}
	}
	
	public void viewDetailedChangeStatus(ScheduleTask task) {

		this.task = task;

		if (this.task.getDisableDelete() == true) {
			FacesUtils.setApprovalWarning("parameter",
					this.task.getJobName(),
					this.task.getApprovalStatus());
		} else {
			RequestContext.getCurrentInstance().execute("PF('dlgConfirmChangeStatus').show()");
		}

	}

	public void changeStatusScheduleTask() {
		
		this.taskTemp = (ScheduleTask) HelperUtils.clone(task);
		
		task.setStatus(task.getStatus()==Constants.STATUS_ACTIVE?Constants.STATUS_INACTIVE:Constants.STATUS_ACTIVE);

		task = schedulerService.requestToModify(taskTemp, task);

		String headerMessage = "Change Status Schedule Task Successfully";
		String bodyMessage = "Supervisor must approve the change status of the <br/>schedule item &apos;"
				+ task.getJobName()
				+ "&apos; to completely the process.";

		FacesUtils.setInfoMessageDialog(headerMessage, bodyMessage);
	}


	public void doSave() {
		if (this.action != null && this.action.equals("edit")) {
			editTask(this.task);
		} else {
			try {

				ScheduleTask taskExist = schedulerService
						.findByCode(
								this.task.getName());

				if (taskExist == null
						|| (taskExist.getName()
								.equals(currentJobName))) {
					createTask(this.task);
				} else {
					String headerMessage = "Attention";
					String bodyMessage = "Task "
							+ this.task.getName()
							+ " already exist in the application.";

					FacesUtils.setWarningMessageDialog(headerMessage,
							bodyMessage);
				}

			} catch (Exception e) {
				String headerMessage = "Error";
				String bodyMessage = "System error";

				FacesUtils.setErrorMessageDialog(headerMessage, bodyMessage);
				e.printStackTrace();
			}
		}

	}

	public void createTask(ScheduleTask task) throws Exception {


		schedulerService.requestToCreate(task);
		
		String headerMessage = "Create Task Successfully";
		String bodyMessage = "Supervisor must approve the creating of the <br/>task &apos;"
				+ task.getJobName() + "&apos; to completely the process.";

		FacesUtils.setInfoMessageDialog(headerMessage, bodyMessage);
	}

	public void editTask(ScheduleTask task) {
		task = schedulerService.requestToModify(taskTemp, task);
		
		RequestContext.getCurrentInstance().execute(
				"PF('insertOrEditTask').hide()");
		RequestContext.getCurrentInstance().execute(
				"PF('insertOrEditTaskPending').hide()");

		String headerMessage = "Edit Schedule Successfully";
		String bodyMessage = "Supervisor must approve the editing of the <br/>schedule &apos;"
				+ task.getName()
				+ "&apos; to completely the process.";

		FacesUtils.setInfoMessageDialog(headerMessage, bodyMessage);
	}

	public void deleteTask() {
		
		schedulerService.requestToRemove(task);

		if (task.getApprovalStatus().equals(Constants.APPROVAL_STATUS_REJECTED_CREATE)){

			String headerMessage = "Success";
			String bodyMessage = "Delete Scheduler " + this.task.getExecutionService() + " Successfully";
	
			FacesUtils.setInfoMessageDialog(headerMessage, bodyMessage);
			
		}else{
		
			String headerMessage = "Delete Task Successfully";
			String bodyMessage = "Supervisor must approve the deleting of the <br/>task &apos;"
					+ task.getJobName() + "&apos; to completely the process.";
	
			FacesUtils.setInfoMessageDialog(headerMessage, bodyMessage);
		
		}

	}

	private class TaskDataModel extends LazyDataModel<ScheduleTask> {
		private static final long serialVersionUID = 1L;

		public TaskDataModel() {

		}

		@Override
	    public List<ScheduleTask> load(int startingAt, int maxPerPage, String sortField, SortOrder sortOrder, Map<String, Object> filters) {

			List<ScheduleTask> tasks = new ArrayList<ScheduleTask>();

			if (filters != null && !filters.isEmpty()) {
				// String userId = filters.get("userId");
				// users = userService.filterUsers(userId,maxPerPage+startingAt,
				// startingAt);
				// setRowCount(userService.countFilteredUser(userId));
				// setPageSize(maxPerPage);

			} else if (action == "search") {
				// users =
				// userService.getUserByCategory(maxPerPage+startingAt,
				// startingAt, userId, branch, role, status);
				// Integer jmlAll = userService.countUserByCategory(userId, branch,
				// role, status);
				// setRowCount(jmlAll);
				// setPageSize(maxPerPage);
			} else {
				tasks = schedulerService.getAllTask(maxPerPage
						+ startingAt, startingAt);
				Integer jmlAll = schedulerService.countAllTask();
				setRowCount(jmlAll);
				setPageSize(maxPerPage);
			}
			return tasks;
		}
	}

}
