package com.bjb.web.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.TreeNode;

import com.bjb.constant.Constants;
import com.bjb.model.Holiday;
import com.bjb.service.HolidayService;
import com.bjb.service.SessionService;
import com.bjb.service.SystemParameterService;
import com.bjb.util.HelperUtils;
import com.bjb.web.bean.support.DiffService;
import com.bjb.web.helper.FacesUtils;

@ManagedBean
@ViewScoped
public class HolidayHandler implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private Holiday holiday;
	
	private Holiday holidayTemp;

	private LazyDataModel<Holiday> holidayList;
	
	private String action;
	
	private String holidayId = "";
	
	private String holidayName = "";
	
	private Date holidayDate = null;

	private List<Integer> status;;
	private List<String> approvalStatus;

	private boolean active;
	private boolean inactive;

	private boolean pending;
	private boolean approved;
	private boolean rejected;
	
	private boolean doShowSearch = false;
	
	private boolean doShowHistory = false;
	
	private String currentHolidayName;

	@ManagedProperty(value="#{sessionService}")
	private SessionService sessionService;
	
	@ManagedProperty(value="#{holidayService}")
	private HolidayService holidayService;

	@ManagedProperty(value="#{systemParameterService}")
	private SystemParameterService systemParameterService;

	@ManagedProperty(value="#{diffService}")
	private DiffService diffService;
	
	public void setDiffService(DiffService diffService) {
		this.diffService = diffService;
	}

	public TreeNode getDiffTreeTable() {
		return diffService.diff(holidayTemp, holiday);
	}
	
	public void setSessionService(SessionService sessionService) {
		this.sessionService = sessionService;
	}

	public void setHolidayService(HolidayService holidayService){
		this.holidayService = holidayService;
	}

	public void setSystemParameterService(HolidayService SystemParameter) {
		this.systemParameterService = systemParameterService;
	}

	@PostConstruct
	public void init(){
		sessionService.authMenu(sessionService.getUser().getId(), "holiday");
		doSearch();
		
	}

	public Holiday getHoliday() {
		return holiday;
	}

	public void setHoliday(Holiday holiday) {
		this.holiday = holiday;
	}
	
	public Holiday getHolidayTemp() {
		return holidayTemp;
	}

	public void setHolidayTemp(Holiday holidayTemp) {
		this.holidayTemp = holidayTemp;
	}

	public LazyDataModel<Holiday> getHolidayList() {
		return holidayList;
	}

	public void setHolidayList(LazyDataModel<Holiday> holidayList) {
		this.holidayList = holidayList;
	}

	public String getHolidayId() {
		return holidayId;
	}

	public void setHolidayId(String holidayId) {
		this.holidayId = holidayId;
	}

	public String getHolidayName() {
		return holidayName;
	}

	public void setHolidayName(String holidayName) {
		this.holidayName = holidayName;
	}

	public Date getHolidayDate() {
		return holidayDate;
	}

	public void setHolidayDate(Date holidayDate) {
		this.holidayDate = holidayDate;
	}
	
	public List<String> getStatus() {
		return approvalStatus;
	}

	public void setStatus(List<String> status) {
		this.approvalStatus = status;
	}

	public boolean isPending() {
		return pending;
	}

	public void setPending(boolean pending) {
		this.pending = pending;
	}

	public boolean isApproved() {
		return approved;
	}

	public void setApproved(boolean approved) {
		this.approved = approved;
	}

	public boolean isRejected() {
		return rejected;
	}

	public void setRejected(boolean rejected) {
		this.rejected = rejected;
	}

	public boolean isDoShowSearch() {
		return doShowSearch;
	}

	public void setDoShowSearch(boolean doShowSearch) {
		this.doShowSearch = doShowSearch;
	}
	
	

	public boolean isDoShowHistory() {
		return doShowHistory;
	}

	public void setDoShowHistory(boolean doShowHistory) {
		this.doShowHistory = doShowHistory;
	}

	public void showSearch(){
		doShowSearch = true;
	}
	
	public void hideSearch(){
		doShowSearch = false;
	}
	
	public void showHistory(){
		doShowHistory = true;
	}
	
	public void hideHistory(){
		doShowHistory = false;
	}
	
	public void doSearch(){
		status = new ArrayList<Integer>();
		if (!active && !inactive) {
			status.add(Constants.STATUS_ACTIVE);
			status.add(Constants.STATUS_INACTIVE);
		} else {
			if (active) {
				status.add(Constants.STATUS_ACTIVE);
			}
			if (inactive) {
				status.add(Constants.STATUS_INACTIVE);
			}
		}
		approvalStatus = new ArrayList<String>();

		if (!pending && !approved && !rejected) {
			approvalStatus.add(Constants.APPROVAL_STATUS_PENDING_CREATE);
			approvalStatus.add(Constants.APPROVAL_STATUS_PENDING_EDIT);
			approvalStatus.add(Constants.APPROVAL_STATUS_PENDING_DELETE);
			approvalStatus.add(Constants.APPROVAL_STATUS_APPROVED_CREATE);
			approvalStatus.add(Constants.APPROVAL_STATUS_APPROVED_EDIT);
			approvalStatus.add(Constants.APPROVAL_STATUS_REJECTED_CREATE);
			approvalStatus.add(Constants.APPROVAL_STATUS_REJECTED_EDIT);
			approvalStatus.add(Constants.APPROVAL_STATUS_REJECTED_DELETE);
		}else{
			if (pending == true){
				approvalStatus.add(Constants.APPROVAL_STATUS_PENDING_CREATE);
				approvalStatus.add(Constants.APPROVAL_STATUS_PENDING_EDIT);
				approvalStatus.add(Constants.APPROVAL_STATUS_PENDING_DELETE);
			}
			if (approved == true){
				approvalStatus.add(Constants.APPROVAL_STATUS_APPROVED_CREATE);
				approvalStatus.add(Constants.APPROVAL_STATUS_APPROVED_EDIT);
			}
			if (rejected == true){
				approvalStatus.add(Constants.APPROVAL_STATUS_REJECTED_CREATE);
				approvalStatus.add(Constants.APPROVAL_STATUS_REJECTED_EDIT);
				approvalStatus.add(Constants.APPROVAL_STATUS_REJECTED_DELETE);
			}
		}

		FacesUtils.resetPage("holidayForm:holidayTable");
		holidayList = new HolidayDataModel();
		
	}

	public void newHoliday(){
		this.holiday = new Holiday();
		SimpleDateFormat sdf = new SimpleDateFormat(Constants.FORMAT_DATE_SHORT);
		String id = sdf.format(new Date());
		BigDecimal seq = holidayService.generateId();
		this.holiday.setCode(id+"HOL"+String.format("%04d", seq.intValue()));
		this.holiday.setStatus(Constants.STATUS_ACTIVE);
		this.action = "";
	}
	
	public void doSave(){
		if (this.action != null && this.action.equals("edit")) {
			editHoliday(this.holiday);
		} else {
			try {

				Holiday holidayExist = holidayService
						.findByCode(
								this.holiday.getName());

				if (holidayExist == null
						|| (holidayExist.getName()
								.equals(currentHolidayName))) {
					createHoliday(this.holiday);
				} else {
					String headerMessage = "Attention";
					String bodyMessage = "Holiday "
							+ this.holiday.getName()
							+ " already exist in the application.";

					FacesUtils.setWarningMessageDialog(headerMessage,
							bodyMessage);
				}

			} catch (Exception e) {
				String headerMessage = "Error";
				String bodyMessage = "System error";

				FacesUtils.setErrorMessageDialog(headerMessage, bodyMessage);
				e.printStackTrace();
			}
		}

	}

	public void editHoliday(Holiday holiday){

		holiday = holidayService.requestToModify(holidayTemp, holiday);
		
		RequestContext.getCurrentInstance().execute(
				"PF('insertOrEditHoliday').hide()");
		RequestContext.getCurrentInstance().execute(
				"PF('insertOrEditHolidayPending').hide()");

		String headerMessage = "Edit Holiday Successfully";
		String bodyMessage = "Supervisor must approve the editing of the <br/>holiday &apos;"
				+ holiday.getName()
				+ "&apos; to completely the process.";

		FacesUtils.setInfoMessageDialog(headerMessage, bodyMessage);

	}
	
	public void createHoliday(Holiday holiday){		
		holidayService.requestToCreate(holiday);
		
		RequestContext.getCurrentInstance().execute("PF('insertOrEditHoliday').hide()");
		RequestContext.getCurrentInstance().execute("PF('insertOrEditHolidayPending').hide()");
		
		String headerMessage = "Create Holiday Successfully";
		String bodyMessage = "Supervisor must approve the creating of the <br/>holiday &apos;"
				+ holiday.getName() + "&apos; to completely the process.";

		FacesUtils.setInfoMessageDialog(headerMessage, bodyMessage);
	}
	
	public void viewDetailedHoliday(Holiday holiday){
		this.holiday = holiday;

		if (this.holiday.getApprovalStatus().equals(
				Constants.APPROVAL_STATUS_PENDING_EDIT)) {
			
			this.holidayTemp = holidayService.findPendingApprovalByCode(holiday.getCode());
					
			if (holidayTemp!=null) {
				RequestContext.getCurrentInstance().execute(
							"PF('viewHolidayEdit').show()");
				return;
			}
						
		} 
		RequestContext.getCurrentInstance().execute("PF('viewHoliday').show()");
	}
	
	public void viewDetailedHolidayEdit(Holiday holiday){
		

		this.holiday = holiday;

		if (this.holiday.getDisableEdit() == true) {
			FacesUtils.setApprovalWarning("parameter",
					this.holiday.getName(),
					this.holiday.getApprovalStatus());
		} else {

			this.action = "edit";
			if (this.holiday.getApprovalStatus().equals(
					Constants.APPROVAL_STATUS_PENDING_EDIT)) {

				this.holidayTemp = holidayService.findPendingApprovalByCode(holiday.getCode());

				if (holidayTemp != null) {
						currentHolidayName= this.holidayTemp.getName();
						RequestContext.getCurrentInstance().execute(
								"PF('insertOrEditHolidayPending').show()");
						return;
				} 
			} 
			this.holidayTemp = HelperUtils.clone(holiday);
			RequestContext.getCurrentInstance().execute(
						"PF('insertOrEditHoliday').show()");
		}

	}
	
	public void viewDetailedDelete(Holiday holiday){

		this.holiday = holiday;
		
		if (this.holiday.getDisableDelete() == true){
			FacesUtils.setApprovalWarning("holiday", this.holiday.getName(), this.holiday.getApprovalStatus());
		}else{
			RequestContext.getCurrentInstance().execute(
					"PF('dlgConfirm').show()");
		}
		
	}
	
	public void deleteHoliday(){
		
		holidayService.requestToRemove(holiday);

		if (holiday.getApprovalStatus().equals(Constants.APPROVAL_STATUS_REJECTED_CREATE)){

			String headerMessage = "Success";
			String bodyMessage = "Delete Holiday " + this.holiday.getName() + " Successfully";
	
			FacesUtils.setInfoMessageDialog(headerMessage, bodyMessage);
			
		}else{
		
			String headerMessage = "Delete Holiday Successfully";
			String bodyMessage = "Supervisor must approve the deleting of the <br/>holiday &apos;"
					+ holiday.getName()+ "&apos; to completely the process.";
	
			FacesUtils.setInfoMessageDialog(headerMessage, bodyMessage);
		
		}
		
	}
	
	public void viewDetailedChangeStatus(Holiday holiday) {

		this.holiday = holiday;

		if (this.holiday.getDisableDelete() == true) {
			FacesUtils.setApprovalWarning("parameter",
					this.holiday.getName(),
					this.holiday.getApprovalStatus());
		} else {
			RequestContext.getCurrentInstance().execute("PF('dlgConfirmChangeStatus').show()");
		}

	}

	public void changeStatusHoliday() {
		this.holidayTemp = (Holiday) HelperUtils.clone(holiday);
		
		holiday.setStatus(holiday.getStatus()==Constants.STATUS_ACTIVE?Constants.STATUS_INACTIVE:Constants.STATUS_ACTIVE);

		holiday = holidayService.requestToModify(holidayTemp, holiday);

		String headerMessage = "Change Status Holiday Successfully";
		String bodyMessage = "Supervisor must approve the change status of the <br/>holiday &apos;"
				+ holiday.getName()
				+ "&apos; to completely the process.";

		FacesUtils.setInfoMessageDialog(headerMessage, bodyMessage);
	}

	private class HolidayDataModel extends LazyDataModel<Holiday>{
		private static final long serialVersionUID = 1L;
		public HolidayDataModel() {
			
		}
		@Override
	    public List<Holiday> load(int startingAt, int maxPerPage, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
			
			List<Holiday> holidays;
			
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			String hd = "";
			if ((holidayDate != null) && (!holidayDate.equals("")))
				hd = df.format(holidayDate);
			
			holidays = holidayService.getHolidayByCategory(maxPerPage+startingAt, startingAt, holidayId, holidayName, hd, approvalStatus);
			Integer jmlAll = holidayService.countHolidayByCategory(holidayId, holidayName, hd, approvalStatus);
			setRowCount(jmlAll);
			setPageSize(maxPerPage);

			return holidays;
		}
	}
	
}