package com.bjb.web.bean;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.omnifaces.model.tree.ListTreeModel;
import org.omnifaces.model.tree.TreeModel;
import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.TreeNode;

import com.bjb.constant.Constants;
import com.bjb.model.Action;
import com.bjb.model.Menu;
import com.bjb.model.Role;
import com.bjb.model.RoleMenu;
import com.bjb.service.MenuService;
import com.bjb.service.RoleService;
import com.bjb.service.SessionService;
import com.bjb.service.SystemParameterService;
import com.bjb.util.HelperUtils;
import com.bjb.web.bean.support.DiffService;
import com.bjb.web.helper.FacesUtils;

@ManagedBean
@ViewScoped
public class RoleWebHandler implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Role role;
	private Role roleTemp;
	private Menu menu;
	private LazyDataModel<Role> roleList;
	private List<Menu> menus;
	private TreeModel<Menu> tree;
	private TreeModel<Menu> selectedMenu;
	private HashMap<Integer, Menu> rootMenu;
	private HashMap<Integer, Menu> childMenu;
	
	private List<Menu> tempMenus;
	private TreeModel<Menu> tempTree;
	private TreeModel<Menu> tempSelectedMenu;
	private HashMap<Integer, Menu> tempRootMenu;
	private HashMap<Integer, Menu> tempChildMenu;
	private String action;
	
	private String roleCode;
	private String roleName;
	private List<Integer> status;;
	private List<String> approvalStatus;

	private boolean active;
	private boolean inactive;

	private boolean pending;
	private boolean approved;
	private boolean rejected;
	
	private boolean doShowSearch = false;
	
	private boolean doShowHistory = false;
	
	private String currentRolename;
	
	@ManagedProperty(value="#{sessionService}")
	private SessionService sessionService;

	@ManagedProperty(value="#{roleService}")
	private RoleService roleService;

	@ManagedProperty(value="#{systemParameterService}")
	private SystemParameterService systemParameterService;

	@ManagedProperty(value="#{menuService}")
	private MenuService menuService;

	@ManagedProperty(value="#{diffService}")
	private DiffService diffService;
	
	public void setDiffService(DiffService diffService) {
		this.diffService = diffService;
	}

	public TreeNode getDiffTreeTable() {
		return diffService.diff(roleTemp, role);
	}
	
	public void setSessionService(SessionService sessionService) {
		this.sessionService = sessionService;
	}

	public void setRoleService(RoleService roleService){
		this.roleService = roleService;
	}

	public void setSystemParameterService(SystemParameterService systemParameterService){
		this.systemParameterService = systemParameterService;
	}
	
	public void setMenuService(MenuService menuService) {
		this.menuService = menuService;
	}
	
	@PostConstruct
	public void init(){
		sessionService.authMenu(sessionService.getUser().getId(), "adminRole");

		roleCode = "";
		roleName = "";
		doSearch();
	}
	
	public void newRole(){
		this.role = new Role();
		SimpleDateFormat sdf = new SimpleDateFormat(Constants.FORMAT_DATE_SHORT);
		String id = sdf.format(new Date());
		Integer seq = roleService.generateId();
//		this.role.setCode(id+"ROLE"+String.format("%04d", seq));
		this.role.setId(seq);
		this.role.setStatus(Constants.STATUS_ACTIVE);
		createTreeMenu();
		this.action = "";
	}
	
	public Role getTempRole() {
		return roleTemp;
	}

	public void setTempRole(Role tempRole) {
		this.roleTemp = tempRole;
	}

	public List<Menu> getTempMenus() {
		return tempMenus;
	}

	public void setTempMenus(List<Menu> tempMenus) {
		this.tempMenus = tempMenus;
	}

	public TreeModel<Menu> getTempTree() {
		return tempTree;
	}

	public void setTempTree(TreeModel<Menu> tempTree) {
		this.tempTree = tempTree;
	}

	public TreeModel<Menu> getTempSelectedMenu() {
		return tempSelectedMenu;
	}

	public void setTempSelectedMenu(TreeModel<Menu> tempSelectedMenu) {
		this.tempSelectedMenu = tempSelectedMenu;
	}

	public HashMap<Integer, Menu> getTempRootMenu() {
		return tempRootMenu;
	}

	public void setTempRootMenu(HashMap<Integer, Menu> tempRootMenu) {
		this.tempRootMenu = tempRootMenu;
	}

	public HashMap<Integer, Menu> getTempChildMenu() {
		return tempChildMenu;
	}

	public void setTempChildMenu(HashMap<Integer, Menu> tempChildMenu) {
		this.tempChildMenu = tempChildMenu;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getRoleCode() {
		return roleCode;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}
	

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public List<String> getStatus() {
		return approvalStatus;
	}

	public void setStatus(List<String> status) {
		this.approvalStatus = status;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public boolean isInactive() {
		return inactive;
	}

	public void setInactive(boolean inactive) {
		this.inactive = inactive;
	}

	public boolean isPending() {
		return pending;
	}

	public void setPending(boolean pending) {
		this.pending = pending;
	}

	public boolean isApproved() {
		return approved;
	}

	public void setApproved(boolean approved) {
		this.approved = approved;
	}

	public boolean isRejected() {
		return rejected;
	}

	public void setRejected(boolean rejected) {
		this.rejected = rejected;
	}

	public boolean isDoShowSearch() {
		return doShowSearch;
	}

	public void setDoShowSearch(boolean doShowSearch) {
		this.doShowSearch = doShowSearch;
	}

	public void showSearch(){
		doShowSearch = true;
	}
	
	public void hideSearch(){
		doShowSearch = false;
	}
	
	
	public void showHistory(){
		doShowHistory = true;
	}
	
	public void hideHistory(){
		doShowHistory = false;
	}
	
	public boolean isDoShowHistory() {
		return doShowHistory;
	}

	public void setDoShowHistory(boolean doShowHistory) {
		this.doShowHistory = doShowHistory;
	}
	
	private class RoleDataModel extends LazyDataModel<Role>{
		private static final long serialVersionUID = 1L;
		public RoleDataModel() {
			
		}
		@Override
	    public List<Role> load(int startingAt, int maxResult, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
			
			List<Role> roles;
			
			roles = roleService.selectByCriteria(startingAt, maxResult, roleCode, roleName, status, approvalStatus);
			Integer jmlAll = roleService.countByCriteria(roleCode, roleName, status,  approvalStatus);
			setRowCount(jmlAll);
			setPageSize(maxResult);
			
			return roles;
		}
	}
	
	public void viewDetailedRole(Role role){

		if (role.getApprovalStatus().equals(
				Constants.APPROVAL_STATUS_PENDING_EDIT)) {
			
			this.roleTemp = HelperUtils.clone(role);
			createTempTreeMenu();

			this.role = roleService.findPendingApprovalByCode(role.getCode());
			createTreeMenu();
					
			if (role!=null) {
				RequestContext.getCurrentInstance().execute(
							"PF('viewRoleEdit').show()");
				return;
			}
						
		} else {
			this.role = role;
			createTreeMenu();
		}
	
		RequestContext.getCurrentInstance().execute("PF('viewRole').show()");
	}	
	

	public void viewDetailedRoleEdit(Role role){

		if (role.getDisableEdit() == true) {
			FacesUtils.setApprovalWarning("parameter",
					role.getName(),
					role.getApprovalStatus());
		} else {
			this.action = "edit";
			if (role.getApprovalStatus().equals(
					Constants.APPROVAL_STATUS_PENDING_EDIT)) {

				this.roleTemp = HelperUtils.clone(role);
				createTempTreeMenu();
				this.role = roleService.findPendingApprovalByCode(role.getCode());
				createTreeMenu();

				if (roleTemp != null) {
					currentRolename = this.roleTemp.getName();
					RequestContext.getCurrentInstance().execute(
							"PF('insertOrEditRolePending').show()");
					return;
				} 
			} else {
				this.role = role;
				createTreeMenu();
				this.roleTemp = HelperUtils.clone(role);
				createTempTreeMenu();
			}
			RequestContext.getCurrentInstance().execute(
						"PF('insertOrEditRole').show()");
		}
	}
	
	public void viewDetailedDelete(Role role){

		this.role = role;
		
		if (this.role.getDisableDelete() == true){
			FacesUtils.setApprovalWarning("role", this.role.getName(), this.role.getApprovalStatus());
		}else{
			RequestContext.getCurrentInstance().execute(
					"PF('dlgConfirm').show()");
		}
		
	}
	
	public void deleteRole(){
		
		roleService.requestToRemove(role);

		if (role.getApprovalStatus().equals(Constants.APPROVAL_STATUS_REJECTED_CREATE)){

			String headerMessage = "Success";
			String bodyMessage = "Delete Role "+ role.getName() +" Successfully";
			
			FacesUtils.setInfoMessageDialog(headerMessage, bodyMessage);
			
		}else{
			
			String headerMessage = "Delete Role Successfully";
			String bodyMessage = "Supervisor must approve the deleting of the <br/>role &apos;"
					+ role.getName() + "&apos; to completely the process.";
	
			FacesUtils.setInfoMessageDialog(headerMessage, bodyMessage);
		}
		
	}
	
	public void viewDetailedChangeStatus(Role role) {

		this.role = role;

		if (this.role.getDisableDelete() == true) {
			FacesUtils.setApprovalWarning("parameter",
					this.role.getName(),
					this.role.getApprovalStatus());
		} else {
			RequestContext.getCurrentInstance().execute("PF('dlgConfirmChangeStatus').show()");
		}

	}

	public void changeStatusRole() {
		
		this.roleTemp = (Role) HelperUtils.clone(role);
		
		role.setStatus(role.getStatus()==Constants.STATUS_ACTIVE?Constants.STATUS_INACTIVE:Constants.STATUS_ACTIVE);

		role = roleService.requestToModify(roleTemp, role);

		String headerMessage = "Change Status Role Successfully";
		String bodyMessage = "Supervisor must approve the change status of the <br/>role &apos;"
				+ role.getName()
				+ "&apos; to completely the process.";

		FacesUtils.setInfoMessageDialog(headerMessage, bodyMessage);
	}

	
	
	public void doSave(){
				
		Set<RoleMenu> selectedMenu = new HashSet<RoleMenu>();
		for (Map.Entry<Integer, Menu> entry : this.childMenu.entrySet()) {
			Menu menu = entry.getValue();
			if (menu!=null && menu.isSelected() == true) {
				selectedMenu.add(new RoleMenu(this.role.getId(), menu));
			}
		}
		
		for (Map.Entry<Integer, Menu> entry : this.rootMenu.entrySet()) {
			Menu menu = entry.getValue();
			if (menu!=null && menu.isSelected() == true) {
				selectedMenu.add(new RoleMenu(this.role.getId(), menu));
			}
		}
		
		this.role.setRoleMenus(selectedMenu);
		
		if (this.action != null && this.action.equals("edit")) {
			editRole(this.role);
		} else {
			try {

				Role roleExist = roleService
						.findByCode(
								this.role.getName());

				if (roleExist == null
						|| (roleExist.getName()
								.equals(currentRolename))) {
					createRole(this.role);
				} else {
					String headerMessage = "Attention";
					String bodyMessage = "Role "
							+ this.role.getName()
							+ " already exist in the application.";

					FacesUtils.setWarningMessageDialog(headerMessage,
							bodyMessage);
				}

			} catch (Exception e) {
				String headerMessage = "Error";
				String bodyMessage = "System error";

				FacesUtils.setErrorMessageDialog(headerMessage, bodyMessage);
				e.printStackTrace();
			}
		}

	}
	
	public void editRole(Role role){
		role = roleService.requestToModify(roleTemp, role);
		
		RequestContext.getCurrentInstance().execute(
				"PF('insertOrEditRole').hide()");
		RequestContext.getCurrentInstance().execute(
				"PF('insertOrEditRolePending').hide()");

		String headerMessage = "Edit Role Successfully";
		String bodyMessage = "Supervisor must approve the editing of the <br/>role &apos;"
				+ role.getName()
				+ "&apos; to completely the process.";

		FacesUtils.setInfoMessageDialog(headerMessage, bodyMessage);

		RequestContext.getCurrentInstance().execute("PF('insertOrEditRole').hide()");
		RequestContext.getCurrentInstance().execute("PF('insertOrEditRolePending').hide()");
		
	}
	
	public void createRole(Role role){
		roleService.requestToCreate(role);

		RequestContext.getCurrentInstance().execute("PF('insertOrEditRole').hide()");
		RequestContext.getCurrentInstance().execute("PF('insertOrEditRolePending').hide()");
		
		String headerMessage = "Create Role Successfully";
		String bodyMessage = "Supervisor must approve the creating of the <br/>role &apos;"
				+ role.getName() + "&apos; to completely the process.";

		FacesUtils.setInfoMessageDialog(headerMessage, bodyMessage);
	}
	
	public void doSearch(){
		
		status = new ArrayList<Integer>();
		if (!active && !inactive) {
			status.add(Constants.STATUS_ACTIVE);
			status.add(Constants.STATUS_INACTIVE);
		} else {
			if (active) {
				status.add(Constants.STATUS_ACTIVE);
			}
			if (inactive) {
				status.add(Constants.STATUS_INACTIVE);
			}
		}
		approvalStatus = new ArrayList<String>();

		if (!pending && !approved && !rejected) {
			approvalStatus.add(Constants.APPROVAL_STATUS_PENDING_CREATE);
			approvalStatus.add(Constants.APPROVAL_STATUS_PENDING_EDIT);
			approvalStatus.add(Constants.APPROVAL_STATUS_PENDING_DELETE);
			approvalStatus.add(Constants.APPROVAL_STATUS_APPROVED_CREATE);
			approvalStatus.add(Constants.APPROVAL_STATUS_APPROVED_EDIT);
			approvalStatus.add(Constants.APPROVAL_STATUS_REJECTED_CREATE);
			approvalStatus.add(Constants.APPROVAL_STATUS_REJECTED_EDIT);
			approvalStatus.add(Constants.APPROVAL_STATUS_REJECTED_DELETE);
		}else{
			if (pending == true){
				approvalStatus.add(Constants.APPROVAL_STATUS_PENDING_CREATE);
				approvalStatus.add(Constants.APPROVAL_STATUS_PENDING_EDIT);
				approvalStatus.add(Constants.APPROVAL_STATUS_PENDING_DELETE);
			}
			if (approved == true){
				approvalStatus.add(Constants.APPROVAL_STATUS_APPROVED_CREATE);
				approvalStatus.add(Constants.APPROVAL_STATUS_APPROVED_EDIT);
			}
			if (rejected == true){
				approvalStatus.add(Constants.APPROVAL_STATUS_REJECTED_CREATE);
				approvalStatus.add(Constants.APPROVAL_STATUS_REJECTED_EDIT);
				approvalStatus.add(Constants.APPROVAL_STATUS_REJECTED_DELETE);
			}
		}

		FacesUtils.resetPage("roleForm:roleTable");
		roleList = new RoleDataModel();	
	}
	
	public void checkThebox(Menu checkedMenu){
		this.selectedMenu = null;
		List<TreeModel<Menu>> root = tree.getChildren();
		getCheckedMenuFromTree(root, checkedMenu);
		if (this.selectedMenu != null) {
			checkTheChild(selectedMenu);
			checkTheParent(selectedMenu);
		}
		
	}
	
	public void checkAction(Menu menu, Action action){
		if (action.getName().equals(menu.getActionList().get(0).getName())) {
			if (action.isSelected()) {
				action.setSelected(false);
			}else{
				action.setSelected(true);
			}
		}
	}
	
	public void checkTheboxPending(Menu checkedMenu){
		this.tempSelectedMenu = null;
		List<TreeModel<Menu>> root = tempTree.getChildren();
		getCheckedMenuFromTreePending(root, checkedMenu);
		if (this.tempSelectedMenu != null) {
			checkTheChild(tempSelectedMenu);
			checkTheParent(tempSelectedMenu);
		}
		
	}
	
	public void getCheckedMenuFromTree(List<TreeModel<Menu>> root, Menu checkedMenu){		
		for (TreeModel<Menu> treeModel : root) {
			Menu data = treeModel.getData();
			if (data.getId().equals( checkedMenu.getId() )) {
				this.selectedMenu = treeModel;
				return;
			}
			if (this.selectedMenu != null) {
				return;
			}
			getCheckedMenuFromTree(treeModel.getChildren(), checkedMenu);
		}
	}
	
	public void getCheckedMenuFromTreePending(List<TreeModel<Menu>> root, Menu checkedMenu){		
		for (TreeModel<Menu> treeModel : root) {
			Menu data = treeModel.getData();
			if (data.getId().equals( checkedMenu.getId() )) {
				this.tempSelectedMenu = treeModel;
				return;
			}
			if (this.tempSelectedMenu != null) {
				return;
			}
			getCheckedMenuFromTree(treeModel.getChildren(), checkedMenu);
		}
	}
	
	public void checkTheChild(TreeModel<Menu> currentParent){
		List<TreeModel<Menu>> children = currentParent.getChildren();
		Menu dataParent = currentParent.getData();
		for (TreeModel<Menu> treeModel : children) {
			Menu data = treeModel.getData();
			data.setSelected(dataParent.isSelected());			
			checkTheChild(treeModel);
		}
	}
	
	public void checkTheParent(TreeModel<Menu> currentParent){
		Menu currentParentData = currentParent.getData();		
		TreeModel<Menu> parent = currentParent.getParent();
		
		if (parent != null && parent.getData() != null && 
				currentParentData != null) {
			Menu data = parent.getData();
			if (currentParentData.isSelected()) { // kalau di check doang parent nya di check
				data.setSelected(currentParentData.isSelected());
				checkTheParent(parent);
			}
		}
	}
	
	public void createTreeMenu(){
		this.menus = menuService.select();
		rootMenu = new HashMap<Integer, Menu>();
		childMenu = new HashMap<Integer, Menu>();
		tree = new ListTreeModel<Menu>();
		for (Menu menu : this.menus) {
			// agar menu di applicationBean tidak berubah
			Menu tempMenu = new Menu(menu);
			// set actions unchecked
			for (Action action:tempMenu.getActionList()) {
				action.setSelected(false);
			}
			
			Collection<RoleMenu> roleMenus = role.getRoleMenus();
			for (RoleMenu roleMenu2 : roleMenus) {
				if (roleMenu2.getMenu().getId().equals(tempMenu.getId())) {
					tempMenu.setSelected(true);
					for (Action action:tempMenu.getActionList()) {
						action.setSelected(roleMenu2.getActionList().contains(action));
					}
				}
			}
			
			if (tempMenu.getParentId() == 0) {
				rootMenu.put(menu.getId(), tempMenu);
			}else{
				childMenu.put(menu.getId(), tempMenu);
			}
		}
		
		for (Map.Entry<Integer, Menu> entry : rootMenu.entrySet()) {
			int parentMenuId = entry.getValue().getId();
			TreeModel<Menu> currentRoot = tree.addChild(entry.getValue());
			createChild(currentRoot, parentMenuId, childMenu);
		}
	}
	
	public void createTempTreeMenu(){
		this.tempMenus = menuService.select();
		tempRootMenu = new HashMap<Integer, Menu>();
		tempChildMenu = new HashMap<Integer, Menu>();
		tempTree = new ListTreeModel<Menu>();
		for (Menu menu : this.tempMenus) {
			// agar menu di applicationBean tidak berubah
			Menu tempMenu = new Menu(menu);
			// set actions unchecked
			for (Action action:tempMenu.getActionList()) {
				action.setSelected(false);
			}
			
			Collection<RoleMenu> roleMenus = roleTemp.getRoleMenus();
			for (RoleMenu roleMenu2 : roleMenus) {
				if (roleMenu2.getMenu().getId().equals(tempMenu.getId())) {
					tempMenu.setSelected(true);
					for (Action action:tempMenu.getActionList()) {
						action.setSelected(roleMenu2.getActionList().contains(action));
					}
				} 
			}
			if (tempMenu.getParentId() == 0) {
				tempRootMenu.put(menu.getId(), tempMenu);
			}else{
				tempChildMenu.put(menu.getId(), tempMenu);
			}
		}
		
		for (Map.Entry<Integer, Menu> entry : tempRootMenu.entrySet()) {
			int parentMenuId = entry.getValue().getId();
			TreeModel<Menu> currentRoot = tempTree.addChild(entry.getValue());
			createChild(currentRoot, parentMenuId, tempChildMenu);
		}
	}
	
	private void createChild(TreeModel<Menu> treeMenu, 
			int menuId, Map<Integer, Menu> childMenu){
		for (Map.Entry<Integer, Menu> entry : childMenu.entrySet()) {
			Menu tempMenu = entry.getValue();
			if (tempMenu.getParentId().equals(menuId)) {
				TreeModel<Menu> currentParent = treeMenu.addChild(tempMenu);	
				createChild(currentParent, tempMenu.getId(), childMenu);
			}
		}
	}
	
	public Role getRole() {
		return role;
	}
	public void setRole(Role role) {
		this.role = role;
	}
	public Menu getMenu() {
		return menu;
	}
	public void setMenu(Menu menu) {
		this.menu = menu;
	}

	public LazyDataModel<Role> getRoleList() {
		return roleList;
	}
	public void setRoleList(LazyDataModel<Role> roleList) {
		this.roleList = roleList;
	}

	public List<Menu> getMenus() {
		return menus;
	}

	public void setMenus(List<Menu> menus) {
		this.menus = menus;
	}

	public TreeModel<Menu> getTree() {
		return tree;
	}

	public void setTree(TreeModel<Menu> tree) {
		this.tree = tree;
	}

	public HashMap<Integer, Menu> getRootMenu() {
		return rootMenu;
	}

	public void setRootMenu(HashMap<Integer, Menu> rootMenu) {
		this.rootMenu = rootMenu;
	}

	public HashMap<Integer, Menu> getChildMenu() {
		return childMenu;
	}

	public void setChildMenu(HashMap<Integer, Menu> childMenu) {
		this.childMenu = childMenu;
	}

	public TreeModel<Menu> getSelectedMenu() {
		return selectedMenu;
	}

	public void setSelectedMenu(TreeModel<Menu> selectedMenu) {
		this.selectedMenu = selectedMenu;
	}

}
