package com.bjb.web.bean;

import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.omnifaces.model.tree.ListTreeModel;
import org.omnifaces.model.tree.TreeModel;
import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.bjb.constant.Constants;
import com.bjb.model.Action;
import com.bjb.model.Aggregator;
import com.bjb.model.ApprovalPending;
import com.bjb.model.Auditable;
import com.bjb.model.Branch;
import com.bjb.model.CollectingAgent;
import com.bjb.model.Feature;
import com.bjb.model.Holiday;
import com.bjb.model.Menu;
import com.bjb.model.Role;
import com.bjb.model.RoleMenu;
import com.bjb.model.ScheduleTask;
import com.bjb.model.SystemParameter;
import com.bjb.model.User;
import com.bjb.model.UserAggregator;
import com.bjb.model.UserBank;
import com.bjb.service.AggregatorService;
import com.bjb.service.ApprovalCompleteService;
import com.bjb.service.ApprovalPendingService;
import com.bjb.service.BranchService;
import com.bjb.service.CollectingAgentService;
import com.bjb.service.FeatureService;
import com.bjb.service.HolidayService;
import com.bjb.service.MenuService;
import com.bjb.service.OperationalService;
import com.bjb.service.RoleService;
import com.bjb.service.SchedulerService;
import com.bjb.service.SessionService;
import com.bjb.service.SystemParameterService;
import com.bjb.service.UserService;
import com.bjb.util.HelperUtils;
import com.bjb.web.helper.FacesUtils;
import com.bjb.web.model.UserSession;

@ManagedBean
@ViewScoped
public class ApprovalWebHandler implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<String> authorities;

	private ApprovalPending approvalPending;

	private UserAggregator user;

	private UserAggregator prevUser;

	private Role role;

	private Role prevRole;

	private ScheduleTask task;

	private ScheduleTask prevTask;

	private SystemParameter systemParameter;

	private SystemParameter prevSystemParameter;

	private Branch branch;

	private Branch prevBranch;

	private Holiday holiday;

	private Holiday prevHoliday;

	private Aggregator aggregator;

	private Aggregator prevAggregator;

	private Feature feature;

	private Feature prevFeature;

	private CollectingAgent collectingAgent;

	private CollectingAgent prevCollectingAgent;

	private List<Integer> hours;
	private List<Integer> minutes;

	private List<String> scheduleTypes;

	private String reason;

	private boolean runOnce = false;
	private boolean repeating = false;
	private boolean complexRepeating = false;
	private boolean repeatingOrComplexRepeating = false;

	private boolean prevRunOnce = false;
	private boolean prevRepeating = false;
	private boolean prevComplexRepeating = false;
	private boolean prevRepeatingOrComplexRepeating = false;

	private List<Menu> menus;
	private TreeModel<Menu> tree;
	private TreeModel<Menu> selectedMenu;
	private HashMap<Integer, Menu> rootMenu;
	private HashMap<Integer, Menu> childMenu;

	private List<Menu> prevMenus;
	private TreeModel<Menu> prevTree;
	private TreeModel<Menu> prevSelectedMenu;
	private HashMap<Integer, Menu> prevRootMenu;
	private HashMap<Integer, Menu> prevChildMenu;

	private LazyDataModel<ApprovalPending> approvalList;

	private List<String> rolesName;
	private List<String> prevRolesName;

	private boolean searchParameterUser;
	private boolean searchParameterRole;
	private boolean searchParameterScheduler;
	private boolean searchParameterSystemParameter;
	private boolean searchParameterBranch;
	private boolean searchParameterHoliday;
	private boolean searchParameterAggregator;
	private boolean searchParameterFeature;

	private boolean searchActionCreate;
	private boolean searchActionEdit;
	private boolean searchActionDelete;

	private String parameterCode;
	private String parameterName;
	private List<String> searchParameter;
	private List<String> searchAction;

	private boolean doShowSearch = false;

	public static Map<BigDecimal, ApprovalPending> approvalTemps = new HashMap<BigDecimal, ApprovalPending>();

	@ManagedProperty(value = "#{sessionService}")
	private SessionService sessionService;

	@ManagedProperty(value = "#{menuService}")
	private MenuService menuService;

	@ManagedProperty(value = "#{userService}")
	private UserService userService;

	@ManagedProperty(value = "#{roleService}")
	private RoleService roleService;

	@ManagedProperty(value = "#{approvalPendingService}")
	private ApprovalPendingService approvalPendingService;

	@ManagedProperty(value = "#{approvalCompleteService}")
	private ApprovalCompleteService approvalCompleteService;

	@ManagedProperty(value = "#{systemParameterService}")
	private SystemParameterService systemParameterService;

	@ManagedProperty(value = "#{branchService}")
	private BranchService branchService;

	@ManagedProperty(value = "#{schedulerService}")
	private SchedulerService schedulerService;

	@ManagedProperty(value = "#{holidayService}")
	private HolidayService holidayService;

	@ManagedProperty(value = "#{aggregatorService}")
	private AggregatorService aggregatorService;

	@ManagedProperty(value = "#{featureService}")
	private FeatureService featureService;

	@ManagedProperty(value = "#{collectingAgentService}")
	private CollectingAgentService collectingAgentService;

	public void setSessionService(SessionService sessionService) {
		this.sessionService = sessionService;
	}

	public void setMenuService(MenuService menuService) {
		this.menuService = menuService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public void setRoleService(RoleService roleService) {
		this.roleService = roleService;
	}

	public void setApprovalPendingService(ApprovalPendingService approvalPendingService) {
		this.approvalPendingService = approvalPendingService;
	}

	public void setApprovalCompleteService(ApprovalCompleteService approvalCompleteService) {
		this.approvalCompleteService = approvalCompleteService;
	}

	public void setSystemParameterService(SystemParameterService systemParameterService) {
		this.systemParameterService = systemParameterService;
	}

	public void setBranchService(BranchService branchService) {
		this.branchService = branchService;
	}

	public void setSchedulerService(SchedulerService schedulerService) {
		this.schedulerService = schedulerService;
	}

	public void setHolidayService(HolidayService holidayService) {
		this.holidayService = holidayService;
	}

	public void setAggregatorService(AggregatorService aggregatorService) {
		this.aggregatorService = aggregatorService;
	}

	public void setFeatureService(FeatureService featureService) {
		this.featureService = featureService;
	}

	public void setCollectingAgentService(CollectingAgentService collectingAgentService) {
		this.collectingAgentService = collectingAgentService;
	}

	@PostConstruct
	public void init() {
		authorities = sessionService.authMenu(sessionService.getUser(), "processApproval");

		approvalTemps.clear();
		user = new UserAggregator();
		role = new Role();
		initSelectItemData();
		initSearch();
		doSearch();
	}

	public void initSearch() {
		parameterCode = "";
		parameterName = "";
		initSearchParameter();
		initSearchAction();
	}

	public boolean isAuthorize(String action) {
		return authorities.contains(action);
	}

	public void initSearchParameter() {
		searchParameter = new ArrayList<String>();
		if (isAuthorize(Constants.APPROVAL_PARAMETER_USER)) {
			searchParameter.add(Constants.APPROVAL_PARAMETER_USER);
		}
		if (isAuthorize(Constants.APPROVAL_PARAMETER_ROLE)) {
			searchParameter.add(Constants.APPROVAL_PARAMETER_ROLE);
		}
		if (isAuthorize(Constants.APPROVAL_PARAMETER_SCHEDULER)) {
			searchParameter.add(Constants.APPROVAL_PARAMETER_SCHEDULER);
		}
		if (isAuthorize(Constants.APPROVAL_PARAMETER_SPARAM)) {
			searchParameter.add(Constants.APPROVAL_PARAMETER_SPARAM);
		}
		if (isAuthorize(Constants.APPROVAL_PARAMETER_BRANCH)) {
			searchParameter.add(Constants.APPROVAL_PARAMETER_BRANCH);
		}
		if (isAuthorize(Constants.APPROVAL_PARAMETER_HOLIDAY)) {
			searchParameter.add(Constants.APPROVAL_PARAMETER_HOLIDAY);
		}
		if (isAuthorize(Constants.APPROVAL_PARAMETER_AGGREGATOR)) {
			searchParameter.add(Constants.APPROVAL_PARAMETER_AGGREGATOR);
		}
		if (isAuthorize(Constants.APPROVAL_PARAMETER_FEATURE)) {
			searchParameter.add(Constants.APPROVAL_PARAMETER_FEATURE);
		}
	}

	public void initSearchAction() {
		searchAction = new ArrayList<String>();
		searchAction.add(Constants.APPROVAL_ACTION_CREATE);
		searchAction.add(Constants.APPROVAL_ACTION_EDIT);
		searchAction.add(Constants.APPROVAL_ACTION_DELETE);
	}

	@PreDestroy()
	public void destroy() {
		// System.out.println("destroy");
	}

	public boolean isDoShowSearch() {
		return doShowSearch;
	}

	public void setDoShowSearch(boolean doShowSearch) {
		this.doShowSearch = doShowSearch;
	}

	public String getParameterCode() {
		return parameterCode;
	}

	public void setParameterCode(String parameterCode) {
		this.parameterCode = parameterCode;
	}

	public String getParameterName() {
		return parameterName;
	}

	public void setParameterName(String parameterName) {
		this.parameterName = parameterName;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public ApprovalPending getApproval() {
		return approvalPending;
	}

	public void setApproval(ApprovalPending approvalPending) {
		this.approvalPending = approvalPending;
	}

	public List<String> getRolesName() {
		return rolesName;
	}

	public void setRolesName(List<String> rolesName) {
		this.rolesName = rolesName;
	}

	public List<String> getPrevRolesName() {
		return prevRolesName;
	}

	public void setPrevRolesName(List<String> prevRolesName) {
		this.prevRolesName = prevRolesName;
	}

	public User getUser() {
		return user;
	}

	public void setUser(UserAggregator user) {
		this.user = user;
	}

	public User getPrevUser() {
		return prevUser;
	}

	public void setPrevUser(UserAggregator prevUser) {
		this.prevUser = prevUser;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Role getPrevRole() {
		return prevRole;
	}

	public void setPrevRole(Role prevRole) {
		this.prevRole = prevRole;
	}

	public ScheduleTask getTask() {
		return task;
	}

	public void setTask(ScheduleTask task) {
		this.task = task;
	}

	public ScheduleTask getPrevTask() {
		return prevTask;
	}

	public void setPrevTask(ScheduleTask prevTask) {
		this.prevTask = prevTask;
	}

	public SystemParameter getSystemParameter() {
		return systemParameter;
	}

	public void setSystemParameter(SystemParameter systemParameter) {
		this.systemParameter = systemParameter;
	}

	public SystemParameter getPrevSystemParameter() {
		return prevSystemParameter;
	}

	public void setPrevSystemParameter(SystemParameter prevSystemParameter) {
		this.prevSystemParameter = prevSystemParameter;
	}

	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	public Branch getPrevBranch() {
		return prevBranch;
	}

	public void setPrevBranch(Branch prevBranch) {
		this.prevBranch = prevBranch;
	}

	public Holiday getHoliday() {
		return holiday;
	}

	public void setHoliday(Holiday holiday) {
		this.holiday = holiday;
	}

	public Holiday getPrevHoliday() {
		return prevHoliday;
	}

	public void setPrevHoliday(Holiday prevHoliday) {
		this.prevHoliday = prevHoliday;
	}

	public Aggregator getAggregator() {
		return aggregator;
	}

	public void setAggregator(Aggregator aggregator) {
		this.aggregator = aggregator;
	}

	public Aggregator getPrevAggregator() {
		return prevAggregator;
	}

	public void setPrevAggregator(Aggregator prevAggregator) {
		this.prevAggregator = prevAggregator;
	}

	public Feature getFeature() {
		return feature;
	}

	public void setFeature(Feature feature) {
		this.feature = feature;
	}

	public Feature getPrevFeature() {
		return prevFeature;
	}

	public void setPrevFeature(Feature prevFeature) {
		this.prevFeature = prevFeature;
	}

	public CollectingAgent getCollectingAgent() {
		return collectingAgent;
	}

	public void setCollectingAgent(CollectingAgent collectingAgent) {
		this.collectingAgent = collectingAgent;
	}

	public CollectingAgent getPrevCollectingAgent() {
		return prevCollectingAgent;
	}

	public void setPrevCollectingAgent(CollectingAgent prevCollectingAgent) {
		this.prevCollectingAgent = prevCollectingAgent;
	}

	public List<Menu> getMenus() {
		return menus;
	}

	public void setMenus(List<Menu> menus) {
		this.menus = menus;
	}

	public TreeModel<Menu> getTree() {
		return tree;
	}

	public void setTree(TreeModel<Menu> tree) {
		this.tree = tree;
	}

	public TreeModel<Menu> getSelectedMenu() {
		return selectedMenu;
	}

	public void setSelectedMenu(TreeModel<Menu> selectedMenu) {
		this.selectedMenu = selectedMenu;
	}

	public HashMap<Integer, Menu> getRootMenu() {
		return rootMenu;
	}

	public void setRootMenu(HashMap<Integer, Menu> rootMenu) {
		this.rootMenu = rootMenu;
	}

	public HashMap<Integer, Menu> getChildMenu() {
		return childMenu;
	}

	public void setChildMenu(HashMap<Integer, Menu> childMenu) {
		this.childMenu = childMenu;
	}

	public LazyDataModel<ApprovalPending> getApprovalList() {
		return approvalList;
	}

	public void setApprovalList(LazyDataModel<ApprovalPending> approvalList) {
		this.approvalList = approvalList;
	}

	public List<Menu> getPrevMenus() {
		return prevMenus;
	}

	public void setPrevMenus(List<Menu> prevMenus) {
		this.prevMenus = prevMenus;
	}

	public TreeModel<Menu> getPrevTree() {
		return prevTree;
	}

	public void setPrevTree(TreeModel<Menu> prevTree) {
		this.prevTree = prevTree;
	}

	public List<Integer> getHours() {
		return hours;
	}

	public void setHours(List<Integer> hours) {
		this.hours = hours;
	}

	public List<Integer> getMinutes() {
		return minutes;
	}

	public void setMinutes(List<Integer> minutes) {
		this.minutes = minutes;
	}

	public List<String> getScheduleTypes() {
		return scheduleTypes;
	}

	public void setScheduleTypes(List<String> scheduleTypes) {
		this.scheduleTypes = scheduleTypes;
	}

	public boolean isRunOnce() {
		return runOnce;
	}

	public void setRunOnce(boolean runOnce) {
		this.runOnce = runOnce;
	}

	public boolean isRepeating() {
		return repeating;
	}

	public void setRepeating(boolean repeating) {
		this.repeating = repeating;
	}

	public boolean isComplexRepeating() {
		return complexRepeating;
	}

	public void setComplexRepeating(boolean complexRepeating) {
		this.complexRepeating = complexRepeating;
	}

	public boolean isRepeatingOrComplexRepeating() {
		return repeatingOrComplexRepeating;
	}

	public void setRepeatingOrComplexRepeating(boolean repeatingOrComplexRepeating) {
		this.repeatingOrComplexRepeating = repeatingOrComplexRepeating;
	}

	public boolean isPrevRunOnce() {
		return prevRunOnce;
	}

	public void setPrevRunOnce(boolean prevRunOnce) {
		this.prevRunOnce = prevRunOnce;
	}

	public boolean isPrevRepeating() {
		return prevRepeating;
	}

	public void setPrevRepeating(boolean prevRepeating) {
		this.prevRepeating = prevRepeating;
	}

	public boolean isPrevComplexRepeating() {
		return prevComplexRepeating;
	}

	public void setPrevComplexRepeating(boolean prevComplexRepeating) {
		this.prevComplexRepeating = prevComplexRepeating;
	}

	public boolean isPrevRepeatingOrComplexRepeating() {
		return prevRepeatingOrComplexRepeating;
	}

	public void setPrevRepeatingOrComplexRepeating(boolean prevRepeatingOrComplexRepeating) {
		this.prevRepeatingOrComplexRepeating = prevRepeatingOrComplexRepeating;
	}

	public TreeModel<Menu> getPrevSelectedMenu() {
		return prevSelectedMenu;
	}

	public void setPrevSelectedMenu(TreeModel<Menu> prevSelectedMenu) {
		this.prevSelectedMenu = prevSelectedMenu;
	}

	public HashMap<Integer, Menu> getPrevRootMenu() {
		return prevRootMenu;
	}

	public void setPrevRootMenu(HashMap<Integer, Menu> prevRootMenu) {
		this.prevRootMenu = prevRootMenu;
	}

	public HashMap<Integer, Menu> getPrevChildMenu() {
		return prevChildMenu;
	}

	public void setPrevChildMenu(HashMap<Integer, Menu> prevChildMenu) {
		this.prevChildMenu = prevChildMenu;
	}

	public boolean isSearchParameterUser() {
		return searchParameterUser;
	}

	public void setSearchParameterUser(boolean searchParameterUser) {
		this.searchParameterUser = searchParameterUser;
	}

	public boolean isSearchParameterRole() {
		return searchParameterRole;
	}

	public void setSearchParameterRole(boolean searchParameterRole) {
		this.searchParameterRole = searchParameterRole;
	}

	public boolean isSearchParameterScheduler() {
		return searchParameterScheduler;
	}

	public void setSearchParameterScheduler(boolean searchParameterScheduler) {
		this.searchParameterScheduler = searchParameterScheduler;
	}

	public boolean isSearchActionCreate() {
		return searchActionCreate;
	}

	public boolean isSearchParameterSystemParameter() {
		return searchParameterSystemParameter;
	}

	public void setSearchParameterSystemParameter(boolean searchParameterSystemParameter) {
		this.searchParameterSystemParameter = searchParameterSystemParameter;
	}

	public boolean isSearchParameterBranch() {
		return searchParameterBranch;
	}

	public void setSearchParameterBranch(boolean searchParameterBranch) {
		this.searchParameterBranch = searchParameterBranch;
	}

	public boolean isSearchParameterHoliday() {
		return searchParameterHoliday;
	}

	public void setSearchParameterAggregator(boolean searchParameterAggregator) {
		this.searchParameterAggregator = searchParameterAggregator;
	}

	public boolean isSearchParameterAggregator() {
		return searchParameterAggregator;
	}

	public void setSearchParameterFeature(boolean searchParameterFeature) {
		this.searchParameterFeature = searchParameterFeature;
	}

	public boolean isSearchParameterFeature() {
		return searchParameterFeature;
	}

	public void setSearchActionCreate(boolean searchActionCreate) {
		this.searchActionCreate = searchActionCreate;
	}

	public boolean isSearchActionEdit() {
		return searchActionEdit;
	}

	public void setSearchActionEdit(boolean searchActionEdit) {
		this.searchActionEdit = searchActionEdit;
	}

	public boolean isSearchActionDelete() {
		return searchActionDelete;
	}

	public void setSearchActionDelete(boolean searchActionDelete) {
		this.searchActionDelete = searchActionDelete;
	}

	public List<String> getSearchParameter() {
		return searchParameter;
	}

	public void setSearchParameter(List<String> searchParameter) {
		this.searchParameter = searchParameter;
	}

	public List<String> getSearchAction() {
		return searchAction;
	}

	public void setSearchAction(List<String> searchAction) {
		this.searchAction = searchAction;
	}

	public void showSearch() {
		doShowSearch = true;
	}

	public void hideSearch() {
		doShowSearch = false;
	}

	public boolean somethingChecked() {
		if (approvalTemps.size() > 0) {
			return true;
		} else
			return false;
	}

	public void doSearch() {
		searchParameter = new ArrayList<String>();
		if ((searchParameterUser == false) && (searchParameterRole == false) && (searchParameterScheduler == false)
				&& (searchParameterSystemParameter == false) && (searchParameterBranch == false)
				&& (searchParameterHoliday == false) && (searchParameterAggregator == false)
				&& (searchParameterFeature == false)) {
			initSearchParameter();
		} else {
			if (searchParameterUser == true) {
				searchParameter.add(Constants.APPROVAL_PARAMETER_USER);
			}
			if (searchParameterRole == true) {
				searchParameter.add(Constants.APPROVAL_PARAMETER_ROLE);
			}
			if (searchParameterScheduler == true) {
				searchParameter.add(Constants.APPROVAL_PARAMETER_SCHEDULER);
			}
			if (searchParameterSystemParameter == true) {
				searchParameter.add(Constants.APPROVAL_PARAMETER_SPARAM);
			}
			if (searchParameterBranch == true) {
				searchParameter.add(Constants.APPROVAL_PARAMETER_BRANCH);
			}
			if (searchParameterHoliday == true) {
				searchParameter.add(Constants.APPROVAL_PARAMETER_HOLIDAY);
			}
			if (searchParameterAggregator == true) {
				searchParameter.add(Constants.APPROVAL_PARAMETER_AGGREGATOR);
			}
			if (searchParameterFeature == true) {
				searchParameter.add(Constants.APPROVAL_PARAMETER_FEATURE);
			}
		}

		searchAction = new ArrayList<String>();
		if (((searchActionCreate == false) && (searchActionEdit == false)) && (searchActionDelete == false)) {
			initSearchAction();
		} else {
			if (searchActionCreate == true) {
				searchAction.add(Constants.APPROVAL_ACTION_CREATE);
			}
			if (searchActionEdit == true) {
				searchAction.add(Constants.APPROVAL_ACTION_EDIT);
			}
			if (searchActionDelete == true) {
				searchAction.add(Constants.APPROVAL_ACTION_DELETE);
			}
		}

		FacesUtils.resetPage("approvalForm:approvalTable");
		approvalList = new ApprovalDataModel();
	}

	public void viewDetail(ApprovalPending approvalPending) {

		try {

			switch (approvalPending.getParameter()) {

			case Constants.APPROVAL_PARAMETER_USER:
				viewDetailUser(approvalPending);
				break;

			case Constants.APPROVAL_PARAMETER_ROLE:
				viewDetailRole(approvalPending);
				break;

			case Constants.APPROVAL_PARAMETER_SCHEDULER:
				viewDetailScheduler(approvalPending);
				break;

			case Constants.APPROVAL_PARAMETER_SPARAM:
				viewDetailSystemParameter(approvalPending);
				break;

			case Constants.APPROVAL_PARAMETER_BRANCH:
				viewDetailBranch(approvalPending);
				break;

			case Constants.APPROVAL_PARAMETER_HOLIDAY:
				viewDetailHoliday(approvalPending);
				break;

			case Constants.APPROVAL_PARAMETER_AGGREGATOR:
				viewDetailAggregator(approvalPending);
				break;

			case Constants.APPROVAL_PARAMETER_FEATURE:
				viewDetailFeature(approvalPending);
				break;

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void viewDetailUser(ApprovalPending approvalPending) throws SQLException, Exception {
		if (approvalPending.getAction().equals(Constants.APPROVAL_ACTION_CREATE)) {
			this.user = (UserAggregator) HelperUtils
					.deserialize(new ByteArrayInputStream(approvalPending.getNewByteObject()));
			this.rolesName = new ArrayList<String>();

			for (Role role : this.user.getRoles()) {
				this.rolesName.add(role.getName());
			}
			RequestContext.getCurrentInstance().execute("PF('viewUser').show()");
		} else if (approvalPending.getAction().equals(Constants.APPROVAL_ACTION_EDIT)) {
			this.user = (UserAggregator) HelperUtils
					.deserialize(new ByteArrayInputStream(approvalPending.getNewByteObject()));

			this.prevUser = (UserAggregator) HelperUtils
					.deserialize(new ByteArrayInputStream(approvalPending.getOldByteObject()));

			this.rolesName = new ArrayList<String>();

			for (Role role : this.user.getRoles()) {
				this.rolesName.add(role.getName());
			}

			this.prevRolesName = new ArrayList<String>();

			for (Role role : this.prevUser.getRoles()) {
				this.prevRolesName.add(role.getName());
			}
			RequestContext.getCurrentInstance().execute("PF('viewEditUser').show()");
		} else if (approvalPending.getAction().equals(Constants.APPROVAL_ACTION_DELETE)) {
			this.user = (UserAggregator) HelperUtils
					.deserialize(new ByteArrayInputStream(approvalPending.getOldByteObject()));

			this.rolesName = new ArrayList<String>();

			for (Role role : this.user.getRoles()) {
				this.rolesName.add(role.getName());
			}

			RequestContext.getCurrentInstance().execute("PF('viewUser').show()");
		}
	}

	public void viewDetailRole(ApprovalPending approvalPending) throws SQLException, Exception {
		if (approvalPending.getAction().equals(Constants.APPROVAL_ACTION_CREATE)) {
			this.role = (Role) HelperUtils.deserialize(new ByteArrayInputStream(approvalPending.getNewByteObject()));
			createTreeMenu();
			RequestContext.getCurrentInstance().execute("PF('viewRole').show()");
		} else if (approvalPending.getAction().equals(Constants.APPROVAL_ACTION_EDIT)) {
			this.role = (Role) HelperUtils.deserialize(new ByteArrayInputStream(approvalPending.getNewByteObject()));
			this.prevRole = (Role) HelperUtils
					.deserialize(new ByteArrayInputStream(approvalPending.getOldByteObject()));
			createTreeMenu();
			createPrevTreeMenu();
			RequestContext.getCurrentInstance().execute("PF('viewEditRole').show()");
		} else if (approvalPending.getAction().equals(Constants.APPROVAL_ACTION_DELETE)) {
			this.role = (Role) HelperUtils.deserialize(new ByteArrayInputStream(approvalPending.getOldByteObject()));
			createTreeMenu();
			RequestContext.getCurrentInstance().execute("PF('viewRole').show()");
		}
	}

	public void viewDetailScheduler(ApprovalPending approvalPending) throws SQLException, Exception {
		if (approvalPending.getAction().equals(Constants.APPROVAL_ACTION_CREATE)) {
			this.task = (ScheduleTask) HelperUtils
					.deserialize(new ByteArrayInputStream(approvalPending.getNewByteObject()));
			checkScheduleType();
			RequestContext.getCurrentInstance().execute("PF('viewScheduler').show()");
		} else if (approvalPending.getAction().equals(Constants.APPROVAL_ACTION_EDIT)) {
			this.task = (ScheduleTask) HelperUtils
					.deserialize(new ByteArrayInputStream(approvalPending.getNewByteObject()));
			this.prevTask = (ScheduleTask) HelperUtils
					.deserialize(approvalPending.getOldBlobObject().getBinaryStream());
			checkScheduleType();
			checkScheduleTypePrev();
			RequestContext.getCurrentInstance().execute("PF('viewSchedulerEdit').show()");
		} else if (approvalPending.getAction().equals(Constants.APPROVAL_ACTION_DELETE)) {
			this.task = (ScheduleTask) HelperUtils
					.deserialize(new ByteArrayInputStream(approvalPending.getOldByteObject()));
			checkScheduleType();
			RequestContext.getCurrentInstance().execute("PF('viewScheduler').show()");
		}
	}

	public void viewDetailSystemParameter(ApprovalPending approvalPending) throws SQLException, Exception {
		if (approvalPending.getAction().equals(Constants.APPROVAL_ACTION_CREATE)) {
			this.systemParameter = (SystemParameter) HelperUtils
					.deserialize(new ByteArrayInputStream(approvalPending.getNewByteObject()));
			RequestContext.getCurrentInstance().execute("PF('viewParameter').show()");
		} else if (approvalPending.getAction().equals(Constants.APPROVAL_ACTION_EDIT)) {
			this.systemParameter = (SystemParameter) HelperUtils
					.deserialize(new ByteArrayInputStream(approvalPending.getNewByteObject()));
			this.prevSystemParameter = (SystemParameter) HelperUtils
					.deserialize(new ByteArrayInputStream(approvalPending.getOldByteObject()));
			RequestContext.getCurrentInstance().execute("PF('viewParameterEdit').show()");
		} else if (approvalPending.getAction().equals(Constants.APPROVAL_ACTION_DELETE)) {
			this.systemParameter = (SystemParameter) HelperUtils
					.deserialize(new ByteArrayInputStream(approvalPending.getOldByteObject()));
			RequestContext.getCurrentInstance().execute("PF('viewParameter').show()");
		}
	}

	public void viewDetailBranch(ApprovalPending approvalPending) throws SQLException, Exception {
		if (approvalPending.getAction().equals(Constants.APPROVAL_ACTION_CREATE)) {
			this.branch = (Branch) HelperUtils
					.deserialize(new ByteArrayInputStream(approvalPending.getNewByteObject()));
			RequestContext.getCurrentInstance().execute("PF('viewBranch').show()");
		} else if (approvalPending.getAction().equals(Constants.APPROVAL_ACTION_EDIT)) {
			this.branch = (Branch) HelperUtils
					.deserialize(new ByteArrayInputStream(approvalPending.getNewByteObject()));
			this.prevBranch = (Branch) HelperUtils
					.deserialize(new ByteArrayInputStream(approvalPending.getOldByteObject()));
			RequestContext.getCurrentInstance().execute("PF('viewBranchEdit').show()");
		} else if (approvalPending.getAction().equals(Constants.APPROVAL_ACTION_DELETE)) {
			this.branch = (Branch) HelperUtils
					.deserialize(new ByteArrayInputStream(approvalPending.getOldByteObject()));
			RequestContext.getCurrentInstance().execute("PF('viewBranch').show()");
		}
	}

	public void viewDetailHoliday(ApprovalPending approvalPending) throws SQLException, Exception {
		if (approvalPending.getAction().equals(Constants.APPROVAL_ACTION_CREATE)) {
			this.holiday = (Holiday) HelperUtils
					.deserialize(new ByteArrayInputStream(approvalPending.getNewByteObject()));
			RequestContext.getCurrentInstance().execute("PF('viewHoliday').show()");
		} else if (approvalPending.getAction().equals(Constants.APPROVAL_ACTION_EDIT)) {
			this.holiday = (Holiday) HelperUtils
					.deserialize(new ByteArrayInputStream(approvalPending.getNewByteObject()));
			this.prevHoliday = (Holiday) HelperUtils
					.deserialize(new ByteArrayInputStream(approvalPending.getOldByteObject()));
			RequestContext.getCurrentInstance().execute("PF('viewHolidayEdit').show()");
		} else if (approvalPending.getAction().equals(Constants.APPROVAL_ACTION_DELETE)) {
			this.holiday = (Holiday) HelperUtils
					.deserialize(new ByteArrayInputStream(approvalPending.getOldByteObject()));
			RequestContext.getCurrentInstance().execute("PF('viewHoliday').show()");
		}
	}

	public void viewDetailAggregator(ApprovalPending approvalPending) throws SQLException, Exception {
		if (approvalPending.getAction().equals(Constants.APPROVAL_ACTION_CREATE)) {
			this.aggregator = (Aggregator) HelperUtils
					.deserialize(new ByteArrayInputStream(approvalPending.getNewByteObject()));
			RequestContext.getCurrentInstance().execute("PF('viewAggregator').show()");
		} else if (approvalPending.getAction().equals(Constants.APPROVAL_ACTION_EDIT)) {
			this.aggregator = (Aggregator) HelperUtils
					.deserialize(new ByteArrayInputStream(approvalPending.getNewByteObject()));
			this.prevAggregator = (Aggregator) HelperUtils
					.deserialize(new ByteArrayInputStream(approvalPending.getOldByteObject()));
			RequestContext.getCurrentInstance().execute("PF('viewAggregatorEdit').show()");
		} else if (approvalPending.getAction().equals(Constants.APPROVAL_ACTION_DELETE)) {
			this.aggregator = (Aggregator) HelperUtils
					.deserialize(new ByteArrayInputStream(approvalPending.getOldByteObject()));
			RequestContext.getCurrentInstance().execute("PF('viewAggregator').show()");
		}
	}

	public void viewDetailFeature(ApprovalPending approvalPending) throws SQLException, Exception {
		if (approvalPending.getAction().equals(Constants.APPROVAL_ACTION_CREATE)) {
			this.feature = (Feature) HelperUtils
					.deserialize(new ByteArrayInputStream(approvalPending.getNewByteObject()));
			RequestContext.getCurrentInstance().execute("PF('viewFeature').show()");
		} else if (approvalPending.getAction().equals(Constants.APPROVAL_ACTION_EDIT)) {
			this.feature = (Feature) HelperUtils
					.deserialize(new ByteArrayInputStream(approvalPending.getNewByteObject()));
			this.prevFeature = (Feature) HelperUtils
					.deserialize(new ByteArrayInputStream(approvalPending.getOldByteObject()));
			RequestContext.getCurrentInstance().execute("PF('viewFeatureEdit').show()");
		} else if (approvalPending.getAction().equals(Constants.APPROVAL_ACTION_DELETE)) {
			this.feature = (Feature) HelperUtils
					.deserialize(new ByteArrayInputStream(approvalPending.getOldByteObject()));
			RequestContext.getCurrentInstance().execute("PF('viewFeature').show()");
		}
	}

	public void viewDetailCollectingAgent(ApprovalPending approvalPending) throws SQLException, Exception {
		if (approvalPending.getAction().equals(Constants.APPROVAL_ACTION_CREATE)) {
			this.collectingAgent = (CollectingAgent) HelperUtils
					.deserialize(new ByteArrayInputStream(approvalPending.getNewByteObject()));
			RequestContext.getCurrentInstance().execute("PF('viewCollectingAgent').show()");
		} else if (approvalPending.getAction().equals(Constants.APPROVAL_ACTION_EDIT)) {
			this.collectingAgent = (CollectingAgent) HelperUtils
					.deserialize(new ByteArrayInputStream(approvalPending.getNewByteObject()));
			this.prevCollectingAgent = (CollectingAgent) HelperUtils
					.deserialize(new ByteArrayInputStream(approvalPending.getOldByteObject()));
			RequestContext.getCurrentInstance().execute("PF('viewCollectingAgentEdit').show()");
		} else if (approvalPending.getAction().equals(Constants.APPROVAL_ACTION_DELETE)) {
			this.collectingAgent = (CollectingAgent) HelperUtils
					.deserialize(new ByteArrayInputStream(approvalPending.getOldByteObject()));
			RequestContext.getCurrentInstance().execute("PF('viewCollectingAgent').show()");
		}
	}

	public void createTreeMenu() {
		this.menus = menuService.select();
		rootMenu = new HashMap<Integer, Menu>();
		childMenu = new HashMap<Integer, Menu>();
		tree = new ListTreeModel<Menu>();
		for (Menu menu : this.menus) {
			// agar menu di applicationBean tidak berubah
			Menu tempMenu = new Menu(menu);

			Collection<RoleMenu> roleMenus = role.getRoleMenus();
			for (RoleMenu roleMenu2 : roleMenus) {
				if (roleMenu2.getMenu().getId().equals(tempMenu.getId())) {
					tempMenu.setSelected(true);
						for (Action action : tempMenu.getActionList()){
							action.setSelected(false);
							for (Action action2 : roleMenu2.getActionList()){
								if (action.getName().equals(action2.getName())){
									action.setSelected(true);
									break;
								}
							}
							
						}
					}
			}

			if (tempMenu.getParentId() == 0) {
				rootMenu.put(menu.getId(), tempMenu);
			} else {
				childMenu.put(menu.getId(), tempMenu);
			}
		}

		for (Map.Entry<Integer, Menu> entry : rootMenu.entrySet()) {
			int parentMenuId = entry.getValue().getId();
			TreeModel<Menu> currentRoot = tree.addChild(entry.getValue());
			createChild(currentRoot, parentMenuId, childMenu);
		}
	}

	public void createPrevTreeMenu() {
		this.prevMenus = menuService.select();
		prevRootMenu = new HashMap<Integer, Menu>();
		prevChildMenu = new HashMap<Integer, Menu>();
		prevTree = new ListTreeModel<Menu>();
		for (Menu menu : this.prevMenus) {
			// agar menu di applicationBean tidak berubah
			Menu tempMenu = new Menu(menu);

			Collection<RoleMenu> roleMenus = prevRole.getRoleMenus();
			for (RoleMenu roleMenu2 : roleMenus) {
				if (roleMenu2.getMenu().getId().equals(tempMenu.getId())) {
					tempMenu.setSelected(true);
					for (Action action : tempMenu.getActionList()){
						action.setSelected(false);
						for (Action action2 : roleMenu2.getActionList()){
							if (action.getName().equals(action2.getName())){
								action.setSelected(true);
								break;
							}
						}
						
					}
				}
			}

			if (tempMenu.getParentId() == 0) {
				prevRootMenu.put(menu.getId(), tempMenu);
			} else {
				prevChildMenu.put(menu.getId(), tempMenu);
			}
		}

		for (Map.Entry<Integer, Menu> entry : prevRootMenu.entrySet()) {
			int parentMenuId = entry.getValue().getId();
			TreeModel<Menu> currentRoot = prevTree.addChild(entry.getValue());
			createChild(currentRoot, parentMenuId, prevChildMenu);
		}
	}

	private void createChild(TreeModel<Menu> treeMenu, int menuId, Map<Integer, Menu> childMenu) {
		for (Map.Entry<Integer, Menu> entry : childMenu.entrySet()) {
			Menu tempMenu = entry.getValue();
			if (tempMenu.getParentId() == menuId) {
				TreeModel<Menu> currentParent = treeMenu.addChild(tempMenu);
				createChild(currentParent, tempMenu.getId(), childMenu);
			}
		}
	}

	public void initSelectItemData() {

		scheduleTypes = new ArrayList<String>();
		scheduleTypes.add(Constants.SCHEDULER_RUN_ONCE);
		scheduleTypes.add(Constants.SCHEDULER_REPEATING);
		scheduleTypes.add(Constants.SCHEDULER_COMPLEX_REPEATING);

		hours = new ArrayList<Integer>();
		for (int i = 0; i < 25; i++) {
			hours.add(i);
		}

		minutes = new ArrayList<Integer>();
		for (int i = 0; i < 61; i++) {
			minutes.add(i);
		}
	}

	public void checkScheduleType() {
		if (task.getSchedulerType().equals(Constants.SCHEDULER_RUN_ONCE)) {
			runOnce = true;
			repeating = false;
			complexRepeating = false;
			repeatingOrComplexRepeating = false;
		} else if (task.getSchedulerType().equals(Constants.SCHEDULER_REPEATING)) {
			runOnce = false;
			repeating = true;
			complexRepeating = false;
			repeatingOrComplexRepeating = true;
		} else if (task.getSchedulerType().equals(Constants.SCHEDULER_COMPLEX_REPEATING)) {
			runOnce = false;
			repeating = false;
			complexRepeating = true;
			repeatingOrComplexRepeating = true;
		} else {
			runOnce = false;
			repeating = false;
			complexRepeating = false;
			repeatingOrComplexRepeating = false;
		}
	}

	public void checkScheduleTypePrev() {
		if (prevTask.getSchedulerType().equals(Constants.SCHEDULER_RUN_ONCE)) {
			prevRunOnce = true;
			prevRepeating = false;
			prevComplexRepeating = false;
			prevRepeatingOrComplexRepeating = false;
		} else if (prevTask.getSchedulerType().equals(Constants.SCHEDULER_REPEATING)) {
			prevRunOnce = false;
			prevRepeating = true;
			prevComplexRepeating = false;
			prevRepeatingOrComplexRepeating = true;
		} else if (prevTask.getSchedulerType().equals(Constants.SCHEDULER_COMPLEX_REPEATING)) {
			prevRunOnce = false;
			prevRepeating = false;
			prevComplexRepeating = true;
			prevRepeatingOrComplexRepeating = true;
		} else {
			prevRunOnce = false;
			prevRepeating = false;
			prevComplexRepeating = false;
			prevRepeatingOrComplexRepeating = false;
		}
	}
	
	public boolean dataAvailable(){
		if(approvalList.getRowCount()>0){
			return true;
		} else return false;
	}

	public void viewRejectConfirmation(){
		if(somethingChecked()){
			RequestContext.getCurrentInstance().execute("PF('dlgConfirmRejectChecked').show()");
		} else {
			String headerMessage = "Error";
			String bodyMessage = "No Items Selected";

			FacesUtils.setErrorMessageDialog(headerMessage, bodyMessage);
		}
	}
	
	public void viewApproveAllConfirmation(){
		if(dataAvailable()){
			RequestContext.getCurrentInstance().execute("PF('dlgConfirmApproveAll').show()");
		} else {
			String headerMessage = "Error";
			String bodyMessage = "No Data Available";

			FacesUtils.setErrorMessageDialog(headerMessage, bodyMessage);
		}
	}
	
	public void viewRejectAllConfirmation(){
		if(dataAvailable()){
			RequestContext.getCurrentInstance().execute("PF('dlgConfirmRejectAll').show()");
		} else {
			String headerMessage = "Error";
			String bodyMessage = "No Data Available";

			FacesUtils.setErrorMessageDialog(headerMessage, bodyMessage);
		}
	}
	
	public void approveChecked() {

		List<ApprovalPending> approvalPendings = new ArrayList<ApprovalPending>();

		Iterator entries = approvalTemps.entrySet().iterator();
		while (entries.hasNext()) {
			Entry thisEntry = (Entry) entries.next();
			ApprovalPending a = (ApprovalPending) thisEntry.getValue();
			approvalPendings.add(a);
		}

		if (approvalPendings.size() > 0) {
			approve(approvalPendings);

			String headerMessage = "Success";
			String bodyMessage = "Approve checked finished";

			FacesUtils.setInfoMessageDialog(headerMessage, bodyMessage);
		} else {
			String headerMessage = "Error";
			String bodyMessage = "No Items Selected";

			FacesUtils.setErrorMessageDialog(headerMessage, bodyMessage);
		}
	}

	public void approveAll() {

		BigDecimal approverId = sessionService.getUser().getId();
		String branchId = sessionService.getUser().getBranch().getId();

		List<ApprovalPending> approvalPendings = approvalPendingService.selectByCriteria(approverId, parameterCode,
				parameterName, searchParameter, searchAction, branchId);

		if (approvalPendings.size() > 0) {
			approve(approvalPendings);

			String headerMessage = "Success";
			String bodyMessage = "Approve all finished";

			FacesUtils.setInfoMessageDialog(headerMessage, bodyMessage);
		} else {
			String headerMessage = "Error";
			String bodyMessage = "No Items Selected";

			FacesUtils.setErrorMessageDialog(headerMessage, bodyMessage);
		}

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void approve(List<ApprovalPending> approvalPendings) {

		if (approvalPendings != null && approvalPendings.size() != 0) {
			for (ApprovalPending a : approvalPendings) {
				try {
					Auditable newObject = null;
					Auditable oldObject = null;
					if (a.getNewByteObject() != null) {
						newObject = (Auditable) HelperUtils.deserialize(new ByteArrayInputStream(a.getNewByteObject()));
					}
					if (a.getOldByteObject() != null) {
						oldObject = (Auditable) HelperUtils.deserialize(new ByteArrayInputStream(a.getOldByteObject()));
					}
					OperationalService operationalService = getService(newObject.getClass());
					if (a.getAction().equals(Constants.APPROVAL_ACTION_DELETE)) {
						operationalService.approveToRemove(newObject, a);
					} else if (a.getAction().equals(Constants.APPROVAL_ACTION_CREATE)) {
						operationalService.approveToCreate(newObject, a);
					} else if (a.getAction().equals(Constants.APPROVAL_ACTION_EDIT)) {
						operationalService.approveToModify(oldObject, newObject, a);
					}
				} catch (Exception e) {
					e.printStackTrace();
					approvalCompleteService.insert(a, Constants.APPROVAL_STATUS_CODE_FAILED, e.getMessage());
				}
			}
		}
	}

	@SuppressWarnings("rawtypes")
	public void rejectChecked() {

		List<ApprovalPending> approvalPendings = new ArrayList<ApprovalPending>();

		Iterator entries = approvalTemps.entrySet().iterator();
		while (entries.hasNext()) {
			Entry thisEntry = (Entry) entries.next();
			ApprovalPending a = (ApprovalPending) thisEntry.getValue();
			approvalPendings.add(a);
		}

			reject(approvalPendings);

			String headerMessage = "Success";
			String bodyMessage = "Reject checked finished";
			
			reason = "";

			FacesUtils.setInfoMessageDialog(headerMessage, bodyMessage);
	}

	public void rejectAll() {

		BigDecimal approverId = sessionService.getUser().getId();
		String branchId = sessionService.getUser().getBranch().getId();

		List<ApprovalPending> approvalPendings = approvalPendingService.selectByCriteria(approverId, parameterCode,
				parameterName, searchParameter, searchAction, branchId);

		if (approvalPendings.size() > 0) {
			reject(approvalPendings);

			String headerMessage = "Success";
			String bodyMessage = "Reject all finished";
			
			reason = "";

			FacesUtils.setInfoMessageDialog(headerMessage, bodyMessage);
		} else {
			String headerMessage = "Error";
			String bodyMessage = "No Items Selected";

			FacesUtils.setErrorMessageDialog(headerMessage, bodyMessage);
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void reject(List<ApprovalPending> approvalPendings) {

		if (approvalPendings != null && approvalPendings.size() != 0) {
			for (ApprovalPending a : approvalPendings) {
				try {
					Auditable newObject = null;
					Auditable oldObject = null;
					if (a.getNewByteObject() != null) {
						newObject = (Auditable) HelperUtils.deserialize(new ByteArrayInputStream(a.getNewByteObject()));
					} 
					if (a.getOldByteObject()!= null) {
						oldObject  = (Auditable) HelperUtils.deserialize(new ByteArrayInputStream(a.getOldByteObject()));
					}
					OperationalService operationalService = getService(newObject.getClass());
					if (a.getAction().equals(Constants.APPROVAL_ACTION_DELETE)) {
						operationalService.rejectToRemove(newObject, a, reason);
					} else if (a.getAction().equals(Constants.APPROVAL_ACTION_CREATE)) {
						operationalService.rejectToCreate(newObject, a, reason);
					} else if (a.getAction().equals(Constants.APPROVAL_ACTION_EDIT)) {
						operationalService.rejectToModify(oldObject, newObject, a, reason);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					approvalCompleteService.insert(a, Constants.APPROVAL_STATUS_CODE_FAILED, e.getMessage());
				}
			}
		}
		reason = "";

	}

	public void processChecked(ApprovalPending approvalPending) {
		if (approvalPending.isSelected()) {
			approvalTemps.put(approvalPending.getId(), approvalPending);
		} else {
			approvalTemps.remove(approvalPending.getId());
		}

	}

	private class ApprovalDataModel extends LazyDataModel<ApprovalPending> {
		private static final long serialVersionUID = 1L;

		public ApprovalDataModel() {

		}

		@Override
		public List<ApprovalPending> load(int startingAt, int maxPerPage, String sortField, SortOrder sortOrder,
				Map<String, Object> filters) {

			List<ApprovalPending> approvalPendings;

			List<BigDecimal> userIdLogins = new ArrayList<BigDecimal>();
			for (UserSession userSession : sessionService.getAllSession()) {
				if (!userSession.getUser().getId().equals(sessionService.getUser().getId()))
					userIdLogins.add(userSession.getUser().getId());
			}
			if (userIdLogins.size() > 0) {
				approvalPendingService.removeApprover(userIdLogins);
			} else {
				approvalPendingService.removeAllApprover();
			}
			approvalPendingService.assignApprover(sessionService.getUser().getId());

			BigDecimal approverId = sessionService.getUser().getId();
			String branchId = sessionService.getUser().getBranch().getId();

			approvalPendings = approvalPendingService.selectByCriteria(startingAt, maxPerPage, approverId,
					parameterCode, parameterName, searchParameter, searchAction, branchId);
			Integer jmlAll = approvalPendingService.countByCriteria(approverId, parameterCode, parameterName,
					searchParameter, searchAction, branchId);
			setRowCount(jmlAll);
			setPageSize(maxPerPage);

			List<ApprovalPending> approvalsFinal = new ArrayList<ApprovalPending>();
			for (ApprovalPending a : approvalPendings) {
				Iterator entries = approvalTemps.entrySet().iterator();
				while (entries.hasNext()) {
					Entry thisEntry = (Entry) entries.next();
					ApprovalPending s = (ApprovalPending) thisEntry.getValue();
					if (a.getId().equals(s.getId())) {
						if (s.isSelected()) {
							a.setSelected(true);
						} else {
							a.setSelected(false);
						}
					}
				}
				approvalsFinal.add(a);
			}

			return approvalsFinal;
		}
	}

	@SuppressWarnings("rawtypes")
	public OperationalService getService(Class auditableClass) {
		if (auditableClass.equals(Aggregator.class)) {
			return aggregatorService;
		} else if (auditableClass.equals(Branch.class)) {
			return branchService;
		} else if (auditableClass.equals(Holiday.class)) {
			return holidayService;
		} else if (auditableClass.equals(Role.class)) {
			return roleService;
		} else if (auditableClass.equals(ScheduleTask.class)) {
			return schedulerService;
		} else if (auditableClass.equals(SystemParameter.class)) {
			return systemParameterService;
		} else if (auditableClass.equals(Feature.class)) {
			return featureService;
		} else if (auditableClass.equals(User.class) || auditableClass.equals(UserAggregator.class)
				|| auditableClass.equals(UserBank.class)) {
			return userService;
		} else {
			return null;
		}
	}
}