package com.bjb.web.bean;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.TreeNode;

import com.bjb.constant.Constants;
import com.bjb.model.SystemParameter;
import com.bjb.service.SessionService;
import com.bjb.service.SystemParameterService;
import com.bjb.util.HelperUtils;
import com.bjb.web.bean.support.DiffService;
import com.bjb.web.helper.FacesUtils;

@ManagedBean
@ViewScoped
public class SystemParameterHandler implements Serializable {
	private static final long serialVersionUID = 1L;

	private SystemParameter systemParameter;
	private SystemParameter systemParameterTemp;

	private String action;

	private String name;
	private String value;
	private List<Integer> status;;
	private List<String> approvalStatus;

	private boolean active;
	private boolean inactive;

	private boolean pending;
	private boolean approved;
	private boolean rejected;

	private boolean saveParameter;

	private LazyDataModel<SystemParameter> parameterList;

	private boolean doShowSearch = false;

	private boolean doShowHistory = false;

	private String currentName;

	@ManagedProperty(value="#{sessionService}")
	private SessionService sessionService;
	
	@ManagedProperty(value="#{systemParameterService}")
	private SystemParameterService systemParameterService;

	@ManagedProperty(value="#{diffService}")
	private DiffService diffService;
	
	public void setDiffService(DiffService diffService) {
		this.diffService = diffService;
	}

	public TreeNode getDiffTreeTable() {
		return diffService.diff(systemParameterTemp, systemParameter);
	}
	
	public void setSystemParameterService(SystemParameterService systemParameterService) {
		this.systemParameterService = systemParameterService;
	}

	@PostConstruct
	public void init() {
		sessionService.authMenu(sessionService.getUser().getId(), "adminParameter");

		parameterList = new ParameterDataModel();

		name = "";
		value = "";

		doSearch();
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public boolean isPending() {
		return pending;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public boolean isInactive() {
		return inactive;
	}

	public void setInactive(boolean inactive) {
		this.inactive = inactive;
	}

	public void setPending(boolean pending) {
		this.pending = pending;
	}

	public boolean isApproved() {
		return approved;
	}

	public void setApproved(boolean approved) {
		this.approved = approved;
	}

	public boolean isRejected() {
		return rejected;
	}

	public void setRejected(boolean rejected) {
		this.rejected = rejected;
	}

	public void showHistory() {
		doShowHistory = true;
	}

	public void hideHistory() {
		doShowHistory = false;
	}

	public boolean isDoShowHistory() {
		return doShowHistory;
	}

	public void setDoShowHistory(boolean doShowHistory) {
		this.doShowHistory = doShowHistory;
	}

	public void setSessionService(SessionService sessionService) {
		this.sessionService = sessionService;
	}

	public boolean isSaveParameter() {
		return saveParameter;
	}

	public void setSaveParameter(boolean saveParameter) {
		this.saveParameter = saveParameter;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public SystemParameter getSystemParameter() {
		return systemParameter;
	}

	public SystemParameter getSystemParameterTemp() {
		return systemParameterTemp;
	}

	public void setSystemParameterTemp(SystemParameter systemParameterTemp) {
		this.systemParameterTemp = systemParameterTemp;
	}

	public void setSystemParameter(SystemParameter systemParameter) {
		this.systemParameter = systemParameter;
	}

	public LazyDataModel<SystemParameter> getParameterList() {
		return parameterList;
	}

	public void setParameterList(LazyDataModel<SystemParameter> parameterList) {
		this.parameterList = parameterList;
	}

	public boolean isDoShowSearch() {
		return doShowSearch;
	}

	public void setDoShowSearch(boolean doShowSearch) {
		this.doShowSearch = doShowSearch;
	}

	public void showSearch() {
		doShowSearch = true;
	}

	public void hideSearch() {
		doShowSearch = false;
		init(); 
	}

	public void viewDetailedParameter(SystemParameter systemParameter) {
		this.systemParameter = systemParameter;

		if (this.systemParameter.getApprovalStatus().equals(
				Constants.APPROVAL_STATUS_PENDING_EDIT)) {
			
			this.systemParameterTemp = systemParameterService.findPendingApprovalByCode(systemParameter.getCode());
					
			if (systemParameterTemp!=null) {
				RequestContext.getCurrentInstance().execute(
							"PF('viewParameterEdit').show()");
				return;
			}
		} 
		RequestContext.getCurrentInstance().execute("PF('viewParameter').show()");
	}

	public void viewDetailedParameterEdit(SystemParameter systemParameter) {

		this.systemParameter = systemParameter;

		if (this.systemParameter.getDisableEdit() == true) {
			FacesUtils.setApprovalWarning("parameter",
					this.systemParameter.getName(),
					this.systemParameter.getApprovalStatus());
		} else {

			this.action = "edit";
			if (this.systemParameter.getApprovalStatus().equals(
					Constants.APPROVAL_STATUS_PENDING_EDIT)) {

				this.systemParameterTemp = systemParameterService.findPendingApprovalByCode(systemParameter.getCode());

				if (systemParameterTemp != null) {
						currentName = this.systemParameterTemp.getName();
						RequestContext.getCurrentInstance().execute(
								"PF('insertOrEditParameterPending').show()");
						return;
				} 
			} 
			this.systemParameterTemp = HelperUtils.clone(systemParameter);
			RequestContext.getCurrentInstance().execute(
						"PF('insertOrEditParameter').show()");
		}
	}

	public void newParameter() {
		this.systemParameter = new SystemParameter();
		SimpleDateFormat sdf = new SimpleDateFormat(Constants.FORMAT_DATE_SHORT);
		String code = sdf.format(new Date());
		String seq = systemParameterService.generateId();
		this.systemParameter.setCode(code + "PARAM" + String.format("%04d", Integer.parseInt(seq)));
		this.systemParameter.setStatus(Constants.STATUS_ACTIVE);
		this.action = "";
	}

	public void createSystemParameter(SystemParameter systemParameter) {

		systemParameterService.requestToCreate(systemParameter);
		
		RequestContext.getCurrentInstance().execute(
				"PF('insertOrEditParameter').hide()");
		RequestContext.getCurrentInstance().execute(
				"PF('insertOrEditParameterPending').hide()");

		String headerMessage = "Create Parameter Successfully";
		String bodyMessage = "Supervisor must approve the creating of the <br/>parameter &apos;"
				+ systemParameter.getName()
				+ "&apos; to completely the process.";

		FacesUtils.setInfoMessageDialog(headerMessage, bodyMessage);
	}

	public void editParameter(SystemParameter systemParameter) {

		systemParameter = systemParameterService.requestToModify(systemParameterTemp, systemParameter);
		
		RequestContext.getCurrentInstance().execute(
				"PF('insertOrEditParameter').hide()");
		RequestContext.getCurrentInstance().execute(
				"PF('insertOrEditParameterPending').hide()");

		String headerMessage = "Edit Parameter Successfully";
		String bodyMessage = "Supervisor must approve the editing of the <br/>parameter &apos;"
				+ systemParameter.getName()
				+ "&apos; to completely the process.";

		FacesUtils.setInfoMessageDialog(headerMessage, bodyMessage);
	}

	public void doSave() {
		if (this.action != null && this.action.equals("edit")) {
			editParameter(this.systemParameter);
		} else {
			try {

				SystemParameter systemParameterExist = systemParameterService
						.findByCode(
								this.systemParameter.getName());

				if (systemParameterExist == null
						|| (systemParameterExist.getName()
								.equals(currentName))) {
					createSystemParameter(this.systemParameter);
				} else {
					String headerMessage = "Attention";
					String bodyMessage = "Parameter "
							+ this.systemParameter.getName()
							+ " already exist in the application.";

					FacesUtils.setWarningMessageDialog(headerMessage,
							bodyMessage);
				}

			} catch (Exception e) {
				String headerMessage = "Error";
				String bodyMessage = "System error";

				FacesUtils.setErrorMessageDialog(headerMessage, bodyMessage);
				e.printStackTrace();
			}
		}

	}

	public void doSearch() {

		status = new ArrayList<Integer>();
		if (!active && !inactive) {
			status.add(Constants.STATUS_ACTIVE);
			status.add(Constants.STATUS_INACTIVE);
		} else {
			if (active) {
				status.add(Constants.STATUS_ACTIVE);
			}
			if (inactive) {
				status.add(Constants.STATUS_INACTIVE);
			}
		}
		approvalStatus = new ArrayList<String>();

		if (!pending && !approved && !rejected) {
			approvalStatus.add(Constants.APPROVAL_STATUS_PENDING_CREATE);
			approvalStatus.add(Constants.APPROVAL_STATUS_PENDING_EDIT);
			approvalStatus.add(Constants.APPROVAL_STATUS_PENDING_DELETE);
			approvalStatus.add(Constants.APPROVAL_STATUS_APPROVED_CREATE);
			approvalStatus.add(Constants.APPROVAL_STATUS_APPROVED_EDIT);
			approvalStatus.add(Constants.APPROVAL_STATUS_REJECTED_CREATE);
			approvalStatus.add(Constants.APPROVAL_STATUS_REJECTED_EDIT);
			approvalStatus.add(Constants.APPROVAL_STATUS_REJECTED_DELETE);
		} else {
			if (pending == true) {
				approvalStatus.add(Constants.APPROVAL_STATUS_PENDING_CREATE);
				approvalStatus.add(Constants.APPROVAL_STATUS_PENDING_EDIT);
				approvalStatus.add(Constants.APPROVAL_STATUS_PENDING_DELETE);
			}
			if (approved == true) {
				approvalStatus.add(Constants.APPROVAL_STATUS_APPROVED_CREATE);
				approvalStatus.add(Constants.APPROVAL_STATUS_APPROVED_EDIT);
			}
			if (rejected == true) {
				approvalStatus.add(Constants.APPROVAL_STATUS_REJECTED_CREATE);
				approvalStatus.add(Constants.APPROVAL_STATUS_REJECTED_EDIT);
				approvalStatus.add(Constants.APPROVAL_STATUS_REJECTED_DELETE);
			}
		}
		FacesUtils.resetPage("parameterForm:parameterTable");
		parameterList = new ParameterDataModel();

	}

	public void viewDetailedDelete(SystemParameter parameter) {

		this.systemParameter = parameter;

		if (this.systemParameter.getDisableDelete() == true) {
			FacesUtils.setApprovalWarning("parameter",
					this.systemParameter.getName(),
					this.systemParameter.getApprovalStatus());
		} else {
			RequestContext.getCurrentInstance().execute("PF('dlgConfirm').show()");
		}

	}

	public void deleteSystemParameter() {

		systemParameterService.requestToRemove(systemParameter);

		if (systemParameter.getApprovalStatus().equals(Constants.APPROVAL_STATUS_REJECTED_CREATE)){

			String headerMessage = "Delete Parameter Successfully";
			String bodyMessage = "Delete System Parameter " + this.systemParameter.getName() + " Successfully";
	
			FacesUtils.setInfoMessageDialog(headerMessage, bodyMessage);
			
		}else{
		
			String headerMessage = "Delete Parameter Successfully";
			String bodyMessage = "Supervisor must approve the deleting of the <br/>parameter &apos;"
					+ systemParameter.getName()
					+ "&apos; to completely the process.";
	
			FacesUtils.setInfoMessageDialog(headerMessage, bodyMessage);
		
		}

	}

	public void viewDetailedChangeStatus(SystemParameter parameter) {

		this.systemParameter = parameter;

		if (this.systemParameter.getDisableDelete() == true) {
			FacesUtils.setApprovalWarning("parameter",
					this.systemParameter.getName(),
					this.systemParameter.getApprovalStatus());
		} else {
			RequestContext.getCurrentInstance().execute("PF('dlgConfirmChangeStatus').show()");
		}

	}

	public void changeStatusSystemParameter() {
		
		this.systemParameterTemp = (SystemParameter) HelperUtils.clone(systemParameter);
		
		systemParameter.setStatus(systemParameter.getStatus()==Constants.STATUS_ACTIVE?Constants.STATUS_INACTIVE:Constants.STATUS_ACTIVE);

		systemParameter = systemParameterService.requestToModify(systemParameterTemp, systemParameter);

		String headerMessage = "Change Status Parameter Successfully";
		String bodyMessage = "Supervisor must approve the change status of the <br/>parameter &apos;"
				+ systemParameter.getName()
				+ "&apos; to completely the process.";

		FacesUtils.setInfoMessageDialog(headerMessage, bodyMessage);
	}

	private class ParameterDataModel extends LazyDataModel<SystemParameter> {
		private static final long serialVersionUID = 1L;

		public ParameterDataModel() {

		}

	    @Override
	    public List<SystemParameter> load(int startingAt, int maxResult, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
			List<SystemParameter> systemParameter = null;

			systemParameter = systemParameterService.getSearchParameter(startingAt, maxResult, 
					name, value, status, approvalStatus);
			Integer jmlAll = systemParameterService.countSearchParameter(
					name, value, status, approvalStatus);
			setRowCount(jmlAll);
			setPageSize(maxResult);

			return systemParameter;
		}

	}

}
