package com.bjb.web.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.TreeNode;

import com.bjb.constant.Constants;
import com.bjb.model.Aggregator;
import com.bjb.model.User;
import com.bjb.service.AggregatorService;
import com.bjb.service.SessionService;
import com.bjb.support.TemplateService;
import com.bjb.util.EncryptUtils;
import com.bjb.util.HelperUtils;
import com.bjb.web.bean.support.AbstractReport;
import com.bjb.web.bean.support.DiffService;
import com.bjb.web.helper.FacesUtils;

@ManagedBean
@ViewScoped
public class AggregatorHandler extends AbstractReport implements Serializable {
	private static final long serialVersionUID = 1L;

	private Aggregator aggregator;
	private Aggregator aggregatorTemp;

	private String action;

	private String code;
	private String name;
	private String address;
	private String phone;
	private String pic;
	private String emailPic;
	private List<Integer> status;
	private List<String> approvalStatus;

	private boolean active;
	private boolean inactive;
	private boolean pending;
	private boolean approved;
	private boolean rejected;

	private boolean saveAggregator;

	private LazyDataModel<Aggregator> aggregatorList;

	private boolean doShowSearch = false;

	private boolean doShowHistory = false;

	private String currentName;
	
	@ManagedProperty(value="#{sessionService}")
	private SessionService sessionService;
	
	@ManagedProperty(value="#{aggregatorService}")
	private AggregatorService aggregatorService;

	@ManagedProperty(value="#{diffService}")
	private DiffService diffService;
	
	public void setDiffService(DiffService diffService) {
		this.diffService = diffService;
	}

	public TreeNode getDiffTreeTable() {
		return diffService.diff(aggregatorTemp, aggregator);
	}
	
	public void setAggregatorService(AggregatorService aggregatorService) {
		this.aggregatorService = aggregatorService;
	}

	@PostConstruct
	public void init() {

		sessionService.authMenu(sessionService.getUser().getId(), "manageAggregator");

		aggregatorList = new AggregatorDataModel();

		active = false;
		inactive = false;
		pending = false;
		approved = false;
		rejected = false;
		code = "";
		name = "";
		address = "";
		phone = "";
		pic = "";
		emailPic = "";

		doSearch();
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public String getEmailPic() {
		return emailPic;
	}

	public void setEmailPic(String emailPic) {
		this.emailPic = emailPic;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public List<String> getStatus() {
		return approvalStatus;
	}

	public void setStatus(List<String> status) {
		this.approvalStatus = status;
	}

	public boolean isPending() {
		return pending;
	}

	public void setPending(boolean pending) {
		this.pending = pending;
	}

	public boolean isApproved() {
		return approved;
	}

	public void setApproved(boolean approved) {
		this.approved = approved;
	}

	public boolean isRejected() {
		return rejected;
	}
	
	public boolean isRejectReason(){
		boolean isRejectReason = false;
		if (this.aggregator != null){
			if (this.aggregator.getApprovalStatus().equals(Constants.APPROVAL_STATUS_REJECTED_CREATE) || 
					this.aggregator.getApprovalStatus().equals(Constants.APPROVAL_STATUS_REJECTED_EDIT) ||
						this.aggregator.getApprovalStatus().equals(Constants.APPROVAL_STATUS_REJECTED_DELETE)){
				isRejectReason = true;
			}
		}
		return isRejectReason;
	}

	public void setRejected(boolean rejected) {
		this.rejected = rejected;
	}

	public void showHistory() {
		doShowHistory = true;
	}

	public void hideHistory() {
		doShowHistory = false;
	}

	public boolean isDoShowHistory() {
		return doShowHistory;
	}

	public void setDoShowHistory(boolean doShowHistory) {
		this.doShowHistory = doShowHistory;
	}

	public SessionService getSessionBean() {
		return sessionService;
	}

	public void setSessionService(SessionService sessionService) {
		this.sessionService = sessionService;
	}

	public boolean isSaveAggregator() {
		return saveAggregator;
	}

	public void setSaveAggregator(boolean saveAggregator) {
		this.saveAggregator = saveAggregator;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Aggregator getAggregator() {
		return aggregator;
	}

	public Aggregator getAggregatorTemp() {
		return aggregatorTemp;
	}

	public void setAggregatorTemp(Aggregator aggregatorTemp) {
		this.aggregatorTemp = aggregatorTemp;
	}

	public void setAggregator(Aggregator aggregator) {
		this.aggregator = aggregator;
	}

	public LazyDataModel<Aggregator> getAggregatorList() {
		return aggregatorList;
	}

	public void setAggregatorList(LazyDataModel<Aggregator> aggregatorList) {
		this.aggregatorList = aggregatorList;
	}

	public boolean isDoShowSearch() {
		return doShowSearch;
	}

	public void setDoShowSearch(boolean doShowSearch) {
		this.doShowSearch = doShowSearch;
	}

	public void showSearch() {
		doShowSearch = true;
	}

	public void hideSearch() {
		doShowSearch = false;
	}
	
	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public boolean isInactive() {
		return inactive;
	}

	public void setInactive(boolean inactive) {
		this.inactive = inactive;
	}

	public void viewDetailedAggregator(Aggregator aggregator) {
		this.aggregator = aggregator;

		if (this.aggregator.getApprovalStatus().equals(
				Constants.APPROVAL_STATUS_PENDING_EDIT)) {
			this.aggregatorTemp = aggregatorService.findPendingApprovalByCode(aggregator.getCode());
			if (this.aggregatorTemp != null) {
				RequestContext.getCurrentInstance().execute(
						"PF('viewAggregatorEdit').show()");
				return;
			}
		} 
		RequestContext.getCurrentInstance().execute("PF('viewAggregator').show()");
	}

	public void viewDetailedAggregatorEdit(Aggregator aggregator) {

		this.aggregator = aggregator;

		if (this.aggregator.getDisableEdit() == true) {
			FacesUtils.setApprovalWarning("aggregator",
					this.aggregator.getName(),
					this.aggregator.getApprovalStatus());
		} else {

			this.action = "edit";
			if (this.aggregator.getApprovalStatus().equals(
					Constants.APPROVAL_STATUS_PENDING_EDIT)) {

				this.aggregatorTemp = aggregatorService.findPendingApprovalByCode(aggregator.getCode());
				
				if (aggregatorTemp!=null) {
					currentName = this.aggregatorTemp.getName();
					RequestContext.getCurrentInstance().execute(
							"PF('insertOrEditAggregatorPending').show()");
					return;
				}
			} 
			this.aggregatorTemp = HelperUtils.clone(aggregator);
			RequestContext.getCurrentInstance().execute(
						"PF('insertOrEditAggregator').show()");
		}
	}
	
	public void newAggregator() {
		this.aggregator = new Aggregator();
		BigDecimal seq = aggregatorService.generateId();
		this.aggregator.setId(seq);
		this.aggregator.setCode("W" + String.format("%04d", seq.intValue()));
		this.aggregator.setStatus(Constants.STATUS_ACTIVE);
		this.aggregator.setStatusCA(Constants.STATUS_INACTIVE);
		this.action = "";
	}

	public void createAggregator(Aggregator aggregator) {
		aggregatorService.requestToCreate(aggregator);

		RequestContext.getCurrentInstance().execute(
				"PF('insertOrEditAggregator').hide()");
		RequestContext.getCurrentInstance().execute(
				"PF('insertOrEditAggregatorPending').hide()");

		String headerMessage = "Create Aggregator Successfully";
		String bodyMessage = "Supervisor must approve the creating of the <br/>aggregator &apos;"
				+ aggregator.getName()
				+ "&apos; to completely the process.";

		FacesUtils.setInfoMessageDialog(headerMessage, bodyMessage);
	}

	public void editAggregator(Aggregator aggregator) {
	
		aggregator = aggregatorService.requestToModify(aggregatorTemp, aggregator);

		RequestContext.getCurrentInstance().execute(
				"PF('insertOrEditAggregator').hide()");
		RequestContext.getCurrentInstance().execute(
				"PF('insertOrEditAggregatorPending').hide()");

		String headerMessage = "Edit Aggregator Successfully";
		String bodyMessage = "Supervisor must approve the editing of the <br/>aggregator &apos;" + aggregator.getName() + "&apos; to completely the process.";

		FacesUtils.setInfoMessageDialog(headerMessage, bodyMessage);
	}

	public void doSave() {
		if (this.action != null && this.action.equals("edit")) {
			editAggregator(this.aggregator);
		} else {
			try {

				Aggregator aggregatorExist = aggregatorService
						.findByCode(
								this.aggregator.getName());

				if (aggregatorExist == null
						|| (aggregatorExist.getName()
								.equals(currentName))) {
					createAggregator(this.aggregator);
				} else {
					String headerMessage = "Attention";
					String bodyMessage = "Aggregator "
							+ this.aggregator.getName()
							+ " already exist in the application.";

					FacesUtils.setWarningMessageDialog(headerMessage,
							bodyMessage);
				}

			} catch (Exception e) {
				String headerMessage = "Error";
				String bodyMessage = "System error";

				FacesUtils.setErrorMessageDialog(headerMessage, bodyMessage);
				e.printStackTrace();
			}
		}

	}

	public void doSearch() {

		status = new ArrayList<Integer>();
		if (!active && !inactive) {
			status.add(Constants.STATUS_ACTIVE);
			status.add(Constants.STATUS_INACTIVE);
		} else {
			if (active) {
				status.add(Constants.STATUS_ACTIVE);
			}
			if (inactive) {
				status.add(Constants.STATUS_INACTIVE);
			}
		}
		approvalStatus = new ArrayList<String>();

		if (!pending && !approved && !rejected) {
			approvalStatus.add(Constants.APPROVAL_STATUS_PENDING_CREATE);
			approvalStatus.add(Constants.APPROVAL_STATUS_PENDING_EDIT);
			approvalStatus.add(Constants.APPROVAL_STATUS_PENDING_DELETE);
			approvalStatus.add(Constants.APPROVAL_STATUS_APPROVED_CREATE);
			approvalStatus.add(Constants.APPROVAL_STATUS_APPROVED_EDIT);
			approvalStatus.add(Constants.APPROVAL_STATUS_REJECTED_CREATE);
			approvalStatus.add(Constants.APPROVAL_STATUS_REJECTED_EDIT);
			approvalStatus.add(Constants.APPROVAL_STATUS_REJECTED_DELETE);
		} else {
			if (pending == true) {
				approvalStatus.add(Constants.APPROVAL_STATUS_PENDING_CREATE);
				approvalStatus.add(Constants.APPROVAL_STATUS_PENDING_EDIT);
				approvalStatus.add(Constants.APPROVAL_STATUS_PENDING_DELETE);
			}
			if (approved == true) {
				approvalStatus.add(Constants.APPROVAL_STATUS_APPROVED_CREATE);
				approvalStatus.add(Constants.APPROVAL_STATUS_APPROVED_EDIT);
			}
			if (rejected == true) {
				approvalStatus.add(Constants.APPROVAL_STATUS_REJECTED_CREATE);
				approvalStatus.add(Constants.APPROVAL_STATUS_REJECTED_EDIT);
				approvalStatus.add(Constants.APPROVAL_STATUS_REJECTED_DELETE);
			}
		}
		FacesUtils.resetPage("aggregatorForm:aggregatorTable");
		aggregatorList = new AggregatorDataModel();

	}

	public void viewDetailedDelete(Aggregator aggregator) {

		this.aggregator = aggregator;

		if (this.aggregator.getDisableDelete() == true) {
			FacesUtils.setApprovalWarning("aggregator",
					this.aggregator.getName(),
					this.aggregator.getApprovalStatus());
		} else {
			RequestContext.getCurrentInstance().execute("PF('dlgConfirm').show()");
		}

	}

	public void deleteAggregator() {
		
		aggregatorService.requestToRemove(aggregator);

		if (aggregator.getApprovalStatus().equals(Constants.APPROVAL_STATUS_REJECTED_CREATE)){

			String headerMessage = "Delete Aggregator Successfully";
			String bodyMessage = "Delete Aggregator " + this.aggregator.getName() + " Successfully";
	
			FacesUtils.setInfoMessageDialog(headerMessage, bodyMessage);
			
		}else{

			String headerMessage = "Delete Aggregator Successfully";
			String bodyMessage = "Supervisor must approve the deleting of the <br/>aggregator &apos;"
					+ aggregator.getName()
					+ "&apos; to completely the process.";
	
			FacesUtils.setInfoMessageDialog(headerMessage, bodyMessage);
		
		}

	}

	public void viewDetailedChangeStatus(Aggregator aggregator) {

		this.aggregator = aggregator;
		
		if (this.aggregator.getDisableDelete() == true) {
			FacesUtils.setApprovalWarning("parameter",
					this.aggregator.getName(),
					this.aggregator.getApprovalStatus());
		} else {
			RequestContext.getCurrentInstance().execute("PF('dlgConfirmChangeStatus').show()");
		}

	}
	

	public void changeStatusAggregator() {
		
		this.aggregatorTemp = (Aggregator) HelperUtils.clone(aggregator);
		
		aggregator.setStatus(aggregator.getStatus()==Constants.STATUS_ACTIVE?Constants.STATUS_INACTIVE:Constants.STATUS_ACTIVE);

		aggregator = aggregatorService.requestToModify(aggregatorTemp, aggregator);

		String headerMessage = "Change Status Aggregator Successfully";
		String bodyMessage = "Supervisor must approve the change status of the <br/>aggregator &apos;"
				+ aggregator.getName()
				+ "&apos; to completely the process.";

		FacesUtils.setInfoMessageDialog(headerMessage, bodyMessage);
	}
	
	@Override
	protected String getCompileFileName() {
		// TODO Auto-generated method stub
		return "Generate Aggregator";
	}
	
	@Override
	protected String getCompileDir() {
		return "/resources/reports/";
    }
	
	@Override
	protected Map<String, Object> getReportParameters() {
		
		TemplateService elService = new TemplateService();

		Map<String, Object> reportParameters = new HashMap<String, Object>();

		String templateBody = aggregatorService.getSystemParameter("WELCOME_TEXT", "Welcome #{#user.fullName}, User Id #{#user.userName} Password #{#user.assword} URL #{#WELCOME_URL}");
        String templateSubject = aggregatorService.getSystemParameter("WELCOME_SUBJECT", "Welcome to BJB SAM !");
        HashMap<String, Object> data = new HashMap<String, Object>();
        data.put("user", aggregator.getUser());
        data.put("WELCOME_URL", aggregatorService.getSystemParameter("THIS_URL", ""));
        data.put("PLAIN_PASSWORD", EncryptUtils.decodeDecrypt(aggregator.getUser().getPlainPassword()));
		
		reportParameters.put("WELCOME_SUBJECT", elService.evaluate(templateSubject, data));
		reportParameters.put("WELCOME_TEXT", elService.evaluate(templateBody, data));
		
		return reportParameters;
	}
	
	public void generatePdf(Aggregator aggregator){

		this.aggregator = aggregator;
		try {
			User user = this.aggregator.getUser();
			if (user != null){
				super.prepareReport();
			}else{
				// TODO Auto-generated catch block
				String headerMessage = "Warning";
				String bodyMessage = "No user for this aggregator !";

				FacesUtils.setWarningMessageDialog(headerMessage, bodyMessage);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			String headerMessage = "Error";
			String bodyMessage = "Generate Report Failed";

			FacesUtils.setErrorMessageDialog(headerMessage, bodyMessage);
			e.printStackTrace();
		} 
	}

	private class AggregatorDataModel extends LazyDataModel<Aggregator> {
		private static final long serialVersionUID = 1L;

		public AggregatorDataModel() {

		}

		@Override
	    public List<Aggregator> load(int startingAt, int maxResult, String sortField, SortOrder sortOrder, Map<String, Object> filters) {

			List<Aggregator> aggregator = null;

			aggregator = aggregatorService.selectByCriteria(startingAt, maxResult,
					code, name, address, phone, pic, emailPic, status, approvalStatus);
			Integer jmlAll = aggregatorService.countByCriteria(code,
					name, address, phone, pic, emailPic, status, approvalStatus);
			setRowCount(jmlAll);
			setPageSize(maxResult);

			return aggregator;
		}

	}

}
