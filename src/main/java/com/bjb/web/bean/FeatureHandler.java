package com.bjb.web.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.TreeNode;

import com.bjb.constant.Constants;
import com.bjb.model.Feature;
import com.bjb.service.FeatureService;
import com.bjb.service.SessionService;
import com.bjb.util.HelperUtils;
import com.bjb.web.bean.support.DiffService;
import com.bjb.web.helper.FacesUtils;

@ManagedBean
@ViewScoped
public class FeatureHandler implements Serializable {
	private static final long serialVersionUID = 1L;

	private Feature feature;
	private Feature featureTemp;

	private String action;

	private String code;
	private String name;
	private String description;
	private String commisionAccountNo;
	private List<Integer> status;;
	private List<String> approvalStatus;

	private boolean active;
	private boolean inactive;

	private boolean pending;
	private boolean approved;
	private boolean rejected;

	private boolean saveFeature;

	private LazyDataModel<Feature> featureList;

	private boolean doShowSearch = false;

	private boolean doShowHistory = false;

	private String currentName;
	
	private List<SelectItem> messageTypeLookup = new ArrayList<SelectItem>();
	
	@ManagedProperty(value="#{sessionService}")
	private SessionService sessionService;
	
	@ManagedProperty(value="#{featureService}")
	private FeatureService featureService;

	@ManagedProperty(value="#{diffService}")
	private DiffService diffService;
	
	public void setDiffService(DiffService diffService) {
		this.diffService = diffService;
	}

	public TreeNode getDiffTreeTable() {
		return diffService.diff(featureTemp, feature);
	}
	
	public void setFeatureService(FeatureService featureService) {
		this.featureService = featureService;
	}

	@PostConstruct
	public void init() {
		sessionService.authMenu(sessionService.getUser().getId(), "manageFeature");

		featureList = new FeatureDataModel();

		pending = false;
		approved = false;
		rejected = false;
		code = "";
		name = "";
		description = "";
		commisionAccountNo = "";

		doSearch();
		
		SelectItem selectItem = new SelectItem();
		selectItem.setValue("ISO");
		selectItem.setLabel("ISO");
		this.messageTypeLookup.add(selectItem);
		selectItem = new SelectItem();
		selectItem.setValue("XML");
		selectItem.setLabel("XML");
		this.messageTypeLookup.add(selectItem);
		selectItem = new SelectItem();
		selectItem.setValue("JSON");
		selectItem.setLabel("JSON");
		this.messageTypeLookup.add(selectItem);

	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getCommisionAccountNo() {
		return commisionAccountNo;
	}

	public void setCommisionAccountNo(String commisionAccountNo) {
		this.commisionAccountNo = commisionAccountNo;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public List<String> getStatus() {
		return approvalStatus;
	}

	public void setStatus(List<String> status) {
		this.approvalStatus = status;
	}

	public boolean isPending() {
		return pending;
	}

	public void setPending(boolean pending) {
		this.pending = pending;
	}

	public boolean isApproved() {
		return approved;
	}

	public void setApproved(boolean approved) {
		this.approved = approved;
	}

	public boolean isRejected() {
		return rejected;
	}

	public void setRejected(boolean rejected) {
		this.rejected = rejected;
	}

	public void showHistory() {
		doShowHistory = true;
	}

	public void hideHistory() {
		doShowHistory = false;
	}

	public boolean isDoShowHistory() {
		return doShowHistory;
	}

	public void setDoShowHistory(boolean doShowHistory) {
		this.doShowHistory = doShowHistory;
	}

	public SessionService getSessionBean() {
		return sessionService;
	}

	public void setSessionService(SessionService sessionService) {
		this.sessionService = sessionService;
	}

	public boolean isSaveFeature() {
		return saveFeature;
	}

	public void setSaveFeature(boolean saveFeature) {
		this.saveFeature = saveFeature;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Feature getFeature() {
		return feature;
	}

	public Feature getFeatureTemp() {
		return featureTemp;
	}

	public void setFeatureTemp(Feature featureTemp) {
		this.featureTemp = featureTemp;
	}

	public void setFeature(Feature feature) {
		this.feature = feature;
	}

	public LazyDataModel<Feature> getFeatureList() {
		return featureList;
	}

	public void setFeatureList(LazyDataModel<Feature> featureList) {
		this.featureList = featureList;
	}

	public boolean isDoShowSearch() {
		return doShowSearch;
	}

	public void setDoShowSearch(boolean doShowSearch) {
		this.doShowSearch = doShowSearch;
	}

	public void showSearch() {
		doShowSearch = true;
	}

	public void hideSearch() {
		doShowSearch = false;
		init();
	}
	
	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public boolean isInactive() {
		return inactive;
	}

	public void setInactive(boolean inactive) {
		this.inactive = inactive;
	}

	public List<SelectItem> getMessageTypeLookup() {
		return messageTypeLookup;
	}

	public void setMessageTypeLookup(List<SelectItem> messageTypeLookup) {
		this.messageTypeLookup = messageTypeLookup;
	}
	
	public void viewDetailedFeature(Feature feature) {
		this.feature = feature;

		if (this.feature.getApprovalStatus().equals(
				Constants.APPROVAL_STATUS_PENDING_EDIT)) {
			
			this.featureTemp = featureService.findPendingApprovalByCode(feature.getCode());
					
			if (featureTemp!=null) {
				RequestContext.getCurrentInstance().execute(
							"PF('viewFeatureEdit').show()");
				return;
			}
						
		} 
		RequestContext.getCurrentInstance().execute("PF('viewFeature').show()");
	}

	public void viewDetailedFeatureEdit(Feature feature) {

		this.feature = feature;

		if (this.feature.getDisableEdit() == true) {
			FacesUtils.setApprovalWarning("parameter",
					this.feature.getName(),
					this.feature.getApprovalStatus());
		} else {
			this.action = "edit";
			if (this.feature.getApprovalStatus().equals(
					Constants.APPROVAL_STATUS_PENDING_EDIT)) {

				this.featureTemp = featureService.findPendingApprovalByCode(feature.getCode());
				if (featureTemp != null) {
					currentName = this.featureTemp.getName();
					RequestContext.getCurrentInstance().execute(
							"PF('insertOrEditFeaturePending').show()");
					return;
				} 
			}
			this.featureTemp = HelperUtils.clone(feature);
			RequestContext.getCurrentInstance().execute(
						"PF('insertOrEditFeature').show()");
		}
	}

	public void newFeature() {
		this.feature = new Feature();
		BigDecimal seq = featureService.generateId();
		this.feature.setId(seq);
		this.feature.setCode("P" + String.format("%04d", seq.intValue()));
		this.feature.setStatus(Constants.STATUS_ACTIVE);
		this.action = "";
	}

	public void createFeature(Feature feature) {
		featureService.requestToCreate(feature);
		
		RequestContext.getCurrentInstance().execute(
				"PF('insertOrEditFeature').hide()");
		RequestContext.getCurrentInstance().execute(
				"PF('insertOrEditFeaturePending').hide()");

		String headerMessage = "Create Feature Successfully";
		String bodyMessage = "Supervisor must approve the creating of the <br/>feature &apos;"
				+ feature.getName()
				+ "&apos; to completely the process.";

		FacesUtils.setInfoMessageDialog(headerMessage, bodyMessage);
	}

	public void editFeature(Feature feature) {
		feature = featureService.requestToModify(featureTemp, feature);
		
		RequestContext.getCurrentInstance().execute(
				"PF('insertOrEditFeature').hide()");
		RequestContext.getCurrentInstance().execute(
				"PF('insertOrEditFeaturePending').hide()");

		String headerMessage = "Edit Feature Successfully";
		String bodyMessage = "Supervisor must approve the editing of the <br/>feature &apos;"
				+ feature.getName()
				+ "&apos; to completely the process.";

		FacesUtils.setInfoMessageDialog(headerMessage, bodyMessage);
	}

	public void doSave() {
		if (this.action != null && this.action.equals("edit")) {
			editFeature(this.feature);
		} else {
			try {

				Feature featureExist = featureService
						.findByCode(
								this.feature.getName());

				if (featureExist == null
						|| (featureExist.getName()
								.equals(currentName))) {
					createFeature(this.feature);
				} else {
					String headerMessage = "Attention";
					String bodyMessage = "Feature "
							+ this.feature.getName()
							+ " already exist in the application.";

					FacesUtils.setWarningMessageDialog(headerMessage,
							bodyMessage);
				}

			} catch (Exception e) {
				String headerMessage = "Error";
				String bodyMessage = "System error";

				FacesUtils.setErrorMessageDialog(headerMessage, bodyMessage);
				e.printStackTrace();
			}
		}

	}

	public void doSearch() {

		status = new ArrayList<Integer>();
		if (!active && !inactive) {
			status.add(Constants.STATUS_ACTIVE);
			status.add(Constants.STATUS_INACTIVE);
		} else {
			if (active) {
				status.add(Constants.STATUS_ACTIVE);
			}
			if (inactive) {
				status.add(Constants.STATUS_INACTIVE);
			}
		}
		approvalStatus = new ArrayList<String>();

		if (!pending && !approved && !rejected) {
			approvalStatus.add(Constants.APPROVAL_STATUS_PENDING_CREATE);
			approvalStatus.add(Constants.APPROVAL_STATUS_PENDING_EDIT);
			approvalStatus.add(Constants.APPROVAL_STATUS_PENDING_DELETE);
			approvalStatus.add(Constants.APPROVAL_STATUS_APPROVED_CREATE);
			approvalStatus.add(Constants.APPROVAL_STATUS_APPROVED_EDIT);
			approvalStatus.add(Constants.APPROVAL_STATUS_REJECTED_CREATE);
			approvalStatus.add(Constants.APPROVAL_STATUS_REJECTED_EDIT);
			approvalStatus.add(Constants.APPROVAL_STATUS_REJECTED_DELETE);
		} else {
			if (pending == true) {
				approvalStatus.add(Constants.APPROVAL_STATUS_PENDING_CREATE);
				approvalStatus.add(Constants.APPROVAL_STATUS_PENDING_EDIT);
				approvalStatus.add(Constants.APPROVAL_STATUS_PENDING_DELETE);
			}
			if (approved == true) {
				approvalStatus.add(Constants.APPROVAL_STATUS_APPROVED_CREATE);
				approvalStatus.add(Constants.APPROVAL_STATUS_APPROVED_EDIT);
			}
			if (rejected == true) {
				approvalStatus.add(Constants.APPROVAL_STATUS_REJECTED_CREATE);
				approvalStatus.add(Constants.APPROVAL_STATUS_REJECTED_EDIT);
				approvalStatus.add(Constants.APPROVAL_STATUS_REJECTED_DELETE);
			}
		}
		FacesUtils.resetPage("featureForm:featureTable");
		featureList = new FeatureDataModel();

	}

	public void viewDetailedDelete(Feature feature) {

		this.feature = feature;

		if (this.feature.getDisableDelete() == true) {
			FacesUtils.setApprovalWarning("feature",
					this.feature.getName(),
					this.feature.getApprovalStatus());
		} else {
			RequestContext.getCurrentInstance().execute("PF('dlgConfirm').show()");
		}

	}

	public void deleteFeature() {
		
		featureService.requestToRemove(feature);

		if (feature.getApprovalStatus().equals(Constants.APPROVAL_STATUS_REJECTED_CREATE)){

			String headerMessage = "Success";
			String bodyMessage = "Delete System Feature " + this.feature.getName() + " Successfully";
	
			FacesUtils.setInfoMessageDialog(headerMessage, bodyMessage);
			
		}else{
		
			String headerMessage = "Delete Feature Successfully";
			String bodyMessage = "Supervisor must approve the deleting of the <br/>feature &apos;"
					+ feature.getName()
					+ "&apos; to completely the process.";
	
			FacesUtils.setInfoMessageDialog(headerMessage, bodyMessage);
		
		}

	}
	
	public void viewDetailedChangeStatus(Feature feature) {

		this.feature = feature;

		if (this.feature.getDisableDelete() == true) {
			FacesUtils.setApprovalWarning("parameter",
					this.feature.getName(),
					this.feature.getApprovalStatus());
		} else {
			RequestContext.getCurrentInstance().execute("PF('dlgConfirmChangeStatus').show()");
		}

	}
	
	public boolean isRejectReason(){
		boolean isRejectReason = false;
		if (this.feature != null){
			if (this.feature.getApprovalStatus().equals(Constants.APPROVAL_STATUS_REJECTED_CREATE) || 
					this.feature.getApprovalStatus().equals(Constants.APPROVAL_STATUS_REJECTED_EDIT) ||
						this.feature.getApprovalStatus().equals(Constants.APPROVAL_STATUS_REJECTED_DELETE)){
				isRejectReason = true;
			}
		}
		return isRejectReason;
	}

	public void changeStatusFeature() {
		
		this.featureTemp = (Feature) HelperUtils.clone(feature);
		
		feature.setStatus(feature.getStatus()==Constants.STATUS_ACTIVE?Constants.STATUS_INACTIVE:Constants.STATUS_ACTIVE);

		feature = featureService.requestToModify(featureTemp, feature);

		String headerMessage = "Change Status Feature Successfully";
		String bodyMessage = "Supervisor must approve the change status of the <br/>feature &apos;"
				+ feature.getName()
				+ "&apos; to completely the process.";

		FacesUtils.setInfoMessageDialog(headerMessage, bodyMessage);
	}


	private class FeatureDataModel extends LazyDataModel<Feature> {
		private static final long serialVersionUID = 1L;

		public FeatureDataModel() {

		}

		@Override
	    public List<Feature> load(int startingAt, int maxResult, String sortField, SortOrder sortOrder, Map<String, Object> filters) {

			List<Feature> feature = null;

			feature = featureService.selectByCriteria(startingAt, maxResult, 
					code, name, description, commisionAccountNo, status, approvalStatus);
			Integer jmlAll = featureService.countByCriteria(code,
					name, description, commisionAccountNo, status, approvalStatus);
			setRowCount(jmlAll);
			setPageSize(maxResult);

			return feature;
		}

	}

}
