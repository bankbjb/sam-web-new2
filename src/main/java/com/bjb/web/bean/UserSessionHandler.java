package com.bjb.web.bean;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import com.bjb.model.User;
import com.bjb.web.listener.UserSessionBinder;
import com.bjb.web.model.UserSession;

@ManagedBean
@SessionScoped
public class UserSessionHandler implements Serializable {
	private static final long serialVersionUID = 1L;
	private User userkill = new User();
	
	private ExternalContext externalContext;
	
	
	public UserSessionHandler() {
		externalContext = FacesContext.getCurrentInstance().getExternalContext();
	}
	
	public void killSession() {
		UserSession userSession = UserSessionBinder.getLogins().remove(userkill);
		if (userSession != null) {
			userSession.getSession().invalidate();
		}
	}

	public User getUserkill() {
		return userkill;
	}

	public void setUserkill(User userkill) {
		this.userkill = userkill;
	}
	
	
}