package com.bjb.web.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.bjb.constant.Constants;
import com.bjb.model.FieldMapping;
import com.bjb.model.MessageField;
import com.bjb.model.MessageMapping;
import com.bjb.service.ApprovalPendingService;
import com.bjb.service.FeatureService;
import com.bjb.service.FieldMappingService;
import com.bjb.service.MessageFieldService;
import com.bjb.service.MessageMappingService;
import com.bjb.service.SessionService;
import com.bjb.service.VariableConverter;
import com.bjb.web.helper.FacesUtils;

@ManagedBean
@ViewScoped
public class MessageMappingHandler implements Serializable {
	private static final long serialVersionUID = 1L;

	private MessageMapping messageMapping;
	private MessageMapping messageMappingTemp;

	private String action;

	private String mode = "REQUEST";
	private String inmode = "RESPONSE";

	private String code;
	private String feature;
	private String messageType;
	private List<Integer> status;;
	private List<String> approvalStatus;

	private boolean active;
	private boolean inactive;

	private boolean pending;
	private boolean approved;
	private boolean rejected;

	private boolean saveMessageMapping;

	private LazyDataModel<MessageMapping> messageMappingList;

	private boolean doShowSearch = false;

	private boolean doShowHistory = false;

	private boolean fmrequest = true;
	private boolean fmresponse = false;

	private String trxtype = "INQUIRY_PAYMENT";
	private List<Integer> listEmpty = new ArrayList<>();

	private List<SelectItem> messageFieldLookup = new ArrayList<SelectItem>();

	private List<SelectItem> variableFieldLookup = new ArrayList<SelectItem>();
	private FieldMapping fieldMapping = new FieldMapping();

	private List<FieldMapping> fieldMappingList = new ArrayList<FieldMapping>();
	private List<FieldMapping> fieldMappingList1 = new ArrayList<FieldMapping>();
	private List<FieldMapping> fieldMappingList2 = new ArrayList<FieldMapping>();
	private List<FieldMapping> fieldMappingList3 = new ArrayList<FieldMapping>();
	private List<FieldMapping> fieldMappingListTemp = new ArrayList<FieldMapping>();

	@ManagedProperty(value = "#{sessionService}")
	private SessionService sessionService;

	@ManagedProperty(value = "#{approvalPendingService}")
	private ApprovalPendingService approvalPendingService;

	@ManagedProperty(value = "#{messageMappingService}")
	private MessageMappingService messageMappingService;

	@ManagedProperty(value = "#{featureService}")
	private FeatureService featureService;

	@ManagedProperty(value = "#{messageFieldService}")
	private MessageFieldService messageFieldService;

	@ManagedProperty(value = "#{fieldMappingService}")
	private FieldMappingService fieldMappingService;

	@ManagedProperty(value = "#{variableConverter}")
	private VariableConverter variableConverter;

	@PostConstruct
	public void init() {
		sessionService.authMenu(sessionService.getUser().getId(), "manageMessageMapping");

		messageMappingList = new MessageMappingDataModel();

		code = "";
		feature = "";
		messageType = "";

		doSearch();

	}

	public List<FieldMapping> getFieldMappingList() {
		if (this.messageMapping != null) {
			if (this.messageMapping.getMessageType().equals("ISO")) {
				fieldMappingList = fieldMappingList1;
			} else if (this.messageMapping.getMessageType().equals("XML")) {
				fieldMappingList = fieldMappingList2;
			} else if (this.messageMapping.getMessageType().equals("JSON")) {
				fieldMappingList = fieldMappingList3;
			}
		}
		return fieldMappingList;

	}

	public void setFieldMappingList(List<FieldMapping> fieldMappingList) {
		this.fieldMappingList = fieldMappingList;
	}

	public List<FieldMapping> getFieldMappingList1() {
		return fieldMappingList1;
	}

	public void setFieldMappingList1(List<FieldMapping> fieldMappingList1) {
		this.fieldMappingList1 = fieldMappingList1;
	}

	public List<FieldMapping> getFieldMappingList2() {
		return fieldMappingList2;
	}

	public void setFieldMappingList2(List<FieldMapping> fieldMappingList2) {
		this.fieldMappingList2 = fieldMappingList2;
	}

	public List<FieldMapping> getFieldMappingList3() {
		return fieldMappingList3;
	}

	public void setFieldMappingList3(List<FieldMapping> fieldMappingList3) {
		this.fieldMappingList3 = fieldMappingList3;
	}

	public void setApprovalPendingService(ApprovalPendingService approvalPendingService) {
		this.approvalPendingService = approvalPendingService;
	}

	public void setMessageMappingService(MessageMappingService messageMappingService) {
		this.messageMappingService = messageMappingService;
	}

	public void setFeatureService(FeatureService featureService) {
		this.featureService = featureService;
	}

	public void setMessageFieldService(MessageFieldService messageFieldService) {
		this.messageFieldService = messageFieldService;
	}

	public void setFieldMappingService(FieldMappingService fieldMappingService) {
		this.fieldMappingService = fieldMappingService;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public FieldMapping getFieldMapping() {
		return fieldMapping;
	}

	public void setFieldMapping(FieldMapping fieldMapping) {
		this.fieldMapping = fieldMapping;
	}

	public String getFeature() {
		return feature;
	}

	public void setFeature(String feature) {
		this.feature = feature;
	}

	public List<String> getStatus() {
		return approvalStatus;
	}

	public void setStatus(List<String> status) {
		this.approvalStatus = status;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public boolean isInactive() {
		return inactive;
	}

	public void setInactive(boolean inactive) {
		this.inactive = inactive;
	}

	public boolean isPending() {
		return pending;
	}

	public void setPending(boolean pending) {
		this.pending = pending;
	}

	public boolean isApproved() {
		return approved;
	}

	public void setApproved(boolean approved) {
		this.approved = approved;
	}

	public boolean isRejected() {
		return rejected;
	}

	public void setRejected(boolean rejected) {
		this.rejected = rejected;
	}

	public void showHistory() {
		doShowHistory = true;
	}

	public void hideHistory() {
		doShowHistory = false;
	}

	public boolean isDoShowHistory() {
		return doShowHistory;
	}

	public void setDoShowHistory(boolean doShowHistory) {
		this.doShowHistory = doShowHistory;
	}

	public SessionService getSessionService() {
		return sessionService;
	}

	public void setSessionService(SessionService sessionService) {
		this.sessionService = sessionService;
	}

	public boolean isSaveMessageMapping() {
		return saveMessageMapping;
	}

	public void setSaveMessageMapping(boolean saveMessageMapping) {
		this.saveMessageMapping = saveMessageMapping;
	}

	public MessageMapping getMessageMapping() {
		return messageMapping;
	}

	public MessageMapping getMessageMappingTemp() {
		return messageMappingTemp;
	}

	public void setMessageMappingTemp(MessageMapping messageMappingTemp) {
		this.messageMappingTemp = messageMappingTemp;
	}

	public void setMessageMapping(MessageMapping messageMapping) {
		this.messageMapping = messageMapping;
	}

	public LazyDataModel<MessageMapping> getMessageMappingList() {
		return messageMappingList;
	}

	public void setMessageMappingList(LazyDataModel<MessageMapping> messageMappingList) {
		this.messageMappingList = messageMappingList;
	}

	public boolean isDoShowSearch() {
		return doShowSearch;
	}

	public void setDoShowSearch(boolean doShowSearch) {
		this.doShowSearch = doShowSearch;
	}

	public void showSearch() {
		doShowSearch = true;
	}

	public void hideSearch() {
		doShowSearch = false;
	}

	public List<SelectItem> getMessageFieldLookup() {
		return messageFieldLookup;
	}

	public List<SelectItem> getVariableFieldLookup() {
		return variableFieldLookup;
	}

	public List<FieldMapping> getSpecificFieldMapping(String reqres, List<FieldMapping> list) {
		List<FieldMapping> listn = new ArrayList<>();
		for (FieldMapping l : list) {
			if (reqres.equalsIgnoreCase("request")) {
				if (l.getRequest().intValue() == 1) {
					listn.add(l);
				}
			} else if (reqres.equalsIgnoreCase("response")) {
				if (l.getResponse().intValue() == 1) {
					listn.add(l);
				}
			}
		}
		return listn;
	}

	public List<FieldMapping> getSpecificFieldMapping(String reqres, List<FieldMapping> list, String trxtype) {
		List<FieldMapping> listn = new ArrayList<>();
		for (FieldMapping l : list) {
			if (reqres.equalsIgnoreCase("request") && l.getTrxType().equalsIgnoreCase(trxtype)) {
				if (l.getRequest().intValue() == 1) {
					listn.add(l);
				}
			} else if (reqres.equalsIgnoreCase("response") && l.getTrxType().equalsIgnoreCase(trxtype)) {
				if (l.getResponse().intValue() == 1) {
					listn.add(l);
				}
			}
		}
		return listn;
	}

	public List<FieldMapping> getRemainingFieldMapping(String reqres, List<FieldMapping> list, String trxtype) {
		List<FieldMapping> listn = new ArrayList<>();
		for (FieldMapping l : list) {
			if (reqres.equalsIgnoreCase("request")) {
				if (l.getResponse().intValue() == 1 && !l.getTrxType().equalsIgnoreCase(trxtype)) {
					listn.add(l);
				}
				if (l.getRequest().intValue() == 1) {
					listn.add(l);
				}
			} else if (reqres.equalsIgnoreCase("response")) {
				if (l.getRequest().intValue() == 1 && !l.getTrxType().equalsIgnoreCase(trxtype)) {
					listn.add(l);
				}
				if (l.getResponse().intValue() == 1) {
					listn.add(l);
				}
			}
		}
		return listn;
	}

	public void viewDetailedMessageMapping(MessageMapping messageMapping) {
		this.messageMapping = messageMappingService.findByCode(messageMapping.getCode());
		RequestContext.getCurrentInstance().execute("PF('viewMessageMapping').show()");
	}

	public void viewDetailedMessageMappingEdit(MessageMapping messageMapping) {
		this.messageMapping = messageMappingService.findByCode(messageMapping.getCode());
		checkMessageType();
		this.fieldMappingListTemp = new ArrayList<FieldMapping>(
				getSpecificFieldMapping(mode, this.messageMapping.getDetails(), this.trxtype));
		for (FieldMapping fieldMapping : getSpecificFieldMapping(mode, this.messageMapping.getDetails(),
				this.trxtype)) {
			MessageField messageField = messageFieldService.findByIdAndMessageType(fieldMapping.getIndex(),
					messageMapping.getMessageType());
			String name = messageField.getName();
			fieldMapping.setName(name);
		}
		if (this.messageMapping.getMessageType().equals("ISO")) {
			this.fieldMappingList1 = getSpecificFieldMapping(mode, this.messageMapping.getDetails(), this.trxtype);
		} else if (this.messageMapping.getMessageType().equals("XML")) {
			this.fieldMappingList2 = getSpecificFieldMapping(mode, this.messageMapping.getDetails(), this.trxtype);
		} else if (this.messageMapping.getMessageType().equals("JSON")) {
			this.fieldMappingList3 = getSpecificFieldMapping(mode, this.messageMapping.getDetails(), this.trxtype);
		}
		this.action = "edit";
		// fmrequest = true;
		fieldMapping.setPadding("RPS");
		fieldMapping.setTrxType(trxtype);
		RequestContext.getCurrentInstance().execute("PF('insertOrEditMessageMapping').show()");
	}

	public void checkMessageType() {
		messageFieldLookup = new ArrayList<SelectItem>();
		variableFieldLookup = new ArrayList<SelectItem>();
		fieldMapping = new FieldMapping();
		List<MessageField> allMessageField = messageFieldService
				.selectByMessageTypeOrderByIdAsc(messageMapping != null ? messageMapping.getMessageType() : "ISO");
		SelectItem selectItem;
		for (MessageField messageField : allMessageField) {
			selectItem = new SelectItem();
			selectItem.setValue(messageField.getId());
			if (messageField.getName().equals("empty")) {
				if (!listEmpty.contains(messageField.getId())) {
					listEmpty.add(messageField.getId());
				}
			}
			if (messageField.getDescription() != null) {
				selectItem.setLabel(messageField.getName() + " - " + messageField.getDescription());
			} else {
				selectItem.setLabel(messageField.getName());
			}
			this.messageFieldLookup.add(selectItem);
		}
		allMessageField = messageFieldService.selectByMessageType("61");
		for (MessageField messageField : allMessageField) {
			selectItem = new SelectItem();
			selectItem.setValue(messageField.getId());
			selectItem.setLabel(messageField.getName() + " - " + messageField.getDescription());
			this.variableFieldLookup.add(selectItem);
		}

		fieldMapping.setPadding("RPS");
		fieldMapping.setTrxType(trxtype);
	}

	public void deleteFieldMapping(FieldMapping fieldMapping) {
		if (this.messageMapping.getMessageType().equals("ISO")) {
			this.fieldMappingList1.remove(fieldMapping);
		} else if (this.messageMapping.getMessageType().equals("XML")) {
			this.fieldMappingList2.remove(fieldMapping);
		} else if (this.messageMapping.getMessageType().equals("JSON")) {
			this.fieldMappingList3.remove(fieldMapping);
		}
	}

	public void newMessageMapping() {
		this.messageMapping = new MessageMapping();
		SimpleDateFormat dateFormat = new SimpleDateFormat(Constants.FORMAT_DATE_SHORT);
		BigDecimal seq = messageMappingService.generateId();
		this.messageMapping.setId(seq);
		this.messageMapping.setCode(dateFormat.format(new Date()) + "MM" + String.format("%04d", seq.intValue()));
		this.messageMapping.setMessageType("ISO");
		this.messageMapping.setStatus(Constants.STATUS_ACTIVE);
		this.action = "";
		// this.fmrequest = true;
		// this.fmresponse = false;

		this.fieldMappingList1 = new ArrayList<FieldMapping>();
		this.fieldMappingList2 = new ArrayList<FieldMapping>();
		this.fieldMappingList3 = new ArrayList<FieldMapping>();
		checkMessageType();
		this.fieldMapping.setPadding("RPS");
		this.fieldMapping.setTrxType(trxtype);
	}

	public void createMessageMapping(MessageMapping messageMapping) {

		try {/*
				 * messageMapping.setCreateDate(HelperUtils.getCurrentDateTime()
				 * ); messageMapping.setCreateWho(sessionService.getUser().
				 * getUserName());
				 * messageMapping.setChangeDate(HelperUtils.getCurrentDateTime()
				 * ); messageMapping.setChangeWho(sessionService.getUser().
				 * getUserName());
				 * messageMapping.setApprovalStatus(Constants.NO_APPROVAL_STATUS
				 * );
				 */

			List<FieldMapping> fieldMappingList = new ArrayList<FieldMapping>();
			if (this.messageMapping.getMessageType().equals("ISO")) {
				fieldMappingList = this.fieldMappingList1;
			} else if (this.messageMapping.getMessageType().equals("XML")) {
				fieldMappingList = this.fieldMappingList2;
			} else if (this.messageMapping.getMessageType().equals("JSON")) {
				fieldMappingList = this.fieldMappingList3;
			}
			this.messageMapping.setDetails(fieldMappingList);
			for (FieldMapping fieldMapping : this.messageMapping.getDetails()) {
				fieldMapping.setMessageMappingId(this.messageMapping.getId());
			}
			messageMappingService.insert(this.messageMapping);

			String headerMessage = "Success";
			String bodyMessage = "Insert Message Mapping Successfully";

			FacesUtils.setInfoMessageDialog(headerMessage, bodyMessage);
		} catch (Exception e) {
			e.printStackTrace();
			String headerMessage = "Error";
			String bodyMessage = "Insert Message Mapping Failed";

			FacesUtils.setErrorMessageDialog(headerMessage, bodyMessage);
		}

	}

	public void editMessageMapping(MessageMapping messageMapping) {
		try {
			/*
			 * messageMapping.setChangeDate(HelperUtils.getCurrentDateTime());
			 * messageMapping.setChangeWho(sessionService.getUser().getUserName(
			 * ));
			 */
			for (FieldMapping fieldMapping : fieldMappingService.findByMessageMappingId(this.messageMapping.getId())) {
				fieldMappingService.delete(fieldMapping);
			}
			messageMappingService.delete(this.messageMapping);
			List<FieldMapping> fieldMappingList = new ArrayList<FieldMapping>();
			if (this.messageMapping.getMessageType().equals("ISO")) {
				fieldMappingList = this.fieldMappingList1;
			} else if (this.messageMapping.getMessageType().equals("XML")) {
				fieldMappingList = this.fieldMappingList2;
			} else if (this.messageMapping.getMessageType().equals("JSON")) {
				fieldMappingList = this.fieldMappingList3;
			}
			List<FieldMapping> listres = getRemainingFieldMapping(inmode, this.messageMapping.getDetails(),
					this.trxtype);
			List<FieldMapping> listadd = new ArrayList<>();
			listadd.addAll(listres);
			listadd.addAll(fieldMappingList);
			this.messageMapping.setDetails(listadd);
			for (FieldMapping fieldMapping : this.messageMapping.getDetails()) {
				fieldMapping.setMessageMappingId(this.messageMapping.getId());
			}
			messageMappingService.insert(this.messageMapping);

			String headerMessage = "Success";
			String bodyMessage = "Edit Message Mapping Successfully";

			FacesUtils.setInfoMessageDialog(headerMessage, bodyMessage);
		} catch (Exception e) {
			e.printStackTrace();
			String headerMessage = "Error";
			String bodyMessage = "Edit Message Mapping Failed";

			FacesUtils.setErrorMessageDialog(headerMessage, bodyMessage);
		}
	}

	public void doSave() {
		if (this.action != null && this.action.equals("edit")) {
			editMessageMapping(messageMapping);
		} else {
			createMessageMapping(messageMapping);
		}
		RequestContext.getCurrentInstance().execute("PF('insertOrEditMessageMapping').hide()");
	}

	public void doSearch() {

		status = new ArrayList<Integer>();
		if (!active && !inactive) {
			status.add(Constants.STATUS_ACTIVE);
			status.add(Constants.STATUS_INACTIVE);
		} else {
			if (active) {
				status.add(Constants.STATUS_ACTIVE);
			}
			if (inactive) {
				status.add(Constants.STATUS_INACTIVE);
			}
		}
		approvalStatus = new ArrayList<String>();
		approvalStatus.add(Constants.NO_APPROVAL_STATUS);
		FacesUtils.resetPage("messageMappingForm:messageMappingTable");
		messageMappingList = new MessageMappingDataModel();

	}

	public void viewDetailedDelete(MessageMapping messageMapping) {

		this.messageMapping = messageMappingService.findByCode(messageMapping.getCode());
		RequestContext.getCurrentInstance().execute("PF('dlgConfirm').show()");

	}

	public boolean checkRemainingLengthVariable(List<FieldMapping> fieldMappingList, FieldMapping newfm) {
		Integer varId = Integer.valueOf(newfm.getVariable());
		Integer length = messageFieldService.findByIdAndMessageType(varId, "61").getLength();
		Integer temp = 0;
		for (FieldMapping fm : fieldMappingList) {
			if (fm.getVariable().equalsIgnoreCase(newfm.getVariable())) {
				temp += (fm.getEndAt() - fm.getStartAt());
			}
		}
		if (temp + (newfm.getEndAt() - newfm.getStartAt()) > length) {
			return false;
		} else {
			return true;
		}
	}

	public boolean checkRemainingLengthVariable(List<FieldMapping> fieldMappingList, FieldMapping newfm,
			String reqres) {
		Integer varId = Integer.valueOf(newfm.getVariable());
		Integer length = messageFieldService.findByIdAndMessageType(varId, "61").getLength();
		Integer temp = 0;
		for (FieldMapping fm : fieldMappingList) {
			if (fm.getVariable().equalsIgnoreCase(newfm.getVariable())) {
				if (reqres.equalsIgnoreCase("request") && fm.getRequest().equals(Integer.valueOf(1))) {
					temp += (fm.getEndAt() - fm.getStartAt());
				} else if (reqres.equalsIgnoreCase("response") && fm.getResponse().equals(Integer.valueOf(1))) {
					temp += (fm.getEndAt() - fm.getStartAt());
				}
			}
		}
		if (temp + (newfm.getEndAt() - newfm.getStartAt()) > length) {
			return false;
		} else {
			return true;
		}
	}

	public void addNewFieldMapping() {
		System.out.println("req:" + fmrequest + " res:" + fmresponse);
		if (fmrequest) {
			fieldMapping.setRequest(1);
		} else {
			fieldMapping.setRequest(0);
		}
		if (fmresponse) {
			fieldMapping.setResponse(1);
		} else {
			fieldMapping.setResponse(0);
		}
		List<FieldMapping> fieldMappingList = new ArrayList<FieldMapping>();

		if (this.messageMapping.getMessageType().equals("ISO")) {
			fieldMappingList = this.fieldMappingList1;
		} else if (this.messageMapping.getMessageType().equals("XML")) {
			fieldMappingList = this.fieldMappingList2;
		} else if (this.messageMapping.getMessageType().equals("JSON")) {
			fieldMappingList = this.fieldMappingList3;
		}

		boolean isMappable = false;
		isMappable = checkRemainingLengthVariable(fieldMappingList, fieldMapping, mode);
		if (!isMappable) {
			String headerMessage = "Warning";
			String bodyMessage = "Field mapping " + fieldMapping.getIndex() + " can't be added to variable "
					+ variableConverter.getName(fieldMapping.getVariable()) + ". Not Enough Space.";
			FacesUtils.setWarningMessageDialog(headerMessage, bodyMessage);
		}

		boolean isExist = false;
		for (FieldMapping fieldMapping : fieldMappingList) {
			if (listEmpty.contains(fieldMapping.getIndex())) {
				System.out.println("empty.....");
				break;
			} else if (fieldMapping.getIndex() == this.fieldMapping.getIndex()) {
				String headerMessage = "Warning";
				String bodyMessage = "Field mapping " + fieldMapping.getName() + " already added";
				FacesUtils.setWarningMessageDialog(headerMessage, bodyMessage);
				isExist = true;
				break;
			}
			if (fieldMapping.getFieldPosition() == this.fieldMapping.getFieldPosition()) {
				String headerMessage = "Warning";
				String bodyMessage = "Field position " + fieldMapping.getName() + " already added";
				FacesUtils.setWarningMessageDialog(headerMessage, bodyMessage);
				isExist = true;
				break;
			}
			if (fieldMapping.getVariable().equalsIgnoreCase(this.fieldMapping.getVariable())) {
				String headerMessage = "Warning";
				String bodyMessage = "Variable " + variableConverter.getName(fieldMapping.getVariable()) + " "
						+ fieldMapping.getName() + " already added";
				FacesUtils.setWarningMessageDialog(headerMessage, bodyMessage);
				isExist = true;
				break;
			}
		}
		MessageField messageField = messageFieldService.findByIdAndMessageType(fieldMapping.getIndex(),
				messageMapping.getMessageType());
		String name = messageField.getName();
		fieldMapping.setName(name);
		if (isMappable) {
			if (!isExist) {
				if (this.messageMapping.getMessageType().equals("ISO")) {
					this.fieldMappingList1.add(fieldMapping);
				} else if (this.messageMapping.getMessageType().equals("XML")) {
					this.fieldMappingList2.add(fieldMapping);
				} else if (this.messageMapping.getMessageType().equals("JSON")) {
					this.fieldMappingList3.add(fieldMapping);
				}
				fieldMapping = new FieldMapping();
				fieldMapping.setPadding("RPS");
				fieldMapping.setTrxType(trxtype);
			}
		}
	}

	public void viewDetailedChangeStatus(MessageMapping messageMapping) {

		this.messageMapping = messageMappingService.findByCode(messageMapping.getCode());
		RequestContext.getCurrentInstance().execute("PF('dlgConfirmChangeStatus').show()");

	}

	public void changeStatusMessageMapping() {

		try {
			/*
			 * messageMapping.setChangeDate(HelperUtils.getCurrentDateTime());
			 * messageMapping.setChangeWho(sessionService.getUser().getUserName(
			 * ));
			 * messageMapping.setStatus(messageMapping.getStatus()==Constants.
			 * STATUS_ACTIVE?Constants.STATUS_INACTIVE:Constants.STATUS_ACTIVE);
			 */
			messageMapping.setStatus(messageMapping.getStatus() == Constants.STATUS_ACTIVE ? Constants.STATUS_INACTIVE
					: Constants.STATUS_ACTIVE);
			messageMappingService.update(messageMapping);
			String headerMessage = "Success";
			String bodyMessage = "Change Status Message Mapping Successfully";

			FacesUtils.setInfoMessageDialog(headerMessage, bodyMessage);
		} catch (Exception e) {
			String headerMessage = "Error";
			String bodyMessage = "Change Status Message Mapping Failed";

			FacesUtils.setErrorMessageDialog(headerMessage, bodyMessage);
		}

	}

	private class MessageMappingDataModel extends LazyDataModel<MessageMapping> {
		private static final long serialVersionUID = 1L;

		public MessageMappingDataModel() {

		}

		@Override
		public List<MessageMapping> load(int startingAt, int maxResult, String sortField, SortOrder sortOrder,
				Map<String, Object> filters) {

			List<MessageMapping> messageMapping = null;

			messageMapping = messageMappingService.selectByCriteria(startingAt, maxResult, code, feature, messageType,
					status);
			Integer jmlAll = messageMappingService.countByCriteria(code, feature, messageType, status);
			setRowCount(jmlAll);
			setPageSize(maxResult);

			return messageMapping;
		}

	}

	public boolean isFmrequest() {
		return fmrequest;
	}

	public void setFmrequest(boolean fmrequest) {
		this.fmrequest = fmrequest;
	}

	public boolean isFmresponse() {
		return fmresponse;
	}

	public void setFmresponse(boolean fmresponse) {
		this.fmresponse = fmresponse;
	}

	public VariableConverter getVariableConverter() {
		return variableConverter;
	}

	public void setVariableConverter(VariableConverter variableConverter) {
		this.variableConverter = variableConverter;
	}

	public String getTrxtype() {
		return trxtype;
	}

	public void setTrxtype(String trxtype) {
		this.trxtype = trxtype;
	}

}
