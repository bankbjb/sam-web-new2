package com.bjb.web.model;

public class DiffItem {
	private String propertyName;
	private Integer propertyIndex;
	private Object baseValue;
	private Object workingValue;
	
	public DiffItem (String propertyName, Object baseValue, Object workingValue) {
		this.propertyName = propertyName;
		this.baseValue = baseValue;
		this.workingValue = workingValue;
	}
	
	public DiffItem (String propertyName, Integer propertyIndex, Object baseValue, Object workingValue) {
		this(propertyName, baseValue, workingValue);
		this.propertyIndex = propertyIndex;
	}
	
	public String getPropertyName() {
		return propertyName;
	}
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
	public Integer getPropertyIndex() {
		return propertyIndex;
	}
	public void setPropertyIndex(Integer propertyIndex) {
		this.propertyIndex = propertyIndex;
	}
	public Object getBaseValue() {
		return baseValue;
	}
	public void setBaseValue(Object baseValue) {
		this.baseValue = baseValue;
	}
	public Object getWorkingValue() {
		return workingValue;
	}
	public void setWorkingValue(Object workingValue) {
		this.workingValue = workingValue;
	}
}
