package com.bjb.web.model;

import javax.servlet.http.HttpSession;

import com.bjb.model.User;

public class UserSession {

	private User user;
	private HttpSession session;
	private String currentTerminal;
	private String userAgent;

	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public String getCurrentTerminal() {
		return currentTerminal;
	}
	public void setCurrentTerminal(String currentTerminal) {
		this.currentTerminal = currentTerminal;
	}
	public String getUserAgent() {
		return userAgent;
	}
	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}
	public HttpSession getSession() {
		return session;
	}
	public void setSession(HttpSession session) {
		this.session = session;
	}

}
