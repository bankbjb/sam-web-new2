package com.bjb.web.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.bjb.model.User;

public class LazyUserDataModel extends LazyDataModel<User> {
	private static final long serialVersionUID = 1L;
	private List<User> users;
	
	public LazyUserDataModel(List<User> users) {
		this.users = users;
	}
	@Override
    public User getRowData(String rowKey) {
        for(User user : users) {
            if(user.getId().equals(rowKey))
                return user;
        }
        return null;
    }
	
	@Override
    public Object getRowKey(User User) {
        return User.getId();
    }

    public List<User> load(int startingAt, int maxPerPage, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
		
        List<User> data = new ArrayList<User>();
        
        //filter
        for(User user : users) {
            boolean match = true;
 
            for(Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
                try {
                    String filterProperty = it.next();
                    String filterValue = (String) filters.get(filterProperty);
                    Object fieldValue = String.valueOf(user.getClass().getField(filterProperty).get(user));
 
                    if(filterValue == null || ((String) fieldValue).startsWith(filterValue.toString())) {
                        match = true;
                    }
                    else {
                        match = false;
                        break;
                    }
                } catch(Exception e) {
                    match = false;
                } 
            }
 
            if(match) {
                data.add(user);
            }
        }

        //rowCount
        int dataSize = data.size();
        this.setRowCount(dataSize);
 
        //paginate
        if(dataSize > maxPerPage) {
            try {
                return data.subList(startingAt, startingAt + maxPerPage);
            }
            catch(IndexOutOfBoundsException e) {
                return data.subList(startingAt, startingAt + (dataSize % maxPerPage));
            }
        }
        else {
            return data;
        }
    }
	
	public List<User> getUsers() {
		return users;
	}
	
	public void setUsers(List<User> users) {
		this.users = users;
	}
	
}
