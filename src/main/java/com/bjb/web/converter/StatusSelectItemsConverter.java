package com.bjb.web.converter;

import java.util.ArrayList;
import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;
import javax.faces.model.SelectItem;

import org.omnifaces.converter.SelectItemsConverter;

@FacesConverter("model.statusSelectItemsConverter")
public class StatusSelectItemsConverter extends SelectItemsConverter {
	
	private static List<SelectItem> options;
	
	static {
		options = new ArrayList<SelectItem>();
		SelectItem selectItem = new SelectItem();
		selectItem.setValue(0);
		selectItem.setLabel(StatusConverter.STATUS_INACTIVE_AS_STRING);
		options.add(selectItem);
		selectItem = new SelectItem();
		selectItem.setValue(1);
		selectItem.setLabel(StatusConverter.STATUS_ACTIVE_AS_STRING);
		options.add(selectItem);
	}
	
	public static List<SelectItem> getOptions() {
		return options;
	}

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value==null) {
			return null;
		} else if ((Integer)value==1) {
			return StatusConverter.STATUS_ACTIVE_AS_STRING;
    	} else if ((Integer)value==0) {
    		return StatusConverter.STATUS_INACTIVE_AS_STRING;
		} else {
			return null;
		}
    }

}
