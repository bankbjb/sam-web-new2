package com.bjb.web.converter;


import java.util.LinkedHashMap;
import java.util.Map;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@FacesConverter("model.statusConverter")
public class StatusConverter implements Converter{

	public static final String STATUS_ACTIVE_AS_STRING= "Enable";
	public static final String STATUS_INACTIVE_AS_STRING = "Disable";
	
	private static Map<String,Object> options;

	static {
		options =  new LinkedHashMap<String, Object>();
		options.put(STATUS_ACTIVE_AS_STRING, new Integer(1));
		options.put(STATUS_INACTIVE_AS_STRING, new Integer(0));
	}
	
	public Map<String,Object> getOptions() {
		return options;
	}

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (STATUS_INACTIVE_AS_STRING.equals(value)) {
			return new Integer(1);
		} else if (STATUS_INACTIVE_AS_STRING.equals(value)) {
			return new Integer(0);
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value==null) {
			return null;
		} else if ((Integer)value==1) {
			return StatusConverter.STATUS_ACTIVE_AS_STRING;
    	} else if ((Integer)value==0) {
    		return StatusConverter.STATUS_INACTIVE_AS_STRING;
		} else {
			return null;
		}
	}

}