package com.bjb.web.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;

import org.omnifaces.converter.SelectItemsConverter;

import com.bjb.model.AuditableObject;

@FacesConverter("auditableSelectItemsConverter")
public class AuditableSelectItemsConverter extends SelectItemsConverter {

   @SuppressWarnings("rawtypes")
   @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        Object id = (value instanceof AuditableObject) ? ((AuditableObject) value).getId() : null;
        return (id != null) ? String.valueOf(id) : null;
    }
}
