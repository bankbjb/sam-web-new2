package com.bjb.web.validator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

@FacesValidator("schedulerRunMaskValidator")
public class SchedulerRunMaskValidator implements Validator {

	@Override
	public void validate(FacesContext arg0, UIComponent arg1, Object value)
			throws ValidatorException {
		
		// TODO Auto-generated method stub
		if (
				(value.toString().equals("Months")) 		||
				(value.toString().equals("Weekly Days")) 	||
				(value.toString().equals("Days"))			||
				(value.toString().equals("Hours"))			||
				(value.toString().equals("Minutes"))
		   ){
			throw new ValidatorException(new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "Validation Error",
					"Invalid Run Mask."));
		}else{
			return;
		}
	}

}
