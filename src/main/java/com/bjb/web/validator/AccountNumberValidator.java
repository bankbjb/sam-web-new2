package com.bjb.web.validator;

import java.util.Map;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.primefaces.validate.ClientValidator;
 
/**
 * Custom JSF Validator for Email input
 */
@FacesValidator("accountNumberValidator")
public class AccountNumberValidator implements Validator, ClientValidator {
 
    private Pattern pattern;
  
    private static final String NUMBER_PATTERN = ".*[^0-9].*"
                    ;
  
    public AccountNumberValidator() {
        pattern = Pattern.compile(NUMBER_PATTERN);
    }
 
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        if(value == null) {
            return;
        }
         
        if(pattern.matcher(value.toString()).matches()) {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Validation Error", 
                        "Invalid number."));
        }
        
        if(value.toString().length() != 13) {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Validation Error", 
                        "Length must be 13."));
        }
        
    }
    
    public boolean basicValidate(String value){
         
        if(pattern.matcher(value.toString()).matches()) {
            return false;
        }
        return true;
    }
 
    public Map<String, Object> getMetadata() {
        return null;
    }
 
    public String getValidatorId() {
        return "emailValidator";
    }
     
}