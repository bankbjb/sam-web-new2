package com.bjb.web.validator;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

@FacesValidator("schedulerDatePastValidator")
public class SchedulerDatePastValidator implements Validator {


	@Override
	public void validate(FacesContext arg0, UIComponent arg1, Object value)
			throws ValidatorException {
		Date input;
		try {
			input = new SimpleDateFormat("yyyy/MM/dd").parse(value.toString());
		
			Date current = new Date();
			Boolean isAfter = input.after(current);
			Boolean isNow = (value.equals(new SimpleDateFormat("yyyy/MM/dd").format(current)));
			
			// TODO Auto-generated method stub
			if ((value == null) || (value == "")) {
				return;
			}

			if (isAfter || isNow) {
				//System.out.println("Valid date");
			}else{
				throw new ValidatorException(new FacesMessage(
						FacesMessage.SEVERITY_ERROR, "Validation Error",
						"Past date."));
			}
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
