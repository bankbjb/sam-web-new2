package com.bjb.web.validator;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.primefaces.validate.ClientValidator;
 
/**
 * Custom JSF Validator for Email input
 */
@FacesValidator("ipAddressValidator")
public class IpAddressValidator implements Validator, ClientValidator {
	
 
    private Pattern pattern;
  
    private static final String EMAIL_PATTERN = "^(?:[0-9]{1,3}\\.){3}[0-9]{1,3}$";
  
    public IpAddressValidator() {
        pattern = Pattern.compile(EMAIL_PATTERN);
    }
 
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        if((value == null) || (value.toString().isEmpty())) {
            return;
        }
         
        if(!pattern.matcher(value.toString()).matches()) {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Validation Error", 
                        "Invalid IP Address."));
        }
    }
    
    public boolean basicvalidate(String value){
    	Matcher matcher;
    	matcher = pattern.matcher(value);
		return matcher.matches();
    	
    }
 
    public Map<String, Object> getMetadata() {
        return null;
    }
 
    public String getValidatorId() {
        return "emailValidator";
    }
     
}