package com.bjb.web.validator;

import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

@FacesValidator("schedulerDateValidator")
public class SchedulerDateValidator implements Validator {

	private Pattern pattern;

	private static final String DATE_PATTERN = "((19|20)\\d\\d)/(0?[1-9]|1[012])/(0?[1-9]|[12][0-9]|3[01])";

	public SchedulerDateValidator() {
		pattern = Pattern.compile(DATE_PATTERN);
	}

	@Override
	public void validate(FacesContext arg0, UIComponent arg1, Object value)
			throws ValidatorException {
		// TODO Auto-generated method stub
		if ((value == null) || (value == "")) {
			return;
		}
		// dd/mm/yyyy
		if (!pattern.matcher(value.toString()).matches()) {
			throw new ValidatorException(new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "Validation Error",
					"Invalid date."));
		}
	}

}
