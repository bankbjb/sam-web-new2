package com.bjb.support;

import java.util.HashMap;
import java.util.Map;

import org.springframework.expression.EvaluationContext;
import org.springframework.expression.Expression;
import org.springframework.expression.common.TemplateParserContext;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.stereotype.Service;

@Service
public class TemplateService  {
	Expression subjectExpression;
	Expression welcomeExpression;
	private Map<String, Expression> cacheExpressions = new HashMap<String, Expression>();
	
	public String evaluate(String template, Map<String, Object> data) {
		Expression templateExpression = cacheExpressions.get(template);
		if (templateExpression == null) {
			SpelExpressionParser parser = new SpelExpressionParser();
			templateExpression = parser.parseExpression(template, new TemplateParserContext());
			cacheExpressions.put(template, templateExpression);
		}
		EvaluationContext context = new StandardEvaluationContext();
		for (String key:data.keySet()) {
			context.setVariable(key, data.get(key));
		}
		return templateExpression.getValue(context, String.class);
	}


	public String evaluate(String template, Object data) {
		Expression templateExpression = cacheExpressions.get(template);
		if (templateExpression == null) {
			SpelExpressionParser parser = new SpelExpressionParser();
			templateExpression = parser.parseExpression(template, new TemplateParserContext());
			cacheExpressions.put(template, templateExpression);
		}
		EvaluationContext context = new StandardEvaluationContext(data);
		return templateExpression.getValue(context, String.class);
	}
}
