package com.bjb.support;

import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class MailingSupport {

	private JavaMailSender mailSender;
	
	@Autowired
	public void setMailSender(JavaMailSender mailSender) {
		this.mailSender = mailSender;
	}

	@Async
 	public void send(final SimpleMailMessage message) {
		mailSender.send(message);
	}

	@Async
 	public void send(final SimpleMailMessage[] messages) {
		mailSender.send(messages);
	}

	@Async
 	public void send(final MimeMessage mimeMessage) {
		mailSender.send(mimeMessage);
	}

	@Async
 	public void send(final MimeMessage[] mimeMessages) {
		mailSender.send(mimeMessages);
	}

	@Async
	public void send(final MimeMessagePreparator messagePreparator) {
		mailSender.send(messagePreparator);
	}

	@Async
	public void send(final MimeMessagePreparator[] messagePreparators) {
		mailSender.send(messagePreparators);
	}

}
