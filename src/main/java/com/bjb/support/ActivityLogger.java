package com.bjb.support;

public interface ActivityLogger {
	public void log(String activityName, Object oldObject, Object newObject, Object[] args);
}
