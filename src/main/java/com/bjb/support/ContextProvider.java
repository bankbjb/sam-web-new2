package com.bjb.support;

import com.bjb.model.User;

public interface ContextProvider {

	public User getUser();

}
