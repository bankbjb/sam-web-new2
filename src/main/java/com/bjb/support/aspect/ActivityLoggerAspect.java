package com.bjb.support.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;

import com.bjb.support.ActivityLogger;
import com.bjb.support.annotation.Recorded;
import com.bjb.support.annotation.RecordingStatus;

public class ActivityLoggerAspect {

	private ActivityLogger activityLogger;

	public void setActivityLogger(ActivityLogger activityLogger) {
		this.activityLogger = activityLogger;
	}
	
	public Object record(ProceedingJoinPoint pjp, Recorded recorded) {
		Object[] args = pjp.getArgs();
		
		Object oldObject = null;
		Object newObject = null;
		Object changedObject = null;
		Object removedObject = null;
		Object findObject = null;
		
		if (args.length == 1) {
			if (recorded.value()==RecordingStatus.CREATE) newObject = args[0];
			if (recorded.value()==RecordingStatus.MODIFY) changedObject = args[0];
			if (recorded.value()==RecordingStatus.REMOVE) removedObject = args[0];			
		} else if (args.length > 0) {
			oldObject = (recorded.oldObject()>0 && recorded.oldObject()<=args.length) ? args[recorded.oldObject()-1]:null;
			newObject = (recorded.newObject()>0 && recorded.newObject()<=args.length) ? args[recorded.newObject()-1]:null;
			changedObject = (recorded.changedObject()>0 && recorded.changedObject()<=args.length) ? args[recorded.changedObject()-1]:null;
			removedObject = (recorded.removedObject()>0 && recorded.removedObject()<=args.length) ? args[recorded.removedObject()-1]:null;
		}
        Object result = null;
		try {
			result = pjp.proceed();
			if ( recorded.oldObject() == Recorded.AS_RETURN_ARGUMENT ) oldObject = result;
			if ( recorded.newObject() == Recorded.AS_RETURN_ARGUMENT ) newObject = result;
			if ( recorded.changedObject() == Recorded.AS_RETURN_ARGUMENT ) changedObject = result;
			if ( recorded.removedObject() == Recorded.AS_RETURN_ARGUMENT ) removedObject = result;
			if ( recorded.findObject() == Recorded.AS_RETURN_ARGUMENT ) findObject = result;
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		MethodSignature signature = (MethodSignature) pjp.getSignature();
		String className = pjp.getTarget().getClass().getSimpleName();
		String methodName = signature.getMethod().getName();

		activityLogger.log(className + "." + methodName, 
				(oldObject==null)?((removedObject==null)?findObject:removedObject):oldObject, 
				(newObject==null)?changedObject:newObject, 
				args);
        
		return result;
	}

}