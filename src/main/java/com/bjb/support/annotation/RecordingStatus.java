package com.bjb.support.annotation;

public enum RecordingStatus {
	IGNORE,
	CREATE,
	MODIFY,
	REMOVE
}
