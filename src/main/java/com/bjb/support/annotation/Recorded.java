package com.bjb.support.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Recorded {

	public static int NONE = -1;
	public static int AS_RETURN_ARGUMENT = 0;
	public static int AS_FIRST_ARGUMENT = 1;
	public static int AS_SECOND_ARGUMENT = 2;
	public static int AS_THIRD_ARGUMENT = 3;
	
	RecordingStatus value() default RecordingStatus.IGNORE;
	
    int oldObject() default NONE;
    int newObject() default NONE;
    int changedObject() default NONE;
    int removedObject() default NONE;
    int findObject() default NONE;

}
