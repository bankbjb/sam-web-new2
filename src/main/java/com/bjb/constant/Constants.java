package com.bjb.constant;


public final class Constants {

	public static final String UIM_ID_APLIKASI					= "UIM_APP_ID";
	
	public static final Integer STATUS_ACTIVE 	= 1;
	public static final Integer STATUS_INACTIVE = 0;

	public static final Integer RESULT_OK = 1;
	public static final Integer RESULT_ERROR = 0;

	public static final Integer ACCESS_LEVEL_AGGREGATOR = 10;
	public static final Integer ACCESS_LEVEL_USER 		= 50;
	public static final Integer ACCESS_LEVEL_ADMIN 		= 100;
	
	public static final String FORMAT_DATE = "MM-dd-yyyy";
	public static final String FORMAT_DATE_SHORT = "yyyyMMdd";
	public static final String FORMAT_DATETIME = "MM-dd-yyyy HH:mm:ss";
	public static final String FORMAT_TIME = "HH:mm:ss";
	public static final String FORMAT_TIME_SHORT = "HHmmss";
	public static final String FORMAT_ID = "yyyyMMdd-HHmmss-sss";
	
	public static final String APPROVAL_STATUS_PENDING_CREATE 	= "Pending - Create";
	public static final String APPROVAL_STATUS_PENDING_EDIT 	= "Pending - Edit";
	public static final String APPROVAL_STATUS_PENDING_DELETE 	= "Pending - Delete";
	public static final String APPROVAL_STATUS_APPROVED_CREATE	= "Approved - Create";
	public static final String APPROVAL_STATUS_APPROVED_EDIT	= "Approved - Edit";
	public static final String APPROVAL_STATUS_REJECTED_CREATE 	= "Rejected - Create";
	public static final String APPROVAL_STATUS_REJECTED_EDIT 	= "Rejected - Edit";
	public static final String APPROVAL_STATUS_REJECTED_DELETE 	= "Rejected - Delete";
	
	public static final String NO_APPROVAL_STATUS 				=  "No Approval Needed";
	
	public static final String APPROVAL_ACTION_CREATE			= "Create";
	public static final String APPROVAL_ACTION_EDIT				= "Edit";
	public static final String APPROVAL_ACTION_DELETE			= "Delete";
	
	public static final String APPROVAL_PARAMETER_USER			= "UserAggregator";
	public static final String APPROVAL_PARAMETER_ROLE			= "Role";
	public static final String APPROVAL_PARAMETER_SCHEDULER		= "Scheduler";
	public static final String APPROVAL_PARAMETER_SPARAM		= "SystemParameter";
	public static final String APPROVAL_PARAMETER_BRANCH		= "Branch";
	public static final String APPROVAL_PARAMETER_HOLIDAY		= "Holiday";
	
	public static final String APPROVAL_PARAMETER_AGGREGATOR		= "Aggregator";
	public static final String APPROVAL_PARAMETER_FEATURE			= "Feature";
	
	public final static String SCHEDULER_RUN_ONCE 				= "One Time Task";
	public final static String SCHEDULER_REPEATING				= "Repeat Interval";
	public final static String SCHEDULER_COMPLEX_REPEATING 		= "Repeat Time";

	public final static String SCHEDULER_STATUS_SHUTDOWN		= "Shutdown";
	public final static String SCHEDULER_STATUS_STARTED 		= "Started";
	public final static String SCHEDULER_STATUS_STANDBY 		= "Standby";
	
	public final static String PAYMENT_RESPONSE_CODE_SUCCESS	= "00";
	public final static String PAYMENT_RESPONSE_MESSAGE_SUCCESS	= "Success";
	public final static String PAYMENT_RESPONSE_CODE_REJECT		= "B5";
	public final static String PAYMENT_RESPONSE_MESSAGE_REJECT 	= "Kode billing sudah dibayar";
	public final static String PAYMENT_RESPONSE_CODE_TIMEOUT	= "TO";
	public final static String PAYMENT_RESPONSE_CODE_TIMEOUT_MPN= "90";
	public final static String PAYMENT_RESPONSE_CODE_M3			= "M3";
	public final static String PAYMENT_RESPONSE_CODE_M2			= "M2";
	public final static String PAYMENT_RESPONSE_CODE_M1			= "M1";
	//CR Issue terkait RC92
	public final static String PAYMENT_RESPONSE_CODE_EC_02		= "02";
	public final static String PAYMENT_RESPONSE_CODE_EC_27		= "27";
	public final static String PAYMENT_RESPONSE_CODE_EC_88		= "88";
	public final static String TRX_RC_ALLOWED					= "TRX_RC_ALLOWED";
	
	public final static String PAYMENT_RESPONSE_MESSAGE_TIMEOUT	= "Timeout";
	public final static String PAYMENT_RESPONSE_MESSAGE_TIMEOUT_MDW	= "No response from middleware";
	public final static String PAYMENT_REKENING_GL				= "REKENING_TITIPAN_TRANSAKSI";
	public final static String PAYMENT_KODE_BANK_PERSEPSI		= "KODE_BANK_PERSEPSI";
	public final static String PAYMENT_CHANNEL_TYPE				= "PAYMENT_CHANNEL_TYPE";
	public final static String PAYMENT_CURRENCY_IDR				= "IDR";
	public final static String PAYMENT_ROLE_ID_SUPERVISOR		= "PAYMENT_ROLE_ID_SUPERVISOR";
	public final static String PAYMENT_REPORT_DIR				= "PAYMENT_REPORT_DIR";
	public final static String PAYMENT_REPORT_DB_URL			= "PAYMENT_REPORT_DB_URL";
	public final static String PAYMENT_REPORT_DB_USERNAME		= "PAYMENT_REPORT_DB_USERNAME";
	public final static String PAYMENT_REPORT_DB_PASSWORD		= "PAYMENT_REPORT_DB_PASSWORD";
	
	public final static String TRANSACTION_EVENT_PAYMENT		= "PAYMENT";	
	public final static String TRANSACTION_EVENT_INQUIRY		= "INQUIRY";	
	public final static String TRANSACTION_EVENT_REINQUIRY		= "REINQUIRY";
	
	public final static String TRANSACTION_FEE_AMOUNT			= "TRANSACTION_FEE_AMOUNT";

	public final static String PAYMENT_REQUEST_TYPE				= "PAYMENT_REQUEST_TYPE";
	public final static String INQUIRY_REQUEST_TYPE				= "INQUIRY_REQUEST_TYPE";

	public final static String REPORT_BPN_DJP_INQUIRY			= "BPN_DJP_Inquiry";
	public final static String REPORT_BPN_DJA_INQUIRY			= "BPN_DJA_Inquiry";
	public final static String REPORT_BPN_DJBC_INQUIRY			= "BPN_DJBC_Inquiry";
	
	public final static String REPORT_BPN_DJP					= "BPN_DJP";
	public final static String REPORT_BPN_DJA					= "BPN_DJA";
	public final static String REPORT_BPN_DJBC					= "BPN_DJBC";
	
	public final static String REPORT_BPN_DJP_SEMENTARA			= "BPN_DJP_Sementara";
	public final static String REPORT_BPN_DJA_SEMENTARA			= "BPN_DJA_Sementara";
	public final static String REPORT_BPN_DJBC_SEMENTARA		= "BPN_DJBC_Sementara";
	
	public final static String BPS_TARGET_DIRECTORY				= "BPS Target Directory";
	
	public final static String APPROVAL_STATUS_CODE_SUCCESS	    = "0";
	public final static String APPROVAL_STATUS_CODE_FAILED	    = "1";
	
	public final static String DEFAULT_ROLE_ID_UPLOAD_USER		= "DEFAULT_ROLE_ID_UPLOAD_USER";

	public final static String WEB_SERVICE_IP				    = "WEB_SERVICE_IP";
	public final static String WEB_SERVICE_PORT				    = "WEB_SERVICE_PORT";
	public final static String WEB_SERVICE_USERNAME			    = "WEB_SERVICE_USERNAME";
	public final static String WEB_SERVICE_PASSWORD			    = "WEB_SERVICE_PASSWORD";
	
	public final static String UIM_HOST							= "UIM_HOST";
	public final static String UIM_PORT							= "UIM_PORT";
	
	public final static String WEB_SERVICE_INTERNAL_IP			= "WEB_SERVICE_INTERNAL_IP";
	public final static String WEB_SERVICE_INTERNAL_PORT		= "WEB_SERVICE_INTERNAL_PORT";
		
	public final static String KODE_CABANG_KANTOR_PUSAT			= "KANTOR_PUSAT";
	
	public final static int ADD_MINUTES_SCHEDULER				= 3;
}
