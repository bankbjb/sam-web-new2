package com.bjb.constant;

public class LoginConstants {

	public static final int SUCCESS = 1;
	public static final int ERROR_USER_LOCKED = 2;
	public static final int ERROR_USER_NOT_FOUND = 3;
	public static final int ERROR_USER_HAS_BEEN_DELETED = 4;
	public static final int ERROR_USER_NOT_ACTIVE = 5;
	public static final int ERROR_PASSWORD_WRONG = 6;
	public static final int ERROR_PASSWORD_OLD_WRONG = 6;
	public static final int ERROR_PASSWORD_EXPIRED = 7;
	public static final int ERROR_PASSWORD_HAS_BEEN_USED = 8;
	public static final int ERROR_ACTION_NOT_APPLICABLE = 96;
	public static final int ERROR_APPLICATION_UNREGISTERED = 97;
	public static final int ERROR_CONNECTION = 98;
	public static final int ERROR_UNKNOWN = 99;
}
