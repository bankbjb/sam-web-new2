package com.bjb.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

public class HelperUtils {
private static String salt = "Random$SaltValue#WithSpecialCharacters12@$@4&#%^$*";
	
	public static String getSalt() {
		return salt;
	}
	
	@SuppressWarnings("static-access")
	public void setSalt(String salt) {
		this.salt = salt;
	}
	
	public static String generateUUID() {
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		Date date = new Date();
		String now = dateFormat.format(date);
		
		UUID idRandom = UUID.randomUUID();
		String id = idRandom.toString()+now;
		////System.out.println(id);
		return id;
	}
	
	public static boolean strNotNullNotEmpty(String str){
		boolean returnVal = false;
			if(str != null && !str.equals("")){
				returnVal = true;
			}
		return returnVal;
	}
	
	public static String md5(String input){
		String md5 = null;
		if(input == null) return null;
		try {
			MessageDigest digest = MessageDigest.getInstance("MD5");
			digest.update(input.getBytes(), 0, input.length());
			md5 = new BigInteger(1, digest.digest()).toString(16);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return md5;
	}
	
	public static Date getCurrentDateTime(){
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		try {
			String currentDateTimeString = dateFormat.format(date);
			return dateFormat.parse(currentDateTimeString);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}
	
	public static String getCurrentDateString(){
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		return dateFormat.format(date);
	}
	
	public static String generateUniqueString(){
		String uuid = generateUUID();
		long l = ByteBuffer.wrap(uuid.toString().getBytes()).getLong();
        return Long.toString(l, Character.MAX_RADIX);
	}
	
		
	public static void main(String[] args) {
		
	}
	
	public static byte[] serialize(Object object) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);
        oos.writeObject(object);
        oos.close();
        return baos.toByteArray();
    }
	
	public static Object deserialize(InputStream stream) throws IOException, ClassNotFoundException {

        ObjectInputStream ois = new ObjectInputStream(stream);
        try {
            return ois.readObject();
        } finally {
            ois.close();
        }
    }
	
	public static String arrayToDelimitedString(String[] s, String d){
		String r = "";
		for (int i =  0; i < s.length ; i++){
			if (i != s.length - 1){
				r = r + s[i] + d;
			}else{
				r = r + s[i];
			}
		}
		return r;
	}
	
	public static String[] delimitedStringToArray(String s, String d){
		String[] r = null;
		r = s.split(d);
		return r;
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T clone(T object) {
		try {
			byte[] source = HelperUtils.serialize(object);
			return (T) HelperUtils.deserialize(new ByteArrayInputStream(source));
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		}
	}
	

}
