package com.bjb.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;

public class CurrencyUtils {
	
	public static String format(String strAngka){
		DecimalFormat myFormatter = new DecimalFormat("###,###");
		String output = myFormatter.format(new BigDecimal(strAngka));
		return output;
	}
	
	public static String convert(String strAngka) {
		String strJmlHuruf = null;
		String urai = null;
		String strTot = null;
		String Bil1 = "";
		String Bil2;
		int z = 0;
		int x = 0;
		int Y = 0;
		int angka = 0;
		boolean flag = false;
		//	inputan strAngka;
		strJmlHuruf = strAngka.trim();
		// //System.out.println(strJmlHuruf.length());
		// //System.out.println("strangka:" + strAngka);
		urai = "";
		// x=1;
		while (x < strJmlHuruf.length()) {
			strTot = strJmlHuruf.trim().substring(x, x + 1);
			angka = Integer.parseInt(strTot);
			Y = Y + angka;
			z = strJmlHuruf.length() - x;

			switch (Integer.parseInt(strTot)) {
			case 0:
				Bil1 = "";
				break;
			case 1:
				if (z == 1 || z == 7 || z == 10 || z == 13) {
					Bil1 = "satu ";
				} else if (z == 4) {
					if (strJmlHuruf.length() > z) // ini perbaikannya
						Bil1 = "satu ";
					else {
						if (x == 1) {
							Bil1 = "se";
						} else if (x == 0) {
							Bil1 = "se";
						}
					}
				}

				else if (z == 2 || z == 5 || z == 8 || z == 11 || z == 14
						&& !flag) {
					x++;
					strTot = strJmlHuruf.substring(x, x + 1);
					z = strJmlHuruf.length() - x;
					Bil2 = "";

					strTot = strJmlHuruf.trim().substring(x, x + 1);
					// z = strJmlHuruf.length() - x + 1;

					switch (Integer.parseInt(strTot)) {
					case 0:
						Bil1 = "sepuluh ";
						flag = true;
						break;
					case 1:
						Bil1 = "sebelas ";
						flag = true;
						break;
					case 2:
						Bil1 = "dua belas ";
						flag = true;
						break;
					case 3:
						Bil1 = "tiga belas ";
						flag = true;
						break;
					case 4:
						Bil1 = "empat belas ";
						flag = true;
						break;
					case 5:
						Bil1 = "lima belas ";
						flag = true;
						break;
					case 6:
						Bil1 = "enam belas ";
						flag = true;
						break;
					case 7:
						Bil1 = "tujuh belas ";
						flag = true;
						break;
					case 8:
						Bil1 = "delapan belas ";
						flag = true;
						break;
					case 9:
						Bil1 = "sembilan belas ";
						flag = true;
						break;
					default:
						break;
					}
				}

				else {
					Bil1 = "se";
					break;
				}

				break;
			case 2:
				Bil1 = "dua ";
				break;
			case 3:
				Bil1 = "tiga ";
				break;
			case 4:
				Bil1 = "empat ";
				break;
			case 5:
				Bil1 = "lima ";
				break;
			case 6:
				Bil1 = "enam ";
				break;
			case 7:
				Bil1 = "tujuh ";
				break;
			case 8:
				Bil1 = "delapan ";
				break;
			case 9:
				Bil1 = "sembilan ";
				break;
			default:

				// Bil1 = "";
				break;
			}
			if ((Integer.parseInt(strTot) > 0)) {
				if (z == 2 || z == 5 || z == 8 || z == 11 || z == 14 && !flag) {
					Bil2 = "puluh ";
				} else if (z == 3 || z == 6 || z == 9 || z == 12 || z == 15) {
					Bil2 = "ratus ";
					/*
					 * else if (z==1 && !flag) { Bil2 = "se "; }
					 */
				} else {
					Bil2 = "";
				}
			} else {
				Bil2 = "";
			}
			if (Y > 0) {
				switch (z) {
				case 4:
					Bil2 = Bil2.trim() + "ribu ";
					Y = 0;
					break;
				case 7:
					Bil2 = Bil2 + "juta ";
					Y = 0;
					break;
				case 10:
					Bil2 = Bil2 + "milyar ";
					Y = 0;
					break;
				case 13:
					Bil2 = Bil2 + "trilyun ";
					Y = 0;
					break;
				/*
				 * default : Bil2 = " "; break;
				 */
				}
			}
			urai = urai + Bil1 + Bil2;
			x++;
		}
		return urai;
	}
}
