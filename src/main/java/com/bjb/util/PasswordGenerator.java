package com.bjb.util;

import java.math.BigInteger;
import java.security.SecureRandom;

public class PasswordGenerator {
	private static SecureRandom random = new SecureRandom();
	
	public static String generatePassword(){
		return nextSessionId();
	}
	
	private static String nextSessionId() {
	    return new BigInteger(130, random).toString(32);
	}
}