package com.bjb.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.bjb.model.CollectingAgent;

public interface CollectingAgentService extends OperationalService<CollectingAgent, BigDecimal> {
	public List<CollectingAgent> selectByCriteria(int startingAt, int maxResult, String code, String name, Date createDate, Integer maxAgent, Integer maxAmount, List<Integer> status);
	public int countByCriteria(String code, String name, Date createDate, Integer maxAgent, Integer maxAmount, List<Integer> status);
	public CollectingAgent findByName(String name);
	public List<CollectingAgent> selectValidCollectingAgent();
}
