package com.bjb.service;

import java.math.BigDecimal;
import java.util.List;

import com.bjb.model.Feature;

public interface FeatureService extends OperationalService<Feature, BigDecimal> {
	public List<Feature> selectByCriteria(int startingAt, int maxResult, String code, String name, String description, String commisionAccountNo, List<Integer> status, List<String> approvalStatus);
	public int countByCriteria(String code, String name, String description, String commisionAccountNo, List<Integer> status, List<String> approvalStatus);
	public Feature findByName(String name);
	public List<Feature> selectValidFeature();
}
