package com.bjb.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.bjb.model.TransactionLog;

public interface TransactionLogService extends CommonService<TransactionLog, BigDecimal> {
	public List<TransactionLog> selectByCriteria(int startingAt, int maxResult, String aggregatorCode,  Date startDate, Date endDate, TransactionLog transactionLog);
	public int countByCriteria(String aggregatorCode, Date startDate, Date endDate, TransactionLog transactionLog);
	public TransactionLog findByName(String name);
	public List<TransactionLog> selectValidTransactionLog();
	public List<Map<Object, Number>> selectTopFeeBasedIncome(Integer maxResults);
	public List<Map<Object, Number>> selectTopBestSeller(Integer maxResults);

}
