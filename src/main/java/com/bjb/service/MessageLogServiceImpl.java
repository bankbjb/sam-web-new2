package com.bjb.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bjb.dao.MessageLogDao;
import com.bjb.model.MessageLog;

@Service("messageLogService")
@Transactional(readOnly=true)
public class MessageLogServiceImpl extends CommonServiceImpl<MessageLog, BigDecimal> implements MessageLogService {

	private MessageLogDao messageLogDao;
	
	@Override
	public List<MessageLog> selectByCriteria(int startingAt, int maxResult, Date startDate, Date endDate, MessageLog messageLog) {
		return messageLogDao.selectByCriteria(startingAt, maxResult, startDate, endDate, messageLog);
	}
	
	@Override
	public MessageLog findByName(String name) {
		return messageLogDao.findByName(name);
	}

	@Override
	public int countByCriteria(Date startDate, Date endDate, MessageLog messageLog) {
		return messageLogDao.countByCriteria(startDate, endDate, messageLog);
	}

	@Override
	public List<MessageLog> selectValidMessageLog() {
		return messageLogDao.selectValidMessageLog();
	}
	
	public MessageLogDao getMessageLogDao() {
		return messageLogDao;
	}

	@Autowired
	public void setMessageLogDao(MessageLogDao messageLogDao) {
		this.messageLogDao = messageLogDao;
		setGenericDao(messageLogDao);
	}
	
}
