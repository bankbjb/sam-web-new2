package com.bjb.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.bjb.model.ActivityLog;

public interface ActivityLogService extends CommonService<ActivityLog, BigDecimal> {
	public List<ActivityLog> selectByCriteria(int startingAt, int maxResult, BigDecimal userId, BigDecimal activityId, Date startDate, Date endDate, ActivityLog activityLog, List<Integer> status);
	public int countByCriteria(BigDecimal userId, BigDecimal activityId, Date startDate, Date endDate, ActivityLog activityLog, List<Integer> status);
}
