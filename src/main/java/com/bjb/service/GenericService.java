package com.bjb.service;

import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Example;

public interface GenericService<E, K> {
    public K generateId() ;
    public void insert(E entity) ;
    public void insertOrUpdate(E entity) ;
    public void update(E entity) ;
    public void delete(E entity);
    public E deleteByKey(K key);
    public E deleteByCode(String code);
    public E find(K key);
    public E findByCode(String code);
    public List<E> select();     
    public List<E> select(int startingAt, int maxResults);     
    public List<E> selectByCriteria(Criterion... criterion);  
    public List<E> selectByCriteria(int startingAt, int maxResults, Criterion... criterion);  
    public List<E> selectByExample(Example example);  
    public List<E> selectByExample(int startingAt, int maxResults, Example example);  
	public int count();
	public int countByCriteria(Criterion... criterion);
	public int countByExample(Example example);
}
