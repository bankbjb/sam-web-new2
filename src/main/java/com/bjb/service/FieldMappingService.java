package com.bjb.service;

import java.math.BigDecimal;
import java.util.List;

import com.bjb.model.FieldMapping;

public interface FieldMappingService extends OperationalService<FieldMapping, BigDecimal> {
	
	public List<FieldMapping> findByMessageMappingId(BigDecimal messageMappingId) ;
}
