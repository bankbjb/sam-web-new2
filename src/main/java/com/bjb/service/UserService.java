package com.bjb.service;

import java.math.BigDecimal;
import java.util.List;

import com.bjb.model.User;

public interface UserService extends OperationalService<User, BigDecimal> {
	public List<User> selectByCriteria(int startingAt, int maxResult, String userName, String fullName, String branchId, List<Integer> status, List<String> approvalStatus);	
	public int countByCriteria(String userName, String fullName, String branchId, List<Integer> status, List<String> approvalStatus);
	public User findByUserName(String userName);
	public User findValidUserByUsername(String userName);
	public int countUserWithRole(Integer roleId);

	public Object[] loginUserBank(String userName, String password);
	public Object[] loginUserAggregator(String userName, String password);

	public int changePasswordUserBank(String userName, String password, String newPassword, boolean force);
	public int changePasswordUserAggregator(String userName, String password, String newPassword, boolean force);

	public int forgetPasswordUserAggregator(String userName);
}
