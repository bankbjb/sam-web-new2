package com.bjb.service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bjb.constant.Constants;
import com.bjb.dao.AggregatorDao;
import com.bjb.dao.BranchDao;
import com.bjb.dao.RoleDao;
import com.bjb.dao.UserDao;
import com.bjb.model.Aggregator;
import com.bjb.model.ApprovalPending;
import com.bjb.model.Auditable;
import com.bjb.model.Role;
import com.bjb.model.UserAggregator;
import com.bjb.support.MailingSupport;
import com.bjb.support.TemplateService;
import com.bjb.util.EncryptUtils;
import com.bjb.util.PasswordUtils;

@Service("aggregatorService")
public class AggregatorServiceImpl extends OperationalServiceImpl<Aggregator, BigDecimal> implements AggregatorService {

	private AggregatorDao aggregatorDao;
	private UserDao userDao;
	private BranchDao branchDao;
	private RoleDao roleDao;
	private MailingSupport mailer;
	private TemplateService elService;
	
	@Autowired
	public void setAggregatorDao(AggregatorDao aggregatorDao) {
		this.aggregatorDao = aggregatorDao;
		setGenericDao(aggregatorDao);
	}
	
	@Autowired
	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	@Autowired
	public void setBranchDao(BranchDao branchDao) {
		this.branchDao = branchDao;
	}

	@Autowired
	public void setRoleDao(RoleDao roleDao) {
		this.roleDao = roleDao;
	}
	
	@Autowired
	public void setMailingSupport(MailingSupport mailingSupport) {
		this.mailer = mailingSupport;
	}


	@Autowired
	public void setElService(TemplateService elService) {
		this.elService = elService;
	}

	@Override
	@Transactional(readOnly = true)
	public List<Aggregator> selectByCriteria(int startingAt, int maxResult, String code, String name, String address, String phone, String pic, String emailPic, List<Integer> status, List<String> approvalStatus) {
		return aggregatorDao.selectByCriteria(startingAt, maxResult, code, name, address, phone, pic, emailPic, status, approvalStatus);
	}
	
	@Override
	@Transactional(readOnly = true)
	public Aggregator findByName(String name) {
		return aggregatorDao.findByName(name);
	}

	@Override
	@Transactional(readOnly = true)
	public int countByCriteria(String code, String name, String address, String phone, String pic, String emailPic,  List<Integer> status, List<String> approvalStatus) {
		return aggregatorDao.countByCriteria(code, name, address, phone, pic, emailPic, status, approvalStatus);
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<Aggregator> selectValidAggregator() {
		return aggregatorDao.selectValidAggregator();
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void postApproval(Auditable<BigDecimal> auditObject, ApprovalPending approvalPending) {
		Aggregator aggregator = (Aggregator) auditObject;
		if (approvalPending.getAction().equals(Constants.APPROVAL_ACTION_DELETE)) {
			//
		} else if (approvalPending.getAction().equals(Constants.APPROVAL_ACTION_CREATE)) {
			UserAggregator user = new UserAggregator();
			user.setId(aggregator.getId());
			user.setAggregatorId(aggregator.getId());
			user.setCode(aggregator.getCode()+"admin");
			user.setApprovalBy(aggregator.getApprovalBy());
			user.setApprovalWho(aggregator.getApprovalWho());
			user.setApprovalDate(aggregator.getApprovalDate());
			user.setApprovalStatus(Constants.APPROVAL_STATUS_APPROVED_CREATE);
			user.setBranch(branchDao.findByCode("9999"));
			user.setContactNo(aggregator.getPhone());
			user.setCreateBy(aggregator.getApprovalBy());
			user.setCreateWho(aggregator.getApprovalWho());
			user.setCreateDate(aggregator.getApprovalDate());
			user.setEmail(aggregator.getEmailPic());
			user.setFullName(aggregator.getPic());
			String plainPassword = "P@ssw0rd";
			String password = PasswordUtils.toSHA256(plainPassword);
			try {
				Integer defaultLen = Integer.parseInt(getSystemParameter("DEFAULT_PASSWORD_LENGTH", "6"));
				Integer noOfCAPSAlpha = Integer.parseInt(getSystemParameter("PASSWORD_MIN_CAPS", "1"));
				Integer noOfDigits = Integer.parseInt(getSystemParameter("PASSWORD_MIN_DIGIT_", "1"));
				Integer noOfSplChars = Integer.parseInt(getSystemParameter("PASSWORD_MIN_SPECIAL", "0"));
				plainPassword = PasswordUtils.generate(defaultLen, defaultLen, noOfCAPSAlpha, noOfDigits, noOfSplChars);
				password = PasswordUtils.toSHA256(plainPassword);
			} catch (IllegalArgumentException e) {
				// do nothing, we have default password already
			}
			user.setPlainPassword(EncryptUtils.encryptEncode(plainPassword));
			user.setPassword(password);
			user.setPhone(aggregator.getPhone());
			HashSet<Role> roles = new HashSet<Role>();
			roles.addAll(roleDao.getAllValidRoleByAccessLevel(Constants.ACCESS_LEVEL_AGGREGATOR));
			user.setRoles(roles);
			user.setStatus(Constants.STATUS_ACTIVE);
			user.setUserName(user.getCode());
			userDao.insert(user);
			aggregator.setUser(user);
			sendEmail(user, "WELCOME_SUBJECT", "WELCOME_TEXT");	
			
		} else if (approvalPending.getAction().equals(Constants.APPROVAL_ACTION_EDIT)) {
		}
	}

	@Override
	public void sendForgetPasswordEmail(UserAggregator user) {
		sendEmail(user, "FORGET_PASSWORD_SUBJECT", "FORGET_PASSWORD_TEXT");	
	}
	 
	private void sendEmail(UserAggregator user, String subjectParamName, String bodyParamName) {
	    String templateBody = getSystemParameter(bodyParamName, "Welcome #{#user.fullName}, User Id #{#user.userName} Password #{#user.assword} URL #{#WELCOME_URL}");
        String templateSubject = getSystemParameter(subjectParamName, "Welcome to BJB SAM !");
        HashMap<String, Object> data = new HashMap<String, Object>();
        data.put("user", user);
        data.put("WELCOME_URL", getSystemParameter("THIS_URL", ""));
        data.put("PLAIN_PASSWORD", EncryptUtils.decodeDecrypt(user.getPlainPassword()));
		SimpleMailMessage message = new SimpleMailMessage();
		message.setSubject(elService.evaluate(templateBody, data));
		message.setText(elService.evaluate(templateSubject, data));
		message.setTo(user.getEmail());
		mailer.send(message);
	}
}
