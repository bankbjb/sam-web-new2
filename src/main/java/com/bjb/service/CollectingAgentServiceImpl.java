package com.bjb.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bjb.dao.CollectingAgentDao;
import com.bjb.model.ApprovalPending;
import com.bjb.model.Auditable;
import com.bjb.model.CollectingAgent;

@Service("collectingAgentService")
@Transactional(readOnly=false)
public class CollectingAgentServiceImpl extends OperationalServiceImpl<CollectingAgent, BigDecimal> implements CollectingAgentService {

	private CollectingAgentDao collectingAgentDao;
	
	public CollectingAgentDao getCollectingAgentDao() {
		return collectingAgentDao;
	}

	@Autowired
	public void setCollectingAgentDao(CollectingAgentDao collectingAgentDao) {
		this.collectingAgentDao = collectingAgentDao;
		setGenericDao(collectingAgentDao);
	}

	@Override
	public List<CollectingAgent> selectByCriteria(int startingAt, int maxResult, String code, String name, Date createDate, Integer maxAgent, Integer maxAmount, List<Integer> status) {
		return collectingAgentDao.selectByCriteria(startingAt, maxResult, code, name, createDate, maxAgent, maxAmount, status);
	}
	
	@Override
	public int countByCriteria(String code, String name, Date createDate, Integer maxAgent, Integer maxAmount, List<Integer> status) {
		return collectingAgentDao.countByCriteria(code, name, createDate, maxAgent, maxAmount, status);
	}

	@Override
	public List<CollectingAgent> selectValidCollectingAgent() {
		return collectingAgentDao.selectValidCollectingAgent();
	}
	
	@Override
	public void postApproval(Auditable<BigDecimal> auditObject, ApprovalPending approvalPending) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public CollectingAgent findByName(String name) {
		return collectingAgentDao.findByName(name);
	}

}
