package com.bjb.service;

import com.bjb.model.ApprovalPending;
import com.bjb.model.Auditable;

public interface OperationalService<E, K> extends CommonService<E, K> {

	public int create(E entity);
	public E modify(E baseEntity, E workingEntity);
	public int remove(E entity);

	public int requestToCreate(E entity);
	public E requestToModify(E baseEntity, E workingEntity);
	public int requestToRemove(E entity);
	
	public int approveToCreate(E entity, ApprovalPending approvalPending);
	public int approveToModify(E baseEntity, E workingEntity, ApprovalPending approvalPending);
	public int approveToRemove(E entity, ApprovalPending approvalPending);

	public int rejectToCreate(E entity, ApprovalPending approvalPending, String reason);
	public int rejectToModify(E baseEntity, E workingEntity, ApprovalPending approvalPending, String reason);
	public int rejectToRemove(E entity, ApprovalPending approvalPending, String reason);

	public E findPendingApprovalByCode(String code);
	public void postApproval(Auditable<K> auditObject, ApprovalPending approvalPending);
	
}
