package com.bjb.service;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bjb.dao.MessageFieldDao;
import com.bjb.model.ApprovalPending;
import com.bjb.model.Auditable;
import com.bjb.model.MessageField;

@Service("messageFieldService")
@Transactional(readOnly=false)
public class MessageFieldServiceImpl extends OperationalServiceImpl<MessageField, BigDecimal> implements MessageFieldService {

	private MessageFieldDao messageFieldDao;
	
	public MessageFieldDao getMessageFieldDao() {
		return messageFieldDao;
	}

	@Autowired
	public void setMessageFieldDao(MessageFieldDao messageFieldDao) {
		this.messageFieldDao = messageFieldDao;
		setGenericDao(messageFieldDao);
	}

	@Override
	public List<MessageField> selectByCriteria(int startingAt, int maxResult, String code, String name, String address, List<Integer> status, List<String> approvalStatus) {
		return messageFieldDao.selectByCriteria(startingAt, maxResult, code, name, address, status, approvalStatus);
	}
	
	@Override
	public List<MessageField> selectByMessageType(String messageType){
		return messageFieldDao.selectByMessageType(messageType);
	}
	
	@Override
	public MessageField findByName(String name) {
		return messageFieldDao.findByName(name);
	}
	
	@Override
	public MessageField findByIdAndMessageType(Integer id, String messageType){
		return messageFieldDao.findByIdAndMessageType(id, messageType);
	}

	@Override
	public int countByCriteria(String code, String name, String address, List<Integer> status, List<String> approvalStatus) {
		return messageFieldDao.countByCriteria(code, name, address, status, approvalStatus);
	}
	
	@Override
	public List<MessageField> selectValidMessageField() {
		return messageFieldDao.selectValidMessageField();
	}

	@Override
	public void postApproval(Auditable<BigDecimal> auditObject, ApprovalPending approvalPending) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<MessageField> selectByMessageTypeOrderByIdAsc(String messageType) {
		// TODO Auto-generated method stub
		return messageFieldDao.selectByMessageTypeOrderByIdAsc(messageType);
	}

}
