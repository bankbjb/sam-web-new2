package com.bjb.service;

import java.util.List;

import com.bjb.model.SystemParameter;

public interface SystemParameterService extends OperationalService<SystemParameter, String> {
	public List<SystemParameter> getSearchParameter(int startingAt, int maxResult, String name, String value, List<Integer> status, List<String> approvalStatus);
	public int countSearchParameter(String name, String value, List<Integer> status, List<String> approvalStatus);
}
