package com.bjb.service;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bjb.dao.ApprovalCompleteDao;
import com.bjb.model.ApprovalComplete;
import com.bjb.model.ApprovalPending;

@Service("approvalCompleteService")
@Transactional(readOnly=false)
public class ApprovalCompleteServiceImpl extends CommonServiceImpl<ApprovalComplete, BigDecimal> implements ApprovalCompleteService {

	private ApprovalCompleteDao approvalCompleteDao;
	
	public ApprovalCompleteDao getApprovalCompleteDao() {
		return approvalCompleteDao;
	}

	@Autowired
	public void setApprovalCompleteDao(ApprovalCompleteDao approvalCompleteDao) {
		this.approvalCompleteDao = approvalCompleteDao;
		setGenericDao(approvalCompleteDao);
	}

	public ApprovalCompleteDao getApprovalLogDao() {
		return approvalCompleteDao;
	}

	@Autowired
	public void setApprovalLogDao(ApprovalCompleteDao approvalCompleteDao) {
		this.approvalCompleteDao = approvalCompleteDao;
	}

	@Override
	public void insert(ApprovalPending approvalPending, String statusCode, String statusMessage) {
		approvalCompleteDao.insert(approvalPending, statusCode, statusMessage);
	}

}
