package com.bjb.service;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bjb.dao.MessageMappingDao;
import com.bjb.model.ApprovalPending;
import com.bjb.model.Auditable;
import com.bjb.model.MessageMapping;

@Service("messageMappingService")
@Transactional(readOnly=false)
public class MessageMappingServiceImpl extends OperationalServiceImpl<MessageMapping, BigDecimal> implements MessageMappingService {

	private MessageMappingDao messageMappingDao;
	
	@Autowired
	public void setMessageMappingDao(MessageMappingDao messageMappingDao) {
		this.messageMappingDao = messageMappingDao;
		setGenericDao(messageMappingDao);
	}
	
	@Override
	public List<MessageMapping> selectByCriteria(int startingAt, int maxResult, String code, String feature,  String messageType,List<Integer> status){
		return messageMappingDao.selectByCriteria(startingAt, maxResult, code, feature,messageType, status);
	}
	
	@Override
	public MessageMapping findByName(String name) {
		return messageMappingDao.findByName(name);
	}

	@Override
	public int countByCriteria(String code, String feature,String messageType, List<Integer> status){
		return messageMappingDao.countByCriteria(code, feature,messageType, status);
	}
	
	@Override
	public List<MessageMapping> selectValidMessageMapping(){
		return messageMappingDao.selectValidMessageMapping();
	}

	@Override
	public void postApproval(Auditable<BigDecimal> auditObject, ApprovalPending approvalPending) {

	}
	
}
