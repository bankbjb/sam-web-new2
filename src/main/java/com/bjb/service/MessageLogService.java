package com.bjb.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.bjb.model.MessageLog;

public interface MessageLogService extends CommonService<MessageLog, BigDecimal> {
	public List<MessageLog> selectByCriteria(int startingAt, int maxResult, Date startDate, Date endDate, MessageLog messageLog);
	public int countByCriteria(Date startDate, Date endDate, MessageLog messageLog);
	public MessageLog findByName(String name);
	public List<MessageLog> selectValidMessageLog();

}
