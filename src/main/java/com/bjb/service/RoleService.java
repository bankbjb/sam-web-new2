package com.bjb.service;

import java.util.List;

import com.bjb.model.Role;

public interface RoleService extends OperationalService<Role, Integer> {
	public List<Role> selectValidRole();
	public List<Role> selectValidRoleByRoleName(String roleName);
	public List<Role> selectByCriteria(int startingAt, int maxResult, String roleId, String roleName, List<Integer> status, List<String> approvalStatus);
	public int countByCriteria(String roleId, String roleName, List<Integer> status, List<String> approvalStatus);
	public int countFilteredRole(String roleName);
	public Role findByRoleName(String roleName) ;
	public List<Role> getAllValidRoleByAccessLevel(Integer accessLevel);
}
