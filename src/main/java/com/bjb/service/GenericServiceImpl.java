package com.bjb.service;

import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Example;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bjb.dao.GenericDao;

@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
public abstract class GenericServiceImpl<E, K> implements GenericService<E, K> {
 
    protected GenericDao<E, K> serviceDao;
	@Autowired
	PlatformTransactionManager transactionManager;
	
	public GenericServiceImpl(GenericDao<E,K> genericDao) {
        this.serviceDao=genericDao;
    }
 
    public GenericServiceImpl() {
    }
 
	public GenericDao<E, K> getGenericDao() {
		return serviceDao;
	}

	public void setGenericDao(GenericDao<E, K> genericDao) {
		this.serviceDao = genericDao;
	}
	
    @Override
    public K generateId() {
    	return serviceDao.generateId();
    }

    @Override
    public void insertOrUpdate(E entity) {
        serviceDao.insertOrUpdate(entity);
    }
 
    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List<E> select() {
        return serviceDao.select();
    }
 
    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public E find(K id) {
        return serviceDao.find(id);
    }
 
    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public E findByCode(String code) {
        return serviceDao.findByCode(code);
	}

    @Override
    public void insert(E entity) {
        serviceDao.insert(entity);
    }
 
    @Override
    public void update(E entity) {
        serviceDao.update(entity);
    }
 
    @Override
    public void delete(E entity) {
        serviceDao.delete(entity);
    }

	@Override
	public E deleteByKey(K key) {
		return serviceDao.deleteByKey(key);
	}

	@Override
	public E deleteByCode(String code) {
		return serviceDao.deleteByCode(code);
	}

	@Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<E> select(int startingAt, int maxResults) {
		return serviceDao.select(startingAt, maxResults);
	}

	@Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<E> selectByCriteria(Criterion... criterion) {
		return serviceDao.selectByCriteria(criterion);
	}

	@Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<E> selectByCriteria(int startingAt, int maxResults, Criterion... criterion) {
		return serviceDao.selectByCriteria(startingAt, maxResults, criterion);
	}

	@Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public int count() {
		return serviceDao.count();
	}

	@Override
	public List<E> selectByExample(Example example) {
		return serviceDao.selectByExample(example);
	}

	@Override
	public List<E> selectByExample(int startingAt, int maxResults, Example example) {
		return serviceDao.selectByExample(startingAt, maxResults, example);
	}

	@Override
	public int countByCriteria(Criterion... criterion) {
		return serviceDao.countByCriteria(criterion);
	}

	@Override
	public int countByExample(Example example) {
		return serviceDao.countByExample(example);
	}

}