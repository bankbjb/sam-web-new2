package com.bjb.service;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.context.FacesContext;

import org.primefaces.component.panelmenu.PanelMenu;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bjb.dao.MenuDao;
import com.bjb.model.Action;
import com.bjb.model.Menu;
import com.bjb.model.Role;
import com.bjb.model.RoleMenu;
import com.bjb.model.User;
import com.bjb.support.ContextProvider;
import com.bjb.web.listener.UserSessionBinder;
import com.bjb.web.model.UserSession;

@Service("sessionService")
@Scope(value="session", proxyMode = ScopedProxyMode.TARGET_CLASS)
@Transactional(readOnly=false)
public class SessionServiceImpl implements SessionService, ContextProvider {
	private User user;
	private Map<String, Object> paramMap = new HashMap<String, Object>();
	
	private Set<Integer> actionList = new HashSet<Integer>();
	private MenuModel menuModel;
	private MenuDao menuDao;

	public MenuDao getMenuDao() {
		return menuDao;
	}

	@Autowired
	public void setMenuDao(MenuDao menuDao) {
		this.menuDao = menuDao;
	}

	public void authMenu(BigDecimal userId, String menuPath) {
		int auth = getMenuAuthority(userId, menuPath);
		if (auth == 0){
			try {
				FacesContext.getCurrentInstance().getExternalContext().redirect("login"+".jsf");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public List<String> authMenu(User user, String menuPath) {
		List<String> auths = getMenuAuthority(user, menuPath);
		if (auths == null){
			try {
				FacesContext.getCurrentInstance().getExternalContext().redirect("login"+".jsf");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return auths;
	}

	public Collection<UserSession> getAllSession() {	
		return UserSessionBinder.getLogins().values();
	}
	
	public void putParam(String key, Object value){
		paramMap.put(key, value);
	}
	public Object getAndRemoveParam(String key){
		return paramMap.remove(key);
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * DefaultMenuModel
	 * @param user
	 */
	public void buildMenu(User user){
		menuModel = new DefaultMenuModel();		
		List<Role> roles = new ArrayList<Role>();
		roles.addAll( user.getRoles() );
		List<Menu> menus = new ArrayList<Menu>();
		
		List<Menu> rootMenuList = new ArrayList<Menu>();
		Map<Integer, List<Menu>> childMap = new HashMap<Integer, List<Menu>>();
	
		for (Role role : roles) {
			for (RoleMenu roleMenu : role.getRoleMenus()) {
				menus.add(roleMenu.getMenu());
			}
		}
		Collections.sort(menus);
		for (Menu menu : menus) {
			if (menu.getParentId() == 0 &&  !menu.isAction() && 
					!rootMenuList.contains(menu)) {				
				rootMenuList.add(menu);
				List<Menu> childMenuList = new ArrayList<Menu>();
				
				for (Menu childMenu : menus) {
					if (childMenu.getParentId() == menu.getId() &&
							!childMenuList.contains(childMenu)) {
						childMenuList.add(childMenu);
					}
				}				
				childMap.put(menu.getId(), childMenuList);
			}
			else if (menu.isAction()) {
				actionList.add(menu.getId());
			}
		}
		Collections.sort(rootMenuList);
		
		for (Menu value : rootMenuList) {
			if (value!=null) {
				DefaultSubMenu subMenu = new DefaultSubMenu();
				subMenu.setLabel(value.getName());
				
				createSubMenu(subMenu, value, childMap);
				menuModel.addElement(subMenu);
			}
		}
	}

	/**
	 * Panel Menu
	 */
	public void buildPanelMenu(User user){
		buildMenu(user);
		PanelMenu panelMenu = new PanelMenu();
		panelMenu.setModel(menuModel);
	}

	public void createSubMenu(DefaultSubMenu subMenu, Menu parentMenu, 
			Map<Integer, List<Menu>> childMenu){
		
		List<Menu> childMenuData = childMenu.get(parentMenu.getId());
		
		for (Menu childData : childMenuData) {			
			if (childData.getPath()!=null && 
					childData.getPath().trim().length()>0) {
				
				DefaultMenuItem menuItem = new DefaultMenuItem();  
				menuItem.setValue(childData.getName());
				menuItem.setUrl(childData.getPath()+".jsf");
				menuItem.setStyleClass(childData.getPath());
				menuItem.setOnclick("PF('loadingDlg').show()");
				menuItem.setOncomplete("PF('loadingDlg').hide()");
		        subMenu.getElements().add(menuItem);
			}
		}	
	}

	@Override
	public int getMenuAuthority(BigDecimal userId, String menuPath) {
		return menuDao.getMenuAuthority(userId, menuPath);
	}
	
	public List<String> getMenuAuthority(User user, String menuPath) {
		List<String> authorities = null;
		for (Role role : user.getRoles()) {
			for (RoleMenu roleMenu : role.getRoleMenus()) {
				if (roleMenu.getMenu().getPath().equals(menuPath)) {
					for (Action action:roleMenu.getActionList()) {
						if (authorities == null) {
							authorities = new ArrayList<String>();
						}
						authorities.add(action.getName());
					};
				}
			}
		}
		return authorities;
	}
	

	public MenuModel getMenuModel() {
		return menuModel;
	}

	public boolean hasAction(Integer menuId){
		return actionList.contains(menuId);
	}

	public void setMenuModel(MenuModel menuModel) {
		this.menuModel = menuModel;
	}
}
