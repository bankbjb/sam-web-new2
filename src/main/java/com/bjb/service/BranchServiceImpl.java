package com.bjb.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bjb.dao.BranchDao;
import com.bjb.model.ApprovalPending;
import com.bjb.model.Auditable;
import com.bjb.model.Branch;

@Service("branchService")
@Transactional(readOnly=false)
public class BranchServiceImpl extends OperationalServiceImpl<Branch, String> implements BranchService {

	private BranchDao branchDao;
	
	public BranchDao getBranchDao() {
		return branchDao;
	}

	@Autowired
	public void setBranchDao(BranchDao branchDao) {
		this.branchDao = branchDao;
		setGenericDao(branchDao);
	}

	@Override
	public List<Branch> getBranchByCategory(int startingAt,
			int maxResult, String branchCode,
			String branchName, List<Integer> status, List<String> approvalStatus) {
		return branchDao.selectByCriteria(startingAt, maxResult, branchCode, branchName, status, approvalStatus);
	}

	@Override
	public Integer countBranchByCategory(String branchCode,
			String branchName, List<Integer> status, List<String> approvalStatus) {
		return branchDao.countByCriteria(branchCode, branchName, status, approvalStatus);
	}

	@Override
	public Branch getBranchByBranchCode(String branchCode) {
		return branchDao.find(branchCode);
	}

	@Override
	public List<Branch> getAllValidBranch() {
		return branchDao.selectValidBranch();
	}

	@Override
	public void postApproval(Auditable<String> auditObject, ApprovalPending approvalPending) {
		// TODO Auto-generated method stub
		
	}

}


