package com.bjb.service;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bjb.constant.Constants;
import com.bjb.model.ApprovalPending;
import com.bjb.model.Auditable;
import com.bjb.support.annotation.Recorded;
import com.bjb.support.annotation.RecordingStatus;
import com.bjb.util.HelperUtils;

@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
public abstract class OperationalServiceImpl<E, K> extends CommonServiceImpl<E, K> implements OperationalService<E, K> {

	private SessionService sessionService;
	private ApprovalPendingService approvalPendingService;
	private ApprovalCompleteService approvalCompleteService;

	@Autowired
	public void setSessionService(SessionService sessionService) {
		this.sessionService = sessionService;
	}

	@Autowired
	public void setApprovalPendingService(ApprovalPendingService approvalPendingService) {
		this.approvalPendingService = approvalPendingService;
	}

	@Autowired
	public void setApprovalCompleteService(ApprovalCompleteService approvalCompleteService) {
		this.approvalCompleteService = approvalCompleteService;
	}

	@SuppressWarnings({ "unused", "unchecked" })
	public E findPendingApprovalByCode(String code)
	{
		E pendingObject = null;
		List<ApprovalPending> approvalPendings = approvalPendingService.selectByParameterCode(code);
		if (approvalPendings.size() > 0) {
			try {
				pendingObject = (E) HelperUtils.deserialize(approvalPendings.get(0).getNewBlobObject().getBinaryStream());
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return pendingObject;

	}

	@SuppressWarnings("rawtypes")
    @Recorded(RecordingStatus.CREATE)
	public int create(E entity) {
		if (entity instanceof Auditable) {
			Auditable auditable = (Auditable) entity;
			if ((auditable.getCreateWho() == null) || (auditable.getCreateWho().equals(""))) {
				auditable.setCreateDate(HelperUtils.getCurrentDateTime());
				auditable.setCreateWho(sessionService.getUser().getUserName());
			}
			auditable.setChangeDate(HelperUtils.getCurrentDateTime());
			auditable.setChangeWho(sessionService.getUser().getUserName());
			auditable.setApprovalStatus(Constants.NO_APPROVAL_STATUS);
		}
		insert(entity);
		return Constants.RESULT_OK;
	}
	
	@SuppressWarnings("rawtypes")
    @Recorded(oldObject = Recorded.AS_FIRST_ARGUMENT, newObject = Recorded.AS_SECOND_ARGUMENT)
	public E modify(E baseEntity, E workingEntity) {
		if (workingEntity instanceof Auditable) {
			Auditable auditable = (Auditable) workingEntity;
			auditable.setChangeDate(HelperUtils.getCurrentDateTime());
			auditable.setChangeWho(sessionService.getUser().getUserName());
		}
		update(workingEntity);
		return workingEntity;
	}
	
	@SuppressWarnings("rawtypes")
    @Recorded(RecordingStatus.REMOVE)
	public int remove(E entity) {
		if (entity instanceof Auditable) {
			Auditable auditable = (Auditable) entity;
			auditable.setChangeDate(HelperUtils.getCurrentDateTime());
			auditable.setChangeWho(sessionService.getUser().getUserName());
		}
		delete(entity);
		return Constants.RESULT_OK;
	}

	@SuppressWarnings("rawtypes")
	@Override
    @Recorded(RecordingStatus.CREATE)
	public int requestToCreate(E entity) {
		int result = Constants.RESULT_ERROR;
		if (entity instanceof Auditable) {
			Auditable auditable = (Auditable) entity;
			auditable.setCreateDate(HelperUtils.getCurrentDateTime());
			auditable.setChangeDate(HelperUtils.getCurrentDateTime());
			auditable.setChangeWho(sessionService.getUser().getUserName());
			auditable.setApprovalStatus(Constants.APPROVAL_STATUS_PENDING_CREATE);
			insert(entity);

			ApprovalPending approvalPending = new ApprovalPending();
			approvalPending.setAction(Constants.APPROVAL_ACTION_CREATE);
			approvalPending.setParameter(auditable.getClass().getSimpleName());
			approvalPending.setParameterCode(auditable.getCode());
			approvalPending.setParameterName(auditable.getName());
			approvalPending.setBranchId(sessionService.getUser().getBranch().getId());
			try {
				approvalPending.setNewByteObject(HelperUtils
						.serialize(auditable));
				result = Constants.RESULT_OK;
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				approvalPendingService.insert(approvalPending);
			}
		}
		return result;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
    @Recorded(oldObject = Recorded.AS_FIRST_ARGUMENT, newObject = Recorded.AS_SECOND_ARGUMENT)
	public E requestToModify(E baseEntity, E workingEntity) {
		if (workingEntity instanceof Auditable) {
			Auditable auditable = (Auditable) workingEntity;
			auditable.setChangeDate(HelperUtils.getCurrentDateTime());
			auditable.setChangeWho(sessionService.getUser().getUserName());
	
			try {
				if (auditable.getApprovalStatus().equals(
						Constants.APPROVAL_STATUS_PENDING_EDIT)) {

					Auditable auditableBase = (Auditable) baseEntity;
					approvalPendingService.updateApprovalByParameterCode(auditableBase.getCode(),
							HelperUtils.serialize(baseEntity),
							auditableBase.getName());

				} else if (auditable.getApprovalStatus().equals(
						Constants.APPROVAL_STATUS_PENDING_CREATE)) {
	
					approvalPendingService.updateApprovalByParameterCode(auditable.getCode(),
							HelperUtils.serialize(auditable),
							auditable.getName());
	
				} else if (auditable.getApprovalStatus().equals(
						Constants.APPROVAL_STATUS_REJECTED_CREATE)) {
	
					ApprovalPending approvalPending = new ApprovalPending();
					approvalPending.setAction(Constants.APPROVAL_ACTION_CREATE);
					approvalPending.setParameter(auditable.getClass().getSimpleName());
					approvalPending.setParameterCode(auditable.getCode());
					approvalPending.setParameterName(auditable.getName());
					approvalPending.setBranchId(sessionService.getUser().getBranch().getId());
					approvalPending.setOldByteObject(HelperUtils
							.serialize(baseEntity));
					approvalPending.setNewByteObject(HelperUtils
							.serialize(auditable));
					approvalPendingService.insert(approvalPending);
	
					auditable = (Auditable) HelperUtils.clone(baseEntity);
					auditable.setApprovalStatus(Constants.APPROVAL_STATUS_PENDING_CREATE);
	
				}  else {
					ApprovalPending approvalPending = new ApprovalPending();
					approvalPending.setAction(Constants.APPROVAL_ACTION_EDIT);
					approvalPending.setParameter(auditable.getClass().getSimpleName());
					approvalPending.setParameterCode(auditable.getCode());
					approvalPending.setParameterName(auditable.getName());
					approvalPending.setBranchId(sessionService.getUser().getBranch().getId());
					approvalPending.setOldByteObject(HelperUtils
							.serialize(baseEntity));
					approvalPending.setNewByteObject(HelperUtils
							.serialize(auditable));
					approvalPendingService.insert(approvalPending);
	
					auditable = (Auditable) HelperUtils.clone(baseEntity);
					auditable.setApprovalStatus(Constants.APPROVAL_STATUS_PENDING_EDIT);
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			workingEntity = (E)auditable;
			update(workingEntity);
			return workingEntity;
		}
		return null;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
    @Recorded(RecordingStatus.REMOVE)
	public int requestToRemove(E entity) {
		int result = Constants.RESULT_ERROR;
		if (entity instanceof Auditable) {
			Auditable auditable = (Auditable) entity;
		
			if (auditable.getApprovalStatus().equals(Constants.APPROVAL_STATUS_REJECTED_CREATE)) {
	
				deleteByKey((K)auditable.getId());
				result = Constants.RESULT_OK;
	
			} else {
				ApprovalPending approvalPending = new ApprovalPending();
				approvalPending.setAction(Constants.APPROVAL_ACTION_DELETE);
				approvalPending.setParameter(auditable.getClass().getSimpleName());
				approvalPending.setParameterCode(auditable.getCode());
				approvalPending.setParameterName(auditable.getName());
				approvalPending.setBranchId(sessionService.getUser().getBranch().getId());
		
				try {
					approvalPending.setNewByteObject(HelperUtils
							.serialize(auditable));
					result = Constants.RESULT_OK;
				} catch (IOException e) {
					e.printStackTrace();
				}
		
				approvalPendingService.insert(approvalPending);
		
				auditable.setApprovalStatus(Constants.APPROVAL_STATUS_PENDING_DELETE);
				insert(entity);
			}
		}
		return result;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
    @Recorded(RecordingStatus.CREATE)
	public int approveToCreate(E entity, ApprovalPending approvalPending) {
		((Auditable)entity).setApprovalStatus(Constants.APPROVAL_STATUS_APPROVED_CREATE);
		postApproval((Auditable)entity, approvalPending);
		((Auditable)entity).setApprovalBy(sessionService.getUser().getId());
		((Auditable)entity).setApprovalWho(sessionService.getUser().getUserName());
		((Auditable)entity).setApprovalDate(new Date());
		insertOrUpdate(entity);						
		approvalCompleteService.insert(approvalPending, Constants.APPROVAL_STATUS_CODE_SUCCESS, "Success");
		approvalPendingService.delete(approvalPending);
		return Constants.RESULT_OK;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	@Recorded(oldObject = Recorded.AS_FIRST_ARGUMENT, newObject = Recorded.AS_SECOND_ARGUMENT)
	public int approveToModify(E baseEntity, E workingEntity, ApprovalPending approvalPending) {
		((Auditable)workingEntity).setApprovalStatus(Constants.APPROVAL_STATUS_APPROVED_EDIT);
		postApproval(((Auditable)workingEntity), approvalPending);
		((Auditable)workingEntity).setApprovalBy(sessionService.getUser().getId());
		((Auditable)workingEntity).setApprovalWho(sessionService.getUser().getUserName());
		((Auditable)workingEntity).setApprovalDate(new Date());
		insertOrUpdate(workingEntity);
		approvalCompleteService.insert(approvalPending, Constants.APPROVAL_STATUS_CODE_SUCCESS, "Success");
		approvalPendingService.delete(approvalPending);
		return Constants.RESULT_OK;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
    @Recorded(RecordingStatus.REMOVE)
	public int approveToRemove(E entity, ApprovalPending approvalPending) {
		postApproval((Auditable)entity, approvalPending);
		deleteByKey((K)((Auditable)entity).getId());
		approvalCompleteService.insert(approvalPending, Constants.APPROVAL_STATUS_CODE_SUCCESS, "Success");
		approvalPendingService.delete(approvalPending);
		return Constants.RESULT_OK;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
    @Recorded(RecordingStatus.CREATE)
	public int rejectToCreate(E entity, ApprovalPending approvalPending, String reason) {
		Auditable auditable = (Auditable) findByCode(((Auditable)entity).getCode());
		auditable.setApprovalStatus(Constants.APPROVAL_STATUS_REJECTED_CREATE);
		auditable.setApprovalBy(sessionService.getUser().getId());
		auditable.setApprovalWho(sessionService.getUser().getUserName());
		auditable.setApprovalDate(new Date());
		auditable.setApprovalReason(reason);
		insertOrUpdate((E)auditable);
		approvalCompleteService.insert(approvalPending, Constants.APPROVAL_STATUS_CODE_SUCCESS, "Success");
		approvalPendingService.delete(approvalPending);
		return Constants.RESULT_OK;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	@Recorded(oldObject = Recorded.AS_FIRST_ARGUMENT, newObject = Recorded.AS_SECOND_ARGUMENT)
	public int rejectToModify(E baseEntity, E workingEntity, ApprovalPending approvalPending, String reason) {
		Auditable auditable = (Auditable) findByCode(((Auditable)baseEntity).getCode());
		auditable.setApprovalStatus(Constants.APPROVAL_STATUS_REJECTED_EDIT);
		auditable.setApprovalBy(sessionService.getUser().getId());
		auditable.setApprovalWho(sessionService.getUser().getUserName());
		auditable.setApprovalDate(new Date());
		auditable.setApprovalReason(reason);
		insertOrUpdate((E)auditable);
		approvalCompleteService.insert(approvalPending, Constants.APPROVAL_STATUS_CODE_SUCCESS, "Success");
		approvalPendingService.delete(approvalPending);
		return Constants.RESULT_OK;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
    @Recorded(RecordingStatus.REMOVE)
	public int rejectToRemove(E entity, ApprovalPending approvalPending, String reason) {
		Auditable auditable = (Auditable) findByCode(((Auditable)entity).getCode());
		auditable.setApprovalStatus(Constants.APPROVAL_STATUS_REJECTED_DELETE);
		auditable.setApprovalBy(sessionService.getUser().getId());
		auditable.setApprovalWho(sessionService.getUser().getUserName());
		auditable.setApprovalDate(new Date());
		auditable.setApprovalReason(reason);
		insertOrUpdate((E)auditable);
		approvalCompleteService.insert(approvalPending, Constants.APPROVAL_STATUS_CODE_SUCCESS, "Success");
		approvalPendingService.delete(approvalPending);
		return Constants.RESULT_OK;
	}

}
