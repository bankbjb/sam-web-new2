package com.bjb.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import com.bjb.constant.Constants;
import com.bjb.dao.SystemParameterDao;
import com.bjb.model.SystemParameter;
import com.bjb.util.EncryptUtils;

@Service("commonService")
public class CommonServiceImpl<E, K> extends GenericServiceImpl<E, K> implements CommonService<E, K> {

	protected SystemParameterDao systemParameterDao;

	@Autowired
	public void setSystemParameterDao(SystemParameterDao systemParameterDao) {
		this.systemParameterDao = systemParameterDao;
	}

	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED)
	public String getSystemParameter(final String parameterName) {
	    TransactionTemplate tmpl = new TransactionTemplate(transactionManager);
	    String value = tmpl.execute(new TransactionCallback<String>() {
	        @Override
	        public String doInTransaction(TransactionStatus status) {
	            SystemParameter parameter =  systemParameterDao.findByCode(parameterName);
	            if (parameter == null ) {
	            	return null;
	            }
	            if (parameter.getSecured() == Constants.STATUS_ACTIVE) {
	            	return EncryptUtils.decodeDecrypt(parameter.getValue());
	            } else {
	            	return parameter.getValue();
	            }
	        }
	    });
		return value;
	}

	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED)
	public String getSystemParameter(final String parameterName, final String defaultValue) {
	    TransactionTemplate tmpl = new TransactionTemplate(transactionManager);
	    String value = tmpl.execute(new TransactionCallback<String>() {
	        @Override
	        public String doInTransaction(TransactionStatus status) {
	            SystemParameter parameter =  systemParameterDao.findByCode(parameterName);
	            if (parameter == null ) {
	            	return defaultValue;
	            }
	            if (parameter.getSecured() == Constants.STATUS_ACTIVE) {
	            	return EncryptUtils.decodeDecrypt(parameter.getValue());
	            } else {
	            	return parameter.getValue();
	            }
	        }
	    });
		return (value!=null) ? value : defaultValue;
	}


}
