package com.bjb.service;

import java.math.BigDecimal;
import java.util.List;

import com.bjb.model.ScheduleTask;

public interface SchedulerService extends OperationalService<ScheduleTask, BigDecimal> {
	public List<ScheduleTask> getAllTask(int maxPerPage, int startingAt);
	public int countAllTask();
	public ScheduleTask getDetailedTaskByName(String name);
	public List<ScheduleTask> getAllValidTask();
	public void updateDateTime(String task, String newDate, String newTime);
}
