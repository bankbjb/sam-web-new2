package com.bjb.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bjb.dao.TransactionLogDao;
import com.bjb.model.TransactionLog;

@Service("transactionLogService")
@Transactional(readOnly=true)
public class TransactionLogServiceImpl extends CommonServiceImpl<TransactionLog, BigDecimal> implements TransactionLogService {

	private TransactionLogDao transactionLogDao;
	
	@Override
	public List<TransactionLog> selectByCriteria(int startingAt, int maxResult, String aggregatorCode, Date startDate, Date endDate, TransactionLog transactionLog) {
		return transactionLogDao.selectByCriteria(startingAt, maxResult, aggregatorCode, startDate, endDate, transactionLog);
	}
	
	@Override
	public TransactionLog findByName(String name) {
		return transactionLogDao.findByName(name);
	}

	@Override
	public int countByCriteria(String aggregatorCode, Date startDate, Date endDate, TransactionLog transactionLog) {
		return transactionLogDao.countByCriteria(aggregatorCode, startDate, endDate, transactionLog);
	}

	@Override
	public List<TransactionLog> selectValidTransactionLog() {
		return transactionLogDao.selectValidTransactionLog();
	}
	
	public TransactionLogDao getTransactionLogDao() {
		return transactionLogDao;
	}

	@Autowired
	public void setTransactionLogDao(TransactionLogDao transactionLogDao) {
		this.transactionLogDao = transactionLogDao;
		setGenericDao(transactionLogDao);
	}
	
	public List<Map<Object, Number>> selectTopFeeBasedIncome(Integer maxResults) {
		return transactionLogDao.selectTopFeeBasedIncome(maxResults);
	}

	public List<Map<Object, Number>> selectTopBestSeller(Integer maxResults) {
		return transactionLogDao.selectTopBestSeller(maxResults);
	}
	
	

}
