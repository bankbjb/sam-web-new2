package com.bjb.service;

import java.math.BigDecimal;

import com.bjb.model.ApprovalComplete;
import com.bjb.model.ApprovalPending;

public interface ApprovalCompleteService extends CommonService<ApprovalComplete, BigDecimal> {

	public void insert(ApprovalPending ApprovalPending, String statusCode, String statusMessage);
}
