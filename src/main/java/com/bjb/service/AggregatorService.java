package com.bjb.service;

import java.math.BigDecimal;
import java.util.List;

import com.bjb.model.Aggregator;
import com.bjb.model.UserAggregator;

public interface AggregatorService extends OperationalService<Aggregator, BigDecimal> {
	public List<Aggregator> selectByCriteria(int startingAt, int maxResult, String code, String name, String address, String phone, String pic, String emailPic, List<Integer> status, List<String> approvalStatus);
	public int countByCriteria(String code, String name, String address, String phone, String pic, String emailPic, List<Integer> status, List<String> approvalStatus);
	public Aggregator findByName(String name);
	public Aggregator deleteByCode(String code) ;
	public List<Aggregator> selectValidAggregator();
	public void sendForgetPasswordEmail(UserAggregator user);
}
