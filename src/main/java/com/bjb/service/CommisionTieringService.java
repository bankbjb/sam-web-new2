package com.bjb.service;

import java.math.BigDecimal;
import java.util.List;

import com.bjb.model.CommisionTiering;

public interface CommisionTieringService extends OperationalService<CommisionTiering, BigDecimal> {
	public List<CommisionTiering> selectByCriteria(int startingAt, int maxResult, String code, String featureMapping, List<Integer> status, List<String> approvalStatus);
	public int countByCriteria(String code, String featureMapping, List<Integer> status, List<String> approvalStatus);
	public CommisionTiering findByName(String name);
}
