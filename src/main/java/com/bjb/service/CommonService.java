package com.bjb.service;

public interface CommonService<E, K> extends GenericService<E, K> {

	public String getSystemParameter(String parameterName);

	public String getSystemParameter(String parameterName, String defaultValue);

}
