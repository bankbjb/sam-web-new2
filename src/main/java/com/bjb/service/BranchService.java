package com.bjb.service;

import java.util.List;

import com.bjb.model.Branch;

public interface BranchService extends OperationalService<Branch, String> {
	public List<Branch> getBranchByCategory(int startingAt, int maxResult, String branchCode, String branchName, List<Integer> status, List<String> approvalStatus);
	public Integer countBranchByCategory(String branchCode, String branchName, List<Integer> status, List<String> approvalStatus);
	public List<Branch> getAllValidBranch();
	public Branch getBranchByBranchCode(String branchCode);
}
