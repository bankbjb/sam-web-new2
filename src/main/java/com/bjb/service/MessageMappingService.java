package com.bjb.service;

import java.math.BigDecimal;
import java.util.List;

import com.bjb.model.MessageMapping;

public interface MessageMappingService extends OperationalService<MessageMapping, BigDecimal> {
	public List<MessageMapping> selectByCriteria(int startingAt, int maxResult, String code, String feature, String messageType, List<Integer> status); 
	public int countByCriteria(String code, String feature,String messageType,  List<Integer> status);
	public MessageMapping findByName(String name);
	public List<MessageMapping> selectValidMessageMapping();
}
