package com.bjb.service;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bjb.constant.Constants;
import com.bjb.dao.ActivityDao;
import com.bjb.dao.ActivityLogDao;
import com.bjb.model.Activity;
import com.bjb.model.ActivityLog;
import com.bjb.model.User;
import com.bjb.support.ActivityLogger;
import com.bjb.support.ContextProvider;
import com.bjb.util.HelperUtils;

@Service("activityLogService")
@Transactional(readOnly=false)
public class ActivityLogServiceImpl extends CommonServiceImpl<ActivityLog, BigDecimal> implements ActivityLogService, ActivityLogger  {

	private ActivityDao activityDao;
	private ActivityLogDao activityLogDao;
	private ContextProvider contextProvider;

	public ActivityDao getActivityDao() {
		return activityDao;
	}	

	public ActivityLogDao getActivityLogLogDao() {
		return activityLogDao;
	}

	@Autowired
	public void setActivityDao(ActivityDao activityDao) {
		this.activityDao = activityDao;
	}

	@Autowired
	public void setActivityLogDao(ActivityLogDao activityLogDao) {
		this.activityLogDao = activityLogDao;
		setGenericDao(activityLogDao);
	}
	
	@Autowired
	public void setContextProvider(ContextProvider contextProvider) {
		this.contextProvider = contextProvider;
	}
    
	public void log(String activityName, Object oldObject, Object newObject, Object[] args) {
		if (contextProvider!=null && contextProvider.getUser()!=null) {
			Activity activity = activityDao.findByCode(activityName);
			User user = contextProvider.getUser();
			if (activity==null) {
				activity = new Activity();
				activity.setCode(activityName);
				activity.setDescription(activityName);
				activity.setStatus(Constants.STATUS_ACTIVE);
				activityDao.insert(activity);
			}
			if (activity.getStatus() == Constants.STATUS_ACTIVE) {
				ActivityLog activityLog = new ActivityLog();
				activityLog.setActivity(activity);
				activityLog.setCreateBy(user.getId());
				activityLog.setCreateDate(new Date());
				activityLog.setCreateWho(user.getUserName());
				activityLog.setStatus(Constants.RESULT_OK);
				try {
					activityLog.setDataBefore(HelperUtils
							.serialize(oldObject));
					activityLog.setDataAfter(HelperUtils
							.serialize(newObject));
				} catch (IOException e) {
					e.printStackTrace();
				}
				insert(activityLog);
			}
		}
	}

	@Override
	public List<ActivityLog> selectByCriteria(int startingAt, int maxResult, BigDecimal userId, BigDecimal activityId, Date startDate,
			Date endDate, ActivityLog activityLog, List<Integer> status) {
		return activityLogDao.selectByCriteria(startingAt, maxResult, userId, activityId, startDate, endDate, activityLog, status);
	}

	@Override
	public int countByCriteria(BigDecimal userId, BigDecimal activityId, Date startDate, Date endDate, ActivityLog activityLog, List<Integer> status) {
		return activityLogDao.countByCriteria(userId, activityId, startDate, endDate, activityLog, status);
	}

}
