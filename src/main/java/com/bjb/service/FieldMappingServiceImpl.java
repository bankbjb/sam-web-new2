package com.bjb.service;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bjb.dao.FieldMappingDao;
import com.bjb.model.ApprovalPending;
import com.bjb.model.Auditable;
import com.bjb.model.FieldMapping;

@Service("fieldMappingService")
@Transactional(readOnly=false)
public class FieldMappingServiceImpl extends OperationalServiceImpl<FieldMapping, BigDecimal> implements FieldMappingService {

	private FieldMappingDao fieldMappingDao;
	
	@Autowired
	public void setFieldMappingDao(FieldMappingDao fieldMappingDao) {
		this.fieldMappingDao = fieldMappingDao;
		setGenericDao(fieldMappingDao);
	}
	
	@Override
	public void postApproval(Auditable<BigDecimal> auditObject,
			ApprovalPending approvalPending) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<FieldMapping> findByMessageMappingId(BigDecimal messageMappingId) {
		// TODO Auto-generated method stub
		return this.fieldMappingDao.findByMessageMappingId(messageMappingId);
	}}
