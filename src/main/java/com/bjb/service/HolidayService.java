package com.bjb.service;

import java.math.BigDecimal;
import java.util.List;

import com.bjb.model.Holiday;

public interface HolidayService extends OperationalService<Holiday, BigDecimal> {
	public List<Holiday> getHolidayByCategory(int maxPerPage, int startingAt, String holidayCode, String holidayName, String sDate, List<String> status);
	public Integer countHolidayByCategory(String holidayCode, String holidayName, String sDate, List<String> status);
	public Holiday getDetailedHolidayByHolidayName(String holidayName);
}
