package com.bjb.service;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bjb.dao.HolidayDao;
import com.bjb.model.ApprovalPending;
import com.bjb.model.Auditable;
import com.bjb.model.Holiday;

@Service("holidayService")
@Transactional(readOnly=false)
public class HolidayServiceImpl extends OperationalServiceImpl<Holiday, BigDecimal> implements HolidayService {
	
	private HolidayDao holidayDao;
	
	public HolidayDao getHolidayDao() {
		return holidayDao;
	}

	@Autowired
	public void setHolidayDao(HolidayDao holidayDao) {
		this.holidayDao = holidayDao;
		setGenericDao(holidayDao);
	}

	@Override
	public List<Holiday> getHolidayByCategory(int maxPerPage,
			int startingAt, String holidayCode, String holidayName,
			String sDate, List<String> status) {
		return holidayDao.getHolidayByCategory(maxPerPage, startingAt, holidayCode, holidayName, sDate, status);
	}

	@Override
	public Integer countHolidayByCategory(String holidayCode, String holidayName,
			String sDate, List<String> status) {
		return holidayDao.countHolidayByCategory(holidayCode, holidayName, sDate, status);
	}

	@Override
	public Holiday getDetailedHolidayByHolidayName(String holidayName) {
		return holidayDao.getDetailedHolidayByName(holidayName);
	}

	@Override
	public void postApproval(Auditable<BigDecimal> auditObject, ApprovalPending approvalPending) {
		// TODO Auto-generated method stub
		
	}

}
