package com.bjb.service;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bjb.dao.ApprovalCompleteDao;
import com.bjb.dao.ApprovalPendingDao;
import com.bjb.model.ApprovalPending;

@Service("approvalPendingService")
@Transactional(readOnly=false)
public class ApprovalPendingServiceImpl extends CommonServiceImpl<ApprovalPending, BigDecimal> implements ApprovalPendingService {

	private ApprovalPendingDao approvalPendingDao;
	private ApprovalCompleteDao approvalCompleteDao;
	
	public ApprovalPendingDao getApprovalDao() {
		return approvalPendingDao;
	}

	@Autowired
	public void setApprovalDao(ApprovalPendingDao approvalPendingDao) {
		this.approvalPendingDao = approvalPendingDao;
		setGenericDao(approvalPendingDao);
	}

	public ApprovalCompleteDao getApprovalLogDao() {
		return approvalCompleteDao;
	}

	@Autowired
	public void setApprovalLogDao(ApprovalCompleteDao approvalCompleteDao) {
		this.approvalCompleteDao = approvalCompleteDao;
	}

	@Override
	public List<ApprovalPending> selectByCriteria(int startingAt,
			int maxResult, BigDecimal approverId, String parameterCode, String parameterName, List<String> searchParameter,List<String> searchAction, String branchId) {
		return approvalPendingDao.selectByCriteria(startingAt, maxResult, approverId, parameterCode, parameterName, searchParameter, searchAction, branchId);
	}

	@Override
	public int countByCriteria(BigDecimal approverId, String parameterCode, String parameterName, List<String> searchParameter,List<String> searchAction, String branchId) {
		return approvalPendingDao.countByCriteria(approverId, parameterCode, parameterName, searchParameter, searchAction, branchId);			
	}

	@Override
	public List<ApprovalPending> selectByCriteria(String paramater, BigDecimal approverId) {
		return approvalPendingDao.selectByCriteria(paramater, approverId);
	}

	@Override
	public void deleteApproval(List<ApprovalPending> approvalPendings) {
		approvalPendingDao.bulkDelete(approvalPendings);	
	}

	@Override
	public List<ApprovalPending> selectByParameterCode(String parameterCode) {
		return approvalPendingDao.selectByParameterCode(parameterCode);	
	}

	@Override
	public void updateApprovalByParameterCode(String parameterCode, byte[] newByteObject, String parameterName) {
		approvalPendingDao.updateApprovalByParameterCode(parameterCode, newByteObject, parameterName);
	}

	@Override
	public void assignApprover(BigDecimal approverId){
		approvalPendingDao.assignApprover(approverId);
	}

	@Override
	public void removeApprover(List<BigDecimal> listApproverId) {
		approvalPendingDao.removeApprover(listApproverId);
	}
	
	@Override
	public void removeAllApprover() {
		approvalPendingDao.removeAllApprover();
	}

	@Override
	public List<ApprovalPending> selectByCriteria(BigDecimal approverId,
			String parameterCode, String parameterName,
			List<String> searchParameter, List<String> searchAction,
			String branchId) {
		return approvalPendingDao.selectByCriteria(approverId, parameterCode, parameterName, searchParameter, searchAction, branchId);
	}

}
