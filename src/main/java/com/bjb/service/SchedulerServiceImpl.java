package com.bjb.service;


import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.List;

import javax.xml.rpc.ServiceException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bjb.constant.Constants;
import com.bjb.dao.SchedulerDao;
import com.bjb.integration.scheduler.SchedulerExecutor;
import com.bjb.model.ApprovalPending;
import com.bjb.model.Auditable;
import com.bjb.model.ScheduleTask;

@Service("schedulerService")
@Transactional(readOnly=false)
public class SchedulerServiceImpl extends OperationalServiceImpl<ScheduleTask, BigDecimal> implements SchedulerService{

	private SchedulerDao schedulerDao;
	
	public SchedulerDao getSchedulerDao() {
		return schedulerDao;
	}

	@Autowired
	public void setSchedulerDao(SchedulerDao schedulerDao) {
		this.schedulerDao = schedulerDao;
		setGenericDao(schedulerDao);
	}

	@Override
	public List<ScheduleTask> getAllTask(int startingAt, int maxResult) {
			return schedulerDao.select(startingAt, maxResult);
	}

	@Override
	public int countAllTask() {
		return schedulerDao.count();
	}

	@Override
	public List<ScheduleTask> getAllValidTask() {
		return schedulerDao.getAllValidTask();
	}

	@Override
	public ScheduleTask getDetailedTaskByName(String name) {
		return schedulerDao.getDetailedTaskByName(name);
	}
	
	@Override
	public void updateDateTime(String task, String newDate, String newTime){
		schedulerDao.updateDateTime(task, newDate, newTime);
	}

	@Override
	public void postApproval(Auditable<BigDecimal> auditObject, ApprovalPending approvalPending) {
		try {
			if (approvalPending.getAction().equals(Constants.APPROVAL_ACTION_DELETE)) {
					SchedulerExecutor.deleteScheduler(((ScheduleTask) auditObject).getTaskId());
			} else if (approvalPending.getAction().equals(Constants.APPROVAL_ACTION_CREATE)) {
				ScheduleTask task = ((ScheduleTask) auditObject);
				String taskId = SchedulerExecutor.createScheduler(task);
				task.setTaskId(taskId);
				SchedulerExecutor.startScheduler(taskId);
				task.setSchedulerStatus(SchedulerExecutor.statusScheduler(task.getTaskId()));
			} else if (approvalPending.getAction().equals(Constants.APPROVAL_ACTION_EDIT)) {
				ScheduleTask task = ((ScheduleTask) auditObject);
				SchedulerExecutor.deleteScheduler(task.getTaskId());
				String taskId = SchedulerExecutor.createScheduler(task);
				task.setTaskId(taskId);
				SchedulerExecutor.startScheduler(taskId);
				task.setSchedulerStatus(SchedulerExecutor.statusScheduler(task.getTaskId()));
			}
		} catch (RemoteException | ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
}
