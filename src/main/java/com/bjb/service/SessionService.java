package com.bjb.service;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuModel;

import com.bjb.model.Menu;
import com.bjb.model.User;
import com.bjb.web.model.UserSession;

public interface SessionService {

	public void authMenu(BigDecimal userId, String menuPath);
	public List<String> authMenu(User user, String menuPath);
	
	public Collection<UserSession> getAllSession();

	public void putParam(String key, Object value);
	
	public Object getAndRemoveParam(String key);

	public User getUser();
	
	public void setUser(User user);

	public void buildMenu(User user);

	public void buildPanelMenu(User user);

	public void createSubMenu(DefaultSubMenu subMenu, Menu parentMenu, Map<Integer, List<Menu>> childMenu);

	public int getMenuAuthority(BigDecimal userId, String menuPath);

	public List<String> getMenuAuthority(User user, String menuPath);

	public MenuModel getMenuModel();

	public boolean hasAction(Integer menuId);

	public void setMenuModel(MenuModel menuModel);

}
