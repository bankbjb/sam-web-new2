package com.bjb.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bjb.dao.RoleDao;
import com.bjb.model.ApprovalPending;
import com.bjb.model.Auditable;
import com.bjb.model.Role;

@Service("roleService")
@Transactional(readOnly=false)
public class RoleServiceImpl extends OperationalServiceImpl<Role, Integer> implements RoleService{

	private RoleDao roleDao;
	
	public RoleDao getRoleDao() {
		return roleDao;
	}
	
	@Autowired
	public void setRoleDao(RoleDao roleDao) {
		this.roleDao = roleDao;
		setGenericDao(roleDao);
	}

	@Override
	public List<Role> selectValidRole() {
		return roleDao.selectValidRole();
	}
	
	@Override
	public List<Role> selectValidRoleByRoleName(String roleName) {
		return roleDao.selectValidRoleByRoleName(roleName);
	}

	@Override
	public int countFilteredRole(String roleId) {
		return roleDao.countFilteredRole(roleId);
	}

	@Override
	public Role find(Integer roleId) {
		return serviceDao.find(roleId);
	}

	@Override
	public Role findByRoleName(String roleName) {
		return roleDao.findByRoleName(roleName);
	}

	@Override
	public List<Role> selectByCriteria(int startingAt, int maxResult,
			String roleCode, String roleName, List<Integer> status, List<String> approvalStatus) {
		return roleDao.getLazyRoleByCategory(startingAt, maxResult, roleCode, roleName, status, approvalStatus);
	}

	@Override
	public int countByCriteria(String roleCode, String roleName, List<Integer> status, List<String> approvalStatus) {
		return roleDao.countAllRoleByCategory(roleCode, roleName, status, approvalStatus);
	}

	@Override
	public void postApproval(Auditable<Integer> auditObject, ApprovalPending approvalPending) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Role> getAllValidRoleByAccessLevel(Integer accessLevel) {
		return roleDao.getAllValidRoleByAccessLevel(accessLevel);
	}

    @Override
    public void insertOrUpdate(Role role) {
		roleDao.deleteRoleMenu(role);
		roleDao.insertOrUpdate(role);
    }

}
