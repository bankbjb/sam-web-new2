package com.bjb.service;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bjb.dao.FeatureMappingDao;
import com.bjb.model.ApprovalPending;
import com.bjb.model.Auditable;
import com.bjb.model.FeatureMapping;

@Service("featureMappingService")
@Transactional(readOnly=false)
public class FeatureMappingServiceImpl extends OperationalServiceImpl<FeatureMapping, BigDecimal> implements FeatureMappingService {

	private FeatureMappingDao featureMappingDao;
	
	@Autowired
	public void setFeatureMappingDao(FeatureMappingDao featureMappingDao) {
		this.featureMappingDao = featureMappingDao;
		setGenericDao(featureMappingDao);
	}
	
	@Override
	public List<FeatureMapping> selectByCriteria(int startingAt, int maxResult, String code, String feature, String aggregator, String ca, List<Integer> status, List<String> approvalStatus){
		return featureMappingDao.selectByCriteria(startingAt, maxResult, code, feature, aggregator, ca, status, approvalStatus);
	}
	
	@Override
	public FeatureMapping findByName(String name) {
		return featureMappingDao.findByName(name);
	}

	@Override
	public int countByCriteria(String code, String feature, String aggregator, String ca, List<Integer> status, List<String> approvalStatus){
		return featureMappingDao.countByCriteria(code, feature, aggregator, ca, status, approvalStatus);
	}
	
	@Override
	public List<FeatureMapping> selectValidFeatureMapping(){
		return featureMappingDao.selectValidFeatureMapping();
	}

	@Override
	public void postApproval(Auditable<BigDecimal> auditObject, ApprovalPending approvalPending) {

	}
	
	@Override
	public List<FeatureMapping> findByFeatureAggregatorCA(String feature, String aggregator, String ca){
		return featureMappingDao.findByFeatureAggregatorCA(feature, aggregator, ca);
	}
	
}
