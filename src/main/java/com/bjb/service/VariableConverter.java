package com.bjb.service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bjb.model.MessageField;

@Service("variableConverter")
public class VariableConverter{

	@Autowired
	private MessageFieldService messageFieldService;

	private Map<Integer, Object> options;
	
	
	@PostConstruct
	public void init() {

		options = new LinkedHashMap<Integer, Object>();
		List<MessageField> list = messageFieldService.selectByMessageType("61");
		for (MessageField m : list) {
			options.put(m.getId(), m.getName());
		}
	}

	public String getName(String id){
		return (String) options.get(Integer.valueOf(id));
	}

	public MessageFieldService getMessageFieldService() {
		return messageFieldService;
	}

	public Map<Integer, Object> getOptions() {
		return options;
	}
	
}
