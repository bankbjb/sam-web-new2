package com.bjb.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bjb.dao.MenuDao;
import com.bjb.model.Menu;

@Service("menuService")
@Transactional(readOnly=false)
public class MenuServiceImpl extends CommonServiceImpl<Menu, Integer>  implements MenuService {
	private MenuDao menuDao;
	
	public MenuDao getMenuDao() {
		return menuDao;
	}

	@Autowired
	public void setMenuDao(MenuDao menuDao) {
		this.menuDao = menuDao;
		setGenericDao(menuDao);
	}
}	

