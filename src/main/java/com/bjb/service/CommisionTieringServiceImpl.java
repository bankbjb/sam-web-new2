package com.bjb.service;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.Expression;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bjb.dao.CommisionTieringDao;
import com.bjb.model.ApprovalPending;
import com.bjb.model.Auditable;
import com.bjb.model.CommisionTiering;

@Service("commisionTieringService")
@Transactional(readOnly=false)
public class CommisionTieringServiceImpl extends OperationalServiceImpl<CommisionTiering, BigDecimal> implements CommisionTieringService {

	private CommisionTieringDao commisionTieringDao;
	Expression subjectExpression;
	Expression welcomeExpression;
	
	@Autowired
	public void setCommisionTieringDao(CommisionTieringDao commisionTieringDao) {
		this.commisionTieringDao = commisionTieringDao;
		setGenericDao(commisionTieringDao);
	}
	
	@Override
	public List<CommisionTiering> selectByCriteria(int startingAt, int maxResult, String code, String featureMapping, List<Integer> status, List<String> approvalStatus) {
		return commisionTieringDao.selectByCriteria(startingAt, maxResult, code, featureMapping, status, approvalStatus);
	}
	
	@Override
	public CommisionTiering findByName(String name) {
		return commisionTieringDao.findByName(name);
	}

	@Override
	public int countByCriteria(String code, String featureMapping, List<Integer> status, List<String> approvalStatus) {
		return commisionTieringDao.countByCriteria(code, featureMapping, status, approvalStatus);
	}

	@Override
	public void postApproval(Auditable<BigDecimal> auditObject, ApprovalPending approvalPending) {

	}
	
}
