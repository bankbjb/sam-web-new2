package com.bjb.service;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bjb.dao.FeatureDao;
import com.bjb.model.ApprovalPending;
import com.bjb.model.Auditable;
import com.bjb.model.Feature;

@Service("featureService")
@Transactional(readOnly=false)
public class FeatureServiceImpl extends OperationalServiceImpl<Feature, BigDecimal> implements FeatureService {

	private FeatureDao featureDao;
	
	public FeatureDao getFeatureDao() {
		return featureDao;
	}

	@Autowired
	public void setFeatureDao(FeatureDao featureDao) {
		this.featureDao = featureDao;
		setGenericDao(featureDao);
	}

	@Override
	public List<Feature> selectByCriteria(int startingAt, int maxResult, String code, String name, String address, String commisionAccountNo, List<Integer> status, List<String> approvalStatus) {
		return featureDao.selectByCriteria(startingAt, maxResult, code, name, address, commisionAccountNo, status, approvalStatus);
	}
	
	@Override
	public Feature findByName(String name) {
		return featureDao.findByName(name);
	}

	@Override
	public int countByCriteria(String code, String name, String address, String commisionAccountNo, List<Integer> status, List<String> approvalStatus) {
		return featureDao.countByCriteria(code, name, address, commisionAccountNo, status, approvalStatus);
	}
	
	@Override
	public List<Feature> selectValidFeature() {
		return featureDao.selectValidFeature();
	}

	@Override
	public void postApproval(Auditable<BigDecimal> auditObject, ApprovalPending approvalPending) {
		// TODO Auto-generated method stub
		
	}

}
