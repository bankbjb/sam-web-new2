package com.bjb.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bjb.constant.Constants;
import com.bjb.dao.SystemParameterDao;
import com.bjb.model.ApprovalPending;
import com.bjb.model.Auditable;
import com.bjb.model.SystemParameter;
import com.bjb.util.EncryptUtils;

@Service("systemParameterService")
@Transactional(readOnly=false)
public class SystemParameterServiceImpl extends OperationalServiceImpl<SystemParameter, String> implements SystemParameterService {

	@Autowired
	public void setSystemParameterDao(SystemParameterDao systemParameterDao) {
		this.systemParameterDao = systemParameterDao;
		setGenericDao(systemParameterDao);
	}
	
	@Override
	public List<SystemParameter> getSearchParameter(int startingAt, int maxResult, String name, String value, List<Integer> status, List<String> approvalStatus) {
		return systemParameterDao.selectByCriteria(startingAt, maxResult, name, value, status, approvalStatus);
	}

	@Override
	public int countSearchParameter(String name, String value, List<Integer> status, List<String> approvalStatus) {
		return systemParameterDao.countByCriteria(name, value, status, approvalStatus);
	}

	@Override
	public void postApproval(Auditable<String> auditObject, ApprovalPending approvalPending) {
		SystemParameter systemParameter = (SystemParameter) auditObject;
		if (approvalPending.getAction().equals(Constants.APPROVAL_ACTION_DELETE)) {
		} else if (approvalPending.getAction().equals(Constants.APPROVAL_ACTION_CREATE)) {
			if(systemParameter.getSecured()==Constants.STATUS_ACTIVE){
				systemParameter.setValue(EncryptUtils.encryptEncode(systemParameter.getValue()));
			}
		} else if (approvalPending.getAction().equals(Constants.APPROVAL_ACTION_EDIT)) {
			if(systemParameter.getSecured()==Constants.STATUS_ACTIVE){
				systemParameter.setValue(EncryptUtils.encryptEncode(systemParameter.getValue()));
			}

		}
	}
	
	

}
