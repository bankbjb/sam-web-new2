package com.bjb.integration.middleware;

import java.math.BigDecimal;
import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.bjb.integration.ws.middleware.SaldoWebServiceServiceLocator;
import com.bjb.integration.ws.middleware.resource.InquirySaldoWebRequest;
import com.bjb.integration.ws.middleware.resource.InquirySaldoWebResponse;

@Service("middlewareService")
public class MiddlewareServiceImpl implements MiddlewareService {
	
	private String endpointAddress;
	
	@Value("#{commonService.getSystemParameter('WS_MIDDLEWARE_ENDPOINT_ADDRESS')}")
	public void setEndpointAddress(String endpointAddress) {
		this.endpointAddress = endpointAddress;
	}
	
	@Override
	public BigDecimal getBalance(String aggregatorCode) {
		
		InquirySaldoWebRequest request = new InquirySaldoWebRequest();
		request.setAggregatorCode(aggregatorCode);
		
		InquirySaldoWebResponse response = null;
		try {
			SaldoWebServiceServiceLocator ws = new SaldoWebServiceServiceLocator();
			ws.setSaldoWebServiceEndpointAddress(endpointAddress);
			response = ws.getSaldoWebService().inquirySaldo(request);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (response!=null) {
			try {
				BigDecimal result = new BigDecimal(response.getSaldo());
				return result;
			} catch (Exception e) {
//				e.printStackTrace();
			}
		};
		return null;
	}

}
