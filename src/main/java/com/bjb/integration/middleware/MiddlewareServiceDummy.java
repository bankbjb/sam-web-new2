package com.bjb.integration.middleware;

import java.math.BigDecimal;

//@Service("middlewareService")
public class MiddlewareServiceDummy implements MiddlewareService {

	@Override
	public BigDecimal getBalance(String aggregatorCode) {
		return new BigDecimal (10000000);
	}

}
