package com.bjb.integration.middleware;

import java.math.BigDecimal;

public interface MiddlewareService {
	public BigDecimal getBalance(String aggregatorCode);
}
