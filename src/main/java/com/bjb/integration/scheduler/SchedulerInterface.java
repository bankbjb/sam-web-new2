/**
 * SchedulerInterface.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bjb.integration.scheduler;

public interface SchedulerInterface extends java.rmi.Remote {
    public void pauseScheduller(java.lang.String schedulerID) throws java.rmi.RemoteException;
    public java.lang.String statusScheduler(java.lang.String schedulerID) throws java.rmi.RemoteException;
    public java.lang.String createScheduller(java.lang.String jobName, java.lang.String executionService, java.lang.String schedulerType, java.lang.String date, java.lang.String time, java.lang.String interval, java.lang.String startDate, java.lang.String endDate, java.lang.String startTime, java.lang.String endTime, java.lang.String maskYear, java.lang.String maskMonth, java.lang.String maskDay, java.lang.String maskWeeklyDay, java.lang.String maskHour, java.lang.String maskMinute, java.lang.String maskSecond) throws java.rmi.RemoteException;
    public void summaryScheduller(java.lang.String schedulerID) throws java.rmi.RemoteException;
    public void resumeScheduller(java.lang.String schedulerID) throws java.rmi.RemoteException;
    public void stopScheduller(java.lang.String schedulerID) throws java.rmi.RemoteException;
    public void startScheduller(java.lang.String schedulerID) throws java.rmi.RemoteException;
}
