package com.bjb.integration.scheduler;

public class SchedulerInterfaceProxy implements com.bjb.integration.scheduler.SchedulerInterface {
  private String _endpoint = null;
  private com.bjb.integration.scheduler.SchedulerInterface schedulerInterface = null;
  
  public SchedulerInterfaceProxy() {
    _initSchedulerInterfaceProxy();
  }
  
  public SchedulerInterfaceProxy(String endpoint) {
    _endpoint = endpoint;
    _initSchedulerInterfaceProxy();
  }
  
  private void _initSchedulerInterfaceProxy() {
    try {
      schedulerInterface = (new com.bjb.integration.scheduler.SchedulerInterfaceServiceLocator()).getSchedulerInterface();
      if (schedulerInterface != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)schedulerInterface)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)schedulerInterface)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (schedulerInterface != null)
      ((javax.xml.rpc.Stub)schedulerInterface)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.bjb.integration.scheduler.SchedulerInterface getSchedulerInterface() {
    if (schedulerInterface == null)
      _initSchedulerInterfaceProxy();
    return schedulerInterface;
  }
  
  public void pauseScheduller(java.lang.String schedulerID) throws java.rmi.RemoteException{
    if (schedulerInterface == null)
      _initSchedulerInterfaceProxy();
    schedulerInterface.pauseScheduller(schedulerID);
  }
  
  public java.lang.String statusScheduler(java.lang.String schedulerID) throws java.rmi.RemoteException{
    if (schedulerInterface == null)
      _initSchedulerInterfaceProxy();
    return schedulerInterface.statusScheduler(schedulerID);
  }
  
  public java.lang.String createScheduller(java.lang.String jobName, java.lang.String executionService, java.lang.String schedulerType, java.lang.String date, java.lang.String time, java.lang.String interval, java.lang.String startDate, java.lang.String endDate, java.lang.String startTime, java.lang.String endTime, java.lang.String maskYear, java.lang.String maskMonth, java.lang.String maskDay, java.lang.String maskWeeklyDay, java.lang.String maskHour, java.lang.String maskMinute, java.lang.String maskSecond) throws java.rmi.RemoteException{
    if (schedulerInterface == null)
      _initSchedulerInterfaceProxy();
    return schedulerInterface.createScheduller(jobName, executionService, schedulerType, date, time, interval, startDate, endDate, startTime, endTime, maskYear, maskMonth, maskDay, maskWeeklyDay, maskHour, maskMinute, maskSecond);
  }
  
  public void summaryScheduller(java.lang.String schedulerID) throws java.rmi.RemoteException{
    if (schedulerInterface == null)
      _initSchedulerInterfaceProxy();
    schedulerInterface.summaryScheduller(schedulerID);
  }
  
  public void resumeScheduller(java.lang.String schedulerID) throws java.rmi.RemoteException{
    if (schedulerInterface == null)
      _initSchedulerInterfaceProxy();
    schedulerInterface.resumeScheduller(schedulerID);
  }
  
  public void stopScheduller(java.lang.String schedulerID) throws java.rmi.RemoteException{
    if (schedulerInterface == null)
      _initSchedulerInterfaceProxy();
    schedulerInterface.stopScheduller(schedulerID);
  }
  
  public void startScheduller(java.lang.String schedulerID) throws java.rmi.RemoteException{
    if (schedulerInterface == null)
      _initSchedulerInterfaceProxy();
    schedulerInterface.startScheduller(schedulerID);
  }
  
  
}