package com.bjb.integration.scheduler;

import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import com.bjb.model.ScheduleTask;

public class SchedulerExecutor {
	
	public static String createScheduler(ScheduleTask task) throws RemoteException, ServiceException{
		return SchedulerExecutor.createScheduler(task.getJobName(), task.getExecutionService(), task.getSchedulerType(), task.getDate(), task.getTime(), task.getInterval(), task.getStartDate(), task.getEndDate(), task.getStartTime(), task.getEndTime(), task.getMaskYear(), task.getMaskMonth(), task.getMaskDay(), task.getMaskWeeklyDay(), task.getMaskHour()+"-"+task.getMaskHourEnd(), task.getMaskMinute(), null);
	}
	
	public static String createScheduler(String jobName, String executionService, String schedulerType, String  date, String time, String interval, String startDate, String endDate, String startTime, String endTime, String maskYear, String maskMonth, String maskDay, String maskWeeklyDay, String maskHour, String maskMinute, String maskSecond) throws RemoteException, ServiceException{
		SchedulerInterfaceServiceLocator sl = new SchedulerInterfaceServiceLocator();
		String schedulerId = sl.getSchedulerInterface().createScheduller(jobName, executionService, schedulerType, date, time, interval, startDate, endDate, startTime, endTime, maskYear, maskMonth, maskDay, maskWeeklyDay, maskHour, maskMinute, maskSecond);
		return schedulerId;
	}
	
	public static void startScheduler(String schedulerID) throws RemoteException, ServiceException{
		SchedulerInterfaceServiceLocator sl = new SchedulerInterfaceServiceLocator();
		sl.getSchedulerInterface().startScheduller(schedulerID);
	}
	
	public static void stopScheduler(String schedulerID) throws RemoteException, ServiceException{
		SchedulerInterfaceServiceLocator sl = new SchedulerInterfaceServiceLocator();
		sl.getSchedulerInterface().stopScheduller(schedulerID);
	}
	
	public static void pauseScheduler(String schedulerID) throws RemoteException, ServiceException{
		SchedulerInterfaceServiceLocator sl = new SchedulerInterfaceServiceLocator();
		sl.getSchedulerInterface().pauseScheduller(schedulerID);
	}
	
	public static void resumeScheduler(String schedulerID) throws RemoteException, ServiceException{
		SchedulerInterfaceServiceLocator sl = new SchedulerInterfaceServiceLocator();
		sl.getSchedulerInterface().resumeScheduller(schedulerID);
	}
	
	public static void deleteScheduler(String schedulerID) throws RemoteException, ServiceException{
		SchedulerInterfaceServiceLocator sl = new SchedulerInterfaceServiceLocator();
		sl.getSchedulerInterface().summaryScheduller(schedulerID);
	}
	
	public static String statusScheduler(String schedulerID) throws RemoteException, ServiceException{
		SchedulerInterfaceServiceLocator sl = new SchedulerInterfaceServiceLocator();
		String status =  sl.getSchedulerInterface().statusScheduler(schedulerID);
		return status;
	}
}
