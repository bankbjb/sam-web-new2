/**
 * SaldoWebService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bjb.integration.ws.middleware;

public interface SaldoWebService extends java.rmi.Remote {
    public com.bjb.integration.ws.middleware.resource.InquirySaldoWebResponse inquirySaldo(com.bjb.integration.ws.middleware.resource.InquirySaldoWebRequest rrequest) throws java.rmi.RemoteException;
}
