package com.bjb.integration.ws.middleware;

public class SaldoWebServiceProxy implements com.bjb.integration.ws.middleware.SaldoWebService {
  private String _endpoint = null;
  private com.bjb.integration.ws.middleware.SaldoWebService saldoWebService = null;
  
  public SaldoWebServiceProxy() {
    _initSaldoWebServiceProxy();
  }
  
  public SaldoWebServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initSaldoWebServiceProxy();
  }
  
  private void _initSaldoWebServiceProxy() {
    try {
      saldoWebService = (new com.bjb.integration.ws.middleware.SaldoWebServiceServiceLocator()).getSaldoWebService();
      if (saldoWebService != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)saldoWebService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)saldoWebService)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (saldoWebService != null)
      ((javax.xml.rpc.Stub)saldoWebService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.bjb.integration.ws.middleware.SaldoWebService getSaldoWebService() {
    if (saldoWebService == null)
      _initSaldoWebServiceProxy();
    return saldoWebService;
  }
  
  public com.bjb.integration.ws.middleware.resource.InquirySaldoWebResponse inquirySaldo(com.bjb.integration.ws.middleware.resource.InquirySaldoWebRequest rrequest) throws java.rmi.RemoteException{
    if (saldoWebService == null)
      _initSaldoWebServiceProxy();
    return saldoWebService.inquirySaldo(rrequest);
  }
  
  
}