package com.bjb.integration.ws;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedProperty;

import com.bjb.constant.Constants;
import com.bjb.model.SystemParameter;
import com.bjb.service.SystemParameterService;
import com.bjb.util.EncryptUtils;

public class WebServiceConfigurator {

	private String userName; 
	
	private String password; 
	
	private String addressInquiry;
	
	private String addressPayment;
	
	private String addressReInquiry;
	
	private String addressScheduler;
	
	private String addressRTGS;
	
	@ManagedProperty(value="#{systemParameterService}")
	private SystemParameterService systemParameterService;

	public void setSystemParameterService(SystemParameterService systemParameterService){
		this.systemParameterService = systemParameterService;
	}

	@PostConstruct
	public void init(){
		
		SystemParameter systemParameter;
		
		systemParameter = systemParameterService.findByCode(Constants.WEB_SERVICE_IP);
		String ip		= systemParameter.getValue();

		systemParameter = systemParameterService.findByCode(Constants.WEB_SERVICE_PORT);
		String port		= systemParameter.getValue();
		
		systemParameter 	= systemParameterService.findByCode(Constants.WEB_SERVICE_INTERNAL_IP);
		String ipInternal 	= systemParameter.getValue();

		systemParameter 	= systemParameterService.findByCode(Constants.WEB_SERVICE_INTERNAL_PORT);
		String portInternal	= systemParameter.getValue();

		systemParameter = systemParameterService.findByCode(Constants.WEB_SERVICE_USERNAME);
		userName = systemParameter.getValue();
		
		systemParameter = systemParameterService.findByCode(Constants.WEB_SERVICE_PASSWORD);
		//Add Decrypt 25 Jul 2016 -- Brian
		password = EncryptUtils.decodeDecrypt(systemParameter.getValue());
		//--------------------------------
		addressInquiry 		= "http://" +ip+ ":" +port+ "/ws/com.btpn.mpn.ws.Inquiry/com_btpn_mpn_ws_Inquiry_Port";
		addressPayment 		= "http://" +ip+ ":" +port+ "/ws/com.btpn.mpn.ws.Payment/com_btpn_mpn_ws_Payment_Port";
		addressReInquiry 	= "http://" +ip+ ":" +port+ "/ws/com.btpn.mpn.ws.Reinquiry/com_btpn_mpn_ws_Reinquiry_Port";
		addressScheduler	= "http://" +ipInternal+ ":" +portInternal+ "/FileProcessing/services/SchedulerInterface";
		addressRTGS 		= "http://" +ipInternal+ ":" +portInternal+ "/FileProcessing/services/RTGSInterface";
	}
	
	public String getUsername() {
		return userName;
	}

	public void setUsername(String username) {
		this.userName = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAddressInquiry() {
		return addressInquiry;
	}

	public void setAddressInquiry(String addressInquiry) {
		this.addressInquiry = addressInquiry;
	}

	public String getAddressPayment() {
		return addressPayment;
	}

	public void setAddressPayment(String addressPayment) {
		this.addressPayment = addressPayment;
	}

	public String getAddressReInquiry() {
		return addressReInquiry;
	}

	public void setAddressReInquiry(String addressReInquiry) {
		this.addressReInquiry = addressReInquiry;
	}

	public String getAddressScheduler() {
		return addressScheduler;
	}

	public void setAddressScheduler(String addressScheduler) {
		this.addressScheduler = addressScheduler;
	}

	public String getAddressRTGS() {
		return addressRTGS;
	}

	public void setAddressRTGS(String addressRTGS) {
		this.addressRTGS = addressRTGS;
	}
	
	
	
	
}
