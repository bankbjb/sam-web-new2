package com.bjb.integration.uim;

import java.io.IOException;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bjb.dao.SystemParameterDao;
import com.bjb.dao.SystemParameterDaoImpl;
import com.bjb.integration.uim.helper.UIMBJBConnector;
import com.bjb.integration.uim.helper.UIMBJBConstants;
import com.bjb.integration.uim.helper.UIMBJBHelper;
import com.bjb.integration.uim.helper.UIMBJBModel;
import com.bjb.integration.uim.model.ChangePasswordResponse;
import com.bjb.integration.uim.model.LoginResponse;

@Service("uimService")
public class UimServiceImpl implements UimService {

	@Override
	public LoginResponse login(String userName, String password) {
		UIMBJBModel uimBJBModel = new UIMBJBModel();
		boolean checked = false;
		String usernameBck = userName;
		LoginResponse response = new LoginResponse();
		try {
			uimBJBModel = sendToUIMLogin(userName, password.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (uimBJBModel.getResponseMessage().equals(UIMBJBConstants.UIM_SUCCESS)){
			response.setStatus(uimBJBModel.getResponseCode());
			HashMap<String, String> userData = new HashMap<>();
			String[] columns = new String[] { "fullName", "nip", "userName", "branchCode", "parentBranchCode",
					"branchName", "parentBranchName", "regionalOfficeCode", "regionalOfficeName", "jobTitle", "jobPosition",
					"mobileNo", "email", "gradeCode", "gradeName", "hrPosition", "password", "isActive", "isExpire", "isLocked",
					"functionCode", "addOnFunctionCode", "debitLimitTransaction", "creditLimitTransaction", "isSupervisor" };

			String uimResponse = uimBJBModel.getRawResponse();
			String[] uimValues = uimResponse.split("\\|");
			for (int idx = 0; idx < uimValues.length && idx < columns.length; idx++) {
				userData.put(columns[idx], uimValues[idx]);
			}
			response.setUserData(userData);
			//return "welcome?faces-redirect=true";
		} else{
			response.setStatus(uimBJBModel.getResponseCode());
		}
		return response;
	}

	@Override
	public ChangePasswordResponse changePassword(String userName, String oldPassword, String newPassword) {
		UIMBJBModel uimBJBModel = new UIMBJBModel();
		boolean checked = false;
		String usernameBck = userName;
		ChangePasswordResponse response = new ChangePasswordResponse();
		try {
			uimBJBModel = sendToUIMPassword(userName, oldPassword.toString(), newPassword.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (uimBJBModel.getResponseMessage().equals(UIMBJBConstants.UIM_SUCCESS)){
			response.setStatus(uimBJBModel.getResponseCode());
		} else {
			response.setStatus(uimBJBModel.getResponseCode());
		}
		return response;
	}

	@Autowired
	private SystemParameterDao systemParameterDao;
	
	public SystemParameterDao getSystemParameterDao() {
		return systemParameterDao;
	}

	public void setSystemParameterDao(SystemParameterDao systemParameterDao) {
		this.systemParameterDao = systemParameterDao;
	}

	public UIMBJBModel sendToUIMLogin(String username, String password) throws IOException{
		SystemParameterDao dao = getSystemParameterDao();
		String request = UIMBJBHelper.packUIMRequestLogin(username,password, dao);
		System.out.println(request);
		//TODO bypass
		String response = "";
		if(username.startsWith("user") || username.startsWith("admin")){
			response = UimServiceDummy.users.get(username);
		}else{
			response = UIMBJBConnector.doSendRequest(request, dao);
		}
		//TODO dont forget above bypass
		System.out.println(response);
		UIMBJBModel uimBJBModel = UIMBJBHelper.unpackUIMResponse(response,UIMBJBConstants.UIM_FLAG_LOGIN);
		return uimBJBModel;
	}

	public UIMBJBModel sendToUIMPassword(String username, String oldPassword,String newPassword) throws IOException{
		SystemParameterDao dao = new SystemParameterDaoImpl();
		String request = UIMBJBHelper.packUIMRequestChangePass(username, oldPassword, newPassword, dao);
		System.out.println(request);
		String response = UIMBJBConnector.doSendRequest(request, dao);
		System.out.println(response);
		UIMBJBModel uimBJBModel = UIMBJBHelper.unpackUIMResponse(response,UIMBJBConstants.UIM_FLAG_CHANGE_PASSWORD);
		return uimBJBModel;
	}

}
