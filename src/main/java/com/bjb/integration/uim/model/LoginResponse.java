package com.bjb.integration.uim.model;

import java.util.HashMap;

public class LoginResponse {
	private int status;
	private HashMap<String, String> userData;
	
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public HashMap<String, String> getUserData() {
		return userData;
	}
	public void setUserData(HashMap<String, String> userData) {
		this.userData = userData;
	}
	
}
