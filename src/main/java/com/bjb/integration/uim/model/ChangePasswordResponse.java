package com.bjb.integration.uim.model;

public class ChangePasswordResponse {
	private int status;

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
}
