package com.bjb.integration.uim;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.bjb.constant.LoginConstants;
import com.bjb.integration.uim.model.ChangePasswordResponse;
import com.bjb.integration.uim.model.LoginResponse;

//@Service("uimService")
public class UimServiceDummy implements UimService {

	public static Map<String, String> users = new HashMap<String, String>();
	private static Map<String, Integer> loginRc = new HashMap<String, Integer>();

	private static Map<String, String> passwords = new HashMap<String, String>();
	private static Map<String, Integer> cpRc = new HashMap<String, Integer>();

	private static String[] columns = new String[] { "fullName", "nip", "userName", "branchCode", "parentBranchCode",
			"branchName", "parentBranchName", "regionalOfficeCode", "regionalOfficeName", "jobTitle", "jobPosition",
			"mobileNo", "email", "gradeCode", "gradeName", "hrPosition", "password", "isActive", "isExpire", "isLocked",
			"functionCode", "addOnFunctionCode", "debitLimitTransaction", "creditLimitTransaction", "isSupervisor" };

	static {
		// user normal, level 0000001 (it regular user)
		users.put("admin1",
				"HERI HERYADI|08.78.2091|admin1|P009|P009|DIVISI TEKNOLOGI INFORMASI|DIVISI TEKNOLOGI INFORMASI|0000|KANTOR PUSAT|OFFICER|Officer|087825252656|hheryadi@bankbjb.co.id|0087|G6|DIVISI TEKNOLOGI INFORMASI|95fbeb8f769d2c0079d1d11348877da944aaefaba6ecf9f7f7dab6344ece8605|Y|N|N|417||0|0|N|100326084142234");
		// user normal, level 0000002 (it supervisor)
		users.put("admin2",
				"SUPERVISOR|08.78.2091|admin2|P009|P009|DIVISI TEKNOLOGI INFORMASI|DIVISI TEKNOLOGI INFORMASI|0000|KANTOR PUSAT|OFFICER|Officer|087825252656|hheryadi@bankbjb.co.id|0087|G6|DIVISI TEKNOLOGI INFORMASI|95fbeb8f769d2c0079d1d11348877da944aaefaba6ecf9f7f7dab6344ece8605|Y|N|N|418||0|0|N|100326084142234");
		// user normal, level 0000003 (bisnis regular user)
		users.put("user1",
				"HERI HERYADI|08.78.2091|user1|P009|P009|DIVISI TEKNOLOGI INFORMASI|DIVISI TEKNOLOGI INFORMASI|0000|KANTOR PUSAT|OFFICER|Officer|087825252656|hheryadi@bankbjb.co.id|0087|G6|DIVISI TEKNOLOGI INFORMASI|95fbeb8f769d2c0079d1d11348877da944aaefaba6ecf9f7f7dab6344ece8605|Y|N|N|419||0|0|N|100326084142234");
		// user normal, level 0000004 (bisnis supervisor)
		users.put("user2",
				"SUPERVISOR|08.78.2091|user2|P009|P009|DIVISI TEKNOLOGI INFORMASI|DIVISI TEKNOLOGI INFORMASI|0000|KANTOR PUSAT|OFFICER|Officer|087825252656|hheryadi@bankbjb.co.id|0087|G6|DIVISI TEKNOLOGI INFORMASI|95fbeb8f769d2c0079d1d11348877da944aaefaba6ecf9f7f7dab6344ece8605|Y|N|N|420||0|0|N|100326084142234");
		// user is not active, column 18=N
		users.put("inactive",
				"HERI HERYADI|08.78.2091|inactive|P009|P009|DIVISI TEKNOLOGI INFORMASI|DIVISI TEKNOLOGI INFORMASI|0000|KANTOR PUSAT|OFFICER|Officer|087825252656|hheryadi@bankbjb.co.id|0087|G6|DIVISI TEKNOLOGI INFORMASI|95fbeb8f769d2c0079d1d11348877da944aaefaba6ecf9f7f7dab6344ece8605|N|N|N|1000||0|0|N|100326084142234");
		// user password is expired, column 19=Y
		users.put("expired",
				"HERI HERYADI|08.78.2091|expired|P009|P009|DIVISI TEKNOLOGI INFORMASI|DIVISI TEKNOLOGI INFORMASI|0000|KANTOR PUSAT|OFFICER|Officer|087825252656|hheryadi@bankbjb.co.id|0087|G6|DIVISI TEKNOLOGI INFORMASI|95fbeb8f769d2c0079d1d11348877da944aaefaba6ecf9f7f7dab6344ece8605|Y|Y|N|1000||0|0|N|100326084142234");
		// user n times wrong password, column 20=Y then user is blocked
		users.put("wrong2",
				"HERI HERYADI|08.78.2091|wrong2|P009|P009|DIVISI TEKNOLOGI INFORMASI|DIVISI TEKNOLOGI INFORMASI|0000|KANTOR PUSAT|OFFICER|Officer|087825252656|hheryadi@bankbjb.co.id|0087|G6|DIVISI TEKNOLOGI INFORMASI|95fbeb8f769d2c0079d1d11348877da944aaefaba6ecf9f7f7dab6344ece8605|Y|N|Y|1000||0|0|N|100326084142234");
		users.put("wrong", "salah");
		users.put("notfound", "tidak ada");
		users.put("error", ":p");

		loginRc.put("salah", LoginConstants.ERROR_PASSWORD_WRONG);
		loginRc.put("tidak ada", LoginConstants.ERROR_USER_NOT_FOUND);
		loginRc.put(":p", LoginConstants.ERROR_APPLICATION_UNREGISTERED);

		// change password with result 'tidak ada'
		passwords.put("tidak ada", "tidak ada");
		// change password with result 'dikunci'
		passwords.put("dikunci", "dikunci");
		// change password with result old password 'salah'
		passwords.put("salah", "salah");
		// change password with result 'password'
		passwords.put("password", "gunakan password lain");

		passwords.put("admin1", "Berhasil95fbeb8f769d2c0079d1d11348877da944aaefaba6ecf9f7f7dab6344ece860595fbeb8f769d2c0079d1d11348877da944aaefaba6ecf9f7f7dab6344ece8605");
		passwords.put("admin2", "Berhasil95fbeb8f769d2c0079d1d11348877da944aaefaba6ecf9f7f7dab6344ece860595fbeb8f769d2c0079d1d11348877da944aaefaba6ecf9f7f7dab6344ece8605");
		passwords.put("user1", "Berhasil95fbeb8f769d2c0079d1d11348877da944aaefaba6ecf9f7f7dab6344ece860595fbeb8f769d2c0079d1d11348877da944aaefaba6ecf9f7f7dab6344ece8605");
		passwords.put("admin2", "Berhasil95fbeb8f769d2c0079d1d11348877da944aaefaba6ecf9f7f7dab6344ece860595fbeb8f769d2c0079d1d11348877da944aaefaba6ecf9f7f7dab6344ece8605");

		cpRc.put("tidak ada", LoginConstants.ERROR_USER_NOT_FOUND);
		cpRc.put("dikunci", LoginConstants.ERROR_USER_LOCKED);
		cpRc.put("salah", LoginConstants.ERROR_PASSWORD_WRONG);
		cpRc.put("gunakan password lain", LoginConstants.ERROR_PASSWORD_HAS_BEEN_USED);
		cpRc.put("Berhasil", LoginConstants.SUCCESS);
	}

	@Override
	public LoginResponse login(String userName, String password) {
		LoginResponse response = new LoginResponse();
		String uimResponse = users.get(userName);
		if (uimResponse == null) {
			response.setStatus(LoginConstants.ERROR_USER_NOT_FOUND);
		} else if (uimResponse.indexOf("|") == -1) {
			response.setStatus(loginRc.get(uimResponse));
		} else {
			String[] values = uimResponse.split("\\|");
			HashMap<String, String> userData = new HashMap<String, String>();
			for (int idx = 0; idx < values.length && idx < columns.length; idx++) {
				userData.put(columns[idx], values[idx]);
			}
			response.setUserData(userData);
			response.setStatus(LoginConstants.SUCCESS);
		}
		return response;
	}

	@Override
	public ChangePasswordResponse changePassword(String userName, String oldPassword, String newPassword) {
		ChangePasswordResponse response = new ChangePasswordResponse();
		String uimResponse = passwords.get(userName);
		if (uimResponse == null) {
			response.setStatus(LoginConstants.ERROR_USER_NOT_FOUND);
		} else if (uimResponse.indexOf("Berhasil") >= 0) {
			response.setStatus(LoginConstants.SUCCESS);
		} else {
			response.setStatus(cpRc.get(uimResponse));
		}
		// TODO Auto-generated method stub
		return response;
	}

}
