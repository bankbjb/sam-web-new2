package com.bjb.integration.uim;

import com.bjb.integration.uim.model.ChangePasswordResponse;
import com.bjb.integration.uim.model.LoginResponse;

public interface UimService {
	public LoginResponse login(String userName, String password);
	public ChangePasswordResponse changePassword(String userName, String oldPassword, String newPassword);
}
