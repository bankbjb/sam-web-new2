package com.bjb.integration.uim.helper;

public class UIMBJBModel {
	
	private String responseMessage;
	private boolean internalUser;
	private boolean tryLoginDepkeu;
	private String uimRoleId;
	private int responseCode;
	private String rawResponse;
	
	public String getResponseMessage() {
		return responseMessage;
	}
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
	public boolean isInternalUser() {
		return internalUser;
	}
	public void setInternalUser(boolean internalUser) {
		this.internalUser = internalUser;
	}
	public boolean isTryLoginDepkeu() {
		return tryLoginDepkeu;
	}
	public void setTryLoginDepkeu(boolean doLogin) {
		this.tryLoginDepkeu = doLogin;
	}
	public String getUimRoleId() {
		return uimRoleId;
	}
	public void setUimRoleId(String uimRoleId) {
		this.uimRoleId = uimRoleId;
	}
	public int getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}
	public String getRawResponse() {
		return rawResponse;
	}
	public void setRawResponse(String rawResponse) {
		this.rawResponse = rawResponse;
	}
	
	
}
